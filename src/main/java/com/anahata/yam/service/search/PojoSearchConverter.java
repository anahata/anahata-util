/*
 * Copyright 2015 Anahata.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.yam.service.search;

import com.anahata.yam.model.search.ExpandSearch;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import static java.util.stream.Collectors.*;

/**
 * Wraps an object into a string version of the same to aid in pojo searches where we want to search against a dynamically generated string such as getDisplayValue().
 * 
 * @author pablo
 */
@AllArgsConstructor
@Getter
public class PojoSearchConverter<T> {
    @ExpandSearch(false)
    private T item;

    @Getter
    private String searchValue;
    
    public static <U> List<U> unwrap(List<PojoSearchConverter<U>> l) {
        return l.stream().map(e -> e.getItem()).collect(toList());
    }
    
    public static <U> List<PojoSearchConverter<U>> wrap(List<U> l, Function<U, String> f) {
        return l.stream().map(e -> new PojoSearchConverter<>(e, f.apply(e))).collect(Collectors.toList());
    }
}
