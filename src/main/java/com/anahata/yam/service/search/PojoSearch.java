package com.anahata.yam.service.search;

/*
 * #%L
 * yam-core
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.reflect.AnahataPropertyUtils;
import com.anahata.yam.model.search.*;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.util.*;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

/**
 * Searches a pojo and any child objects for matches of a given {@link FullTextCriteria} producing a 'matching' flag, a
 * score and filtering and sorting any child objects.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
public class PojoSearch {
    /**
     * The object being processed.
     */
    private final Object object;

    /**
     * The criteira.
     */
    private final FullTextCriteria criteria;

    /**
     * The depth of this processor (used in recursive processing).
     */
    private final int depth;

    /**
     * The parents objects (used in recursive processing to detect and avoid cyclic references).
     */
    private final Set<Object> parents;

    /**
     * An additional filter to filter child objects. Optional.
     */
    @Getter
    private final PojoFilter filter;

    /**
     * A comparator factory to be used as second sort criteria for child object collections. Optional.
     */
    @Getter
    private final PojoComparatorFactory comparatorFactory;

    /**
     * The score from evaluating this object.
     */
    @Getter
    private double score;

    /**
     * All the tokens matched by this processor and it's parent processors (if this processor is part of a nested
     * scenario).
     */
    @Getter
    private Set<SearchToken> matchedTokens;

    /**
     * Minimal public constructor without explicit filtering or child comparation.
     *
     * @param object   the object being evaluated.
     * @param criteria the criteria the object is being evaluated for.
     *
     */
    public PojoSearch(Object object, FullTextCriteria criteria) {
        this(object, criteria, null, null);
    }

    /**
     * Public all args constructor. Processes the object.
     *
     * @param object            the object being evaluated.
     * @param criteria          the criteria the object is being evaluated for.
     * @param filter            a explicit filter to evaluate objects against. Optional
     * @param comparatorFactory a comparator factory to be used as second sort criteria for child object collections.
     *                          Optional.
     */
    public PojoSearch(Object object, FullTextCriteria criteria, PojoFilter filter,
            PojoComparatorFactory comparatorFactory) {
        this.object = object;
        this.criteria = criteria;
        this.depth = 1;
        this.parents = new HashSet<>();
        this.matchedTokens = new HashSet<>();
        this.filter = filter;
        this.comparatorFactory = comparatorFactory;
        process();
    }

    /**
     * Constructor for nested scenario.
     *
     * @param object            the object being evaluated.
     * @param criteria          the criteria the object is being evaluated for.
     * @param filter            a explicit filter to evaluate objects against. Optional
     * @param comparatorFactory a comparator factory to be used as second sort criteria for child object collections.
     * @param depth             the depth from the root processor.
     * @param parents           all parents to the root processor.
     * @param matchedTokens     the tokens matched in the hierarchy so far.
     */
    private PojoSearch(Object object, FullTextCriteria criteria, PojoFilter filter,
            PojoComparatorFactory comparatorFactory, int depth,
            Set<Object> parents, Set<SearchToken> matchedTokens) {
        Validate.isTrue(!parents.contains(object));
        this.object = object;
        this.criteria = criteria;
        this.depth = depth;
        this.parents = new HashSet<>(parents);
        this.parents.add(object);
        this.matchedTokens = new HashSet<>(matchedTokens);
        this.filter = filter;
        this.comparatorFactory = comparatorFactory;
        process();
    }

    /**
     * True if this object (or any of its nested children if {@link FullTextCriteria#expandSearch} was specified)
     * matches all primal tokens in {@link #criteria}.
     *
     * @return as described.
     */
    public boolean matchesAllTokens() {
        return matchedTokens.containsAll(criteria.getSearchTokens().getPrimalTokens());
    }

    /**
     * Checks if the object being processed matched the criteria. For this method to return true the score has to be
     * greater than zero. If the criteria is set to match all tokens, all tokens have to be matched.
     *
     * @return true if the score is greater than zero and matches all tokens according to criteria,
     */
    public boolean matches() {

        if (score == 0) {
            return false;
        }

        if (criteria.isMatchAllTokens() && !matchesAllTokens()) {
            return false;
        }

        return true;
    }

    /**
     * Processes a list of objects, the list will be sorted by score if
     *
     * @param list              the objects
     * @param fullTextCriteria  the criteria
     * @param filter            an optional additional
     * @param comparatorFactory a comparator factory to be used if
     * @param sort
     */
    public static void processList(List list, FullTextCriteria fullTextCriteria, PojoFilter filter,
            boolean sort, PojoComparatorFactory comparatorFactory) {
        Map<Object, Double> scores = new HashMap<>();

        Iterator it = list.iterator();

        while (it.hasNext()) {
            Object o = it.next();
            if (o != null) {
                PojoSearch pp = new PojoSearch(o, fullTextCriteria, filter, comparatorFactory);
                log.trace("PojoSearch matches ={}. score={} matchedTokens={}", pp.matches(), pp.getScore(),
                        pp.getMatchedTokens());
                if (pp.matches()) {
                    scores.put(o, pp.getScore());
                } else {
                    it.remove();
                }
            } else {
                it.remove();
            }
        }

        if (list.size() > 1) {
            if (sort) {
                //sort by score and secondary as specified by comparator factory
                if (comparatorFactory != null) {
                    Comparator comp = comparatorFactory.getComparator(list.iterator().next().getClass());
                    if (comp != null) {
                        Collections.sort(list, comp);
                    }
                }
                Collections.sort(list, new ScoreComparator(scores));
            }
        }

    }

    /**
     * Adds the score and the matched tokens from another processor.
     *
     * @param other the other processor.
     */
    private void add(PojoSearch other) {

        score += other.score;
        matchedTokens.addAll(other.matchedTokens);

    }

    /**
     * Calculates the ranking of an object when compared with a series of search tokens.
     *
     * @param object         the object to score
     * @param criteria       the full text criteria
     * @param recursiveStack set of objects already processed to avoid cyclic referencing.
     * @return the number of times a token was matched;
     * @throws NullpointerException if object, tokens or recursiveStack are null
     */
    private void process() {
        log.trace("PojoSearch processing: {}", object);
        if (filter != null && !filter.matches(object)) {
            return;
        }

        if (object instanceof Searchable) {
            this.processProperty("value", String.class, ((Searchable)object).getValue());
            return;
        }

        Class clazz = object.getClass();
        Map<String, Collection> collections = new HashMap<>();

        while (!clazz.equals(Object.class)) {
            for (PropertyDescriptor pd : PropertyUtils.getPropertyDescriptors(clazz)) {
                String propertyName = pd.getName();
                log.trace("PojoSearch processing property: {}", propertyName);

                //only attributes with getter and field
                if (pd.getReadMethod() == null) {
                    continue;
                }

                Field f;
                try {
                    f = clazz.getDeclaredField(propertyName);
                } catch (NoSuchFieldException | SecurityException e) {
                    //no field for this property, continue with next property
                    continue;
                }

                //skip transients
                if (f.isAnnotationPresent(Transient.class)) {
                    continue;
                }

                //check @ExpandSearch(false) at field level to precent invoking lazy loaded getters
                ExpandSearch fieldExpandSearch = f.getAnnotation(ExpandSearch.class);
                if (fieldExpandSearch != null && !fieldExpandSearch.value()) {
                    continue;
                }

                //check @ExpandSearch(true) by looking at field type to precent invoking lazy loaded getters                
                ExpandSearch fieldTypeExpandSearch = f.getType().getAnnotation(ExpandSearch.class);
                if (fieldTypeExpandSearch != null && !fieldTypeExpandSearch.value()) {
                    continue;
                }

                //If the field is a collection and does not have an @ExpandSearch(true) annotation, skip
                if (Collection.class.isAssignableFrom(clazz) && (fieldExpandSearch == null || !fieldExpandSearch.value())) {
                    continue;
                }

                log.trace("Property {} of object {} qualifies for scoring based on class metadata. Fetching property ",
                        propertyName, object);

                Object propertyValue = AnahataPropertyUtils.getProperty(object, pd.getName());
                if (propertyValue != null) {

                    //TODO: move isSearchableType to a package that both jpa search and pojo search depend on
                    if (isSearchableType(propertyValue.getClass())) {

                        if (propertyValue.getClass().isAnnotationPresent(Embeddable.class)) {
                            //for embeddable, we use same depth
                            PojoSearch pp = new PojoSearch(propertyValue, criteria, filter, comparatorFactory,
                                    depth, new HashSet<>(parents), matchedTokens);
                            add(pp);

                        } else {
                            processProperty(propertyName, f.getType(), propertyValue);
                        }

                    } else if (criteria.isExpandSearch() && f.isAnnotationPresent(ExpandSearch.class)) {

                        if (propertyValue.getClass().isAnnotationPresent(javax.persistence.Entity.class)) {
                            //treat @ManyToOne and @OneToOne like attributes
                            //cyclic check
                            if (!parents.contains(propertyValue)) {
                                log.trace(
                                        "Property {} of object {} is @Entity and expandSearch is enabled. Drilling down.",
                                        propertyName, object);
                                PojoSearch pp = new PojoSearch(propertyValue, criteria, filter,
                                        comparatorFactory, depth + 1,
                                        parents,
                                        matchedTokens);
                                add(pp);
                            }

                        } else if (propertyValue instanceof Collection) {

                            Collection col = (Collection)propertyValue;

                            if (!col.isEmpty()) {
                                log.trace(
                                        "Property {} of object {} is a collection with elements. Adding to collections "
                                        + "list to process once all non-collection attributes have been processed.",
                                        propertyName, object);
                                //child objects will be processed once all other attributes are processed to
                                //compute hierarchy matched tokens correctly.
                                collections.put(propertyName, col);
                            }

                        }
                    }
                }
            }
            clazz = clazz.getSuperclass();
        }

        //process child collections
        for (Map.Entry<String, Collection> colMapEntry : collections.entrySet()) {
            String propertyName = colMapEntry.getKey();
            Collection col = colMapEntry.getValue();
            log.trace("Processing collection for property {} of object {}.",
                    propertyName, object);

            List<PojoSearch> childrenProcessors = new ArrayList<>();

            for (Iterator it = col.iterator(); it.hasNext();) {
                Object child = it.next();

                if (child.getClass().isAnnotationPresent(javax.persistence.Entity.class)) {

                    if (!parents.contains(child)) {
                        //cyclic check
                        PojoSearch pp = new PojoSearch(child, criteria, filter, comparatorFactory, depth + 1,
                                parents,
                                matchedTokens);
                        childrenProcessors.add(pp);
                    } else {
                        it.remove();
                    }

                }

            }

            Map<Object, Double> scores = new HashMap<>();

            for (PojoSearch pp : childrenProcessors) {
                if (pp.matches()) {
                    add(pp);
                    scores.put(pp.object, pp.getScore());
                } else {
                    col.remove(pp.object);
                }

            }

            if (col instanceof List) {
                if (!col.isEmpty()) {
                    if (comparatorFactory != null) {
                        Comparator comp = comparatorFactory.getComparator(col.iterator().next().getClass());
                        if (comp != null) {
                            Collections.sort((List)col, comp);
                        }
                    }
                    Collections.sort((List)col, new ScoreComparator(scores));
                }

            }

        }
        this.matchedTokens = (Set)Collections.unmodifiableSet(matchedTokens);

    }

    /**
     * Computes the score for a single property.
     *
     * @param object        the object being evaluated
     * @param propertyName  the property being evaluated
     * @param propertyType  the type of the property
     * @param propertyValue the value of the property being evaluated
     * @param criteria      the criteria based on which the evaluation is made
     * @param depth         the depth in the object grapth at which <code>object</code> is present
     */
    private void processProperty(String propertyName, Class propertyType, Object propertyValue) {
        String searchableValue = propertyValue instanceof Searchable
                ? ((Searchable)propertyValue).getValue()
                : String.valueOf(propertyValue);
        boolean treatAsExactMatch = Long.class.equals(propertyType) || Integer.class.equals(propertyType);

        if (searchableValue != null) {
            searchableValue = searchableValue.toLowerCase();

            double baseScore = 0;
            int matchedTokensCount = 0;

            for (SearchToken token : criteria.getSearchTokens().getAllTokens()) {

                boolean matches = treatAsExactMatch ? StringUtils.equals(searchableValue, token.getToken()) : searchableValue.contains(
                        token.getToken());
                if (matches) {

                    matchedTokensCount++;
                    matchedTokens.add(token);

                    if (searchableValue.equals(token.getToken())) {
                        //Exact match
                        //TODO: test this
                        baseScore += (token.getWeight() * 5);

                    } else {
                        //TODO: Check and give more score to exact word match, i.e.:
                        //contains ' token ', ends with ' token'

                        //Count how many times it matches the token
                        int points = StringUtils.countMatches(searchableValue, token.getToken());

                        //it starts with the token, give an extra point
                        if (searchableValue.startsWith(token.getToken())) {
                            points += 1;
                        }

                        //it starts with the token and it is the first word of the value
                        if (searchableValue.startsWith(token.getToken() + " ")) {
                            points += 1;
                        }

                        //check if the last word of the value is the token
                        if (searchableValue.endsWith(" " + token.getToken())) {
                            points += 1;
                        }

                        //check if the token is a word contained in the value
                        if (searchableValue.contains(" " + token.getToken() + " ")) {
                            points += StringUtils.countMatches(searchableValue,
                                    " " + token.getToken() + " ");
                        }

                        if (log.isTraceEnabled()) {
                            log.trace("token processed. Object:" + object + "property=" + propertyName
                                    + " propertyValue=" + searchableValue
                                    + " Token:" + token + " Token Weight:"
                                    + token.getWeight() + " points:" + points);
                        }

                        //base score is the weight of the token multiplied by the number of occurences
                        baseScore += (token.getWeight() * points);

                    }
                }

            }

            if (matchedTokensCount > 0) {
                //Give Strings that match a larger number of tokens
                //have more weight than those that match one token many times
                double propertyScore = baseScore * Math.pow(100, matchedTokensCount) / (double)depth;
                log.trace("propety score: {} propertyName={}, propertyValue={}, object={}", propertyScore, propertyName,
                        propertyValue, object);
                this.score += propertyScore;

            }
        }
    }

    @RequiredArgsConstructor
    private static class ScoreComparator implements Comparator {
        final Map<Object, Double> scores;

        @Override
        public int compare(Object o1, Object o2) {
            double diff = scores.get(o2) - scores.get(o1);
            if (diff > 0) {
                return 1;
            } else if (diff == 0) {
                return 0;
            } else {
                return -1;
            }

        }
    }

    /**
     * Determines whether a java type is searchable for a google-search like jpa query. The implementation returns true
     * if the class is a subclass of java.lang.Number, a String or implements Searchable.
     *
     * TODO: dates?
     *
     * @param type the java type
     * @return true if it is searchable
     */
    @SuppressWarnings("unchecked")
    public static boolean isSearchableType(Class type) {
        if (Number.class.isAssignableFrom(type)) {
            return true;
        }

        if (String.class.equals(type)) {
            return true;
        }

        if (Searchable.class.isAssignableFrom(type)) {
            return true;
        }

        if (type.isAnnotationPresent(Embeddable.class)) {
            return true;
        }

        return false;
    }
}
