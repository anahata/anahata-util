package com.anahata.yam.model.search;

/*
 * #%L
 * yam-core
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.Validate;

/**
 * Represents a search token with it's weight. A search token is a string within a search query or it may be a
 * concatenation of other tokens.
 *
 * @author pablo
 */
@Getter
@ToString
public class SearchToken implements Serializable {
    /**
     * The search token.
     */
    private String token;

    /**
     * The weight if a field matches this token.
     */
    private int weight;

    /**
     * the type of token *
     */
    private TokenType type;

    /**
     * If this token is of type {@link TokenType#COMPOSITE} the tokens that it is formed of.
     */
    private List<SearchToken> subTokens;

    /**
     * Creates a search token specifying a given token and its weight, the token will be trimmed and converted to lower
     * case.
     *
     * @param token the string which will be trimmed and converted to lower case. Required with at least one character.
     * @param weight the token weight, should be > 1
     * @param type token type. Required.
     */
    SearchToken(String token, int weight, TokenType type) {
        Validate.notNull(weight, token, "token can not be null");
        Validate.isTrue(!token.trim().isEmpty(), "token can not be empty");
        Validate.isTrue(weight > 0, "weight should be > 0 for token '" + token + "'");
        Validate.notNull(type, "type is required");
        Validate.isTrue(type == TokenType.BASIC || type == TokenType.QUOTED,
                "This constructor only allows BASIC and QUOTED token types");

        this.token = token.trim().toLowerCase();
        this.weight = weight;
        this.type = type;

    }

    /**
     * Constructor for composite tokens.
     *
     *
     * @param subTokens the tokens
     */
    SearchToken(List<SearchToken> subTokens) {


        Validate.notNull(subTokens, "subTokenscan not be null");
        Validate.isTrue(subTokens.size() >= 2, "subtokens must contain at least two toknes");

        StringBuilder sb = new StringBuilder();
        for (SearchToken st : subTokens) {
            Validate.isTrue(st.type != TokenType.COMPOSITE,
                    "A composite token can only be formed by non composite toknes");
            sb.append(st.getToken());
            sb.append(" ");
            weight += st.getWeight();
        }

        this.token = sb.toString().trim();
        this.subTokens = Collections.unmodifiableList(subTokens);
        this.type = TokenType.COMPOSITE;

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + Objects.hashCode(this.token);
        hash = 61 * hash + this.weight;
        hash = 61 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 61 * hash + Objects.hashCode(this.subTokens);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SearchToken other = (SearchToken) obj;
        if (!Objects.equals(this.token, other.token)) {
            return false;
        }
        if (this.weight != other.weight) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if (type == TokenType.COMPOSITE) {
            if (!CollectionUtils.isEqualCollection(this.subTokens, other.subTokens)) {
                return false;
            }

        }

        return true;
    }
}
