package com.anahata.yam.model.search;

/*
 * #%L
 * yam-core
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.formatting.Displayable;

/**
 * Interface for Java Types used in searches whose value at the database level is different than how the value is
 * displayed: e.g. Phone numbers, ABNs, etc.
 *
 * @author pablo
 */
public interface Searchable extends Displayable {
    /**
     * The raw value which is the target of search queries, this is the format in which it should be stored in the DB.
     *
     * @return the raw value
     */
    public String getValue();
}
