package com.anahata.yam.model.search;

/*
 * #%L
 * yam-core
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Contains the results of a {@link FullTextSearchRequest}.
 *
 * @param <T> the type of the search results
 * @author pablo
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
public class SearchResponse<T> implements Serializable {
    /**
     * The search request which originated this response.
     */
    private AbstractSearchRequest request;

    /**
     * The results start page (First page is 0).
     */
    private long totalCount;

    /**
     * The search results.
     */
    private List<T> page;

    /**
     * Returns the total number of pages based on the total number of results and the page size specified in the
     * request. For example if the {@link #request} has 10 items per page and there are 11 results, this method will
     * return 2.
     *
     * @return the total number of pages.
     */
    public int getTotalPages() {
        return (int)Math.ceil((float)totalCount / request.getPageSize());
    }
}
