package com.anahata.yam.model.search;

/*
 * #%L
 * yam-core
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import lombok.*;
import org.apache.commons.lang3.Validate;

/**
 * Contains the information required to perform a search.
 *
 * @author pablo
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public abstract class AbstractSearchRequest implements Serializable {
    
    /**
     * Include deleted records.
     */
    private boolean includeDeleted;

    /**
     * The sort key.
     */
    private Enum sortKey;

    /**
     * Whether to sort ascending
     */
    private boolean sortAscending;
    
    /**
     * Whether all tokens in {@link #query} should be matched.
     */
    private boolean matchAllTokens;
    
    /**
     * Whether to expand the search to child tables following {@link ExpandSearch} annotation.
     */
    private boolean expandSearch = true;

    /**
     * The results start page (First page is 0).
     */
    @Setter(AccessLevel.NONE)
    private int startPage = 0;

    /**
     * The number of elements per page.
     */
    @Setter(AccessLevel.NONE)
    private int pageSize = 100;


    /**
     * Sets the page size (must be > 0)
     *
     * @param pageSize the page size.
     */
    public void setPageSize(int pageSize) {
        Validate.isTrue(pageSize > 0, "pageSize must be > 0");
        this.pageSize = pageSize;
    }
    

    /**
     * Sets the page size (must be >= 0)
     *
     * @param startPage the first page.
     */
    public void setStartPage(int startPage) {
        Validate.isTrue(startPage >= 0, "startPage must be >= 0");
        this.startPage = startPage;
    }

    /**
     * Returns index of the first element (start page x page size).
     *
     * @return the index of the first element.
     */
    public int getStartIndex() {
        return pageSize * startPage;
    }
    
    
}
