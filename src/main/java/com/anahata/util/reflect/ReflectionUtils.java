package com.anahata.util.reflect;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.*;
import java.util.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.reflect.ConstructorUtils;

/**
 * General reflection utilities.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ReflectionUtils {
    private static final String SINGLETON_INSTANCE_FIELD_NAME = "INSTANCE";

    private static final String WELD_PROXY_SUBCLASS_NAME = "$Proxy$_$$_WeldSubclass";

    /**
     * Get a singleton instance of a class, if there is a public static final INSTANCE variable defined of the same type
     * as the class. If there is none, just instantiate that class.
     *
     * @param <T>   The class type.
     * @param clazz The class to get the singleton instance of.
     * @param args  Optional args to instantiate with, if instantiating.
     * @return The singleton instance, or a new instance if it could not be obtained.
     * @throws RuntimeException If there was an error accessing the public static final field or attempting to
     *                          instantiate the class.
     */
    public static <T> T getInstance(Class<T> clazz, Object... args) {
        if (clazz == null) {
            return null;
        }

        Field[] fields = clazz.getDeclaredFields();

        for (Field field : fields) {
            if (clazz.equals(field.getType()) && field.getName().equals(SINGLETON_INSTANCE_FIELD_NAME)) {
                int mods = field.getModifiers();

                if (Modifier.isPublic(mods) && Modifier.isStatic(mods) && Modifier.isFinal(mods)) {
                    try {
                        return (T)field.get(clazz);
                    } catch (IllegalArgumentException | IllegalAccessException e) {
                        throw new RuntimeException("Error accessing field " + field.getName() + " of type "
                                + field.getType().getName(), e);
                    }
                }
            }
        }

        try {
            return invokeConstructor(clazz, args);
        } catch (Exception e) {
            throw new RuntimeException(
                    "Exception invoking constructor for class" + clazz.getName() + " with arguments: " + Arrays.toString(
                            args) + ". Was this class ment to have a public static final " + SINGLETON_INSTANCE_FIELD_NAME + " singleton field?",
                    e);
        }
    }

    /**
     * Get all declared fields of the class and its superclasses.
     *
     * @param clazz The class. Can be null.
     * @return A list of fields. Never null.
     */
    public static List<Field> getAllDeclaredFields(Class clazz) {
        if (clazz == null) {
            return Collections.emptyList();
        }

        List<Field> fields = new ArrayList<>(Arrays.asList(clazz.getDeclaredFields()));
        fields.addAll(getAllDeclaredFields(clazz.getSuperclass()));
        return fields;
    }

    /**
     * Get the real name of a class, taking into account CDI proxying, e.g. Weld will use an inner subclass as
     * $Proxy$_$$_WeldSubclassForm. If the inner subclass is found, that portion of the name is removed.
     *
     * @param clazz
     * @return
     */
    public static String getRealClassName(Class clazz) {
        if (clazz == null) {
            return null;
        }

        return StringUtils.removeEnd(clazz.getName(), WELD_PROXY_SUBCLASS_NAME);
    }

    /**
     * Get the class of a given class name, taking into account Weld CDI proxying.
     *
     * @param className The class name.
     * @return The non-proxyed class. Returns null if className is null.
     * @throws RuntimeException If a ClassNotFoundException is encountered (wrapped).
     */
    public static Class forName(String className) {
        if (StringUtils.isBlank(className)) {
            return null;
        }

        className = StringUtils.removeEnd(className, WELD_PROXY_SUBCLASS_NAME);

        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Unable to load class: " + className, e);
        }
    }

    /**
     * Invoke the constructor of a class with given arguments. Wraps ConstructorUtils#invokeConstructor in a runtime
     * exception.
     *
     * @param <T>   The class type.
     * @param clazz The class.
     * @param args  The args.
     * @return The constructed class.
     * @throws RuntimeException If a wrapped error occurs.
     */
    public static <T> T invokeConstructor(Class<T> clazz, Object... args) {
        try {
            return ConstructorUtils.invokeConstructor(clazz, args);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            throw new RuntimeException("Unable to instantiate class: " + clazz.getName(), e);
        }
    }

    /**
     * Read a class field value, even if private.
     *
     * @param <T>        The type of the field to read.
     * @param object     The object to read from.
     * @param fieldClass The field class. Required.
     * @param field      The field. Required.
     * @return The read field, which can be null. Will be null if object is null.
     * @throws NullPointerException If fieldClass or field are null.
     * @throws RuntimeException     If the fieldClass class doesn't match the field class, or the value cannot be read.
     */
    public static <T> T readField(Object object, Class<T> fieldClass, Field field) {
        Validate.notNull(fieldClass);
        Validate.notNull(field);

        if (object == null) {
            return null;
        }

        if (!fieldClass.isAssignableFrom(field.getType())) {
            throw new RuntimeException("Cannot get field value for field " + field.getName()
                    + " because it is not type " + fieldClass.getName() + ", field type is " + field.getType().getName());
        }

        field.setAccessible(true);

        try {
            return (T)field.get(object);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException("Could not get field value for field " + field.getName(), e);
        }
    }

    /**
     * Get the generic args for a field.
     *
     * @param field The field. Required.
     * @return The args, or null if none found.
     * @throws NullPointerException If field is null.
     */
    public static Class[] getGenericArgs(Field field) {
        Validate.notNull(field);
        final Type type = field.getGenericType();

        if (type instanceof ParameterizedType) {
            ParameterizedType ptype = (ParameterizedType)type;
            final Type[] args = ptype.getActualTypeArguments();
            final Class[] result = new Class[args.length];

            for (int i = 0; i < args.length; i++) {
                result[i] = getRawClass(args[i]);
            }

            return result;
        }

        return null;
    }

    /**
     * Get the generic args of a class, given an instance.
     *
     * @param <T>            The type of the class to check.
     * @param <U>            The implementation class type.
     * @param referenceClass The reference class.
     * @param clazz          The class to check.
     * @return An array of types, or null if not found.
     */
    public static <T, U extends T> Class[] getGenericArgs(Class<T> referenceClass, Class<U> clazz) {
        for (Type type : clazz.getGenericInterfaces()) {
            Class[] result = getGenericArgs(type, referenceClass);

            if (result != null) {
                return result;
            }
        }

        // Walk up the inheritance hierarchy, checking interfaces and the class itself along the way.
        Class superclass = clazz.getSuperclass();

        if (superclass != null && !Object.class.equals(superclass)) {
            Class[] result = getGenericArgs(referenceClass, superclass);

            if (result != null) {
                return result;
            }
        }

        final Type type = clazz.getGenericSuperclass();
        return getGenericArgs(type, referenceClass);
    }

    private static Class[] getGenericArgs(Type type, Class referenceClass) {
        if (!(type instanceof ParameterizedType)) {
            return null;
        }

        final ParameterizedType ptype = (ParameterizedType)type;
        final Type rawType = ptype.getRawType();

        if (rawType instanceof Class && referenceClass.isAssignableFrom((Class)rawType)) {
            final Type[] typeArgs = ptype.getActualTypeArguments();

            if (typeArgs != null) {
                Class[] types = new Class[typeArgs.length];

                for (int i = 0; i < typeArgs.length; i++) {
                    types[i] = getRawClass(typeArgs[i]);
                }

                return types;
            }
        }

        return null;
    }

    /**
     * Get the raw class of a type.
     *
     * @param type The type.
     * @return The raw class.
     */
    public static Class getRawClass(Type type) {
        if (type instanceof Class) {
            return (Class)type;
        }

        if (type instanceof ParameterizedType) {
            ParameterizedType ptype = (ParameterizedType)type;
            return (Class)ptype.getRawType();
        }

        return Object.class;
    }

    /**
     * Get an instance of a class if it is not the base class. This can be used with generics where a default value is
     * set to an interface, and this can return a null if the default value is used, otherwise it will instantiate a
     * concrete class. This calls getInstance() and will handle a public static INSTANCE field for singletons.
     *
     * @param <T>       The base type to compare against.
     * @param <V>       The concrete type to compare.
     * @param baseClass The base class.
     * @param clazz     The concrete class.
     * @param args      Optional args to instantiate with, if instantiating.
     * @return null if clazz is baseClass, otherwise clazz instantiated with the default constructor.
     * @throws RuntimeException If there was an error accessing the public static final field or attempting to
     *                          instantiate the class.
     */
    public static <T, V extends T> T getInstanceIfNotBase(Class<T> baseClass, Class<V> clazz, Object... args) {
        return clazz.equals(baseClass) ? null : getInstance(clazz, args);
    }

    /**
     * Gets the hierarchy of a class from Object downwards.
     *
     * @param clazz the class
     * @return the hierarchy.
     */
    public static List<Class> getHierarchy(Class clazz) {
        List<Class> hierarchy = new ArrayList();
        Class pathElement = clazz;
        while (pathElement != null) {
            hierarchy.add(0, pathElement);
            pathElement = pathElement.getSuperclass();
        }
        return hierarchy;
    }
}
