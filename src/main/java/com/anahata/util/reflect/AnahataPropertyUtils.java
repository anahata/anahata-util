package com.anahata.util.reflect;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.Validate;

/**
 * Property utilities, generally a wrapper around Apache Commons BeanUtils throwing runtime exceptions and some
 * additional functionality.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 * @author Pablo Rodriguez <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnahataPropertyUtils {
    /**
     * Return the value of the specified property of the specified bean, no matter which property reference format is
     * used, with no type conversions.
     *
     * @param bean Bean whose property is to be extracted
     * @param name Possibly indexed and/or nested name of the property to be extracted
     * @return the property value
     *
     * @throws IllegalArgumentException if <code>bean</code> or <code>name</code> is null
     * @throws RuntimeException         If the caller does not have access to the property accessor method, or the
     *                                  property accessor method throws an exception, or an accessor method for this
     *                                  propety cannot be found
     */
    public static Object getProperty(Object bean, String name) {
        try {
            return PropertyUtils.getProperty(bean, name);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Same as describe but filtering properties declared in a class higher than or equal to the specified class.
     *
     * TODO: Unit test
     *
     * @param bean             the bean
     * @param stopClass        the parent class from which no more properties should be returned.
     * @param includeStopClass flag indicating whether properties in stopClass should be included.
     * @return the map containing they property names and the values.
     */
    public static Map<String, Object> describe(Object bean, Class stopClass, boolean includeStopClass) {
        final Map<String, Object> ret = new HashMap<>();
        final PropertyDescriptor[] descriptors = PropertyUtils.getPropertyDescriptors(bean);

        for (final PropertyDescriptor descriptor : descriptors) {
            if (descriptor.getReadMethod() != null) {
                final Class declaringClass = descriptor.getReadMethod().getDeclaringClass();

                if (declaringClass.equals(stopClass) && !includeStopClass) {
                    continue;
                }

                if (stopClass.isAssignableFrom(declaringClass)) {
                    ret.put(descriptor.getName(), getProperty(bean, descriptor.getName()));
                }
            }
        }
        return ret;
    }

    /**
     * Get a PropertyDescriptor for a property of a given class.
     *
     * @param beanClass The bean class. Required.
     * @param name      The property Name. Required.
     * @return The descriptor, or null if not found.
     * @throws NullPointerException If any arg is null.
     */
    public static PropertyDescriptor getPropertyDescriptor(Class beanClass, String name) {
        Validate.notNull(beanClass);
        Validate.notNull(name);
        PropertyDescriptor[] descriptors = PropertyUtils.getPropertyDescriptors(beanClass);

        for (PropertyDescriptor descriptor : descriptors) {
            if (descriptor.getName().equals(name)) {
                return descriptor;
            }
        }

        return null;
    }
    
    public static void copyProperties(Object dest, Object orig) {
        try {
            PropertyUtils.copyProperties(dest, orig);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map describe(Object bean) {
        try {
            return PropertyUtils.describe(bean);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    public static PropertyDescriptor getPropertyDescriptor(Object bean, String name) {
        try {
            return PropertyUtils.getPropertyDescriptor(bean, name);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    public static Class getPropertyType(Object bean, String name) {
        try {
            return PropertyUtils.getPropertyType(bean, name);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Set the value of the specified property of the specified bean, no matter which property reference format is used,
     * with no type conversions.
     *
     * @param bean  Bean whose property is to be modified
     * @param name  Possibly indexed and/or nested name of the property to be modified
     * @param value Value to which this property is to be set
     * @throws RuntimeException    If there is any error setting the property.
     * @throws NestedNullException If there is a null value in the nested property path.
     */
    public static void setProperty(Object bean, String name, Object value)
            throws RuntimeException, NestedNullException {
        try {
            PropertyUtils.setProperty(bean, name, value);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Set the value of the specified property of the specified bean, no matter which property reference format is used,
     * with no type conversions. This version allows for nested nulls, in which case the set does nothing.
     *
     * @param bean  Bean whose property is to be modified
     * @param name  Possibly indexed and/or nested name of the property to be modified
     * @param value Value to which this property is to be set
     * @return true if the value could be set, or null if there was a nested null in the path.
     * @throws RuntimeException    If there is any error setting the property.
     */
    public static boolean setPropertyNulls(Object bean, String name, Object value) {
        try {
            PropertyUtils.setProperty(bean, name, value);
            return true;
        } catch (NestedNullException e) {
            return false;
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
}
