
package com.anahata.util.formatting;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Locale;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * TODO: Move to AnahataUtil.
 * 
 * @author pablo
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FormatUtils {
    
    /**
     * Formats an address into a single line string. Converts the country code to the country name 
     * if the country is other than Australia (AU).
     * 
     * @param l1 address line 1
     * @param l2 address line 2
     * @param l3 address line 3
     * @param locality locality
     * @param state state 
     * @param postcode post code
     * @param countryCode the country code
     * @return 
     */
    public static String formatAddress(String l1, String l2, String l3, String locality, String state, String postcode, String countryCode) {
        
        String country;
        
        if (countryCode != null && !countryCode.equals("AU")) {
            Locale locale = new Locale("en", countryCode);
            country = locale.getDisplayCountry();
        } else {
            country = null;
        }
        
        return toStringSkipNulls(l1, l2, l3, locality, state, postcode, country);
             
    }
    
    /**
     * Converts an array of strings to a single string skipping any nulls.
     * 
     * @param tokens the strings
     * @return the single line string.
     */
    public static String toStringSkipNulls(String... tokens) {
        StringBuilder sb = new StringBuilder();
        for (String s: tokens) {
            if (s != null) {
                if (sb.length() > 0) {
                    sb.append(" ");
                }
                sb.append(s);
            }
        }
        return sb.toString();
        
    }
}
