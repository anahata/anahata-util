package com.anahata.util.formatting;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.lang.AnahataWordUtils;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FormattingUtils {
    /**
     * Ensures that columns are no shorter than this as if we had to truncate the value of a property, we could at least
     * append '...'.
     */
    private static final int COLLECTION_AS_TABLE_MIN_COLUMN_WIDTH = 6;

    /**
     * Defailt maximum column width.
     */
    private static final int COLLECTION_AS_TABLE_DEFAULT_MAX_COLUMN_WIDTH = 90;

    /**
     * Concerts a collection of Displayables to a comma separated String.
     *
     * @param displayables the collection of displayables.
     * @return
     */
    public static String displayablesToString(Collection<? extends Displayable> displayables) {
        if (displayables == null) {
            return StringUtils.EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        for (Displayable d : displayables) {
            if (d != null) {
                if (!StringUtils.isEmpty(d.getDisplayValue())) {
                    if (sb.length() > 0) {
                        sb.append(", ");
                    }
                    sb.append(d.getDisplayValue().trim());
                }
            }
        }
        return sb.toString();
    }

    /**
     * Converts an array of strings to a single string skipping any nulls.
     *
     * @param tokens the strings
     * @return the single line string.
     */
    public static String toStringSkipNulls(String... tokens) {
        StringBuilder sb = new StringBuilder();
        for (String s : tokens) {
            if (s != null) {
                if (sb.length() > 0) {
                    sb.append(" ");
                }
                sb.append(s);
            }
        }
        return sb.toString();

    }

    /**
     * Converts a collection to a table string infering the column names from the type of the first object in the list
     * and taking a default column maxWidth value.
     *
     * @param collection the collection
     * @return the table
     */
    public static String collectionToTableString(Collection<?> collection) {
        return collectionToTableString(collection, null, COLLECTION_AS_TABLE_DEFAULT_MAX_COLUMN_WIDTH);
    }

    /**
     * Converts a collection to a table string infering the column names from the type of the first object in the list.
     *
     * @param collection  the collection
     * @param maxColWidth the maximum column width (for properties whose string representation is too long)
     * @return the table
     */
    public static String collectionToTableString(Collection<?> collection, int maxColWidth) {
        return collectionToTableString(collection, null, maxColWidth);
    }

    /**
     * Converts a collection to a table string converting the properties of
     * <code>type</code> into columns and restriceted by a maximum column width (in characters).
     *
     * @param collection  the collection
     * @param type        the type of the objects in the collection, if null it will take the type of the first object
     *                    in the collection
     * @param maxColWidth
     * @return the maximum column width (for properties whose string representation is too long)
     */
    public static String collectionToTableString(Collection<?> collection, Class<?> type, int maxColWidth) {

        if (collection == null) {
            return "<null>";
        }

        if (type == null) {
            type = getFirstObjectType(collection);
        }

        if (type == null) {
            //means type has not being given and table is empty, there is nothing to render.
            return "<none>";
        }


        HashMap<String, Integer> columnWidths = new HashMap<>();
        Field[] fields = type.getDeclaredFields();
        AccessibleObject.setAccessible(fields, true);
        
        for (Field field : fields) {
            String fieldName = field.getName();
            
            for (Object o : collection) {
                try {
                    Object fieldValue = field.get(o);
                    int length = String.valueOf(fieldValue).length();
                    
                    if (maxColWidth != -1) {
                        length = Math.min(length, maxColWidth);

                        String displayName = AnahataWordUtils.humaniseCamelCase(fieldName);
                        length = Math.max(length, displayName.length());
                        length = Math.max(length, COLLECTION_AS_TABLE_MIN_COLUMN_WIDTH);
                    }

                    Integer maxSoFar = columnWidths.get(fieldName);
                    if (maxSoFar == null || length > maxSoFar) {
                        columnWidths.put(fieldName, length);
                    }
                } catch (IllegalAccessException ex) {
                    throw new InternalError("Unexpected IllegalAccessException: " + ex.getMessage());
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        sb.append("|");
        for (Field field : fields) {
            String displayName = AnahataWordUtils.humaniseCamelCase(field.getName());
            int colWidth = columnWidths.get(field.getName());
            String format = "%1$-" + colWidth + "s";
            String output = String.format(format, displayName);
            sb.append(output);
            sb.append("|");
        }
        sb.append("\n");
        sb.append("|");
        for (Field field : fields) {
            int colWidth = columnWidths.get(field.getName());
            for (int i = 0; i < colWidth; i++) {
                sb.append("-");
            }
            sb.append("+");
        }
        sb.append("\n");
        
        for (Object o : collection) {
            sb.append("|");
            for (Field field : fields) {
                Object fieldValue = null;
                try {
                    fieldValue = field.get(o);
                } catch (IllegalAccessException ex) {
                    throw new InternalError("Unexpected IllegalAccessException: " + ex.getMessage());
                }
                int colWidth = columnWidths.get(field.getName());
                boolean number = Number.class.isAssignableFrom(field.getType());
                String leftJustified = number ? "" : "-";
                String format = "%1$" + leftJustified + colWidth + "s";
                String output = String.format(format, fieldValue).replace("\n", "\\n");
                if (output.length() > colWidth) {
                    output = output.substring(0, (colWidth - COLLECTION_AS_TABLE_MIN_COLUMN_WIDTH / 2)) + "...";
                }

                sb.append(output);
                sb.append("|");
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    /**
     * Returns the type of the first object or
     * <code>null</code> if the collection doesn't have a (non null) object.
     *
     * @param <T> the generic type of the collection
     * @param c   the first object's type
     * @return the type of the first object or <code>null</code> if the collection doesn't have a (non null) object.
     */
    @SuppressWarnings("unchecked")
    private static <T> Class<T> getFirstObjectType(Collection<T> c) {
        for (T o : c) {
            if (o != null) {
                return (Class<T>)o.getClass();
            }
        }
        
        return null;
    }
    
    /**
     * Get a displayable value, handling a null object.
     * 
     * @param displayable The displayable. Can be null.
     * @return The displayable value. If displayable is null, this will return null.
     */
    public static String getDisplayableValue(Displayable displayable) {
        return displayable == null ? null : displayable.getDisplayValue();
    }
    
    /**
     * Format a numeric value with a suffix, adding 's' to the suffix if the value is not 1.
     *
     * @param amt    The value.
     * @param suffix The suffix.
     * @return The string.
     */
    public static String formatAmountSuffix(int amt, String suffix) {
        StringBuilder sb = new StringBuilder();
        sb.append(amt);
        sb.append(' ');
        sb.append(suffix);

        if (amt != 1) {
            sb.append('s');
        }

        return sb.toString();
    }
}
