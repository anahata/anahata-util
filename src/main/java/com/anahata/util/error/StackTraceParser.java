package com.anahata.util.error;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

/**
 * Parse a stack trace into a Throwable.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class StackTraceParser {
    private static final boolean LOG_ENABLED = false;

    /**
     * Parse a list of lines into a Throwable.
     *
     * @param lines The lines. Required.
     * @return The Throwable.
     * @throws NullPointerException If lines is null.
     */
    public static Throwable parse(final List<String> lines) {
        Validate.notNull(lines);
        List<StackTraceElement> stes = new ArrayList<>(lines.size() - 1);
        List<StackTraceDetail> allStes = new ArrayList<>();
        int size = lines.size();
        String message = lines.get(0);
        final int colonPos = message.indexOf(':');
        String tClass = colonPos >= 0 ? message.substring(0, colonPos) : message;
        
        for (int i = 1; i < size; i++) {
            final String line = lines.get(i);
            log("Processing line: " + line);

            if (line.startsWith("Caused by:")) {
                log("Found caused by, adding existing detail");
                allStes.add(new StackTraceDetail(tClass, message, stes));
                message = line;
                tClass = message.substring(message.indexOf(':') + 2);
                stes.clear();
            } else if (line.startsWith("\t... ")) {
                // Ignore
            } else if (line.indexOf("(") >= 0) {
                final int methodEnd = line.indexOf("(");
                final int methodStart = line.substring(0, methodEnd).lastIndexOf(".") + 1;
                final int lineNumberStart = line.indexOf(":");
                int lineNumber = -1;

                if (lineNumberStart == -1) {
                    if (line.contains("(Native Method)")) {
                        lineNumber = -2;
                    }
                } else {
                    try {
                        lineNumber = Integer.valueOf(line.substring(lineNumberStart + 1, line.indexOf(")",
                                lineNumberStart)));
                    } catch (NumberFormatException e) {
                        lineNumber = -1;
                    }
                }

                String className = line.substring(4, methodStart - 1);
                String fileName = className.substring(className.lastIndexOf('.') + 1) + ".java";
                String methodName = line.substring(methodStart, methodEnd);
                StackTraceElement ste = new StackTraceElement(className, methodName, fileName, lineNumber);
                stes.add(ste);
            }
        }

        allStes.add(new StackTraceDetail(tClass, message, stes));
        Throwable t = null;

        for (int i = allStes.size() - 1; i >= 0; i--) {
            StackTraceDetail std = allStes.get(i);
            t = new Throwable(std.getMessage(), t);
            t.fillInStackTrace();
            t.setStackTrace(std.getStes().toArray(new StackTraceElement[std.getStes().size()]));
        }

        return t;
    }

    private static void log(String message) {
        if (LOG_ENABLED) {
            System.out.printf("StackTraceParser: %s%n", message);
        }
    }

    @Getter
    private static class StackTraceDetail {
        private String className;

        private String message;

        private List<StackTraceElement> stes;

        private StackTraceDetail(String className, String message, List<StackTraceElement> stes) {
            this.className = className;
            this.message = message;
            this.stes = new ArrayList<>(stes);
        }
    }
}
