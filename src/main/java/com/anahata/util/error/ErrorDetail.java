package com.anahata.util.error;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import ch.qos.logback.classic.spi.ILoggingEvent;
import com.anahata.util.config.AboutConfig;
import com.anahata.util.env.ApplicationEnvironment;
import com.anahata.util.logging.PerformanceWarning;
import com.anahata.util.net.InetUtils;
import com.anahata.util.plaf.RuntimeUtils;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.Validate;

/**
 * Error detail.
 * 
 * @author Robert Nagajek
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
public class ErrorDetail {
    private String applicationName;
    
    private String applicationVersion;
    
    private Date created;
    
    private String appUserName;
    
    private String appUserEmail;
    
    private InetAddress localIpAddress;
    
    private String externalIpAddress;
    
    private String message;
    
    private List<ILoggingEvent> events;
    
    private List<PerformanceWarning> performanceWarnings = new ArrayList();
    
    private List<byte[]> screenshots = new ArrayList<>();
    
    private ApplicationEnvironment applicationEnv;
    
    private String device;
    
    private String javaVendor;
    
    private String javaVersion;
    
    private String javaFxVersion;
    
    private String osName;
    
    private String osVersion;
    
    private String osArchitecture;
    
    private String userName;
    
    private String userHome;
    
    private String userWorking;
    
    private String sysProps;
    
    private String memStats = RuntimeUtils.getMemoryStats();
    
    private Throwable throwable;
    
    // The attributes below are user modifable.
    
    @Setter
    private String userComments = null;
    
    @Setter
    private boolean sendScreenshot = true;
    
    /**
     * Returns true if a screenshot can be sent. This is the case when a screenshot is present and the user has
     * indicated that it can be sent.
     * 
     * @return true if it can be sent, false if not.
     */
    public boolean canSendScreenshot() {
        return !screenshots.isEmpty() && sendScreenshot;
    }
    
    public static class Builder {
        private ErrorDetail target = new ErrorDetail();
        
        public Builder about(AboutConfig config, String device) {
            return this.applicationName(config.getAppName())
            .applicationVersion(config.getBuildUID())
            .message("[" + config.getEnvironment() + "][" + device + "] " + config.getAppName() + " Errors")
            .device(device)
            .applicationEnv(config.getEnvironment());
        }
        public Builder applicationName(String applicationName) {
            target.applicationName = applicationName;
            return this;
        }
        
        public Builder applicationVersion(String applicationVersion) {
            target.applicationVersion = applicationVersion;
            return this;
        }
        
        public Builder message(String message) {
            target.message = message;
            return this;
        }
        
        public Builder created(Date created) {
            target.created = created;
            return this;
        }
        
        public Builder appUserName(String username) {
            Validate.notNull(username);
            target.appUserName = username;
            return this;
        }
        
        public Builder appUserEmail(String email) {
            Validate.notNull(email);
            target.appUserEmail = email;
            return this;
        }
        
        public Builder localIpAddress(InetAddress localIpAddress) {
            Validate.notNull(localIpAddress);
            target.localIpAddress = localIpAddress;
            return this;
        }
        
        public Builder externalIpAddress(String externalIpAddress) {
            Validate.notNull(externalIpAddress);
            target.externalIpAddress = externalIpAddress;
            return this;
        }
        
        public Builder events(List<ILoggingEvent> events) {
            Validate.notNull(events);
            target.events = events;
            Collections.reverse(target.events);
            return this;
        }
        
        public Builder screenshot(byte[] screenshot) {
            Validate.notNull(screenshot);
            target.screenshots.add(screenshot);
            return this;
        }
        
        public Builder applicationEnv(ApplicationEnvironment applicationEnv) {
            Validate.notNull(applicationEnv);
            target.applicationEnv = applicationEnv;
            return this;
        }
        
        public Builder device(String device) {
            Validate.notNull(device);
            target.device = device;
            return this;
        }
        
        public Builder userComments(String userComments) {
            Validate.notNull(userComments);
            target.userComments = userComments;
            return this;
        }                
        
        public Builder throwable(Throwable throwable) {
            Validate.notNull(throwable);
            target.throwable = throwable;
            return this;
        }
        
        public Builder javaVendor(String javaVendor) {
            Validate.notNull(javaVendor);
            target.javaVendor = javaVendor;
            return this;
        }

        public Builder javaVersion(String javaVersion) {
            Validate.notNull(javaVersion);
            target.javaVersion = javaVersion;
            return this;
        }

        public Builder javaFxVersion(String javaFxVersion) {
            Validate.notNull(javaFxVersion);
            target.javaFxVersion = javaFxVersion;
            return this;
        }

        public Builder osName(String osName) {
            Validate.notNull(osName);
            target.osName = osName;
            return this;
        }

        public Builder osVersion(String osVersion) {
            Validate.notNull(osVersion);
            target.osVersion = osVersion;
            return this;
        }

        public Builder osArchitecture(String osArchitecture) {
            Validate.notNull(osArchitecture);
            target.osArchitecture = osArchitecture;
            return this;
        }

        public Builder userName(String userName) {
            Validate.notNull(userName);
            target.userName = userName;
            return this;
        }

        public Builder userHome(String userHome) {
            Validate.notNull(userHome);
            target.userHome = userHome;
            return this;
        }

        public Builder userWorking(String userWorking) {
            Validate.notNull(userWorking);
            target.userWorking = userWorking;
            return this;
        }
        
        /**
         * Set the current default operating system details:
         * <ul>
         *   <li>Java vendor name</li>
         *   <li>Java version number</li>
         *   <li>OS name</li>
         *   <li>OS version</li>
         *   <li>OS architecture</li>
         *   <li>OS user name</li>
         *   <li>OS user home directory</li>
         *   <li>OS user working directory</li>
         *   <li>Local IP address</li>
         *   <li>External IP address</li>
         * </ul>
         * 
         * @return this
         */
        public Builder osDetails() {
            target.javaVendor = System.getProperty("java.vendor");
            target.javaVersion = System.getProperty("java.version");
            target.javaFxVersion = System.getProperty("javafx.runtime.version");
            target.osName = System.getProperty("os.name");
            target.osVersion = System.getProperty("os.version");
            target.osArchitecture = System.getProperty("os.arch");
            target.userName = System.getProperty("user.name");
            target.userHome = System.getProperty("user.home");
            target.userWorking = System.getProperty("user.dir");
            
            try {
                target.localIpAddress = InetAddress.getLocalHost();
            } catch (UnknownHostException e) {
                target.localIpAddress = null;
            }
            
            target.externalIpAddress = InetUtils.getExternalIpAddress();
            
            StringBuilder sb = new StringBuilder();
            for (String s: System.getProperties().stringPropertyNames()) {
                sb.append(s).append(" = ").append(s).append("<br/>");
            }
            target.sysProps = sb.toString();
            
            return this;
        }
        
        public ErrorDetail build() {
            if (target.created == null) {
                target.created = new Date();
            }
            
            return target;
        }
    }
}
