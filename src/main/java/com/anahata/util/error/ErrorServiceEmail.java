package com.anahata.util.error;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.ThrowableProxy;
import com.anahata.util.config.internal.ErrorEmailConfig;
import com.anahata.util.html.HtmlBuilder;
import com.anahata.util.html.StyleBuilder;
import com.anahata.util.html.TableBuilder;
import com.anahata.util.logging.FxThreadWarning;
import com.anahata.util.logging.FxThreadWarning.FxThreadSnapshot;
import com.anahata.util.logging.PerformanceWarning;
import com.anahata.util.logging.RemoteServiceWarning;
import java.io.*;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.activation.DataSource;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import static com.anahata.util.lang.Nvl.*;
import static org.apache.commons.lang3.StringUtils.*;

/**
 * Manage recording application errors by sending an email.
 *
 * @author Robert Nagajek
 */
@ApplicationScoped
@Slf4j
public class ErrorServiceEmail implements ErrorService, Serializable {

    private static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss,SSS";

    private static final int WIDTH_DATETIME = 165;

    private static final int WIDTH_USER = 70;

    private static final int WIDTH_THREAD = 80;

    private static final int WIDTH_LEVEL = 50;

    private static final int WIDTH_LOGGER = 250;

    @Inject
    private ErrorEmailConfig config;

    private String bodyStyle;

    private String textStyle;

    private String tableStyle;

    private String headingStyle;

    private String cellStyle;

    private String logCellStyle;

    private String oddCellStyle;

    private String evenCellStyle;

    private String throwableCellStyle;

    private String infoCellStyle;

    private String labelStyle;

    @PostConstruct
    void postConstruct() {
        bodyStyle = new StyleBuilder()
                .style("font-family", "Arial")
                .build();

        textStyle = new StyleBuilder()
                .stylePx("font-size", 13)
                .build();

        tableStyle = new StyleBuilder()
                .style("border-collapse", "collapse")
                .build();

        headingStyle = new StyleBuilder()
                .style("font-family", "Arial")
                .stylePx("font-size", 12)
                .style("font-weight", "bold")
                .style("border", "solid 1px #CCCCCC")
                .style("background-color", "#AAAAAA")
                .build();

        cellStyle = new StyleBuilder()
                .style("font-family", "Arial")
                .style("vertical-align", "top")
                .stylePx("padding-left", 2)
                .stylePx("padding-right", 2)
                .build();

        logCellStyle = new StyleBuilder(cellStyle)
                .style("border", "solid 1px #CCCCCC")
                .stylePx("font-size", 11)
                .build();

        infoCellStyle = new StyleBuilder(cellStyle)
                .stylePx("font-size", 13)
                .build();

        labelStyle = new StyleBuilder(infoCellStyle)
                .stylePx("width", 130)
                .style("font-weight", "bold")
                .build();

        oddCellStyle = new StyleBuilder(logCellStyle)
                .style("background-color", "none")
                .build();

        evenCellStyle = new StyleBuilder(logCellStyle)
                .style("background-color", "#EEEEEE")
                .build();

        throwableCellStyle = new StyleBuilder(logCellStyle)
                .style("background-color", "#FFD7D7")
                .build();
    }

    @Override
    public void lodge(ErrorDetail error) throws ErrorLodgementException {
        log.debug("Lodging ErrorDetail by email");
        Validate.notNull(error, "error is required");

        try {

            HtmlEmail email = new HtmlEmail();
            email.setHostName(config.getErrorEmailHost());

            if (config.getErrorEmailSmtpPort() != null) {
                email.setSmtpPort(config.getErrorEmailSmtpPort());
            }

            if (config.getErrorEmailUser() != null) {
                email.setAuthentication(config.getErrorEmailUser(), config.getErrorEmailPassword());
            }

            email.setSSLOnConnect(config.isErrorEmailSSL());
            email.setFrom(config.getErrorEmailFrom());
            email.setSubject(buildSubject(error));

            if (error.getAppUserEmail() != null) {
                email.addReplyTo(error.getAppUserEmail());
            }
            email.setMsg(buildMessage(error, email));
            email.addTo(config.getErrorEmailTo());
            if (!isBlank(config.getErrorEmailCc())) {
                email.addCc(config.getErrorEmailCc());
            }
            if (!StringUtils.isEmpty(error.getUserComments())) {
                email.addHeader("X-Priority", "1 (Highest)");
            }
            log.debug("Sending error email to {}", config.getErrorEmailTo());
            email.send();
            log.debug("error email sent to {}", config.getErrorEmailTo());
        } catch (EmailException e) {
            throw new ErrorLodgementException("Exception sending error email using " + config, e);
        }
    }

    private static String buildSubject(ErrorDetail error) throws ErrorLodgementException {
        String subjectPreffix = "";
        if (!StringUtils.isEmpty(error.getUserComments())) {
            if (!StringUtils.isEmpty(error.getUserName())) {
                subjectPreffix += "[" + error.getUserName() + "]";
            }
        }
        return subjectPreffix + error.getMessage();
    }

    private String buildMessage(ErrorDetail error, HtmlEmail email) throws ErrorLodgementException {
        final InetAddress localIpAddress = error.getLocalIpAddress();

        DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_PATTERN);

        HtmlBuilder hb = new HtmlBuilder()
                .style(bodyStyle)
                .span(textStyle, "Error encountered. Details:")
                .br()
                .br()
                .body(new TableBuilder()
                        .style(tableStyle)
                        .trTdLabelValue(labelStyle, "Date / Time", infoCellStyle, dateFormat.format(error.getCreated()))
                        .trTdLabelValue(labelStyle, "Application", infoCellStyle, error.getApplicationName())
                        .trTdLabelValue(labelStyle, "Version", infoCellStyle, error.getApplicationVersion())
                        .trTdLabelValue(labelStyle, "Environment", infoCellStyle, error.getApplicationEnv())
                        .trTdLabelValue(labelStyle, "Memory", infoCellStyle, error.getMemStats())
                        .trTdLabelValue(labelStyle, "Device", infoCellStyle, error.getDevice())
                        .trTdLabelValue(labelStyle, "Application User", infoCellStyle, error.getAppUserName())
                        .trTdLabelValue(labelStyle, "Local IP Address", infoCellStyle,
                                localIpAddress == null ? null : localIpAddress.getHostAddress())
                        .trTdLabelValue(labelStyle, "Local Host Name", infoCellStyle,
                                localIpAddress == null ? null : localIpAddress.getHostName())
                        .trTdLabelValue(labelStyle, "External IP Address", infoCellStyle, error.getExternalIpAddress())
                        .trTdLabelValue(labelStyle, "Java Vendor", infoCellStyle, error.getJavaVendor())
                        .trTdLabelValue(labelStyle, "Java Version", infoCellStyle, error.getJavaVersion())
                        .trTdLabelValue(labelStyle, "JavaFX Version", infoCellStyle, error.getJavaFxVersion())
                        .trTdLabelValue(labelStyle, "OS Name", infoCellStyle, error.getOsName())
                        .trTdLabelValue(labelStyle, "OS Version", infoCellStyle, error.getOsVersion())
                        .trTdLabelValue(labelStyle, "OS Architecture", infoCellStyle, error.getOsArchitecture())
                        .trTdLabelValue(labelStyle, "OS User", infoCellStyle, error.getUserName())
                        .trTdLabelValue(labelStyle, "OS User Home Dir", infoCellStyle, error.getUserHome())
                        .trTdLabelValue(labelStyle, "OS User Working Dir", infoCellStyle, error.getUserWorking())
                        .trTdLabelValue(labelStyle, "User Comments", infoCellStyle, error.getUserComments())
                        .build())
                .br();

        if (error.canSendScreenshot()) {
            log.debug("sending {} screenshots {}", error.getScreenshots().size());
            for (int i = 0; i < error.getScreenshots().size(); i++) {
                int idx = i + 1;
                log.debug("attaching screenshot {}", idx);

                hb.span(textStyle, "Screenshot-" + idx)
                        .br()
                        .br();
                StringBuilder sb = new StringBuilder();
                sb.append("<img src=cid:");
                try {
                    sb.append(email.embed(new PngDataSource("ErrorScreenshot-" + idx + ".png",
                            error.getScreenshots().get(i)),
                            "Error Screenshot-" + idx));
                } catch (EmailException e) {
                    throw new ErrorLodgementException(e);
                }
                sb.append(" />");
                hb.body(sb.toString());
                hb.br().br();
            }
        } else {
            log.debug("Not sending screenshot ");
        }

        if (error.getEvents() != null) {
            hb.span(textStyle, "Log trace:")
                    .br()
                    .br();
            TableBuilder tb = new TableBuilder()
                    .style(tableStyle)
                    .th(headingStyle(WIDTH_DATETIME), "Date / Time");

            //we don't have app user name
            boolean server = "Server".equalsIgnoreCase(error.getDevice());
            if (server) {
                tb.th(headingStyle(WIDTH_USER), "User");
            }

            tb.th(headingStyle, "Thread")
                    .th(headingStyle(WIDTH_LEVEL), "Level")
                    .th(headingStyle(WIDTH_LOGGER), "Logger")
                    .th(headingStyle, "Message");

            boolean white = true;

            for (ILoggingEvent event : error.getEvents()) {
                String user = nvl(event.getMDCPropertyMap().get("user"));

                String style = white ? oddCellStyle : evenCellStyle;

                tb.tr()
                        .td(style(style, WIDTH_DATETIME), dateFormat.format(new Date(event.getTimeStamp())));
                if (server) {
                    tb.td(style(style, WIDTH_USER), user);
                }
                tb.td(style(style, WIDTH_THREAD), StringUtils.trim(event.getThreadName()))
                        .td(style(style, WIDTH_LEVEL), StringUtils.trim(event.getLevel().toString()))
                        .td(style(style, WIDTH_LOGGER), StringUtils.trim(event.getLoggerName()))
                        .td(style, StringUtils.trim(event.getFormattedMessage()));
                final IThrowableProxy throwableProxy = event.getThrowableProxy();

                if (throwableProxy != null) {
                    Throwable throwable = ((ThrowableProxy)throwableProxy).getThrowable();
                    tb.tr().td(throwableCellStyle, pre(throwable), server ? 6 : 5);
                }

                white = !white;
            }

            hb.body(tb.build());
        } else {
            hb.span(textStyle, "No log trace was present.");
        }

        int fxCurrent = 0;
        int current = 0;
        if (!error.getPerformanceWarnings().isEmpty()) {
            
            for (PerformanceWarning warn : error.getPerformanceWarnings()) {
                current++;
                hb.hr();
                hb.h2("Performance Item #" + current + "/" + error.getPerformanceWarnings().size());
                hb.span(warn.getTimestamp().toString());
                if (warn instanceof FxThreadWarning) {                    
                    fxCurrent++;
                    FxThreadWarning fxWarn = (FxThreadWarning)warn;
                    
                    TableBuilder beforeAfter = new TableBuilder()
                            .style(tableStyle)
                            .th(headingStyle, "Upon detection")
                            .th(headingStyle, "After (" + fxWarn.getUnresponsiveTime() + " ms.)");
                    
                    beforeAfter.tr();
                    int monitor = 0;
                    StringBuilder before = new StringBuilder();
                    for (byte[] barr: fxWarn.getBefore()) {
                        monitor++;
                        before.append("Monitor ").append(monitor);
                        before.append("<br/>");
                        before.append(embedImage(email, barr, "FxThreadUnresponsive-" + fxCurrent + "-monitor-" + monitor + "-before"));                        
                    }
                    beforeAfter.td(before.toString());
                    
                    monitor = 0;
                    StringBuilder after = new StringBuilder();
                    for (byte[] barr: fxWarn.getAfter()) {
                        monitor++;
                        after.append("Monitor ").append(monitor);
                        after.append("<br/>");
                        after.append(embedImage(email, barr, "FxThreadUnresponsive-" + fxCurrent + "-monitor-" + monitor + "-after"));                        
                    }
                    beforeAfter.td(after.toString());
                    
                    hb.body(beforeAfter.build());
                    hb.span(labelStyle, "Fx Thread Stack Traces");
                    
                    TableBuilder tb = new TableBuilder()
                            .style(tableStyle)
                            .th(headingStyle(WIDTH_DATETIME), "Date / Time")
                            .th(headingStyle, "Unresponsive time")
                            .th(headingStyle, "Stack Trace");
                    
                    int snapCount = 0;
                    for (FxThreadSnapshot snap : fxWarn.getFxThreadSnapshots()) {
                        tb.tr();
                        String style = snapCount % 2 == 0 ? oddCellStyle : evenCellStyle;
                        tb.td(style, warn.getTimestamp().toString());
                        tb.td(style, snap.getUnresponsiveTime() + " ms.");
                        tb.td(style, snap.stackTraceHtml());
                    }

                    hb.body(tb.build());
                    
                    hb.h3("After");
                    hb.br();
                    
                } else if (warn instanceof RemoteServiceWarning) {

                    RemoteServiceWarning rsWarn = (RemoteServiceWarning)warn;

                    String mainDetails = new TableBuilder()
                    //.trTdLabelValue(labelStyle, "Date / Time", infoCellStyle, rsWarn.getTimestamp().toString())                    
                    .trTdLabelValue(labelStyle, "Class.Method", infoCellStyle, rsWarn.getClassName() + "." + rsWarn.getMethodName())
                    .trTdLabelValue(labelStyle, "Reasons", infoCellStyle, rsWarn.getReasons().toString())
                    .trTdLabelValue(labelStyle, "Round Trip time", infoCellStyle, rsWarn.getTime() + " ms.").build();
                            
                    TableBuilder tb = new TableBuilder()
                            .style(tableStyle)
                            .th(headingStyle(WIDTH_DATETIME), "Details")
                            .th(headingStyle, "Request Payload")
                            .th(headingStyle, "Response Payload");

                    tb.tr()
                            .td(cellStyle, mainDetails)
                            .td(cellStyle, FileUtils.byteCountToDisplaySize(rsWarn.getArgsSize()) + "<br/>" + rsWarn.getArgsString())
                            .td(cellStyle, FileUtils.byteCountToDisplaySize(rsWarn.getRetSize()) + " b<br/>" + rsWarn.getRetString());

                    hb.body(tb.build());
                }
            }

        }

        return hb.build();
    }

    private String embedImage(HtmlEmail email, byte[] barr, String name) throws ErrorLodgementException {
        StringBuilder sb = new StringBuilder();
        sb.append("<img src=cid:");
        try {
            sb.append(email.embed(new PngDataSource(name + ".png", barr), name));
        } catch (EmailException e) {
            throw new ErrorLodgementException(e);
        }
        sb.append(" />");
        return sb.toString();
    }

    private String pre(Throwable throwable) {
        return "<pre>" + ExceptionUtils.getStackTrace(throwable) + "</pre>";
    }

    private String style(String baseStyle, int width) {
        return new StyleBuilder(baseStyle)
                .stylePx("width", width)
                .build();
    }

    private String headingStyle(int width) {
        return style(headingStyle, width);
    }

    @RequiredArgsConstructor
    private static class PngDataSource implements DataSource {

        @Getter
        private final String name;

        private final byte[] data;

        private long maxSize;

        public PngDataSource(String name, byte[] data, long maxSize) {
            this.name = name;
            this.data = data;
            this.maxSize = maxSize;

        }

        @Override
        public InputStream getInputStream() throws IOException {
            return new ByteArrayInputStream(data);
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            throw new UnsupportedOperationException("Not supported");
        }

        @Override
        public String getContentType() {
            return "image/png";
        }
    }
}
