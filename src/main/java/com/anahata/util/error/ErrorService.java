package com.anahata.util.error;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


/**
 * Manage recording application errors. This must be injected with CDI, and an implementation configured in beans.xml.
 * 
 * @author Robert Nagajek
 */
public interface ErrorService {
    /**
     * Lodge an error. Depending on the implementation, this may attempt to perform an optimal lodgement so duplicates
     * are not lodged.
     * 
     * @param error The error. Required.
     * @throws ErrorLodgementException If there is an exception attempting to lodge the error.
     * @throws NullPointerException If error is null.
     */
    public void lodge(ErrorDetail error) throws ErrorLodgementException;
}
