package com.anahata.util.search;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author pablo
 */

public final class AnahataSearchUtils {
    /**
     * Performs a case-insensitive check on whether <tt>source</tt> contains any of the strings in <tt>tokens</tt>.
     *
     * @param source the source string
     * @param tokens the tokens we are checking for
     * @return true if source contains any of tokens
     */
    public static boolean contains(Object source, String[] tokens) {
        if (source == null || tokens == null || tokens.length == 0) {
            return false;
        }
        
        String sourceString = String.valueOf(source);
        
        for (String token : tokens) {
            Validate.notNull(token, "Tokens contains null element");
            Pattern pattern = makeTokenMatchPattern(token.toLowerCase());
            Matcher m = pattern.matcher(sourceString);
            if (m.find()) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * Makes a regular expression pattern for a given string which will match strings containing
     * <code>plain<code> even if the string contains white spaces or characters surrounded by round bracketsL '(', ')'.
     *
     * @param token
     * @return the pattern
     */
    public static Pattern makeTokenMatchPattern(String token) {
        StringBuilder sb = new StringBuilder();
        char[] carray = token.toCharArray();
        
        for (int i = 0; i < carray.length; i++) {
            sb.append("\\s*?");
            sb.append(Pattern.quote(")")).append("*?");
            sb.append("\\s*?");
            sb.append(Pattern.quote("(")).append("*?");
            sb.append(Pattern.quote(String.valueOf(carray[i])));
        }
        
        sb.append("\\s*?");
        sb.append(Pattern.quote(")")).append("*?");
        sb.append("\\s*?");
        sb.append(Pattern.quote("(")).append("*?");
        Pattern pattern = Pattern.compile(sb.toString(), Pattern.CASE_INSENSITIVE);
        return pattern;
    }

    /**
     * Splits a string into tokens divided by a whitespace but ignoring white spaces in quote or double quote delimited
     * sections. For example <hi>one 'two three'</hi> would return 2 tokens: 'one' and 'two three'
     *
     * @param queryString the string to split into tokens.
     * @return the list of tokens.
     */
    public static List<String> tokenize(String queryString) {
        List<String> matchList = new ArrayList<>();
        Pattern regex = Pattern.compile("[^\\s\"']+|\"([^\"]*)\"|'([^']*)'");
        Matcher regexMatcher = regex.matcher(queryString);
        
        while (regexMatcher.find()) {
            if (regexMatcher.group(1) != null) {
                matchList.add(regexMatcher.group(1));
            } else if (regexMatcher.group(2) != null) {
                matchList.add(regexMatcher.group(2));
            } else {
                matchList.add(regexMatcher.group());
            }
        }
        
        return matchList;
    }
}
