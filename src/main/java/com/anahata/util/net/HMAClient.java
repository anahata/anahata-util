package com.anahata.util.net;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class HMAClient {

    @Getter
    @Setter
    private String realPublicIp;
    @Getter
    @Setter
    private String executable;
    
    public HMAClient(String executable, String realPublicIp) {
        this.executable = executable;
        this.realPublicIp = realPublicIp;
    }

    public HMAClient(String executable) {
        this(executable, InetUtils.getExternalIpAddress());
    }

    /**
     * Disconnects to the vpn.
     *
     * @throws Exception
     */
    public void connect() throws Exception {
        log.debug("Before connecting, public ip is: {}", InetUtils.getExternalIpAddress());
        run("-connect", false);
        log.debug("After connecting, public ip is: {}", InetUtils.getExternalIpAddress());
    }

    /**
     * Disconnects from the vpn.
     *
     * @throws Exception
     */
    public void disconnect() throws Exception {
        log.debug("Before connecting, public ip is: {}", InetUtils.getExternalIpAddress());
        run("-disconnect", true);
        log.debug("After connecting, public ip is: {}", InetUtils.getExternalIpAddress());
    }

    /**
     * Invoke an SO command to renove the ip
     *
     * @param externalCommand Command of SO to renove ip
     * @return the new ip address
     * @throws IOException
     */
    public String changeIp() throws Exception {
        try {

            String oldPublicIp = InetUtils.getExternalIpAddress();
            run("-changeip", true);

            boolean changed = false;
            String newPublicIp = null;
            int count = 0;
            while (true) {
                if (count % 60 == 59) {
                    //renewing ip
                    log.debug("Count " + count + " calling changeIp again");
                    changeIp();
                }
                newPublicIp = InetUtils.getExternalIpAddress();
                if (!newPublicIp.equals(oldPublicIp)) {
                    changed = true;
                    log.debug("External ip change detected {}", oldPublicIp, newPublicIp);
                }

                if (changed && newPublicIp.equals(oldPublicIp)) {
                    log.debug("External ip changed but got the same ip again, changing again");
                    newPublicIp = changeIp();
                }

                if (changed && !newPublicIp.equals(realPublicIp)) {
                    break;
                }
                log.debug("Waiting for new IP address");
                Thread.sleep(1000);
            }

            log.debug("Previous ip: {}, new ip {} ", oldPublicIp, newPublicIp);
            return newPublicIp;
        } catch (InterruptedException ex) {
            log.error("Exception running HMA", ex);
            throw ex;
        }

    }

    private void run(String parameter, boolean wait) throws Exception {
        String cmd = executable + " " + parameter;
        log.debug(cmd);

        Process p = Runtime.getRuntime().exec(cmd);

        if (wait) {
            //p.waitFor();
        }

//        String line;
//        try (BufferedReader reader = new BufferedReader(
//                new InputStreamReader(p.getInputStream()))) {
//            line = reader.readLine();
//            while (line != null) {
//                log.debug("HMA process output: {}", line);
//                line = reader.readLine();
//            }
//        }
//
//        try (BufferedReader errorReader = new BufferedReader(
//                new InputStreamReader(p.getErrorStream()))) {
//            String errorLine = errorReader.readLine();
//            while (line != null) {
//                log.error("HMA process error: {}", errorLine);
//                errorLine = errorReader.readLine();
//            }
//        }
    }
}
