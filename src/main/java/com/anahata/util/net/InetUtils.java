package com.anahata.util.net;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class InetUtils {

    private static final String[] CHECK_IP_URLS = new String[]{
        "http://checkip.amazonaws.com/"
    };
    
    /**
     * Get the external IP address. If it can't be found, return a null. This
     * makes calls to external web sites to do the check.
     *
     * @return The external IP address.
     */
    public static String getExternalIpAddress() {
        for (String urlName : CHECK_IP_URLS) {
            URL url = newURL(urlName);

            try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()))) {
                return br.readLine();
            } catch (IOException e) {
                // Ignore.
            }
        }

        return null;
    }

    /**
     * Create a URL from a URL string. This wraps the MalformedURLException in a
     * RuntimeException.
     *
     * @param urlName The URL name.
     * @return The URL.
     * @throws RuntimeException If the urlName is malformed.
     */
    public static URL newURL(String urlName) {
        try {
            return new URL(urlName);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }    
}