package com.anahata.util.cdi;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import com.anahata.util.logging.logback.ErrorEventAppender;
import java.util.Iterator;
import java.util.List;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.*;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.impl.StaticLoggerBinder;

/**
 * This extension performs an injection onto the Logback ErrorEventAppender appender class, so CDI can be used with it.
 * This is required as Logback instantiates the class outside of CDI's control.
 * <br/>
 * This is enabled via the META-INF/services/javax.enterprise.inject.spi.Extension file containing this class.
 *
 * @author Robert Nagajek
 */
@Slf4j
public class LogbackWeldExtension implements Extension {
    @SuppressWarnings("unchecked")
    public void bind(@Observes AfterDeploymentValidation event, BeanManager manager) {
        Object loggerFactory = StaticLoggerBinder.getSingleton().getLoggerFactory();
        if (loggerFactory instanceof LoggerContext) {
            List<Logger> loggers = ((LoggerContext)loggerFactory).getLoggerList();
            
            for (Logger logger : loggers) {
                Appender appender = findAppender(logger);
                
                if (appender != null) {
                    AnnotatedType type = manager.createAnnotatedType(appender.getClass());
                    InjectionTarget target = manager.createInjectionTarget(type);
                    CreationalContext creationalContext = manager.createCreationalContext(null);
                    target.inject(appender, creationalContext);
                }
            }
        } else {
            log.warn(
                    "LogbackWeldExtension could not find Logback's LoggerContext instead found {} "
                            + "as StaticLoggerBinder.getSingleton().getLoggerFactory(). Most likely the "
                            + "logback-classic dependency is not available on the class path or there is "
                            + "another SLF4J binding before on the classpth. Check SLF4J initialization logs",
                    loggerFactory);
        }
    }
    
    private Appender findAppender(Logger logger) {
        Iterator<Appender<ILoggingEvent>> i = logger.iteratorForAppenders();
        
        while (i.hasNext()) {
            Appender<ILoggingEvent> appender = i.next();
            
            if (appender instanceof ErrorEventAppender) {
                return appender;
            }
        }
        
        return null;
    }
}
