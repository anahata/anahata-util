package com.anahata.util.cdi;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.annotation.Annotation;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

/**
 * Obtain CDI managed instances of classes. This works in Java SE (using Weld SE) and Java EE.
 *
 * @author Robert Nagajek
 */
@Slf4j
public final class Cdi {
    private static final Cdi INSTANCE = new Cdi();

    private boolean se;

    //storing it as object to prevent weld scanning from throwing an exception when in glassfish.
    private Object weldContainer = null;

    //storing it as object to prevent weld scanning from throwing an exception when in glassfish.
    private Object weld = null;

    private Cdi() {
        try {
            Class.forName("org.jboss.weld.environment.se.Weld");
            se = true;
            //moving shutdown hook registration to before initialize as per https://issues.jboss.org/browse/WELD-1579
            //log.debug("Cdi SE env detected, Registering shutdown hook before weld initialization");
            //Runtime.getRuntime().addShutdownHook(new CdiShutdownHook());            
            log.info("Cdi SE env detected, initializing weld");
            weld = new Weld();
            //log.debug("Shutdown hook registered, initializing Weld SE");
            long ts = System.currentTimeMillis();
            weldContainer = ((Weld)weld).initialize();
            ts = System.currentTimeMillis() - ts;
            log.info("Cdi SE initialised in {} ms", ts);
            //looks like weld has a shutdown method too, do we need to add a shutdown hook or call it explicetely
            //in the app?
        } catch (ClassNotFoundException e) {
            log.debug("Could not locate Weld SE class, assuming JEE environment {}", e.toString());
            se = false;
        }
    }

    /**
     * Determine if the CDI environment is Java SE or not.
     *
     * @return true if Java SE, false if Java EE.
     */
    public static boolean isSe() {
        return INSTANCE.se;
    }

    /**
     * Delegates to {@link BeanManager#fireEvent(java.lang.Object, java.lang.annotation.Annotation[])}.
     *
     * @param event      the event to be fired
     * @param qualifiers the qualifiers for the event.
     */
    public static void fireEvent(Object event, Annotation... qualifiers) {
        getBeanManager().fireEvent(event, qualifiers);
    }

    /**
     * Retrieves the CDI Bean Manager by performing a JNDI lookup when running in EE environment or returning the bean
     * manager of the associated Weld-SE instance when running in SE environment.
     *
     * @return the bean manager.
     */
    public static BeanManager getBeanManager() {
        if (INSTANCE.se) {
            return ((WeldContainer)INSTANCE.weldContainer).getBeanManager();
        } else {
            return lookupBeanManager();
        }
    }

    /**
     * Get a CDI instance of a class.
     *
     * @param <T>        The type.
     * @param clazz      The class.
     * @param qualifiers Optional qualifiers.
     * @return The instance.
     */
    public static <T> T get(Class<T> clazz, Annotation... qualifiers) {
        return INSTANCE.getBean(clazz, qualifiers);
    }

    private <T> T getBean(Class<T> clazz, Annotation... qualifiers) {
        Validate.notNull(clazz);

        if (se) {
            return ((WeldContainer)weldContainer).instance().select(clazz, qualifiers).get();
        }

        // TODO rework JavaEE to use qualifiers.
        //final String name = StringUtils.uncapitalize(clazz.getSimpleName());
        BeanManager bm = lookupBeanManager();        
        Bean<?> bean = bm.getBeans(clazz, qualifiers).iterator().next();
        CreationalContext<?> ctx = bm.createCreationalContext(bean);
        @SuppressWarnings("unchecked")
        T result = (T)bm.getReference(bean, clazz, ctx);        
        return result;
    }

    /**
     * Get the CDI bean manager.
     *
     * @return The bean manager.
     * @throws RuntimeException If the bean manager can't be found.
     */
    private static BeanManager lookupBeanManager() {
        try {
            InitialContext ic = new InitialContext();
            return (BeanManager)ic.lookup("java:comp/BeanManager");
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Shutdown hook to call weld.shutdown() on System.exit
     */
    private class CdiShutdownHook extends Thread {

        public CdiShutdownHook() {
            super.setName("CdiShutdownHook");
        }

        @Override
        public void run() {
            
            Weld weld_ = (Weld)weld;
            if (weld_ != null) {
                log.debug("Calling weld.shutdown on {}", weld);
                weld_.shutdown();
                log.debug("Completed weld.shutdown ");
            } else {
                log.warn("Weld SE was null on CDI shutdown hook. Initialization must have failed");
            }
            
        }
    }
}
