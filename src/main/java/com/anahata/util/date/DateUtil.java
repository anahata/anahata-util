/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.date;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.time.DateUtils;

/**
 *
 * @author vijay
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DateUtil {
    private static final SimpleDateFormat SHORT_DATE_FORMAT = new SimpleDateFormat("dd/MM/yy");

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    private static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd/MM/yyyy hh:mm a");

    private static final SimpleDateFormat FULL_DATE_TIME_FORMAT = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");

    private static final SimpleDateFormat MONTH_FULLYEAR_FORMAT = new SimpleDateFormat("MM/yyyy");

    public static String format(Date date) {
        if (date != null) {
            return DATE_FORMAT.format(date);
        }
        return "";
    }

    public static String formatDateTime(Date date) {
        if (date != null) {
            return DATE_TIME_FORMAT.format(date);
        }
        return "";
    }

    public static String formatFullDateTime(Date date) {
        if (date != null) {
            return FULL_DATE_TIME_FORMAT.format(date);
        }
        return "";
    }

    public static String formatToMonthYear(Date date) {
        if (date != null) {
            return MONTH_FULLYEAR_FORMAT.format(date);
        }
        return "";
    }

    public static String formatToShortDate(Date date) {
        if (date != null) {
            return SHORT_DATE_FORMAT.format(date);
        }
        return "";
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diff = date2.getTime() - date1.getTime();
        return timeUnit.convert(diff, TimeUnit.MILLISECONDS);
    }

    /**
     * Convert java.time.LocalDate to a java.util.Date using startOfDay an the system's default time zone.
     *
     * @param localDate required
     * @return Date
     */
    public static Date toDate(@NonNull LocalDate localDate) {
        Instant instant = localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }

    /**
     * Convert java.time.LocalDateTime to a java.util.Date using startOfDay an the system's default time zone.
     *
     * @param localDateTime required
     * @return Date
     */
    public static Date toDate(@NonNull LocalDateTime localDateTime) {
        Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }

    /**
     * Convert java.time.LocalDate to a java.util.Date representing the startOfDay of the following day an the system's
     * default time zone.
     *
     * @param localDate required
     * @return Date
     */
    public static Date toNextDayDate(@NonNull LocalDate localDate) {
        Instant instant = localDate.plusDays(1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }

    /**
     * Converts a java.util.Date to a java.util.Calendar
     *
     * @param d
     * @return
     */
    public static Calendar toCalendar(Date d) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        return cal;
    }

    /**
     * Computes the exact start time of the given date by setting 00:00:00:000
     * for HH:mm:ss.SSS
     *
     * @param date The date to be modified.
     * @return Modified date.
     */
    public static Date startOfDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    /**
     * Computes the end time of the given date by setting 23:59:59:999 for
     * HH:mm:ss.SSS
     *
     * @param date The date to be modified.
     * @return Modified date.
     */
    public static Date endOfDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        return c.getTime();
    }

    /**
     * Checks if a given collection contains a date that is the same day as in apache commons
     * DateUtils.isSameDay(Date,Date).
     *
     * @param day the day
     * @param col the collection
     * @return true or false ha ha
     */
    public static boolean containsDay(Date day, Collection<Date> col) {
        if (col.stream().anyMatch((col1) -> (DateUtils.isSameDay(day, col1)))) {
            return true;
        }
        return false;
    }

    /**
     * Adds working days (skipping saturdays and sundays).
     *
     * @param d1
     * @param workingDays
     * @param publicHolidays - public holidays
     * @return
     */
    public static Date addWorkingDays(Date d1, int workingDays, Collection<Date> publicHolidays) {
        Calendar c = toCalendar(d1);
        for (int i = 0; i < workingDays; i++) {
            do {
                c.add(Calendar.DATE, 1);
            } while (!containsDay(d1, publicHolidays)
                    && c.get(c.get(Calendar.DAY_OF_WEEK)) == Calendar.SATURDAY
                    || c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY);
        }
        return c.getTime();
    }

    /**
     * Returns the date of the first day of the month of the provided date.
     *
     * @param date Date in a month.
     * @return Date of the first day of the month
     */
    public static Date getFirstDateOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }
}
