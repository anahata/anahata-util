/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.docmosis;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.io.FileModificationListener;
import com.anahata.util.io.FileMonitor;
import com.anahata.util.plaf.OSUtils;
import com.anahata.util.plaf.ProcessUtils;
import java.io.File;
import java.util.Date;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Utility methods for working with Docmosis.
 * 
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DocmosisUtils {
    /**
     * Gets the executable libreoffice file by looking up soffice.exe in the windows PATH or soffice in linux path.
     *
     * @see {@link OSUtils#findExecutableOnPath(java.lang.String, java.lang.String, java.lang.String) }
     * @return the executable libreoffice file.
     */
    public static File getLibreOfficeExecutable() {
        File libreOfficeExecutalbe = OSUtils.findExecutableOnPath("soffice.exe", "soffice", "soffice");
        return libreOfficeExecutalbe;
    }
    
    /**
     * Checks if libreoffice is installed and available on system's PATH.
     * @see #getLibreOfficeExecutable() 
     * @return true if available, false if not.
     */
    public static boolean isLibreOfficeExecutableOnSystemPath() {
        return getLibreOfficeExecutable() != null;
    }
    
    /**
     * Edits a file in libreoffice.
     * 
     * @param name the name of the file
     * @param data the content of the file
     * @param listener listenes for changes to the file
     * @throws Exception 
     */
    public static void editInLibreOffice(String name, byte[] data, FileModificationListener listener) throws Exception  {        
        if (isLibreOfficeExecutableOnSystemPath()) {
            ProcessUtils.editFile(getLibreOfficeExecutable(), name, ".~lock.%s#", data, listener);
        } else {
            throw new IllegalStateException("LibreOffice not found on system path");
        }
    }
    
    public static void main(String[] args) throws Exception {
        editInLibreOffice("popo.txt", "some text".getBytes(), new FileModificationListener() {
            @Override
            public void fileModified(FileMonitor monitor, Date prevLastModified, Date newLastModified) {
                System.out.println("!!! File modified");
            }
        });
        while (true) {
            Thread.sleep(500);
        }
        
    }
    
    
}
