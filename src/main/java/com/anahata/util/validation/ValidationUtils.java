package com.anahata.util.validation;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.util.cdi.Cdi;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ElementKind;
import javax.validation.Path.Node;
import javax.validation.Validator;
import javax.validation.groups.Default;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * Helpers for validating beans.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ValidationUtils {
    private static final Class<?>[] DEFAULT_VALIDATION_GROUP = {Default.class};

    public static String getValidationPropertyName(ConstraintViolation cv) {
        return getValidationPropertyName(cv, false);
    }

    /**
     * Get the property name from a constraint violation that is for the root bean being validated. This will ignore any
     * nested validations through
     *
     * @Valid, and if there are root bean validations on methods using the Anahata
     * @AssertTrue annotations, it will handle those with the correct property name.
     *
     * @param cv           The constraint violation. Required.
     * @param includeIndex Set to true to include indexed text such as [0].
     * @return The property name, or null if it doesn't apply to the root level bean.
     * @throws NullPointerException If cv is null.
     */
    public static String getValidationPropertyName(ConstraintViolation cv, boolean includeIndex) {
        Validate.notNull(cv);
        List<String> nodes = new ArrayList<>();
        String prev = null;

        for (Iterator<Node> iter = cv.getPropertyPath().iterator(); iter.hasNext();) {
            Node node = iter.next();

            if (node.getKind() == ElementKind.PROPERTY) {
                String name = node.getName();

                if (name.startsWith("#")) {
                    prev = null;
                    name = name.substring(1);
                }

                if (prev != null) {
                    if (node.getIndex() != null && includeIndex) {
                        prev = prev + "[" + node.getIndex() + "]";
                    }

                    nodes.add(prev);
                }

                prev = name;
            }
        }

        if (prev != null) {
            nodes.add(prev);
        }

        StringBuilder sb = new StringBuilder();
        boolean first = true;

        for (String node : nodes) {
            if (first) {
                first = false;
            } else {
                sb.append('.');
            }

            sb.append(node);
        }

//        String path = cv.getPropertyPath().toString();
//        final Iterator<Node> pathIter = cv.getPropertyPath().iterator();
//        String[] pathArray = StringUtils.split(path, '.');
//        List<String> pathList = Arrays.asList(pathArray);
//
//        for (ListIterator<String> iter = pathList.listIterator(); iter.hasNext(); ) {
//            String value = iter.next();
//
//            if (value.startsWith("#")) {
//                iter.set(value.substring(1));
//
//                if (iter.hasPrevious()) {
//                    iter.previous();
//                    iter.remove();
//                }
//            }
//        }
//
//
//        String propertyName = pathIter.next().getName();
//
//        // A nested property is for a method invocation. If the root and leaf don't match, it is nested via @Valid and
//        // we want to report the top level property name.
//
//        if (pathIter.hasNext() && cv.getRootBeanClass().equals(cv.getLeafBean().getClass())) {
//            propertyName = pathIter.next().getName();
//        }
//
//        return propertyName;
        return sb.toString();
    }

    /**
     * Get the currently active validation groups for a bean. Standard beans will only return Default, beans
     * implementing ConditionalValidation can return different values depending on the state of the object.
     *
     * @param bean The bean. Can be null.
     * @return The validation groups. Can be null.
     */
    public static Class<?>[] getValidationGroups(Object bean) {
        if (bean == null) {
            return null;
        }

        if (!(bean instanceof ConditionalValidation)) {
            return DEFAULT_VALIDATION_GROUP;
        }

        ConditionalValidation cv = (ConditionalValidation)bean;
        return cv.getValidationGroups();
    }

    /**
     * Validate an object, handling an instance of ConditionalValidation to use conditional validation groups.
     *
     * @param <T>       The object type.
     * @param validator The validator. Required.
     * @param object    The object to validate.
     * @return The validation result.
     * @throws NullPointerException If validator is null.
     */
    public static <T> Set<ConstraintViolation<T>> validate(Validator validator, T object) {
        Validate.notNull(validator);
        return validator.validate(object, getValidationGroups(object));
    }

    /**
     * Validate an object using optional groups. If the validation fails, throw a ConstraintViolationException.
     *
     * @param <T>    The type being validated.
     * @param object The object to validate.
     * @param groups The optional groups for validation.
     * @throws ConstraintViolationException If validation fails.
     */
    public static <T> void validate(T object, Class... groups) throws ConstraintViolationException {
        validate(Cdi.get(Validator.class), object, groups);
    }

    /**
     * Validate an object using optional groups. If the validation fails, throw a ConstraintViolationException.
     *
     * @param <T>       The type being validated.
     * @param validator the Validator. Required.
     * @param object    The object to validate.
     * @param groups    The optional groups for validation.
     * @throws ConstraintViolationException If validation fails.
     */
    public static <T> void validate(Validator validator, T object, Class... groups) throws ConstraintViolationException {
        Validate.notNull(validator);
        Set<ConstraintViolation<T>> violations = validator.validate(object, groups);

        if (violations != null && !violations.isEmpty()) {
            log.debug("Validation failed for {}, groups={}, violations={}", object, groups, violations);
            throw new ConstraintViolationException(violations);
        }
    }

    /**
     * Determine if a validation passes, using optional groups.
     *
     * @param validator The validator. Required.
     * @param object    The object to validate. Required.
     * @param groups    The groups for validation. Optional.
     * @return true if validation passes, false if not.
     */
    public static boolean isValid(Validator validator, Object object, Class... groups) {
        Validate.notNull(validator);
        Validate.notNull(object);
        return validator.validate(object, groups).isEmpty();
    }
    
    /**
     * Determine if a validation passes, using optional groups and a cdi looked up validator.
     *
     * @param object    The object to validate. Required.
     * @param groups    The groups for validation. Optional.
     * @return true if validation passes, false if not.
     */
    public static boolean isValid(Object object, Class... groups) {
        return isValid(Cdi.get(Validator.class), object);
    }

    /**
     * Convertsa a ser of violations to string.
     *
     * @param violations the violations
     * @return the string.
     */
    public static String toString(Set<ConstraintViolation<?>> violations) {
        StringBuilder sb = new StringBuilder();
        sb.append(violations.size());
        sb.append(" Constraint violations: ");
        int counter = 1;
        for (ConstraintViolation cv : violations) {
            sb.append("\n\tViolation ");
            sb.append(counter).append(" of ").append(violations.size());
            sb.append(":[");
            sb.append("root bean =");
            sb.append(cv.getRootBean());
            sb.append(",leaf bean =");
            sb.append(cv.getLeafBean());
            sb.append(", path = ");
            sb.append(cv.getPropertyPath());
            sb.append(", message = ");
            sb.append(cv.getMessage());
            sb.append(", invalid value= ");
            sb.append(cv.getInvalidValue());
            sb.append("] ");
        }
        return sb.toString();
    }

    /**
     * Gets the details of a throwable. Iterates over the list of nested exceptions looking for
     * ConstraintViolationExceptions and extracts it's details.
     *
     * @param t the throwable
     * @return the details or null
     */
    public static String getConstraintValidationDetails(Throwable t) {
        String details = null;
        for (Throwable t1 : ExceptionUtils.getThrowableList(t)) {
            if (t1 instanceof ConstraintViolationException) {
                ConstraintViolationException cve = (ConstraintViolationException)t1;
                details = ValidationUtils.toString((Set)cve.getConstraintViolations());
            }
        }
        return details;
    }
}
