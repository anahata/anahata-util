package com.anahata.util.validation;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import javax.validation.groups.Default;

/**
 * Allows classes using bean validation to implement a conditional validation method that can be queried for which
 * groups are currently active for validation.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface ConditionalValidation {
    /**
     * Default validation class.
     */
    public static final Class<?>[] DEFAULT = new Class<?>[]{Default.class};

    /**
     * Get the currently active validation groups for bean validation.
     *
     * @return The validation groups.
     */
    public Class<?>[] getValidationGroups();
}
