package com.anahata.util.validation;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Arrays;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Extension of apache commons Validate class.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnahataValidate {
    /**
     * Checks that a given enum value is within a set of values..
     *
     * @param <T>    the type of the enum
     * @param value  the value being evaluated
     * @param values the values
     */
    public static <T extends Enum> void in(T value, T... values) {
        for (T t : values) {
            if (t == value) {
                return;
            }
        }
        throw new IllegalArgumentException(value + " is not one of " + Arrays.toString(values));
    }
}
