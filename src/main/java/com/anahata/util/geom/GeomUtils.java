package com.anahata.util.geom;
/*
 * Copyright 2015 Anahata.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 *
 * @author sai.dandem
 */
public class GeomUtils {
    private static final DecimalFormat formatDistance = new DecimalFormat("###0.0");

    private static final DecimalFormat formatCoords = new DecimalFormat("###0.00000");

    private static final double PERTH_LATITUDE = -31.9525739;

    private static final double PERTH_LONGITUDE = 115.8610488;

    public static double formatDistance(double value) {
        return Double.valueOf(formatDistance.format(value));
    }

    public static String formatCoords(double value) {
        
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(5, RoundingMode.CEILING);
        return bd.toString();
    }

    public static double distanceToPerthCBD(double lat1, double lng1) {
//        double earthRadius = 3958.75;
//        double dLat = Math.toRadians(PERTH_LATITUDE - lat1);
//        double dLng = Math.toRadians(PERTH_LONGITUDE - lng1);
//        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
//                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(PERTH_LATITUDE))
//                * Math.sin(dLng / 2) * Math.sin(dLng / 2);
//        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
//        return earthRadius * c;

        return pointToPointDistance(PERTH_LATITUDE, PERTH_LONGITUDE, lat1, lng1);
    }

    public static void main(String[] args) {
        System.out.println("Hello World!" + pointToPointDistance(40.006731d, -4.272978d, 40.068699d, -4.732280));
        System.out.println("Hello World!" + pointToPointDistance(40.006731d, -4.272978d, 40.068699d, -4.732280, "M"));
    }

    /**
     *
     * @param lat1 Latitude of point 1 (in decimal degrees)
     * @param lon1 Longitude of point 1 (in decimal degrees)
     * @param lat2 Latitude of point 2 (in decimal degrees)
     * @param lon2 Longitude of point 2 (in decimal degrees)
     * @param unit the unit you desire for results: where: 'M' is statute miles,'N' is nautical miles, 'K' is kilometers
     *             (default)
     * @return
     */
    public static double pointToPointDistance(double lat1, double lon1, double lat2, double lon2, String... unit) {
        double theta = lon1 - lon2;
        double deg2rad = Math.PI / 180.0;
        double rad2deg = 180 / Math.PI;
        double dist = Math.sin(lat1 * deg2rad) * Math.sin(lat2 * deg2rad) + Math.cos(lat1 * deg2rad) * Math.cos(
                lat2 * deg2rad) * Math.cos(theta * deg2rad);
        dist = Math.acos(dist);
        dist = dist * rad2deg;
        dist = dist * 60 * 1.1515;
        String option = unit.length != 0 ? unit[0] : "K";
        switch (option) {
            case "K":
                dist = dist * 1.609344;
                break;
            case "N":
                dist = dist * 0.8684;
                break;
        }
        return (dist);
    }
}
