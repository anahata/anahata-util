package com.anahata.util.props;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.application.JWSUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

/**
 * Provides structured access to properties.
 *
 * @author Robert Nagajek
 */
@Slf4j
public final class StructuredProperties {
    /**
     * The source data.
     */
    @Getter
    private final Properties properties;

    @Getter
    private String prefix;

    /**
     * Constructor using java util properties.
     *
     * @param props    The properties file to wrap. Required.
     * @param prefixes The prefix property names must have. If multiples, will be concatenated as "a.b.c" etc.
     * @throws NullPointerException If props is null.
     */
    public StructuredProperties(Properties props, String... prefixes) {
        Validate.notNull(props);

        prefix = StringUtils.join(prefixes, ".");

        if (StringUtils.isEmpty(prefix)) {
            prefix = "";
        } else {
            prefix = prefix + ".";
        }

        this.properties = props;
    }

    /**
     * Get a required String property.
     *
     * @param key The property key.
     * @return The property value.
     * @throws NullPointerException     If key is null.
     * @throws IllegalArgumentException If the property is not found.
     */
    public String getString(String key) {
        Validate.notNull(key);
        String result = getProperty(key);
        Validate.isTrue(result != null, "Property not found: %s", key);
        return result;
    }

    /**
     * Get an optional String property, returning a default value if not found.
     *
     * @param key          The property key.
     * @param defaultValue The default value, used if key is not found.
     * @return The property value.
     * @throws NullPointerException If key is null.
     */
    public String getString(String key, String defaultValue) {
        Validate.notNull(key);
        return getProperty(key, defaultValue);
    }

    /**
     * Get a required Integer property.
     *
     * @param key The property key.
     * @return The property value.
     * @throws NullPointerException     If key is null.
     * @throws IllegalArgumentException If the property is not found.
     * @throws NumberFormatException    If the value of the property is not an integer.
     */
    public Integer getInteger(String key) {
        String value = getString(key);
        return Integer.valueOf(value);
    }

    /**
     * Get a required Integer property.
     *
     * @param key          The property key.
     * @param defaultValue the default value
     * @return The property value.
     * @throws NullPointerException     If key is null.
     * @throws IllegalArgumentException If the property is not found.
     * @throws NumberFormatException    If the value of the property is not an integer.
     */
    public Integer getInteger(String key, Integer defaultValue) {
        String value = getString(key, null);
        return value != null ? Integer.valueOf(value) : defaultValue;
    }

    /**
     * Get a required Long property.
     *
     * @param key The property key.
     * @return The property value.
     * @throws NullPointerException     If key is null.
     * @throws IllegalArgumentException If the property is not found.
     * @throws NumberFormatException    If the value of the property is not a long.
     */
    public Long getLong(String key) {
        String value = getString(key);
        return Long.valueOf(value);
    }
    
    /**
     * Get an optional Long property.
     *
     * @param key The property key.
     * @param defaultValue the default value
     * @return The property value.
     * @throws NullPointerException     If key is null.
     * @throws IllegalArgumentException If the property is not found.
     * @throws NumberFormatException    If the value of the property is not a long.
     */
    public Long getLong(String key, Long defaultValue) {
        String value = getString(key, null);
        return value != null ? Long.valueOf(value) : defaultValue;
    }

    /**
     * Get a required Enum property.
     *
     * @param <T>      The enum type.
     * @param enumType The enum class.
     * @param key      The property key.
     * @return The property value.
     * @throws NullPointerException     If enumType or key are null.
     * @throws IllegalArgumentException If the property is not found, or the value is not an enum value.
     */
    public <T extends Enum<T>> T getEnum(Class<T> enumType, String key) {
        Validate.notNull(enumType);
        String value = getString(key);
        return Enum.valueOf(enumType, value);
    }

    /**
     * Get a required Enum property specifying a default value.
     *
     * @param <T>          The enum type.
     * @param enumType     The enum class.
     * @param key          The property key.
     * @param defaultValue the default value if the property is not present.
     * @return The property value.
     * @throws NullPointerException     If enumType or key are null.
     * @throws IllegalArgumentException If the property is not found, or the value is not an enum value.
     */
    public <T extends Enum<T>> T getEnum(Class<T> enumType, String key, T defaultValue) {
        Validate.notNull(enumType);
        String value = getString(key, null);
        return value != null ? Enum.valueOf(enumType, value) : defaultValue;
    }

    /**
     * Get a required Double property.
     *
     * @param key The property key.
     * @return The property value.
     * @throws NullPointerException     If key is null.
     * @throws IllegalArgumentException If the property is not found.
     * @throws NumberFormatException    If the value of the property is not a double.
     */
    public Double getDouble(String key) {
        String value = getString(key);
        return Double.valueOf(value);
    }

    /**
     * Get a required Date property.
     *
     * @param key    The property key.
     * @param format The date format to parse.
     * @return The property value.
     * @throws NullPointerException     If any param is null.
     * @throws IllegalArgumentException If the property is not found.
     * @throws RuntimeException         If the date cannot be parsed (wraps ParseException).
     */
    public Date getDate(String key, String format) {
        Validate.notNull(format);
        String value = getString(key);

        try {
            return new SimpleDateFormat(format).parse(value);
        } catch (ParseException e) {
            throw new RuntimeException("Exception parsing date value '" + key + "' with format " + format);
        }
    }

    /**
     * Get a required boolean value.
     *
     * @param key The property key. Required.
     * @return The boolean value.
     * @throws NullPointerException If any param is null.
     */
    public boolean getBoolean(String key) {
        String value = getString(key);
        return Boolean.valueOf(value);
    }

    /**
     * Get a required boolean value.
     *
     * @param key          The property key. Required.
     * @param defaultValue the default value
     * @return The boolean value.
     * @throws NullPointerException If any param is null.
     */
    public boolean getBoolean(String key, boolean defaultValue) {
        String value = getString(key, String.valueOf(defaultValue));
        return Boolean.valueOf(value);
    }

    public Class getClass(String key, Class defaultValue) throws ClassNotFoundException {
        String value = getString(key, null);
        return (value != null) ? Class.forName(value) : null;
    }

    /**
     * Get a list of key/value pairs for a given key prefix. The key prefix excludes any prefix set for this object.
     * Examples:
     *
     * In props file:
     * <pre>
     * yam.theme.Dark=dark
     * yam.theme.Light=light
     * </pre>
     *
     * Accessing:
     * <pre>
     * StructuredProperties props = new StructureProperties(props, "yam");
     * List&lt;StructuredPropertyEntry&lt;String&gt;&gt; themes = props.getStringList("theme");
     * </pre>
     *
     * The resulting list will have entries:
     * <pre>
     * Dark,dark
     * Light,light
     * </pre>
     *
     * @param keyPrefix
     * @return
     */
    public List<StructuredPropertyEntry<String>> getStringList(@NonNull String keyPrefix) {
        List<StructuredPropertyEntry<String>> values = new ArrayList<>();
        @SuppressWarnings("unchecked")
        Enumeration<String> propNames = (Enumeration<String>)properties.propertyNames();
        String pre = prefix + keyPrefix + ".";
        int len = pre.length();

        while (propNames.hasMoreElements()) {
            String propName = propNames.nextElement();

            if (propName.startsWith(pre)) {
                StructuredPropertyEntry<String> entry = new StructuredPropertyEntry<>(propName.substring(len),
                        properties.getProperty(propName));
                values.add(entry);
            }
        }

        return values;
    }

    /**
     * Converts a key to it's fully qualified name and retrieves its value.
     *
     * @param key the unprefixed key
     * @return the value
     */
    private String getProperty(String key) {
        String effectiveKey = prefix + key;
        log.trace("Looking up effective key {}", effectiveKey);
        String value = JWSUtils.getSystemProperty(effectiveKey);
        if (value == null) {            
            value = properties.getProperty(effectiveKey);
        } else {
            log.info("value for property {} taken from system property val= {}", effectiveKey, value);
        }
        
        return value;
    }

    /**
     * Converts a key to it's fully qualified name and retrieves its value.
     *
     * @param key the unprefixed key
     * @return the value
     */
    private String getProperty(String key, String defaultValue) {
        String val = getProperty(key);

        if (val == null) {
            return defaultValue;
        } else {
            return val;
        }

    }
}
