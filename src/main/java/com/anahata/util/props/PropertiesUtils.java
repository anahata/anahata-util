package com.anahata.util.props;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.env.ApplicationEnvironment;
import com.anahata.util.lang.ResourceUtils;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * Properties utilities.
 *
 * @author Robert Nagajek
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class PropertiesUtils {
    private static final String APP_PROPS = "/application.properties";

    /**
     * Load properties from a properties file on the classpath. If the file isn't found, this will do nothing.
     *
     * @param props        The properties file, mandatory.
     * @param propFileName The name of the properties file, using an absolute reference such as "/a/b/c.properties",
     *                     mandatory.
     * @throws NullPointerException     If any arguments are null.
     * @throws IllegalArgumentException If the property file could not be found.
     * @throws RuntimeException         If the properties file can't be loaded due to an IO exception.
     */
    public static void load(Properties props, String propFileName) {
        //log.debug("Loading {}", propFileName);
        Validate.notNull(props);
        Validate.notNull(propFileName);
        URL u = ResourceUtils.getResource(propFileName);

        Validate.isTrue(u != null, "Could not load properties file: %s", propFileName);
        log.debug("Loading {}", u);

        InputStream is = null;
        try {
            is = u.openStream();
            Properties loaded = new Properties();
            loaded.load(u.openStream());
            log.debug("Loaded from {}:", propFileName);
            logDebugAlphaSorted(loaded);
            props.putAll(loaded);
            log.debug("Resulting properties:");
            logDebugAlphaSorted(loaded);
        } catch (IOException e) {
            throw new RuntimeException("Error loading properties file: " + propFileName, e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    log.error("There was an error closing the properties file input stream for properties file: "
                            + propFileName, e);
                }
            }
        }
    }

    /**
     * Nice toString representation of properties sorted by property name.
     *
     * @param props
     * @return
     */
    public static String toStringAlphaSorted(Properties props) {

        StringBuilder sb = new StringBuilder();
        SortedMap<String, String> propMap = new TreeMap<>();

        for (final String propName : props.stringPropertyNames()) {
            final String propValue = props.getProperty(propName);
            propMap.put(propName, propValue);
        }

        for (final String propName : propMap.keySet()) {
            if (sb.length() > 0) {
                sb.append("\n");
            }
            sb.append(propName).append("=").append(propMap.get(propName));
        }

        return sb.toString();
    }

    /**
     * does log.debug of the properties sorted out alphabetically
     *
     * @param props
     * @return
     */
    private static void logDebugAlphaSorted(Properties props) {
        String[] s = toStringAlphaSorted(props).split("\n");
        for (String s1 : s) {
            log.debug(s1);
        }
    }

    /**
     * Load application properties from the classpath. These are properties in /application.properties. Environment
     * specific properties are then loaded if present, in the form /application-{env}.properties, which override the
     * base properties.
     *
     * @param props The properties to populate. Required.
     * @param env   The application environment. Required.
     * @throws NullPointerException     If any arguments are null.
     * @throws IllegalArgumentException If the property file could not be found.
     * @throws RuntimeException         If the properties file can't be loaded due to an IO exception.
     */
    public static void loadAppProps(Properties props, ApplicationEnvironment env) {
        Validate.notNull(props);
        Validate.notNull(env);

        log.debug("Loading properties from file {}", APP_PROPS);
        load(props, APP_PROPS);
        final String propsFile = "/application-" + env.toString().toLowerCase() + ".properties";
        log.debug("Loading properties from file {}", propsFile);

        try {
            load(props, propsFile);
        } catch (IllegalArgumentException e) {
            log.info("No environment specific property file found for: {}", propsFile);
        }

        if (env == ApplicationEnvironment.LOCAL) {
            String osUserName = System.getProperty("user.name");
            final String osUserPropsFile = "/application-" + env.toString().toLowerCase() + "-" + osUserName + ".properties";
            log.debug("Loading properties from file {}", osUserPropsFile);
            try {
                load(props, osUserPropsFile);
            } catch (IllegalArgumentException e) {
                log.info("No os user environment specific property file found for: {}", osUserPropsFile);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Final properties:");
            logDebugAlphaSorted(props);
        }
    }

    /**
     * Given properties, set system properties from these by removing a prefix such as "sysprop.".
     *
     * @param props  The properties. Required.
     * @param prefix The prefix. A single word such as "sysprop". Properties would then be in the format "sysprop.xxx".
     *               Required.
     * @throws NullPointerException If any arg is null.
     */
    public static void setSystemProperties(Properties props, String prefix) {
        Validate.notNull(props);
        Validate.notNull(prefix);
        prefix += ".";
        final int start = prefix.length();

        for (String prop : props.stringPropertyNames()) {
            if (prop.startsWith(prefix)) {
                final String name = prop.substring(start);
                final String value = props.getProperty(prop);
                log.debug("Setting system property name={} value={}", name, value);
                System.getProperties().setProperty(name, value);
            }
        }
    }

    /**
     * Given properties, set system properties from these by removing a specific prefix of "sysprop.".
     *
     * @param props The properties. Required.
     * @throws NullPointerException If any arg is null.
     */
    public static void setSystemProperties(Properties props) {
        setSystemProperties(props, "sysprop");
    }
}
