package com.anahata.util.jpa.eclipselink.converter;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Currency;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.converters.Converter;
import org.eclipse.persistence.sessions.Session;

/**
 * Store Currency values for JPA.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class CurrencyConverter implements Converter {
    @Override
    public void initialize(DatabaseMapping mapping, Session session) {
    }

    @Override
    public Object convertObjectValueToDataValue(Object objectValue, Session session) {
        if (objectValue == null) {
            return null;
        }

        Currency currency = (Currency)objectValue;
        return currency.getCurrencyCode();
    }

    @Override
    public Object convertDataValueToObjectValue(Object dataValue, Session session) {
        if (dataValue == null) {
            return null;
        }
        String currencyCode = (String)dataValue;
        try {
            return Currency.getInstance(currencyCode);
        } catch (Exception e) {
            throw new IllegalArgumentException("Exception converting currency with code '" + currencyCode + "'", e);
        }
    }

    @Override
    public boolean isMutable() {
        return false;
    }
}
