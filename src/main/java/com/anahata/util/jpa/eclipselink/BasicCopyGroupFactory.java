/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.jpa.eclipselink;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.eclipse.persistence.sessions.CopyGroup;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
public final class BasicCopyGroupFactory implements CopyGroupFactory {

    private final Class baseClass;

    private final Map<Class, Set<CopyGroupAttribute>> attributes;

    private final Map<Class, CopyGroup> predefined;

    BasicCopyGroupFactory(Class baseClass, Map<Class, Set<CopyGroupAttribute>> attributes,
            Map<Class, CopyGroup> predefined) {
        Validate.notNull(baseClass);
        Validate.notNull(attributes);
        Validate.isTrue(attributes.keySet().contains(baseClass), "Base class %s not present in attributes",
                baseClass.getSimpleName());
        this.baseClass = baseClass;
        this.attributes = attributes;
        this.predefined = predefined;
    }

    @Override
    public CopyGroup newInstance() {
        long ts = System.currentTimeMillis();
        Map<Class, CopyGroup> copyGroups = new HashMap<>();

        for (Class clazz : attributes.keySet()) {
            CopyGroup copyGroup = new CopyGroup();
            copyGroups.put(clazz, copyGroup);
        }

        for (Entry<Class, Set<CopyGroupAttribute>> attrEntry : attributes.entrySet()) {
            CopyGroup copyGroup = copyGroups.get(attrEntry.getKey());

            for (CopyGroupAttribute attr : attrEntry.getValue()) {
                if (attr.getRefClass() == null) {
                    log.trace("Adding simple attribute {}.{}", attrEntry.getKey().getSimpleName(), attr.getName());
                    copyGroup.addAttribute(attr.getName());
                } else {
                    CopyGroup refCopyGroup = predefined.get(attr.getRefClass());
                    if (refCopyGroup == null) {
                        log.trace("No predefined copygroup for class " + attr.getRefClass());
                        refCopyGroup = copyGroups.get(attr.getRefClass());
                    } else {
                        refCopyGroup = refCopyGroup.clone();
                        log.trace("Found predefined copygroup for class {} {}: ", attr.getRefClass(),
                                refCopyGroup.getAttributeNames(), System.identityHashCode(refCopyGroup));
                    }
                    Validate.notNull(refCopyGroup, "Could not find copygroup for class: %s",
                            attr.getRefClass().getSimpleName());
                    log.trace("Adding copygroup attribute {}.{} to {}.{}", attrEntry.getKey().getSimpleName(),
                            attr.getName(), System.identityHashCode(copyGroup), System.identityHashCode(refCopyGroup));
                    copyGroup.addAttribute(attr.getName(), refCopyGroup);
                }
            }
        }

        CopyGroup ret = copyGroups.get(baseClass);
        ts = System.currentTimeMillis() - ts;
        log.trace("returning cg {} for {} took {}", System.identityHashCode(ret), baseClass, ts);

        return ret;
    }
}
