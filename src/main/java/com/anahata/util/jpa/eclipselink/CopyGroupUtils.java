package com.anahata.util.jpa.eclipselink;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.jpa.JPAPropertyUtils;
import com.anahata.util.jpa.JpaPropertyDescriptor;
import java.util.HashMap;
import java.util.Map;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.eclipse.persistence.sessions.CopyGroup;

/**
 * Utility class to create EclipseLink Copy Groups.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class CopyGroupUtils {
    /**
     * Add all non-relationship JPA attributes to the given attribute group.
     *
     * @param cg      The copy group. Required.
     * @param classes The classes. At least one is required.
     * @throws NullPointerException     If any arg is null.
     * @throws IllegalArgumentException If there are no classes passed.F
     */
    public static void addAllNonAssociationAttributes(CopyGroup cg, Class... classes) {
        Validate.notNull(cg);
        Validate.notNull(classes);
        Validate.isTrue(classes.length > 0, "At least one class must be provided");

        Map<String, JpaPropertyDescriptor> props = new HashMap<>();

        for (Class c : classes) {
            props.putAll(JPAPropertyUtils.describe(c));
        }

        for (JpaPropertyDescriptor desc : props.values()) {
            if (!desc.isRelationship()) {
                //log.trace("Adding property: {}", desc.getPropertyName());
                cg.addAttribute(desc.getPropertyName());
            }
        }
    }
}
