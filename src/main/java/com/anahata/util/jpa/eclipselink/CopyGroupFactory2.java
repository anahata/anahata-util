package com.anahata.util.jpa.eclipselink;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.util.jpa.JPAPropertyUtils;
import com.anahata.util.jpa.JpaEntityUtils;
import com.anahata.util.jpa.JpaPropertyDescriptor;
import com.anahata.util.reflect.ReflectionUtils;
import java.io.Serializable;
import java.util.*;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.metamodel.Attribute;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SerializationUtils;

/**
 * Creates full copygroup templates for all entities or a specific entity, allows the user to modify the template and
 * produces new instances by cloning the template.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
public class CopyGroupFactory2 implements Serializable {
    private final Map<Class, Set<Class>> subClasses;

    private final Set<Class> topLevel = new HashSet<>();

    private final Map<Class, TypedCopyGroup> groups = new HashMap<>();

    public CopyGroupFactory2(@NonNull EntityManager em) {
        subClasses = JpaEntityUtils.getSubclasses(em);

        for (Class c : subClasses.keySet()) {
            log.trace("Subclasses of {}: {}", c, subClasses.get(c));
            topLevel.add(getCopyGroupClass(c));
        }
        
        log.trace("top level entities: {}", topLevel);
        
        for (Class class1 : topLevel) {
            getCopyGroup(class1);
        }
    }

    public CopyGroupFactory2(@NonNull EntityManager em, Class c) {
        subClasses = JpaEntityUtils.getSubclasses(em);
        getCopyGroup(c);
    }

    public CopyGroupFactory2 excludeAttribute(Attribute att) {
        Class clazz = att.getDeclaringType().getJavaType();
        getGroup(clazz).removeAttribute(att.getName());
        return this;
    }

    public TypedCopyGroup newInstance(Class c) {
        TypedCopyGroup template = getGroup(c);
        return SerializationUtils.clone(template);
        //return (TypedCopyGroup)getGroup(c).clone();
    }

    public TypedCopyGroup getGroup(Class type) {
        type = getCopyGroupClass(type);
        return groups.get(type);
    }

    private TypedCopyGroup getCopyGroup(Class clazz) {
        clazz = getCopyGroupClass(clazz);

        if (groups.containsKey(clazz)) {
            return groups.get(clazz);
        } else {
            log.trace("Didn't have copyGroup for class {}, currentGroups = {}", clazz, groups.keySet());
        }

        Collection<JpaPropertyDescriptor> props = new ArrayList<>(JPAPropertyUtils.describe(clazz).values());
        Set<Class> subclasses = subClasses.get(clazz);
        
        if (subclasses != null) {
            for (Class c : subclasses) {
                props.addAll(new ArrayList<>(JPAPropertyUtils.describe(c, false, false, null).values()));
            }
        }
        
        List<Class> allTypes = new ArrayList<>();
        allTypes.add(clazz);
        
        if (subclasses != null) {
            allTypes.addAll(subclasses);
        }

        TypedCopyGroup cg = new TypedCopyGroup(false, allTypes.toArray(new Class[allTypes.size()]));
        log.trace("Storing group for " + clazz);
        groups.put(clazz, cg);

        for (JpaPropertyDescriptor jpd : props) {
            if (jpd.isRelationship() || jpd.isEmbedded()) {
                cg.addAttribute(jpd.getPropertyName(), getCopyGroup(jpd.getEntityType()));
            } else {
                cg.addAttribute(jpd.getPropertyName());
            }
        }

        return cg;
    }

    @SuppressWarnings("unchecked")
    private Class getCopyGroupClass(final Class clazz) {
        Class ret = clazz;
        List<Class> hierarchy = ReflectionUtils.getHierarchy(clazz);
        
        for (Class hierarchyElement : hierarchy) {
            if (hierarchyElement.isAnnotationPresent(Entity.class) && subClasses.containsKey(hierarchyElement)) {
                ret = hierarchyElement;
                break;
            }
        }
        
        log.trace("CopyGroup class for {} = {} ", clazz, ret);
        return ret;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();        
        int i = 0;
        for (Class c: groups.keySet()) {            
            i++;
            sb.append("------------------------- ").append(i).append("/").append(groups.size()).append(" ")
                    .append(c.getName()).append("--------------------------------");
            sb.append(groups.get(c));
        }
        return sb.toString();
    }
}
