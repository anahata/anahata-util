package com.anahata.util.jpa.eclipselink;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.util.jpa.JPAPropertyUtils;
import com.anahata.util.jpa.JpaEntityUtils;
import com.anahata.util.jpa.JpaPropertyDescriptor;
import java.util.*;
import java.util.Map.Entry;
import javax.persistence.EntityManager;
import javax.persistence.metamodel.Attribute;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.eclipse.persistence.sessions.CopyGroup;

/**
 * Build a CopyGroupFactory.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public class CopyGroupFactoryBuilder {
    @Getter
    private final Class baseClass;

    private int depth = 1;

    private final Set<Class> includeClasses = new HashSet<>();

    private final Set<Class> excludeClasses = new HashSet<>();

    private final Map<Class, CopyGroup> predefined = new HashMap<>();

    private final Map<Class, Set<String>> excludeAttributes = new HashMap<>();

    private final Map<Class, Set<Class>> subClasses;

    public CopyGroupFactoryBuilder(@NonNull EntityManager em, @NonNull Class baseClass) {
        log.trace("--> CopyGroupFactoryBuilder() {}", baseClass);
        this.baseClass = baseClass;
        subClasses = JpaEntityUtils.getSubclasses(em);
        log.trace("<-- CopyGroupFactoryBuilder() {}", baseClass);
    }

    public CopyGroupFactoryBuilder depth(int depth) {
        Validate.isTrue(depth >= 1, "The depth must be >= 1");
        this.depth = depth;
        return this;
    }

    public CopyGroupFactoryBuilder includeClass(Class clazz) {
        Validate.notNull(clazz);
        includeClasses.add(clazz);
        return this;
    }

    public CopyGroupFactoryBuilder predefined(Class clazz, CopyGroup cg, boolean includeSubclasses) {
        Validate.notNull(clazz);
        predefined.put(clazz, cg);
        if (includeSubclasses) {
            for (Class subclass : subClasses.get(clazz)) {
                predefined.put(subclass, cg);
            }
        }
        return this;
    }

    public CopyGroupFactoryBuilder excludeClass(Class clazz) {
        Validate.notNull(clazz);
        excludeClasses.add(clazz);
        return this;
    }

    public CopyGroupFactoryBuilder excludeAttributes(Class clazz, Attribute... attributes) {
        Validate.notNull(clazz);
        Validate.notNull(attributes);
        Validate.isTrue(attributes.length >= 1, "At least one attribute must be provided to exclude");
        Set<String> attrs = excludeAttributes.get(clazz);

        if (attrs == null) {
            attrs = new HashSet<>();
            excludeAttributes.put(clazz, attrs);
        }

        for (Attribute attr : attributes) {
            attrs.add(attr.getName());
        }

        return this;
    }

    public CopyGroupFactoryBuilder excludeAttributes(Class clazz, String... attributes) {
        Validate.notNull(clazz);
        Validate.notNull(attributes);
        Validate.isTrue(attributes.length >= 1, "At least one attribute must be provided to exclude");
        Set<String> attrs = excludeAttributes.get(clazz);

        if (attrs == null) {
            attrs = new HashSet<>();
            excludeAttributes.put(clazz, attrs);
        }

        attrs.addAll(Arrays.asList(attributes));
        return this;
    }

    public CopyGroupFactory build() {
        log.trace("--> CopyGroupFactoryBuilder.build() {}", baseClass);

        CopyGroup cg = predefined.get(baseClass);
        if (cg != null) {
            return new CloningCopyGroupFactory(cg);
        }

        Map<Class, Set<CopyGroupAttribute>> attrMap = new HashMap<>();
        Map<Class, CopyGroupAttribute> postProcessMap = new HashMap<>();
        scan(attrMap, postProcessMap, baseClass, depth);

        for (Entry<Class, CopyGroupAttribute> postEntry : postProcessMap.entrySet()) {
            if (attrMap.containsKey(postEntry.getKey())) {
                log.trace("build: post-setting refclass for class {}", postEntry.getKey().getSimpleName());
                postEntry.getValue().setRefClass(postEntry.getKey());
            }
        }

        log.trace("<-- CopyGroupFactoryBuilder.build() {}", baseClass);
        return new BasicCopyGroupFactory(baseClass, attrMap, predefined);
    }

    private void scan(Map<Class, Set<CopyGroupAttribute>> attrMap, Map<Class, CopyGroupAttribute> postProcessMap,
            Class clazz, int currDepth) {
        String indent = StringUtils.leftPad("", depth - currDepth, " ");
        log.trace("{} {} scan : entry {}.", baseClass.getSimpleName(), indent, clazz);
        if (attrMap.keySet().contains(clazz)) {
            return;
        }

        if (predefined.containsKey(clazz)) {
            log.trace("{} {} scan : predefined contains {}. Will skip ", baseClass.getSimpleName(), indent, clazz);
            return;
        }

        final String className = clazz.getSimpleName();

        Set<CopyGroupAttribute> attrs = new HashSet<>();
        attrMap.put(clazz, attrs);

        Map<String, JpaPropertyDescriptor> props = JPAPropertyUtils.describe(clazz);
        @SuppressWarnings("unchecked")
        Set<Class> classes = subClasses.get(clazz);

        if (classes != null) {
            for (Class c : classes) {
                props.putAll(JPAPropertyUtils.describe(c));
            }
        }

        Set<String> excludeAttrs = excludeAttributes.get(clazz);

        for (JpaPropertyDescriptor prop : props.values()) {
            final String propName = prop.getPropertyName();
            log.trace("{} {} scan : Processing prop {}.{} ", baseClass.getSimpleName(), indent, className, propName);
            if (excludeAttrs == null || !excludeAttrs.contains(propName)) {

                if ((prop.isRelationship() || prop.isEmbedded()) && !excludeClasses.contains(prop.getEntityType())) {
                    final Class entityType = prop.getEntityType();

                    if (currDepth > 1 || includeClasses.contains(entityType)) {
                        scan(attrMap, postProcessMap, entityType, currDepth - 1);
                    }

                    if (attrMap.keySet().contains(entityType) || predefined.containsKey(entityType)) {
                        log.trace("{} {} scan : Adding prop {}.{} => {}", baseClass.getSimpleName(), indent, className,
                                propName,
                                entityType.getSimpleName());
                        attrs.add(new CopyGroupAttribute(propName, entityType));
                    } else {
                        log.trace("{} {} scan : Adding prop {}.{} => PENDING", baseClass.getSimpleName(), indent,
                                className, propName);
                        final CopyGroupAttribute cga = new CopyGroupAttribute(propName, null);
                        attrs.add(cga);
                        postProcessMap.put(entityType, cga);
                    }
                } else {
                    log.trace("{} {} scan : Adding prop {}.{}", baseClass.getSimpleName(), indent, className,
                            propName);
                    attrs.add(new CopyGroupAttribute(propName, null));
                }
            }
        }
        log.trace("{} {} scan : exit {}.", baseClass.getSimpleName(), indent, clazz);
    }
}
