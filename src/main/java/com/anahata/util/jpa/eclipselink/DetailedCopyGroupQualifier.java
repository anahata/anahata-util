package com.anahata.util.jpa.eclipselink;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.enterprise.util.AnnotationLiteral;
import lombok.NonNull;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public class DetailedCopyGroupQualifier extends AnnotationLiteral<DetailedCopyGroup> implements DetailedCopyGroup {
    private Class baseEntity;

    public DetailedCopyGroupQualifier(@NonNull Class type) {
        this.baseEntity = type;
    }

    @Override
    public Class baseEntity() {
        return baseEntity;
    }

    @Override
    public String variant() {
        return DEFAULT;
    }
}
