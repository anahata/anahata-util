package com.anahata.util.jpa.eclipselink;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.jpa.JPAPropertyUtils;
import com.anahata.util.jpa.JpaPropertyDescriptor;
import java.util.*;
import javax.persistence.metamodel.Attribute;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.eclipse.persistence.sessions.CopyGroup;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Slf4j
public class TypedCopyGroup extends CopyGroup {
    private List<Class> types;

    private final Set<String> allAtts = new HashSet<>();

    public TypedCopyGroup(Class... types) {
        this(true, types);
    }

    public TypedCopyGroup(boolean addAll, Class... types) {
        Validate.notEmpty(types);
        this.types = Arrays.asList(types);

        for (Class c : types) {
            Map<String, JpaPropertyDescriptor> props = JPAPropertyUtils.describe(c);
            allAtts.addAll(props.keySet());
        }

        if (addAll) {
            log.trace("All Atts for {} are {}", types, allAtts);
            addAttributes(allAtts);
        }
    }

    @Override
    public final void addAttributes(Collection<String> attributes) {
        Validate.notEmpty(attributes);

        for (String string : attributes) {
            addAttribute(string);
        }
    }
    
    public TypedCopyGroup getGroup(Attribute attribute) {
        Validate.isTrue(allAtts.contains(attribute.getName()),
                    "Cannot get CopyGroup for attribute " + attribute);
        return (TypedCopyGroup) super.getGroup(attribute.getName());
    }

    public void addAttribute(Attribute att) {
        addAttribute(att.getName());
    }
    
    public void removeAttribute(Attribute att) {
        removeAttribute(att.getName());
    }

    @Override
    public void addAttribute(String attribute) {
        Validate.notNull(attribute);

        //as this would be difficult, attributes with a . child notation are allowed to pass through
        if (!attribute.contains(".")) {
            Validate.isTrue(allAtts.contains(attribute),
                    "Cannot add attribute " + attribute + " for types " + Arrays.asList(types)
                    + " valid attributes are: " + allAtts);
        }

        super.addAttribute(attribute);
    }

    @Override
    public void addAttribute(String s, CopyGroup copyGroup) {
        if (!(copyGroup instanceof TypedCopyGroup)) {
            throw new IllegalArgumentException("Any CopyGroups added should be of type typedCopyGroup");
        }

        super.addAttribute(s, copyGroup);
    }

    public <T extends TypedCopyGroup> T addAttribute(Attribute att, T typedCopyGroup) {
        return addAttribute(att.getName(), typedCopyGroup);
    }

    public <T extends TypedCopyGroup> T addAttribute(String attribute, T typedCopyGroup) {
        Validate.notNull(attribute);
        Validate.notNull(typedCopyGroup);
        Validate.isTrue(allAtts.contains(attribute),
                getClass().getSimpleName() + " cannot add attribute " + attribute + " for types "
                + Arrays.asList(types) + " valid attributes are: " + allAtts);
        Set<Class> supportedTypes = new HashSet<>();
        
        //we loop through all supported types
        for (Class t : types) {
            Map<String, JpaPropertyDescriptor> props = JPAPropertyUtils.describe(t);
            //check if such attribute name exists
            if (props.containsKey(attribute)) {
                JpaPropertyDescriptor jpd = props.get(attribute);

                if (jpd.isRelationship() || jpd.isEmbedded()) {
                    Class attributeType = jpd.getEntityType();
                    supportedTypes.add(attributeType);
                    
                    //If it is a relationship or an embeddable, we allow copy groups
                    
                    for (Class typedCopyGroupType: typedCopyGroup.getTypes()) {
                        //if the type on the cg being added is a the same or a subclass of the type of the 
                        //attribute being added as per this copygroup, then we are good to go
                        if (attributeType.isAssignableFrom(typedCopyGroupType)) {// a bit too lineant on embedded types but ok I suppose
                            super.removeAttribute(attribute);
                            super.addAttribute(attribute, typedCopyGroup);
                            return typedCopyGroup;
                        }
                    }
                    
                }
            }
        }

        throw new IllegalArgumentException(getClass().getSimpleName() + " Cannot add copygroup "
                + typedCopyGroup.getClass().getSimpleName() + " for attribute " + attribute
                + " valid types for this attribute are:" + supportedTypes + " (or subclasses) specified copygroup represents types: "
                + typedCopyGroup.getTypes());
    }
    
    /**
     * Removes everything from this copy group.
     */
    public void clear() {
        for (String s : getAttributeNames()) {
            removeAttribute(s);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int indent = 0;
        Set<TypedCopyGroup> allreadyPrinted = new HashSet();
        toString(sb, indent, allreadyPrinted);
        return sb.toString();
    }

    private void appendType(StringBuilder sb) {
        sb.append("{");
        boolean first = true;
        for (Class currType : types) {
            if (!first) {
                sb.append(",");
            }
            sb.append(currType.getSimpleName());
            first = false;
        }
        sb.append("}:id=");
        sb.append(System.identityHashCode(this));
        sb.append(" copies:");
        sb.append(getCopies().size());
    }

    public void toString(StringBuilder sb, int indent, Set<TypedCopyGroup> alreadyPrinted) {
        alreadyPrinted.add(this);

        appendType(sb);
        indent = indent + 2;
        String padding = StringUtils.leftPad("", indent, " ");
        List<String> atts = new ArrayList(getAttributeNames());
        Collections.sort(atts);
        for (String s : atts) {
            sb.append("\n");
            sb.append(padding);
            sb.append("+");
            sb.append(s);
            TypedCopyGroup cg = (TypedCopyGroup)getGroup(s);
            if (cg != null) {
                sb.append(" = ");
                if (alreadyPrinted.contains(cg)) {
                    sb.append("Ref:");
                    cg.appendType(sb);
                } else {
                    int newIndent = indent + s.length() + 4;
                    cg.toString(sb, newIndent, alreadyPrinted);
                }
            }
        }
    }
}
