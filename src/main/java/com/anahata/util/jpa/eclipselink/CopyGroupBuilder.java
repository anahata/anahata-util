package com.anahata.util.jpa.eclipselink;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Collection;
import org.apache.commons.lang3.Validate;
import org.eclipse.persistence.sessions.CopyGroup;

/**
 * Build an EclipseLink CopyGroup nicely.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class CopyGroupBuilder {
    private CopyGroup cg = new CopyGroup();

    /**
     * Add all non-relationship attributes of the given class to the top level of the CopyGroup.
     *
     * @param classes The classes. Required.
     * @return The builder.
     * @throws NullPointerException If c is null.
     */
    public CopyGroupBuilder add(Class... classes) {
        Validate.notNull(classes);
        CopyGroupUtils.addAllNonAssociationAttributes(cg, classes);
        return this;
    }

    /**
     * Add an attribute to the CopyGroup.
     *
     * @param name The attribute name/path. Required.
     * @return The builder.
     * @throws NullPointerException If name is null.
     */
    public CopyGroupBuilder add(String name) {
        Validate.notNull(name);
        cg.addAttribute(name);
        return this;
    }
    
    /**
     * Adds a collection of attributes to the CopyGroup.
     *
     * @param names The attribute names/path. Required.     
     * @return The builder.
     * @throws NullPointerException If name is null.
     */
    public CopyGroupBuilder addAll(Collection<String> names) {
        Validate.notNull(names);        
        cg.addAttributes(names);
        return this;
    }
    
    /**
     * Adds a collection of attributes for a given attribute to the CopyGroup.
     *
     * @param name The attribute names. Required.     
     * @param names The names/paths of the name attribute. Required.     
     * @return The builder.
     * @throws NullPointerException If name is null.
     */
    public CopyGroupBuilder addAll(String name, Collection<String> names) {
        Validate.notNull(name);
        Validate.notNull(names);        
        for (String string : names) {
            cg.addAttribute(name + "." + string);
        }
        return this;
    }

    /**
     * Add all attributes of the given class to the given path of the CopyGroup.
     *
     * @param name    The attribute name/path. Required.
     * @param classes The classes. Required.
     * @return The builder.
     * @throws NullPointerException If any arg is null.
     */
    public CopyGroupBuilder add(String name, Class... classes) {
        Validate.notNull(name);
        Validate.notNull(classes);
        CopyGroup cgr = new CopyGroup();
        CopyGroupUtils.addAllNonAssociationAttributes(cgr, classes);
        cg.addAttribute(name, cgr);
        return this;
    }

    /**
     * Add all attributes of the given subgroup to the top level of the CopyGroup.
     *
     * @param name     The attribute name/path. Required.
     * @param subGroup The subgroup. Required.
     * @return The builder.
     * @throws NullPointerException If any arg is null.
     */
    public CopyGroupBuilder add(String name, CopyGroup subGroup) {
        Validate.notNull(name);
        Validate.notNull(subGroup);
        cg.addAttribute(name, subGroup);
        return this;
    }

    public CopyGroup build() {
        return cg;
    }
}
