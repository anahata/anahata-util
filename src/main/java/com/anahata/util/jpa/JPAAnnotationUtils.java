package com.anahata.util.jpa;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Utility methods to introspect JPA Annotations.
 *
 * //TODO Cache Ids?
 *
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JPAAnnotationUtils {
    
    /**
     * Checks if a given class has the annotation
     * <code>javax.persistence.Entity</code>
     *
     * @param an class.
     * @return true if <code>c</code> class has the annotation <code>javax.persistence.Entity</code>
     */
    public static boolean isEntity(Class c) {
        return c.isAnnotationPresent(Entity.class);
    }

    /**
     * Retrieves the value of the attribute annotated with with
     * <code>@javax.persistence.Id</code> or <code>@javax.persistence.EmbeddedId</code> by invoking its getter.
     *
     * @param an instance of a JPA entitiy.
     * @return the value of the attribute annotated with <code>@javax.persistence.Id</code>
     * @deprecated This can be obtained from PersistenceUnitUtil#getIdentifier(Object), part of JPA 2.0.
     */
    @Deprecated
    public static Object getId(Object o) throws IntrospectionException, NoSuchFieldException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException {

        Field idField = getIdField(o.getClass());
        for (PropertyDescriptor pd : Introspector.getBeanInfo(o.getClass()).getPropertyDescriptors()) {
            if (pd.getReadMethod() == null) {
                continue;
            }
            if (pd.getName().equals(idField.getName())) {
                return pd.getReadMethod().invoke(o, (Object[])null);
            }

        }
        throw new IllegalArgumentException("Could not find getter for field annotated with @" + Id.class.getName() + "");
    }

    /**
     * Retrieves the Field annotated with
     * <code>@javax.persistence.Id</code> or <code>@javax.persistence.EmbeddedId</code>
     *
     * @param the class of a JPA entitiy.
     * @return the value of the attribute annotated with <code>@javax.persistence.Id</code>
     */
    public static Field getIdField(Class c) throws IntrospectionException, NoSuchFieldException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException {


        for (Field f : c.getDeclaredFields()) {
            if (f.isAnnotationPresent(Id.class) || f.isAnnotationPresent(EmbeddedId.class)) {
                return f;
            }
        }

        throw new IllegalArgumentException(
                "No field annotated as @Id or @EmbeddedId");
    }

    /**
     * Retrieves the Field annotated with
     * <code>@javax.persistence.Id</code> or <code>@javax.persistence.EmbeddedId</code>
     *
     * @param the class of a JPA entitiy.
     * @return the value of the attribute annotated with <code>@javax.persistence.Id</code>
     *
     */
    public static boolean isId(Class c, String propertyName) throws IntrospectionException, NoSuchFieldException,
            IllegalAccessException,
            IllegalArgumentException, InvocationTargetException {
        Field f = getIdField(c);
        return f.getName().equals(propertyName);

    }
}
