package com.anahata.util.jpa;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.reflect.ReflectionUtils;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.Transient;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.Validate;

/**
 * Property utils for JPA entities.
 *
 * @author Pablo Rodriguez <pablo@anahata-it.com.au>
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JPAPropertyUtils {
    public static <T> void updateBasicProperties(T source, T target) {
        Class clazz = source.getClass();

        while (!Object.class.equals(clazz)) {
            for (Field f : clazz.getDeclaredFields()) {
                try {
                    PropertyDescriptor pd = PropertyUtils.getPropertyDescriptor(source, f.getName());

                    if (isBasicUpdatableProperty(clazz, f, pd)) {
                        Object val = PropertyUtils.getProperty(source, f.getName());
                        PropertyUtils.setProperty(target, f.getName(), val);
                        log.trace("{} Updating {} : {}" + source.getClass().getSimpleName(), pd.getName(), val);
                    }
                } catch (IntrospectionException | NoSuchFieldException | IllegalArgumentException | IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
                }
            }

            clazz = clazz.getSuperclass();
        }
    }

    public static boolean isBasicUpdatableProperty(Class clazz, Field f, PropertyDescriptor pd)
            throws IntrospectionException, NoSuchFieldException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException {
        if (pd.getReadMethod() == null || pd.getWriteMethod() == null) {
            return false;
        }

        if (f.isAnnotationPresent(Transient.class)) {
            return false;
        }

        // FIXME Needs anahata-util method added
        if (JPAAnnotationUtils.isId(clazz, f.getName())) {
            return false;
        }

        if (f.getType().isAnnotationPresent(Entity.class)) {
            return false;
        }

        if (Collection.class.isAssignableFrom(f.getType())) {
            return false;
        }

        return true;
    }

    public static Map<String, JpaPropertyDescriptor> describe(Class beanClass) {
        return describe(beanClass, false, true, Object.class);
    }

    public static Map<String, JpaPropertyDescriptor> describe(Class beanClass, boolean setterRequired,
            boolean includeSuper, Class stopClass) {
        Map<String, Field> fields = describeDeclaredFields(beanClass, includeSuper, stopClass);
        try {
            PropertyDescriptor[] propertyDescriptors = Introspector.getBeanInfo(beanClass).getPropertyDescriptors();
            Map<String, PropertyDescriptor> propertyDescriptorsByPropertyName = new HashMap<>();
            Map<String, JpaPropertyDescriptor> ret = new HashMap<>(propertyDescriptors.length);
            for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
                propertyDescriptorsByPropertyName.put(propertyDescriptor.getName(), propertyDescriptor);
            }

            for (String property : fields.keySet()) {
                PropertyDescriptor pd = propertyDescriptorsByPropertyName.get(property);
                Method setter = pd != null ? pd.getWriteMethod() : null;
                Method getter = pd != null ? pd.getReadMethod() : null;

                if (!setterRequired || setter != null) {
                    Field f = fields.get(property);

                    if (f != null) {
                        Class[] genericTypes = ReflectionUtils.getGenericArgs(f);
                        if (genericTypes == null) {
                            genericTypes = new Class[0];
                        }
//                        System.out.println("GenericTypes for field: " + f.getName() + " are " + Arrays.asList(
//                                genericTypes) + " type=" + f.getType());
                        JpaPropertyDescriptor jpaPropertyDescriptor = new JpaPropertyDescriptor(f.getName(), f.getType(),
                                genericTypes);

                        for (Annotation annotation : f.getAnnotations()) {
                            jpaPropertyDescriptor.addAnnotation(annotation.annotationType());
                        }

                        if (getter != null) {
                            for (Annotation annotation : getter.getAnnotations()) {
                                jpaPropertyDescriptor.addAnnotation(annotation.annotationType());
                            }
                        }

                        if (!jpaPropertyDescriptor.isTransient()) {
                            ret.put(property, jpaPropertyDescriptor);
                        }
                    }
                }
            }

            return ret;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get all declared fields of the class and its superclasses.
     *
     * @param clazz     The class. Can not be null.
     * @param stopClazz The stop class. Can be null.
     * @return A list of fields. Never null.
     */
    private static Map<String, Field> describeDeclaredFields(Class<?> clazz, boolean includeSuper, Class<?> stopClass) {
        if (stopClass == null) {
            stopClass = Object.class;
        }
        Validate.notNull(clazz, "clazz is required");
        Map<String, Field> ret = new HashMap<>();
        if (stopClass == null || !stopClass.equals(clazz)) {
            for (Field f : clazz.getDeclaredFields()) {
                if (!f.getName().startsWith("_persistence") && !Modifier.isStatic(f.getModifiers())) {
                    ret.put(f.getName(), f);
                }
            }
            if (includeSuper) {
                Class parent = clazz.getSuperclass();
                if (parent != null) {
                    ret.putAll(describeDeclaredFields(parent, true, stopClass));
                }
            }
        }
        return ret;

    }
}
