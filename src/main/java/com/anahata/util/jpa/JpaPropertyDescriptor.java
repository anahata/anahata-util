package com.anahata.util.jpa;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import lombok.*;

/**
 * Describe a JPA property.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(of = "propertyName")
@ToString
public final class JpaPropertyDescriptor {
    @Getter
    private String propertyName;

    @Getter
    private Class type;

    private Class[] genericTypes;

    final private Set<Class<? extends Annotation>> annotations = new HashSet<>();

    void addAnnotation(Class<? extends Annotation> annotation) {
        annotations.add(annotation);
    }

    public boolean isOneToMany() {
        return annotations.contains(OneToMany.class);
    }

    public boolean isManyToOne() {
        return annotations.contains(ManyToOne.class);
    }

    public boolean isOneToOne() {
        return annotations.contains(OneToOne.class);
    }

    public boolean isManyToMany() {
        return annotations.contains(ManyToMany.class);
    }

    public boolean isEmbedded() {
        return annotations.contains(Embedded.class);
    }

    public boolean isTransient() {
        return annotations.contains(Transient.class);
    }

    public boolean isRelationship() {
        return isOneToMany() || isManyToOne() || isOneToOne() || isManyToMany();
    }

    public Class getEntityType() {
        if (Collection.class.isAssignableFrom(type)) {
            return genericTypes[0];
        } else if (isEmbedded() || isRelationship()) {
            return type;
        } else {
            throw new IllegalStateException("property " + propertyName + " does not represent an entity type");
        }
    }
}
