package com.anahata.util.jpa;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.sessions.CopyGroup;

/**
 * Utility methods for JPA.
 *
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class JPAUtils {
    /**
     * Persists or merges an object depending on whether the entity has an id attribute.
     *
     * @param <T>    the type of the entity being persisted
     * @param em     the entityManager that will perform the operation
     * @param entity the entity to be saved.
     * @return the managed entity.
     */
    public static <T> T save(EntityManager em, T entity) {
        Validate.notNull(entity, "can not save null entity");
        Object id = em.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity);

        if (id == null) {
            em.persist(entity);
        } else {
            entity = em.merge(entity);
        }

        return entity;
    }

    /**
     * Creates a <code>TypedQuery</code> to do <code>Select * FROM T</code>.
     *
     * @param <T>    the entity type
     * @param em     the entityManager
     * @param entity the entity class
     * @return the query to retrieve all records.
     */
    public static <T> TypedQuery<T> selectAll(EntityManager em, Class<T> entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(entity);
        Root<T> from = cq.from(entity);
        CriteriaQuery<T> select = cq.select(from);
        TypedQuery<T> typedQuery = em.createQuery(select);
        return typedQuery;
    }

    /**
     * Creates a <code>TypedQuery</code> to do <code>Select Count(*) FROM T</code>.
     *
     * @param entityManager the entityManager
     * @param entity        the entity class
     * @return the query to retrieve all records.
     */
    public static TypedQuery<Long> selectCountAll(EntityManager entityManager, Class<?> entity) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<?> from = criteriaQuery.from(entity);
        CriteriaQuery<Long> select = criteriaQuery.select(criteriaBuilder.count(from));
        TypedQuery<Long> typedQuery = entityManager.createQuery(select);
        return typedQuery;
    }

    /**
     * Creates a <code>TypedQuery</code> to do <code>SELECT * FROM T WHERE A = V </code>, or
     * <code>SELECT * FROM T WHERE A IN (V1, V2, ... Vn)</code> if more than one value is specified.
     *
     * @param <A>
     * @param <T>           the entity type
     * @param entityManager the entityManager
     * @param entity        the entity class
     * @param attribute     the column
     * @param values        the value for that column
     * @return the query to retrieve the matching records.
     */
    public static <A, T extends A> TypedQuery<T> findByField(
            EntityManager entityManager, Class<T> entity, SingularAttribute<A, ?> attribute, Object... values) {
        return findByField(entityManager, entity, attribute, null, false, values);
    }

    /**
     * Creates a <code>TypedQuery</code> to do <code>SELECT * FROM T WHERE A = V </code>, or
     * <code>SELECT * FROM T WHERE A IN (V1, V2, ... Vn)</code> if more than one value is specified.
     *
     * @param <A>
     * @param <T>           the entity type
     * @param entityManager the entityManager
     * @param entity        the entity class
     * @param attribute     the column
     * @param values        the value for that column
     * @return the query to retrieve the matching records.
     */
    public static <A, T extends A> TypedQuery<T> findByField(
            EntityManager entityManager, Class<T> entity, SingularAttribute<A, ?> attribute, List values) {
        return findByField(entityManager, entity, attribute, null, false, (Object[])values.toArray());
    }

    /**
     * Checks if there is another record with the same value for a given field.
     *
     * @param <A>
     * @param <T>           the entity type
     * @param entityManager the entityManager
     * @param candidate     the object we are evaluating for duplicates
     * @param entity        the base entity class from which we want to search
     * @param attribute     - the field
     * @param values        - the values for that field
     * @return true if there isa record other than <code>candidate</code> with field=value
     */
    public static <A, T extends A> boolean hasDuplicate(EntityManager entityManager, T candidate, Class<T> entity,
            SingularAttribute<A, ?> attribute, Object... values) {
        List<T> ret = findByField(entityManager, entity, attribute, null, false, values).setMaxResults(2).getResultList();

        if (ret.size() > 1) {
            return true;
        } else if (ret.size() == 1 && !Objects.equals(candidate, ret.get(0))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Creates a <code>TypedQuery</code> to do <code>SELECT * FROM T WHERE A = V ORDER BY O ASC/DESC</code>. or
     * <code>SELECT * FROM T WHERE A IN (V1, V2, ... Vn) ORDER BY O ASC/DESC</code> if more than one value is specified.
     *
     * @param <T>           the entity type
     * @param entityManager the entityManager
     * @param entity        the entity class
     * @param attribute     the column
     * @param orderBy       the attribute to order by
     * @param asc           true if order in ascending order.
     * @param values        the values that column can match,
     * @return the query to retrieve the matching records.
     */
    public static <A, T extends A, V> TypedQuery<T> findByField(EntityManager entityManager, Class<T> entity,
            SingularAttribute<A, V> attribute, SingularAttribute<A, V> orderBy, boolean asc, Object... values) {
        return findByFieldSorted(entityManager, entity, attribute, orderBy, asc, values);

    }
    
    /**
     * Creates a <code>TypedQuery</code> to do <code>SELECT * FROM T WHERE A = V ORDER BY O ASC/DESC</code>. or
     * <code>SELECT * FROM T WHERE A IN (V1, V2, ... Vn) ORDER BY O ASC/DESC</code> if more than one value is specified.
     *
     * @param <T>           the entity type
     * @param entityManager the entityManager
     * @param entity        the entity class
     * @param attribute     the column
     * @param orderBy       the attribute to order by
     * @param asc           true if order in ascending order.
     * @param values        the values that column can match,
     * @return the query to retrieve the matching records.
     */
    public static <A, T extends A, V> TypedQuery<T> findByFieldSorted(EntityManager entityManager, Class<T> entity,
            SingularAttribute<A, V> attribute, SingularAttribute<A, V> orderBy, boolean asc, Object... values) {
        Validate.notNull(values, "Values can not be null");
        Validate.notNull(entityManager, "entityManager can not be null");
        Validate.notNull(entity, "entity can not be null");

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entity);
        Root<T> from = criteriaQuery.from(entity);

        if (values.length > 1) {
            criteriaQuery.where(from.get(attribute).in(Arrays.asList(values)));
        } else if (values.length > 0) {
            criteriaQuery.where(criteriaBuilder.equal(from.get(attribute), values[0]));
        }

        CriteriaQuery<T> select = criteriaQuery.select(from);

        if (orderBy != null) {
            if (asc) {
                select.orderBy(criteriaBuilder.asc(from.get(orderBy)));
            } else {
                select.orderBy(criteriaBuilder.desc(from.get(orderBy)));
            }
        }

        TypedQuery<T> typedQuery = entityManager.createQuery(select);
        return typedQuery;
    }

    /**
     * Creates a <code>TypedQuery</code> to do <code>SELECT * FROM T WHERE A = V ORDER BY O ASC/DESC</code>.
     *
     * @param <A>
     * @param <T>           the entity type
     * @param entityManager the entityManager
     * @param entity        the entity class
     * @param attribute     - the columnd
     * @param value         - the value for that column
     * @param orderBy       - the attribute to order by
     * @param asc           - true if order in ascending order.
     * @return the query to retrieve the matching records.
     *
     * @deprecated use {@link #findByField(javax.persistence.EntityManager,
     * java.lang.Class, javax.persistence.metamodel.SingularAttribute,
     * javax.persistence.metamodel.SingularAttribute, boolean, java.lang.Object[])
     */
    @Deprecated
    public static <A, T extends A> TypedQuery<T> findByField(
            EntityManager entityManager, Class<T> entity, SingularAttribute<A, ?> attribute, Object value,
            SingularAttribute<A, ?> orderBy, boolean asc) {
        return findByField(entityManager, entity, orderBy, attribute, asc, value);
    }

    /**
     * Get a managed instance of the given entity.
     *
     * @param em       The entity manager. Can't be null.
     * @param entity   The entity. Can be null.
     * @param lockMode the LockModeType to be passed to <code>entityManager.find(Class, Object, LockModeType)</code>
     * @return The managed entity, or null if null passed in or not found.
     * @throws IntrospectionException
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     */
    public static Object getManaged(EntityManager em, Object entity, LockModeType lockMode)
            throws IntrospectionException, NoSuchFieldException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException {
        Validate.notNull(em);
        Object managed = entity;

        if (entity != null) {
            if (entity instanceof Collection) {
                managed = getManagedEntityCollection(em, (Collection)entity, lockMode);
            } else if (entity.getClass().isArray()) {
                managed = getManagedEntityArray(em, (Object[])entity, lockMode);
            } else {
                managed = getManagedEntity(em, entity, lockMode);
            }

        }

        return managed;
    }

    @SuppressWarnings("unchecked")
    private static Collection getManagedEntityCollection(EntityManager em, Collection col, LockModeType lockMode)
            throws IntrospectionException, NoSuchFieldException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException {
        if (col.isEmpty()) {
            return col;
        }

        Object[] arr = col.toArray();
        arr = getManagedEntityArray(em, arr, lockMode);

        col.clear();
        col.addAll(Arrays.asList(arr));
        return col;
    }

    private static Object[] getManagedEntityArray(EntityManager em, Object[] arr, LockModeType lockMode)
            throws IntrospectionException, NoSuchFieldException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException {
        Object[] ret = new Object[arr.length];

        for (int i = 0; i < arr.length; i++) {
            ret[i] = getManagedEntity(em, arr[i], lockMode);
        }

        return ret;
    }

    private static Object getManagedEntity(EntityManager em, Object entity, LockModeType lockMode)
            throws IntrospectionException, NoSuchFieldException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException {
        Object managed = entity;

        if (entity != null) {
            if (JPAAnnotationUtils.isEntity(entity.getClass())) {
                Object id = em.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity);

                if (id == null) {
                    throw new IllegalArgumentException(
                            "The ID for the given entity has not been set. Cannot convert to a managed entity: " + entity);
                }
                managed = em.find(entity.getClass(), id, lockMode);
            }
        }

        return managed;
    }

    /**
     * Copies an entitiy entities via copy groups.
     *
     * @param <T>    the entity type
     * @param em     the entity manager
     * @param cg     the copy group
     * @param object the object
     * @return the copied object
     */
    @SuppressWarnings("unchecked")
    public static <T> T copy(EntityManager em, CopyGroup cg, T object) {        
        if (em.isJoinedToTransaction()) {
            //log.debug("Flushing before copy");
            em.flush();
        } else {
            log.trace("Not flushing before copy");
        }
        return (T)em.unwrap(JpaEntityManager.class).copy(object, cg);
    }

    /**
     * Copies a list of entities via copy groups.
     *
     * @param <T>     the entity type
     * @param em      the entity manager
     * @param cg      the copy group
     * @param objects the objects
     * @return the copied objects
     */
    public static <T> List<T> copy(EntityManager em, CopyGroup cg, List<T> objects) {
        if (em.isJoinedToTransaction()) {
            log.debug("Flushing before copy");
            em.flush();
        } else {
            log.trace("Not flushing before copy");
        }
        
        return (List<T>)em.unwrap(JpaEntityManager.class).copy(objects, cg);
        //return copyOneByOne(em, cg, objects);
    }

    /**
     * Copies a list of entities via copy groups.
     *
     * @param <T>     the entity type
     * @param em      the entity manager
     * @param cg      the copy group
     * @param objects the objects
     * @return the copied objects
     */
    public static <T> List<T> copyOneByOne(EntityManager em, CopyGroup cg, List<T> objects) {
        List<T> ret = new ArrayList<>();
        for (T t : objects) {
            log.trace("Copying {}", t);
            T copy = copy(em, cg, t);
            log.trace("Copy = {}", copy);
            ret.add(copy);

        }
        return ret;
    }

    /**
     * Remove an entity by id.
     *
     * @param <T>         The entity type.
     * @param em          The entity manager.
     * @param entityClass The entity class.
     * @param primaryKey  The primary key.
     * @throws NullPointerException     If any arg is null.
     * @throws IllegalArgumentException If the entity cannot be found for the given primary key.
     */
    public static <T> void deleteById(@NonNull EntityManager em, @NonNull Class<T> entityClass,
            @NonNull Object primaryKey) {
        T entity = em.find(entityClass, primaryKey);
        Validate.isTrue(entity != null, "Could not find entity %s with primary key %d", entityClass.getName(),
                primaryKey);
        em.remove(entity);
    }
}
