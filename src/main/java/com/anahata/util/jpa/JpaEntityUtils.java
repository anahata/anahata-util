/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.jpa;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.metamodel.IdentifiableType;
import javax.persistence.metamodel.ManagedType;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * General JPA entity utilities.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JpaEntityUtils {
    /**
     * Return all managed classes in the entity manager in a map describing all subclasses for each one.
     * 
     * @param em The entity manager, required.
     * @return A map, keyed by class, with each value being a set of classes that can be in the subclass hierarchy of
     *         the class key.
     * @throws NullPointerException If em is null.
     */
    public static Map<Class, Set<Class>> getSubclasses(@NonNull EntityManager em) {
        Set<ManagedType<?>> types = em.getMetamodel().getManagedTypes();
        Map<Class, Set<Class>> classes = new HashMap<>();
        
        for (ManagedType<?> type : types) {
            Class cls = type.getJavaType();
            setClass(classes, cls, null);
            
            if (type instanceof IdentifiableType) {
                IdentifiableType itype = (IdentifiableType)type;
                IdentifiableType stype = itype.getSupertype();
                
                while (stype != null) {
                    Class scls = stype.getJavaType();
                    setClass(classes, scls, cls);
                    cls = scls;
                    stype = stype.getSupertype();
                }
            }
        }
        
        return classes;
    }
    
    private static Set<Class> setClass(Map<Class, Set<Class>> classes, Class cls, Class subCls) {
        Set<Class> subs = classes.get(cls);

        if (subs == null) {
            subs = new HashSet<>();
            classes.put(cls, subs);
        }
        
        if (subCls != null) {
            subs.add(subCls);
            Set<Class> subs2 = classes.get(subCls);
            
            if (subs2 != null) {
                subs.addAll(subs2);
            }
        }
        
        return subs;
    }
}
