/*
 *  Copyright © - 2013 Thermal Building Inspections. All rights reserved.
 */
package com.anahata.util.jpa;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import lombok.Getter;
import org.apache.commons.lang3.Validate;

/**
 * Base class for JPA operations.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public abstract class JpaBase {
    @Getter
    private final EntityManager em;
    
    private final EntityTransaction tx;
    
    protected JpaBase(EntityManager em) {
        Validate.notNull(em);
        this.em = em;
        tx = em.getTransaction();
    }
    
    protected final void begin() {
        tx.begin();
    }

    protected final void commit() {
        tx.commit();
    }

    protected final void rollback() {
        tx.rollback();
    }

    protected final void persist(Object obj) {
        em.persist(obj);
    }

    protected final <T> T merge(T t) {
        return em.merge(t);
    }

    protected final void flush() {
        em.flush();
    }

    protected final <T> T find(Class<T> entityClass, Object primaryKey) {
        return em.find(entityClass, primaryKey);
    }

    protected final <T> T find(Class<T> entityClass, Enum primaryKey) {
        return em.find(entityClass, primaryKey.name());
    }

    /**
     * Run a query returning a single result.
     *
     * @param <T>         Result type.
     * @param query       Query.
     * @param resultClass Result class.
     * @return Result.
     */
    protected final <T> T querySingle(String query, Class<T> resultClass) {
        return em.createQuery(query, resultClass).getSingleResult();
    }

    /**
     * Query multiple results.
     *
     * @param <T>         Result type.
     * @param query       Query.
     * @param resultClass Result class.
     * @return Results.
     */
    protected final <T> List<T> query(String query, Class<T> resultClass) {
        return em.createQuery(query, resultClass).getResultList();
    }

    /**
     * Run an update.
     *
     * @param query The update query.
     * @return The number of rows updated.
     */
    protected final int update(String query) {
        return em.createQuery(query).executeUpdate();
    }

    protected final int truncate(Class entity) {
        return em.createQuery("delete from " + entity.getSimpleName()).executeUpdate();
    }
}
