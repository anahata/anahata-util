package com.anahata.util.jpa;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 * An enum that can be converted to a specific type of value via a separate field.
 * 
 * @param <T> Type to convert to from the enum.
 * @param <E> Enum to convert to.
 * @author Robert Nagajek
 */
public interface ConvertableEnum<T, E extends Enum> {
    /**
     * Given a raw value, convert to an enum.
     * 
     * @param value The raw value.
     * @return The enum.
     * @throws IllegalArgumentException If the enum can't be found for the value.
     */
    public E toEnum(T value);
    
    /**
     * Convert an enum value to a raw value.
     * 
     * @return The raw value.
     */
    public T toValue();
}
