package com.anahata.util.jpa;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.Validate;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.converters.Converter;
import org.eclipse.persistence.sessions.Session;

/**
 * Map enums with a custom field.
 * 
 * @author Robert Nagajek
 */
public abstract class EnumConverter implements Converter {
    @Override
    public Object convertObjectValueToDataValue(Object objectValue, Session session) {
        if (objectValue == null) {
            return null;
        }
        
        Validate.isInstanceOf(ConvertableEnum.class, objectValue);
        ConvertableEnum enumValue = (ConvertableEnum)objectValue;
        return enumValue.toValue();
    }

    @Override
    public Object convertDataValueToObjectValue(Object dataValue, Session session) {
        if (dataValue == null) {
            return null;
        }
        
        @SuppressWarnings("unchecked")
        Object enumValue = getConvertableEnum().toEnum(dataValue);
        return enumValue;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public void initialize(DatabaseMapping mapping, Session session) {
    }
    
    /**
     * Implementor must provide the convertable enum. Return any instance.
     * 
     * @return The instance. Can't be null.
     */
    protected abstract ConvertableEnum getConvertableEnum();
}
