package com.anahata.util.ejb;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.jpa.JPAUtils;
import java.lang.annotation.Annotation;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * EJB Interceptor that converts method parameters to managed JPA entities.
 * <b>Warning:</b> This clases uses the default entity manager assuming only one persistence unit is involved.
 * 
 * //TODO: add Logging
 * //TODO: collections of entities
 * 
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
@Interceptor
@ManagedParams
public abstract class AbstractManagedParamsInterceptor {
    /**
     * Not using static to preserve the logger name as the actual subclass name.
     */
    private final Logger log = LoggerFactory.getLogger(getClass());

    @AroundInvoke
    public final Object intercept(InvocationContext ic) throws Exception {
        return intercept(getEntityManager(), ic);
    }

    public final Object intercept(EntityManager em, InvocationContext ic) throws Exception {
        
        log.trace("Considering " + ic.getMethod() + " ");
        
        if (ic.getParameters() == null || ic.getParameters().length == 0) {
            return ic.proceed();
        }

        ManagedParams methodLevelAnnotation = ic.getMethod().getAnnotation(ManagedParams.class);
        boolean manageAll = methodLevelAnnotation != null;
        Annotation[][] paramsAnnotations = ic.getMethod().getParameterAnnotations();
        Object[] params = ic.getParameters();
        Object[] managedParams = new Object[ic.getParameters().length];

        for (int i = 0; i < params.length; i++) {
            Object o = params[i];

            if (o == null) {
                continue;
            }

            boolean attempt = false;
            LockModeType lockMode = LockModeType.NONE;


            Annotation[] paramAnnotations = paramsAnnotations[i];
            lockMode = methodLevelAnnotation != null ? methodLevelAnnotation.lockMode() : LockModeType.NONE;
            ManagedParam paramLevelAnnotation = null;

            for (Annotation a : paramAnnotations) {
                if (a instanceof ManagedParam) {
                    paramLevelAnnotation = (ManagedParam) a;
                    //we override lock mode
                    lockMode = paramLevelAnnotation.lockMode();
                }
            }

            attempt = manageAll || ArrayUtils.contains(paramAnnotations, ManagedParam.class);

            if (attempt) {
                log.trace("Converting to managed: {} with lock mode= {}", o, lockMode);

                managedParams[i] = JPAUtils.getManaged(em, o, lockMode);
            } else {
                log.trace("Will not convert to managed: {}", o);
                managedParams[i] = o;
            }
        }

        ic.setParameters(managedParams);
        return ic.proceed();
    }

    /**
     * Subclasses need only to implement this.
     * 
     * @return the entityManager.
     */
    protected abstract EntityManager getEntityManager();

}
