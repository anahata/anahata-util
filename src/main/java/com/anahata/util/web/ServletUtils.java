/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.web;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Enumeration;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Utility methods for the Servlet api.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ServletUtils {
    /**
     * Converts a request's headers to a string.
     *
     * @param request the request. Must be an instance of HttpServletRequest.
     * @return the headers in string format
     */
    public static String headersToString(ServletRequest request) {
        return headersToString((HttpServletRequest)request);
    }

    /**
     * Converts a request's headers to a string.
     *
     * @param request the request
     * @return the headers in string format
     */
    public static String headersToString(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();

            Enumeration<String> values = request.getHeaders(headerName);
            while (values.hasMoreElements()) {
                sb.append(headerName).append(" : ").append(values.nextElement()).append("\n");
            }
        }
        return sb.toString();
    }
}
