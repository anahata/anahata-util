/*
 * Copyright 2016 Anahata.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.util.excel;

/**
 *
 * @author sai.dandem
 */
public final class ExcelColumn {

    private ExcelColumn() {}

    public static int toNumber(String name) {
        int number = 0;
        for (int i = 0; i < name.length(); i++) {
            number = number * 26 + (name.charAt(i) - ('A' - 1));
        }
        return number;
    }

    public static String toName(int number) {
        StringBuilder sb = new StringBuilder();
        while (number-- > 0) {
            sb.append((char)('A' + (number % 26)));
            number /= 26;
        }
        return sb.reverse().toString();
    }
    
    public static void main(String[] args) {
         System.out.println(toNumber("BA"));
         System.out.println(toName(53));

         System.out.println(toNumber("AAA"));
         System.out.println(toName(703));

         System.out.println(toNumber("Z"));
         System.out.println(toName(26));

         System.out.println(toNumber("ZZ"));
         System.out.println(toName(702));

    }
}
