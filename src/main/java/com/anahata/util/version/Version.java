package com.anahata.util.version;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Utility methods to display application version. This class expects a file called 
 * <code>anahata-version.properties</code> to be present in the class path.
 * 
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Deprecated
public final class Version {
    /**
     * The name of the properties file with version info, value is <code>anahata-version.properties</code>.
     */
    private static final String ANAHATA_VERSION_PROPERTIES_FILE_NAME = "anahata-version.properties";
    
    /**
     * <code>hudson.build.id</code>
     */
    private static final String BUILD_ID = "hudson.build.id";
    
    /**
     * <code>hudson.build.tag</code>
     */
    private static final String BUILD_TAG = "hudson.build.tag";
    
    /**
     * The content of file <code>anahata-version.properties</code>.
     */
    private static final Properties PROPERTIES = new Properties();

    static {
        InputStream is = null;
        try {
            URL u2 = Version.class.getResource("/" + ANAHATA_VERSION_PROPERTIES_FILE_NAME);            
            System.out.println("anahata-version.properties at: " + u2);
            PROPERTIES.load(u2.openStream());
            System.out.println("Properties are: " + PROPERTIES);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            //throw new RuntimeException(e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                    //throw new RuntimeException(e);
                }
            }
        }
    }

    /**
     * @return all version related properties
     */
    public static Properties getProperties() {

        Properties copy = new Properties();
        copy.putAll(PROPERTIES);
        return copy;
    }
    /**
     * Retrieves the value for property {@link #BUILD_ID}
     * @return 
     */
    public static String getBuildID() {
        return PROPERTIES.getProperty(BUILD_ID);
    }
    
    /**
     * Retrieves the value for property {@link #BUILD_tag}
     * @return 
     */
    public static String getBuildTag() {
        return PROPERTIES.getProperty(BUILD_TAG);
    }
}
