package com.anahata.util.xml;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class XMLUtils {
    /**
     * Formats a node into a string.
     *
     * @param node the node
     * @return the string
     */
    public static String format(Node node) {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            StreamResult result = new StreamResult(new StringWriter());
            DOMSource source = new DOMSource(node);
            transformer.transform(source, result);

            String xmlString = result.getWriter().toString();
            //System.out.println(xmlString);
            return xmlString;
        } catch (TransformerException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static Document DOMParseXML(InputStream is) throws SAXException, IOException, ParserConfigurationException,
            FactoryConfigurationError {
        return DOMParseXML(is, false);
    }

    public static Document DOMParseXML(InputStream is, boolean namespaceAware) throws SAXException, IOException,
            ParserConfigurationException, FactoryConfigurationError {
        return getDocumentBuilder(namespaceAware).parse(is);
    }

    private static DocumentBuilder getDocumentBuilder(boolean namespaceAware) throws ParserConfigurationException {
        DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
        f.setNamespaceAware(namespaceAware);
        return f.newDocumentBuilder();
    }

    public static String getNodeText(Node root, String path) {
        String s = "";

        Node n = getNode(root, path);

        if (n != null) {
            s = getTextContent(n);
        }

        return s;
    }

    public static String getTextContent(Node node) {
        String s = null;

        if (node.getNodeType() == Node.TEXT_NODE) {
            s += node.getNodeValue();
        }

        NodeList list = node.getChildNodes();

        for (int i = 0; i < list.getLength(); i++) {
            Node n = list.item(i);

            if (n.getNodeType() == Node.TEXT_NODE) {
                if (s == null || s.length() == 0) {
                    s = n.getNodeValue();
                } else {
                    s += " " + n.getNodeValue();
                }
            }
        }

        return s;
    }

    public static Node getNode(Node root, String path) {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }

        String[] nodeNames = path.split("/");

        Node start = root;
        Node node = null;

        for (int i = 0; i < nodeNames.length; i++) {
            node = findNode(start, nodeNames[i]);

            if (node == null) {
                return null;
            }

            start = node;
        }

        return node;
    }

    private static Node findNode(Node root, String name) {
        if (name.equals(root.getNodeName())) {
            return root;
        }

        NodeList list = root.getChildNodes();

        for (int i = 0; i < list.getLength(); i++) {
            Node n = list.item(i);

            if (name.equals(n.getNodeName())) {
                return n;
            }
        }

        return null;
    }
}
