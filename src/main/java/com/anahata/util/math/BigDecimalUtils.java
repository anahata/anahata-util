/*
 * Copyright 2015 Anahata.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.util.math;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.function.Function;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;
import static com.anahata.util.lang.Nvl.*;

/**
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BigDecimalUtils {

    public static final BigDecimal TEN = new BigDecimal(10);

    public static final BigDecimal HUNDRED = new BigDecimal(100);

    public static final BigDecimal THOUSAND = new BigDecimal(1000);

    /**
     * Sums the value returned by a given method for a list of objects.
     *
     * Usefull for example to get the total of a List of objects that have a method or a property that returns a
     * BigDecimal.
     * For example Job has a method called <code>public BigDecimal getTotal()</code>, with this method one can do
     * sum(Job::getTotal,
     * jobs).
     *
     * @param <T>
     * @param vh
     * @param bds
     * @return
     */
    public static <T> BigDecimal sum(Function<T, BigDecimal> vh, Iterable<T> bds) {
        Validate.notNull(bds);

        BigDecimal ret = BigDecimal.ZERO;

        for (T t : bds) {
            ret = ret.add(nvl(vh.apply(t)));
        }

        return ret;
    }

    /**
     * Sums up all big decimals, uses BigDecimal.ZERO for null values (nvl)
     *
     * @param bds The BigDecimal instances.
     * @return Sum of all BigDecimal values.
     * @throws NullPointerException if array is null.
     */
    public static BigDecimal sum(BigDecimal... bds) {
        Validate.notNull(bds);

        BigDecimal ret = BigDecimal.ZERO;

        for (BigDecimal bd : bds) {
            ret = ret.add(nvl(bd));
        }

        return ret;
    }

    public static boolean gt(BigDecimal d1, BigDecimal d2) {
        return nvl(d1).compareTo(nvl(d2)) > 0;
    }

    public static boolean ge(BigDecimal d1, BigDecimal d2) {
        return nvl(d1).compareTo(nvl(d2)) >= 0;
    }

    public static boolean lt(BigDecimal d1, BigDecimal d2) {
        return nvl(d1).compareTo(nvl(d2)) < 0;
    }

    public static boolean le(BigDecimal d1, BigDecimal d2) {
        return nvl(d1).compareTo(nvl(d2)) <= 0;
    }

    public static boolean eq(BigDecimal d1, BigDecimal d2) {
        return nvl(d1).compareTo(nvl(d2)) == 0;
    }

    public static String formatCurrency(BigDecimal bd) {
        if (bd == null) {
            return "";
        }

        return NumberFormat.getCurrencyInstance().format(bd);
    }

    public static String formatCurrencyShort(BigDecimal bd) {
        if (bd == null) {
            return "";
        }
        String val = NumberFormat.getCurrencyInstance().format(bd);
        if (val.endsWith(".00")) {
            val = val.substring(0, val.length() - 3);
        }
        return val;
    }
}
