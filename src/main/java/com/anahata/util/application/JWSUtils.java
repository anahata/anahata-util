package com.anahata.util.application;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.config.internal.AnahataUtilConfig;
import com.anahata.util.plaf.OSUtils;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.jnlp.BasicService;
import javax.jnlp.ServiceManager;
import javax.jnlp.UnavailableServiceException;
import lombok.extern.slf4j.Slf4j;

/**
 * Utility methods for Java Web Start environments.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@ApplicationScoped
@Slf4j
public class JWSUtils {
    private static final String JNLP_SYS_PROP_PREFFIX = "jnlp.";

    @Inject
    private AnahataUtilConfig config;

    /**
     * Fetches a system property, depending on whether the application is running on a jnlp sandbox or not it will
     * preffix the key with "jnlp."
     *
     * @param key the system property name (without the .jnlp)
     * @return the system prop value.
     */
    public static String getSystemProperty(String key) {
        key = isJavaWebStart() ? JNLP_SYS_PROP_PREFFIX + key : key;
        return System.getProperty(key);
    }

    /**
     * Calculates the http, protocol and port portions of the URL of an application running on a Java Web Start
     * container or returns {@link #DEFAULT_STANDALONE_BASE_URL} if running outside of a JWS container (i.e. on a
     * standalone java process.)
     *
     * @return The base protocol, host and url components of the base url in format: protocol://host:port (without the
     *         trailing forward slash)
     * @throws UnavailableServiceException
     */
    public String getApplicationURL() {
        return getApplicationURL(config.getNonJwsApplicationUrl());
    }

    /**
     * Calculates the host where the application is running.
     *
     * @return the host component of the application url.
     *
     */
    public String getApplicationHost() {
        String appURL = getApplicationURL();
        URL url;
        try {
            url = new URL(appURL);
            return url.getHost();
        } catch (MalformedURLException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Calculates the host where the application is running.
     *
     * @return the host component of the application url.
     *
     */
    public int getApplicationPort() {
        String appURL = getApplicationURL();
        URL url;
        try {
            url = new URL(appURL);
            return url.getPort();
        } catch (MalformedURLException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Calculates the host where the application is running.
     *
     * @return the host component of the application url.
     *
     */
    public String getApplicationProtocol() {
        String appURL = getApplicationURL();
        URL url;
        try {
            url = new URL(appURL);
            return url.getProtocol();
        } catch (MalformedURLException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Returns up to the the first /yadayada/ after the host and port.
     *
     * @param nonJwsApplicationURL
     * @return the base application URL e.g. http://localhost:8080/jobtracking-web
     */
    public String getApplicationURL(String nonJwsApplicationURL) {
        if (isJavaWebStart()) {
            try {
                BasicService bs = (BasicService)ServiceManager.lookup(BasicService.class.getName());
                String codeBase = bs.getCodeBase().toString();
                String url = codeBase.substring(0, codeBase.indexOf("/webstart"));
                return url;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            log.debug("Running in non JNLP environment");
            return nonJwsApplicationURL;
        }
    }

    /**
     * The web appliction context path: e.g magefrigor-web
     *
     * @param nonJwsApplicationURL
     * @return the base application URL e.g. http://localhost:8080/magefrigor-web
     */
    public String getApplicationContextPath() {
        String base = getApplicationURL();
        String[] comps = base.split("/");
        return comps[comps.length - 1];
    }

    /**
     * Checks is the JVM is running on a Java Web Start environment.
     *
     * @return true if in JWS environment.
     */
    public static boolean isJavaWebStart() {
        boolean jnlpAware = ServiceManager.getServiceNames() != null;
        return jnlpAware;
    }
    
    /**
     * Checks is the JVM was launched via Mapacho.
     *
     * @return true if in JWS environment.
     */
    public static boolean isMapacho() {
        return System.getProperty("mapacho.descriptor") != null;
    }
    
    /**
     * Checks if there is support to restart the application either via mapacho or javawebstart.
     * 
     * @return true if jvm was launched via jws or via mapacho.
     */
    public static boolean isRelaunchSupported() {
        return isJavaWebStart() || isMapacho();
    }
    
    /**
     * Executes a javaws OS process to relaunch the current webstart application assuming the jnlp is called
     * launch.jnlp
     * 
     * @throws UnavailableServiceException
     * @throws IOException 
     */
    public static void relaunchApplication() throws UnavailableServiceException, IOException {
        relaunchApplication("launch.jnlp");
    }

    /**
     * Executes a javaws OS process to relaunch the current webstart application.
     *
     * @param jnlpFileName
     * @throws UnavailableServiceException
     * @throws IOException
     */
    public static void relaunchApplication(String jnlpFileName) throws UnavailableServiceException, IOException {

        String codebase = System.getProperty("mapacho.codebase");
        String javaHome = System.getProperty("mapacho.java.home");
        
        if (codebase == null) {
            BasicService bs = (BasicService)ServiceManager.lookup(BasicService.class.getName());
            codebase = bs.getCodeBase().toString();            
            javaHome = System.getProperty("java.home"); 
        } 
        
        codebase = codebase + jnlpFileName;
        
        String executable = "javaws";
        if (OSUtils.isWindows()) {
            executable += ".exe";
        }

        File f = new File(javaHome);
        f = new File(f, "bin");
        f = new File(f, executable);
        
        log.info("Relaunching Application, url={} codeBase={}", codebase);
        
        //Doesn't seem to be a way to get the jnlp file name,
        //http://stackoverflow.com/questions/3721070/how-to-get-jnlp-href-attribute-at-run-time-in-java-web-start-app-similar-to-bas
        //hardcoding it for now.
        ProcessBuilder pb = new ProcessBuilder(f.getAbsolutePath(), codebase);
        pb.start();
        
    }
}
