package com.anahata.util.mime;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;

/**
 * Utility class to work with mime types. Uses Apache Tika.
 *
 * @author pablo
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MimeUtils {

    public static final Set<String> COMPRESSED_MIME_TYPES = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(
            new String[]{
                "image/jpeg",
                "image/jpg",
                "image/gif",
                "image/png",
                "application/x-compress",
                "application/x-compressed",
                "application/x-zip-compressed",
                "application/zip",
                "multipart/x-zip",
                "application/x-rar-compressed",
                "application/x-bzip",
                "application/x-bzip2",})));

    /**
     * Returns the mime type of a file.
     *
     * @param file the filw whose input type we want to detect. Required.
     * @return the mime type
     * @throws IOException
     */
    public static String getMimeType(java.io.File file) throws IOException {
        Validate.notNull(file, "File is required");
        Detector dd = new DefaultDetector();
        Metadata m = new Metadata();
        m.set(TikaCoreProperties.RESOURCE_NAME_KEY, file.getName());
        try (TikaInputStream is = TikaInputStream.get(file)) {
            return dd.detect(is, m).toString();
        }
    }

    /**
     * Returns the mime type of a byte array.
     *
     * @param data     the file data.
     * @param fileName the file name.
     * @return the mime type
     * @throws IOException
     */
    public static String getMimeType(byte[] data, String fileName) throws IOException {
        Validate.notNull(data, "data is required");
        Validate.notNull(fileName, "File name is required");
        Detector dd = new DefaultDetector();
        Metadata m = new Metadata();
        m.set(TikaCoreProperties.RESOURCE_NAME_KEY, fileName);
        try (TikaInputStream is = TikaInputStream.get(new ByteArrayInputStream(data))) {
            return dd.detect(is, m).toString();
        }
    }

    /**
     *
     * @param mimeType
     * @return
     */
    public static boolean isCompressed(String mimeType) {
        return COMPRESSED_MIME_TYPES.contains(mimeType.toLowerCase());
    }
    
    public static boolean isCompressed(java.io.File file) throws IOException {
        return isCompressed(getMimeType(file));
    }
}
