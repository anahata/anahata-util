package com.anahata.util.populate;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.lang.reflect.Field;
import java.util.Date;
import java.util.Random;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Methods to populate an object's fields with random values.
 * 
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PopulateUtils {

    /**
     * Instantiates an object and sets all fields with mock values. If the field is a string, it will append a suffix.
     * 
     * @param <T> the type
     * @param c the class of the object to be instantiated
     * @param suffix all string fields will be appended this suffix if a String, a suffix (optional)
     * @return an instance of <code>c</code> with all fields populated with mock values.
     * @throws Exception -
     */
    public static <T> T newInstanceMockFields(Class<T> c, String suffix) throws Exception {
        
        T ret = c.newInstance();
        
        setFieldsWithMockValues(ret, suffix);
        
        return ret;
    }
    
    /**
     * Sets all fields with mock values. If the field is a string, it will generate
     * a random string with this format <i>fieldName-randomValue-suffix</i>.
     * 
     * @param o the object whose fields we want to mock.
     * @param suffix all string fields will be appended this suffix (optional)
     * @throws Exception 
     */
    public static void setFieldsWithMockValues(Object o, String suffix) throws Exception {
        for (Field f : o.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            f.set(o, getRandomValue(f.getType(), f.getName(), suffix));
        }
    }

    /**
     * Makes a random value of the given type. Supported types are: any instance of <code>Number</code>,
     * <code>String</code> and <code>java.util.Date</code>.
     * 
     * @param <T> the type
     * @param c the type
     * @param preffix if a String, a preffix (optional)
     * @param suffix if a String, a suffix (optional)
     * @return the random 
     * @throws Exception 
     */
    public static <T> T getRandomValue(Class<T> c, String preffix, String suffix) throws Exception {

        preffix = preffix == null ? "" : preffix + "-";
        suffix = suffix == null ? "" : suffix + "-";

        if (Number.class.isAssignableFrom(c)) {
            int value = new Random().nextInt(Byte.MAX_VALUE);
            return c.getConstructor(String.class).newInstance(String.valueOf(value));
        } else if (String.class.isAssignableFrom(c)) {
            return (T) (preffix + System.currentTimeMillis() + suffix);
        } else if (Date.class.isAssignableFrom(c)) {
            return (T) new Date();
        } else if (Boolean.class.isAssignableFrom(c)) {
            return (T) (Math.random() > 0.5 ? Boolean.TRUE : Boolean.FALSE);
        } else {
            return null;
        }

    }

}
