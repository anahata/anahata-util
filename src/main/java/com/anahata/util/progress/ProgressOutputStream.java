/*
 * Copyright 2015 Anahata.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.util.progress;

/**
 *
 * @author pablo
 */
import java.io.IOException;
import java.io.OutputStream;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProgressOutputStream extends OutputStream {

    private final OutputStream outstream;

    private long bytesWritten = 0;
    
    private double lastProgress = 0;

    private long totalLength;

    private final ProgressListener progressListener;

    public ProgressOutputStream(OutputStream outstream, long totalLength, ProgressListener pl) {
        this.outstream = outstream;
        this.progressListener = pl;
        this.totalLength = totalLength;
    }

    @Override
    public void write(int b) throws IOException {
        outstream.write(b);
        bytesWritten++;
        updateProgress();
    }

    @Override
    public void write(byte[] b) throws IOException {
        outstream.write(b);
        bytesWritten += b.length;
        updateProgress();
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        outstream.write(b, off, len);
        bytesWritten += len;
        updateProgress();
    }

    @Override
    public void flush() throws IOException {
        outstream.flush();
    }

    @Override
    public void close() throws IOException {
        outstream.close();
    }

    private void updateProgress() {
        double progress = (double)(bytesWritten + 1) / (totalLength + 1);
        double diff = progress - lastProgress;
        if (diff > 0.05) {            
            log.trace("progress={}", progress);
            progressListener.progress(progress);
            lastProgress = progress;
        } else {
            log.trace("ignoring progress={}", progress);
        }
        
    }
}
