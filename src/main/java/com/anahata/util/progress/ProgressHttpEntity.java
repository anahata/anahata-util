/*
 * Copyright 2015 Anahata.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.util.progress;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.AllArgsConstructor;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
/**
 *
 * @author pablo
 */
@AllArgsConstructor
public class ProgressHttpEntity implements HttpEntity{
    private final HttpEntity delegate;
    private final ProgressListener pl;

    @Override
    public boolean isRepeatable() {
        return delegate.isRepeatable();
    }

    @Override
    public boolean isChunked() {
        return delegate.isChunked();
    }

    @Override
    public long getContentLength() {
        return delegate.getContentLength();
    }

    @Override
    public Header getContentType() {
        return delegate.getContentType();
    }

    @Override
    public Header getContentEncoding() {
        return delegate.getContentEncoding();
    }

    @Override
    public InputStream getContent() throws IOException, IllegalStateException {
        return delegate.getContent();
    }

    @Override
    public void writeTo(OutputStream outstream) throws IOException {
        delegate.writeTo(new ProgressOutputStream(outstream, (long) getContentLength(), pl));
    }

    @Override
    public boolean isStreaming() {
        return delegate.isStreaming();
    }

    @Override
    public void consumeContent() throws IOException {
        delegate.consumeContent();
    }

    
    
}
