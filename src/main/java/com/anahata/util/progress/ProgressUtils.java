package com.anahata.util.progress;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.concurrent.TimeUnit;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Aids in the calculation of estimated time remaining.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ProgressUtils {

    /**
     * Calculates the estimated time remaining.
     *
     * @param startTime - the start time
     * @param processed the number of processed items
     * @param total     - the total number of items
     * @return the estimated remaining time
     */
    public static long getEstimatedDuration(long startTime, long processed, long total) {
        float ellapsed = System.currentTimeMillis() - startTime;
        float timePerFile = ellapsed / processed;
        float remainingFiles = total - processed;
        Float fl = new Float(remainingFiles * timePerFile);
        long remainingTime = fl.longValue();
        return remainingTime;
    }

    /**
     * Calculates the estimated time remaining as a hh:mm:ss String.
     *
     * @param startTime - the start time
     * @param processed the number of processed items
     * @param total     - the total number of items
     * @return the estimated remaining time
     */
    public static String getEstimatedDurationDisplay(long startTime, long processed, long total) {
        return durationToHhMmSs(getEstimatedDuration(startTime, processed, total));
    }

    /**
     * Converts a duration in milliseconds to hours, minutes and seconds.
     *
     * @param duration - the duration
     * @return the duration in hh:mm:ss format
     */
    public static String durationToHhMmSs(long duration) {
        String time = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(duration),
                TimeUnit.MILLISECONDS.toMinutes(duration)
                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duration)),
                TimeUnit.MILLISECONDS.toSeconds(duration)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
        return time;
    }
}
