package com.anahata.util.model;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.iterators.FilterIterator;
import org.apache.commons.lang3.Validate;

/**
 * A predicate used to filter over Activatable collections. Provides iterating methods as well.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ActivePredicate implements Predicate {
    public static ActivePredicate INSTANCE = new ActivePredicate();

    @Override
    public boolean evaluate(Object object) {
        boolean satisfies = false;

        if (object instanceof Activatable) {
            Activatable activatable = (Activatable) object;
            satisfies = activatable.isActive();
        }

        return satisfies;
    }

    /**
     * Provide an iterator over a collection that only returns active entries.
     *
     * @param <T> The type, must implement Activatable.
     * @param coll The collection, can be null.
     * @return The iterator, never null.
     */
    public static <T extends Activatable> Iterator<T> iterator(Collection<T> coll) {
        if (coll == null) {
            return Collections.emptyIterator();
        }

        return new FilterIterator(coll.iterator(), ActivePredicate.INSTANCE);
    }

    /**
     * Get a single active item contained within an Activatable collection. If there is more than one item, an exception
     * is thrown.
     *
     * @param <T> The collection type, must implement Activatable for the elements.
     * @param coll The collection. This can be null.
     * @return The item. If not found, null is returned.
     * @throws IllegalArgumentException If more than one item is found.
     */
    public static <T extends Activatable> T singleItem(Collection<T> coll) {
        Iterator<T> it = iterator(coll);

        if (!it.hasNext()) {
            return null;
        }

        T result = it.next();
        Validate.isTrue(!it.hasNext(), "The collection had more than one element: %s", coll);
        return result;
    }

    /**
     * Provide a list of Activatable entries that are only active, from the given source list.
     *
     * @param <T> The type, must implement Activatable.
     * @param coll The collection, can be null.
     * @return The list, never null.
     */
    public static <T extends Activatable> List<T> list(Collection<T> coll) {
        List<T> result = new ArrayList<>();
        Iterator<T> it = iterator(coll);

        while (it.hasNext()) {
            result.add(it.next());
        }

        return result;
    }

    /**
     * Removes inactive entries from a given the collection.
     *
     * @param <T> The type, must implement Activatable.
     * @param coll The removed entities
     * @return The list, never null.
     */
    public static <T extends Activatable> List<T> removeInactive(Collection<T> coll) {
        List<T> result = new ArrayList<>();
        Iterator<T> it = iterator(coll);
        while (it.hasNext()) {
            T t = it.next();
            if (!t.isActive()) {
                it.remove();
                result.add(t);
            }
        }
        return result;
    }
}
