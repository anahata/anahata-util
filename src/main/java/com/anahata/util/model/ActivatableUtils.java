package com.anahata.util.model;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Utilities for Activatable classes.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ActivatableUtils {
    /**
     * Determine if the given object is active. This is true if the object is null, implements Activatable and
     * isActive() == true, or doesn't implement Activatable.
     *
     * @param object The object to check. Can be null.
     * @return true if active, false if not.
     */
    public static boolean isActive(Object object) {
        if (object == null) {
            return true;
        }

        if (object instanceof Activatable) {
            Activatable activatable = (Activatable)object;

            if (!activatable.isActive()) {
                return false;
            }
        }

        return true;
    }
    
    /**
     * Check if a given object is an Activatable and inactive.
     *
     * @param object The object to check. Can be null.
     * @return true if inactive, false if null or active.
     */
    public static boolean isInactive(Object object) {
        if (object == null) {
            return false;
        }

        if (object instanceof Activatable) {
            Activatable activatable = (Activatable)object;

            if (!activatable.isActive()) {
                return true;
            }
        }

        return false;
    }
}
