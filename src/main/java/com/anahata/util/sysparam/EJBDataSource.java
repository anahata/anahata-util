package com.anahata.util.sysparam;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;
import java.util.Properties;
import javax.naming.InitialContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Robert Nagajek
 */
public final class EJBDataSource implements SystemParameterDataSource {
    private static final Logger log = LoggerFactory.getLogger(EJBDataSource.class);
    
    /** System parameter to set the name of the EJB to look up from JNDI. */
    public static final String SYSPARAM_EJB_JNDINAME = "com.anahata.util.sysparam.ejb.jndiname";
    
    @Override
    public Properties load() {
        Properties props = new Properties();

        try {
            final String jndiName = System.getProperty(SYSPARAM_EJB_JNDINAME);
            
            if (jndiName == null) {
                RuntimeException e = new RuntimeException("No system property defined for the EJB JNDI NAME - " + SYSPARAM_EJB_JNDINAME);
                log.error("No EJB JNDI system property found", e);
                throw e;
            }
            
            InitialContext ic = new InitialContext();
            SystemParameterService paramEjb = (SystemParameterService)ic.lookup(jndiName);
            List<SystemParameterValue> params = paramEjb.getAllSystemParameters();

            for (SystemParameterValue param : params) {
                props.put(param.getSystemParameterKey(), param.getSystemParameterValue());
            }
        } catch (Exception e) {
            log.error("Error loading database properites through the EJB", e);
            throw new RuntimeException(e);
        }

        return props;
    }
}
