package com.anahata.util.sysparam;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Reloadable system parameters. This class will reload system parameters in a given time period defined by property
 * <code>sysparam.expiry</code>, which has a value containing the number of seconds between refreshes. If the value
 * is set to 0 (or doesn't exist), reloading is disabled.
 * <br>
 * The usage of this class is to always call SystemParameter.getXXX() and never to cache the returned values as
 * attributes of a class, so that updated values can always be obtained.
 *
 * @author Robert Nagajek
 */
public final class SystemParameter {
    private static final Logger log = LoggerFactory.getLogger(SystemParameter.class);

    /** System parameter to set the implementation class for reading system parameters. */
    public static final String SYSPARAM_IMPL_CLASS_NAME = "com.anahata.util.sysparam.classname";
    
    /** The time in seconds it takes for system parameters to expire and get reloaded. */
    public static final String PARAM_EXPIRY = "sysparam.expiry";
    
    /** Default expiry. 0 means that there is no reload. */
    private static final long DEFAULT_EXPIRY = 0L;

    /** The data source providing system parameters. Can't change once set. */
    private static SystemParameterDataSource dataSource;

    private static Properties props;

    private static long lastRefresh = 0L;

    private static long expiry = DEFAULT_EXPIRY;

    private static synchronized void init() {
        
        String dsClassName = System.getProperty(SYSPARAM_IMPL_CLASS_NAME);

        if (dsClassName == null) {
            // Default to the file system implementation.

            log.trace("init: No property for the class name, defaulting to EJBDataSource");
            dataSource = new EJBDataSource();
        } else {
            log.trace("init: Instantiating class {}", dsClassName);
            try {
                Class c = Class.forName(dsClassName);
                dataSource = (SystemParameterDataSource)c.newInstance();
            } catch (Exception e) {
                log.error("init: Could not instantiate class {}", dsClassName, e);
                throw new RuntimeException(e);
            }
        }

        props = dataSource.load();
        expiry = getExpiry();
        log.info("init: System parameters expiry time set to {} seconds", expiry);
        lastRefresh = System.currentTimeMillis();
    }

    private SystemParameter() {
    }

    /**
     * Get a String system property.
     *
     * @param key The key of the property.
     * @return The property value. It will be null if not found.
     */
    public static String getString(String key) {
        return getProperties().getProperty(key);
    }

    /**
     * Get a String system property with a default value
     *
     * @param key The key of the property.
     * @param defaultValue The default value if the key is not found.
     * @return The property value. It will be the default value if not found.
     */
    public static String getString(String key, String defaultValue) {
        return getProperties().getProperty(key, defaultValue);
    }

    /**
     * Get a Boolean system property.
     *
     * @param key The key of the property.
     * @return The property value. It will be null if not found.
     */
    public static Boolean getBoolean(String key) {
        String value = getProperties().getProperty(key);
        return value == null ? null : Boolean.valueOf(value);
    }

    /**
     * Get a Boolean system property with a default value.
     *
     * @param key The key of the property.
     * @param defaultValue The default value if the key is not found.
     * @return The property value. It will be null if not found.
     */
    public static Boolean getBoolean(String key, Boolean defaultValue) {
        String value = getProperties().getProperty(key);
        return value == null ? defaultValue : Boolean.valueOf(value);
    }

    /**
     * Check if properties should be reloaded.
     */
    private static synchronized Properties getProperties() {
        if (dataSource == null) {
            init();
        }
        if (expiry != 0L && (System.currentTimeMillis() - lastRefresh > (expiry * 1000L))) {
            props = dataSource.load();
            expiry = getExpiry();
            log.info("getProperties: System properties reloaded, expiry time set to {} seconds", expiry);
            lastRefresh = System.currentTimeMillis();
        }

        return props;
    }

    private static long getExpiry() {
        return Long.valueOf(props.getProperty(PARAM_EXPIRY, String.valueOf(DEFAULT_EXPIRY)));
    }
}
