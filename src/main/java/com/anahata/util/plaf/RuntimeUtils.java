/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.plaf;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.io.FileUtils;

/**
 *
 * Utility classes to work with the system runtime,
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RuntimeUtils {

    /**
     * Gets a string indicating the memory stats (Free, total, max, used).
     *
     * @return the memory stats.
     */
    public static String getMemoryStats() {
        Runtime rt = Runtime.getRuntime();
        long total = rt.totalMemory();
        long max = rt.maxMemory();
        long used = total - rt.freeMemory();
        long free = max - used;
        StringBuilder st = new StringBuilder();
        st.append("Free: ").append(FileUtils.byteCountToDisplaySize(free)).
                append(", Used : ").append(FileUtils.byteCountToDisplaySize(used)).
                append(", Max Heap Size : ").append(FileUtils.byteCountToDisplaySize(max)).
                append(", Current Heap Size: ").append(FileUtils.byteCountToDisplaySize(total));

        return st.toString();
    }

    /**
     * Executes the given command line and returns the process output as stream.
     *
     * @param cmd Command that need to be executed.
     * @return InputStream.
     * @throws IOException
     */
    public static InputStream execCmd(String cmd) throws IOException {
        Process run = Runtime.getRuntime().exec(cmd);
        return run.getInputStream();
    }
}
