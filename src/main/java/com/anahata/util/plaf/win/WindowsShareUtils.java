/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.plaf.win;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;

/**
 *
 * @author pablo
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class WindowsShareUtils {

    /**
     * Creates a shared folder if it does not exist in the given network.
     *
     * @param ipaddress
     *                  Network in which to check.
     * @param shareName
     *                  Name of the shared folder.
     * @param file
     *                  File to be shared.
     * @return Returns {@code true} if the share is created successfully, returns {@code false} if there are any access
     *         constraints else returns {@code null} if the share already exists.
     * @throws HeadlessException
     * @throws IOException
     */
    public static Boolean createSharedFolder(String ipaddress, String shareName, File file) throws HeadlessException,
            IOException {
        if (!checkIfShareExists(ipaddress, shareName)) {
            String filePath = file.getAbsolutePath();
            try {
                String line = String.format("net share %s=\"%s\" /GRANT:Everyone,FULL", shareName, filePath);
                log.debug("Executing {}", line);
                CommandLine cmdLine = CommandLine.parse(line);
                DefaultExecutor executor = new DefaultExecutor();
                //executor.setExitValue(2);
                int exitValue = executor.execute(cmdLine);
                log.debug("Exit value " + exitValue);
                return true;
            } catch (Exception e) {
                log.warn(e.getMessage());
                return false;
            }
        }
        return null;
    }

    /**
     * Checks whether the provided shared folder already exists in the given network.(using Runtime class)
     *
     * @param ipaddress
     *                  Network in which to check.
     * @param shareName
     *                  Name of the shared folder.
     * @return {@code true} if it already exists else returns {@code false}.
     * @throws IOException
     */
    public static boolean checkIfShareExists(String ipaddress, String shareName) throws IOException {
        final String l = "net view \\\\" + ipaddress;
        final Process process2 = Runtime.getRuntime().exec(l);
        final BufferedReader is2 = new BufferedReader(new InputStreamReader(process2.getInputStream()));
        final StringBuilder str = new StringBuilder(" ");
        String line2;
        while ((line2 = is2.readLine()) != null) {
            str.append(line2.trim()).append(" ");
        }
        return str.toString().contains(" " + shareName + " "); // Checking if the o/p contains shareName.
    }

}
