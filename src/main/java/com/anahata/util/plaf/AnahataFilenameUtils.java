package com.anahata.util.plaf;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.lang.AnahataStringUtils;
import java.util.regex.Pattern;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.Validate;

/**
 * Utility methods and constants to work with file names which could be considered an extension of commons-io
 * FilenameUtils.
 *
 * @author pablo
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnahataFilenameUtils {
    /**
     * If a file or folder name that is being handeld contains a character that is an illegal file name character in
     * some platforms this string will be the default replacement for that character.
     */
    public static final String DEFAULT_ILLGEAL_FILE_NAME_CHARACTER_REPLACEMENT_VALUE = "_";

    /**
     * If a name is too long base name will be truncated towards the end and this character sequence will be used to
     * jping the two bits.
     */
    public static final String NAME_TOO_LONG_CONCAT_TOKEN = "...";

    /**
     * Number of characters to the right of the end of the file base name where the concat token will be inserted
     * {@link #NAME_TOO_LONG_CONCAT_TOKEN}.
     */
    public static final int NAME_TOO_LONG_CONCAT_TOKEN_DITANCE_FROM_BASE_NAME_END = 10;

    /**
     * Superset of all illegal filename characters in different platforms. From
     * {@linkplain  http://support.grouplogic.com/?p=1607}
     *
     */
    public static final char[] ILLEGAL_FILE_NAME_CHARACTERS = {'/', '?', '<', '>', '\\', ':', '*', '|', '\"'};

    /**
     * Maximum length for a file name.
     */
    public static final int MAX_FILE_NAME_LENGTH = 256;

    /**
     * Pattern to validate a file name is valid across all platforms. <b>This pattern doesn't include length validation
     * when it should.</b>
     *
     * <code><pre>
     *  "# Match a valid Windows filename (unspecified file system).          \n" +
     * "^                                # Anchor to start of string.        \n" +
     * "(?!                              # Assert filename is not: CON, PRN, \n" +
     * "  (?:                            # AUX, NUL, COM1, COM2, COM3, COM4, \n" +
     * "    CON|PRN|AUX|NUL|             # COM5, COM6, COM7, COM8, COM9,     \n" +
     * "    COM[1-9]|LPT[1-9]            # LPT1, LPT2, LPT3, LPT4, LPT5,     \n" +
     * "  )                              # LPT6, LPT7, LPT8, and LPT9...     \n" +
     * "  (?:\\.[^.]*)?                  # followed by optional extension    \n" +
     * "  $                              # and end of string                 \n" +
     * ")                                # End negative lookahead assertion. \n" +
     * "[^<>:\"/\\\\|?*\\x00-\\x1F]*     # Zero or more valid filename chars.\n" +
     * "[^<>:\"/\\\\|?*\\x00-\\x1F\\ .]  # Last char is not a space or dot.  \n" +
     * "$                                # Anchor to end of string.            ",
     *
     * </code><pre> {@value #VALID_FILE_NAME_PATTERN}
     */
    public static final Pattern VALID_FILE_NAME_PATTERN = Pattern.compile(
            "# Match a valid Windows filename (unspecified file system).          \n"
            + "^                                # Anchor to start of string.        \n"
            + "(?!                              # Assert filename is not: CON, PRN, \n"
            + "  (?:                            # AUX, NUL, COM1, COM2, COM3, COM4, \n"
            + "    CON|PRN|AUX|NUL|             # COM5, COM6, COM7, COM8, COM9,     \n"
            + "    COM[1-9]|LPT[1-9]            # LPT1, LPT2, LPT3, LPT4, LPT5,     \n"
            + "  )                              # LPT6, LPT7, LPT8, and LPT9...     \n"
            + "  (?:\\.[^.]*)?                  # followed by optional extension    \n"
            + "  $                              # and end of string                 \n"
            + ")                                # End negative lookahead assertion. \n"
            + "[^<>:\"/\\\\|?*\\x00-\\x1F]*     # Zero or more valid filename chars.\n"
            + "[^<>:\"/\\\\|?*\\x00-\\x1F\\ .]  # Last char is not a space or dot.  \n"
            + "$                                # Anchor to end of string.            ",
            Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.COMMENTS);

    /**
     * Replaces all illegal characters on a given file name for the requested token.
     *
     * @param fileName     a file name
     * @param replaceToken the token for which illegal characters would have to be replaced
     * @return the valid file name.
     */
    public static String replaceIllegalCharacters(String fileName, String replaceToken) {
        return AnahataStringUtils.replaceChars(fileName, ILLEGAL_FILE_NAME_CHARACTERS, replaceToken);
    }

    /**
     * Replaces all illegal characters on a given file name using
     * {@link #DEFAULT_ILLGEAL_FILE_NAME_CHARACTER_REPLACEMENT_VALUE}.
     *
     * @param fileName     a file name
     * @param replaceToken the token for which illegal characters would have to be replaced
     * @return the valid file name.
     */
    public static String replaceIllegalCharacters(String fileName) {
        return AnahataStringUtils.replaceChars(fileName, ILLEGAL_FILE_NAME_CHARACTERS,
                DEFAULT_ILLGEAL_FILE_NAME_CHARACTER_REPLACEMENT_VALUE);
    }

    /**
     * Checks if a given file name is a valid file name in all platforms.
     *
     * @param fileName the file name to validate.
     *
     * @return true if valid, false otherwise.
     */
    public static boolean isValidFileName(String fileName) {
        Validate.notNull(fileName);
        boolean patternValid = VALID_FILE_NAME_PATTERN.matcher(fileName).matches();
        return patternValid && fileName.length() <= MAX_FILE_NAME_LENGTH;
    }

    /**
     * Ensures a given file name is not longer than the maximum length by removing any overflowing characters from the
     * base name and preserving the file extension.
     *
     * @param fileName the file name to validate.
     *
     * @return true if valid, false otherwise.
     * @throws IllegalArgumentException if the file extension is longer than {@link #MAX_FILE_NAME_LENGTH} -2}
     */
    public static String trimToMaxLength(String fileName) {
        Validate.notNull(fileName);
        if (fileName.length() > MAX_FILE_NAME_LENGTH) {
            String ext = FilenameUtils.getExtension(fileName);
            int maxExtLength = MAX_FILE_NAME_LENGTH - (NAME_TOO_LONG_CONCAT_TOKEN_DITANCE_FROM_BASE_NAME_END + 2);
            if (ext.length() > (maxExtLength)) {
                throw new IllegalArgumentException(
                        "Extension is too long for this alogrithm. Extension length="
                        + ext.length() + " max= " + ext.length() + " File name:" + fileName);
            }
            if (!ext.isEmpty()) {
                ext = "." + ext;
            }

            int maxBaseLength = MAX_FILE_NAME_LENGTH - ext.length();

            String base = FilenameUtils.getBaseName(fileName);

            String baseSplitter = NAME_TOO_LONG_CONCAT_TOKEN;
            int baseTrimDistanceFromEnd = NAME_TOO_LONG_CONCAT_TOKEN_DITANCE_FROM_BASE_NAME_END;
            String basePreffix = fileName.substring(0, maxBaseLength - baseTrimDistanceFromEnd);
            String baseSuffix = base.substring(base.length() - (baseTrimDistanceFromEnd - baseSplitter.length()));

            return basePreffix + baseSplitter + baseSuffix + ext;

        }
        return fileName;
    }

    /**
     * Ensures a given file name is not longer than the maximum length and that it doesn't have any illegal characters.
     *
     * @param fileName the file name to validate.
     *
     * @return true if valid, false otherwise.
     * @throws IllegalArgumentException if the file extension is longer than {@link #MAX_FILE_NAME_LENGTH} -2}
     */
    public static String toValidFileName(String fileName) {
        return trimToMaxLength(replaceIllegalCharacters(fileName));
    }
}
