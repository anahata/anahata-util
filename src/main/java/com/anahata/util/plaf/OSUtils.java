package com.anahata.util.plaf;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.plaf.win.WinRegistry;
import java.io.File;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Provides information about the operating system the JVM is running in.
 *
 * @author Robert Nagajek
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class OSUtils {
    private static final String SYSPROP_OS_NAME = "os.name";

    private static final String SYSPROP_OS_USER_NAME = "user.name";

    private static final String SYSPROP_OS_USER_HOME = "user.home";

    private static final String OS_NAME_UNKNOWN = "unknown";

    private static final String OS_NAME_MAC_1 = "mac";

    private static final String OS_NAME_MAC_2 = "darwin";

    private static final String OS_NAME_WINDOWS = "windows";

    private static final String OS_NAME_LINUX = "linux";

    private static final String OS_NAME_SUNOS = "sunos";

    /**
     * Determine if running under Windows.
     *
     * @return true if running under Windows, false if not.
     */
    public static boolean isWindows() {
        final String osName = getOsName();
        return osName.startsWith(OS_NAME_WINDOWS);
    }

    /**
     * Determine if running under Mac OS.
     *
     * @return true if running under Mac OS, false if not.
     */
    public static boolean isMac() {
        final String osName = getOsName();
        return osName.startsWith(OS_NAME_MAC_1) || osName.startsWith(OS_NAME_MAC_2);
    }

    /**
     * Determine if running under Linux.
     *
     * @return true if running under Linux, false if not.
     */
    public static boolean isLinux() {
        final String osName = getOsName();
        return osName.startsWith(OS_NAME_LINUX);
    }

    /**
     * Determine if running under Sun OS.
     *
     * @return true if running under Sun OS, false if not.
     */
    public static boolean isSunOS() {
        final String osName = getOsName();
        return osName.startsWith(OS_NAME_SUNOS);
    }

    private static String getOsName() {
        return System.getProperty(SYSPROP_OS_NAME, OS_NAME_UNKNOWN).toLowerCase();
    }

    /**
     * Checks if a file with the given executable exists in any directory listed in the OS PSTH environment variable.
     *
     * @param windowsName The name of the file in windows
     * @param macName     the name of the file in mac
     * @param linuxName   the name of the file in linux
     * @return the File or null if it couldn't be found.
     */
    public static File findExecutableOnPath(String windowsName, String macName, String linuxName) {
        if (isWindows()) {
            return findExecutableOnPath(windowsName);
        } else if (isMac()) {
            return findExecutableOnPath(macName);
        } else if (isLinux() || isSunOS()) {
            return findExecutableOnPath(linuxName);
        } else {
            throw new IllegalStateException("Unknown OS");
        }
    }

    /**
     * Checks if a file with the given name exists in any directory listed in the OS PSTH environment variable.
     *
     * @param executableName the name of the executable (e.g. java.exe)
     * @return the File or null if it couldn't be found.
     */
    public static File findExecutableOnPath(String executableName) {
        String systemPath = System.getenv("PATH");
        String[] pathDirs = systemPath.split(File.pathSeparator);
        File fullyQualifiedExecutable = null;

        for (String pathDir : pathDirs) {
            File file = new File(pathDir, executableName);

            if (file.isFile()) {
                fullyQualifiedExecutable = file;
                break;
            }
        }

        if (fullyQualifiedExecutable == null && isWindows()) {
            String location = WinRegistry.findAppLocation(executableName);

            if (location != null) {
                fullyQualifiedExecutable = new File(WinRegistry.findAppLocation(executableName));
            }
        }

        return fullyQualifiedExecutable;
    }

    /**
     * Gets the user name system property.
     *
     * @return the user name system property.
     */
    public static String getUserName() {
        return System.getProperties().getProperty(SYSPROP_OS_USER_NAME);
    }

    /**
     * Gets the user home directory system property.
     *
     * @return the user home system property.
     */
    public static String getUserHome() {
        return System.getProperties().getProperty(SYSPROP_OS_USER_HOME);
    }

}
