/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.plaf;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.io.AnahataFileUtils;
import com.anahata.util.io.FileModificationListener;
import com.anahata.util.io.FileMonitor;
import java.io.File;
import java.io.IOException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ProcessUtils {
    public static void editFile(File editor, String name, String lockFileNamePattern, byte[] data,
            FileModificationListener listener) throws
            IOException, InterruptedException {
        name = AnahataFilenameUtils.toValidFileName(name);
        File file = AnahataFileUtils.createTempFile(name, data, true);
        File lockFile = lockFileNamePattern != null ? new File(file.getParent(), String.format(lockFileNamePattern, name)) : null;
        ProcessBuilder pb = new ProcessBuilder(editor.getAbsolutePath(), file.getAbsolutePath());
        pb.directory(file.getParentFile());
        log.debug("Launching {} {} ", editor, name);
        Process process = pb.start();
        FileMonitor fm = new FileMonitor(file, listener);
        int exitCode = process.waitFor();
        log.debug("Editor Process {} {} finished with exit code: {} ", editor, name, exitCode);
        if (lockFile != null) {
            log.debug("Checking if lock file still present: editor={} file={} lockFile={}", editor, name, lockFile);
            do {
                Thread.sleep(500);
            } while ((lockFile.exists()));
            log.debug("Lock file not present: editor={} file={} lockFile={}", editor, name, lockFile);
        }
        log.debug("Closing file monitor for {} {} ", editor, name);
        fm.close();
    }
}
