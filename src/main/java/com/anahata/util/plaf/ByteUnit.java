/*
 * Copyright 2014 Anahata.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.util.plaf;

import com.anahata.util.formatting.Displayable;
import lombok.AllArgsConstructor;
import lombok.Getter;
/**
 * Enumeration for byte units (BYTE, KILOBYTE, MEGABYTE, GIGABYTE, TERABYTE).
 * 
 * @author pablo
 */
@AllArgsConstructor
@Getter
public enum ByteUnit implements Displayable {
    BYTE("Bytes"),
    KILOBYTE("KB"),
    MEGABYTE("MB"),
    GIGABYTE("GB"),
    TERABYTE("TB"),
    PETABYTE("PB");
    
    private String displayValue;
    
    /**
     * Gets the number of bytes for 1 unit.
     * 
     * @return the number of bytes for 1 unit.
     */
    public long getNumberOfBytes() {        
        if (this == BYTE) {
            return 1;
        } else {
            return (long) Math.pow(1024, ordinal());
        }
    }
    
    /**
     * Converts between units.
     * 
     * @param qty the quantity of this unit
     * @param toUnit the target unit
     * @return the number of units in <i>toUnit</i>
     */
    public double to(double qty, ByteUnit toUnit) {        
        double thisBytes = getNumberOfBytes() * qty; //1
        double otherBytes = toUnit.getNumberOfBytes(); //1024
        return thisBytes / otherBytes;
    }
    


    @Override
    public String toString() {
        return displayValue;
    }
    
    //    public static void main(String[] args) {
//        double d = ByteUnit.MEGABYTE.to(10, BYTE);
//        System.out.println(new BigDecimal(d));
//        double d2 = ByteUnit.MEGABYTE.to(100, TERABYTE);
//        System.out.println(new BigDecimal(d2));
//    }
}
