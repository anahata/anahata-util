package com.anahata.util.plaf;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JniUtils {
    public static void loadPackagedLibrary(String resource) {

        String fileName = null;
        try {
            // have to use a stream
            URL url = JniUtils.class.getResource(resource);
            fileName = resource.substring(resource.lastIndexOf("/") + 1);
            log.info("Creating temp file for native library " + fileName);
            File fileOut = File.createTempFile("anahata-jni-utils", fileName);
            try (InputStream in = url.openStream(); OutputStream out = new FileOutputStream(fileOut)) {
                log.info("Writing native library " + url.getFile() + " to: " + fileOut.getAbsolutePath());
                IOUtils.copy(in, out);
            }
            log.info("Loading native library " + fileOut.toString());
            System.load(fileOut.toString());
        } catch (Exception e) {
            log.error("Failed to load native library", e);
            throw new RuntimeException("Failed to load library resource=" + resource + " fileName=" + fileName, e);
        }
    }

    public static void addToLibraryPath(String resource) {

        String javaLibraryPath = System.getProperty("java.library.path");
        log.info("javaLibraryPath = " + javaLibraryPath);
        String fileName = null;
        try {
            // have to use a stream
            URL url = JniUtils.class.getResource(resource);
            fileName = resource.substring(resource.lastIndexOf("/") + 1);
            log.info("Creating temp file for native library " + fileName);
            File tempDir = FileUtils.getTempDirectory();
            tempDir = new File(tempDir, "anahata-util-jni-" + System.currentTimeMillis());
            tempDir.mkdir();
            log.info("temp dir will be " + tempDir);
            File fileOut = new File(tempDir, fileName);
            try (InputStream in = url.openStream(); OutputStream out = new FileOutputStream(fileOut)) {
                log.info("Writing native library " + url.getFile() + " to: " + fileOut.getAbsolutePath());
                IOUtils.copy(in, out);
            }
            log.info("library deployed to " + fileOut + " size=" + fileOut.length());
            javaLibraryPath += System.getProperty("path.separator") + tempDir;
            log.info("Updating java.library.path to " + javaLibraryPath);
            System.setProperty("java.library.path", javaLibraryPath);
            log.info("Writing native library " + url.getFile() + " to: " + fileOut.getAbsolutePath());
        } catch (Exception e) {
            log.error("Failed to load native library", e);
            throw new RuntimeException("Failed to load library resource=" + resource + " fileName=" + fileName, e);
        }
    }
}
