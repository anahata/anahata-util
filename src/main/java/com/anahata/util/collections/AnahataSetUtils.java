package com.anahata.util.collections;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnahataSetUtils {
    /**
     * Create a set from values.
     * 
     * @param <T> The type.
     * @param values The values. Can be null.
     * @return The set. Never null, will be an empty set if null is passed in.
     */
    public static <T> Set<T> asSet(T... values) {
        if (values == null) {
            return new HashSet<>(0);
        }
        
        Set<T> set = new HashSet<>(values.length);
        set.addAll(Arrays.asList(values));
        return set;
    }
}
