package com.anahata.util.collections;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 *
 * @author pablo
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ListUtils {
    /**
     * Adds an element to a sorted list at the position corresponding to the sort order. This method helping
     * in keeping a non-autosortable List implementation (Such as a JavaFX ObservableList sorted).
     * 
     * @param list
     * @param object
     * @param comparator 
     */
    @SuppressWarnings("unchecked")
    public static void addSorted(List list, Object object, Comparator comparator) {
        int currChildrenCount = list.size();
        int position = -1;
        
        for (int i = 0; i < currChildrenCount; i++) {

            Object object2 = list.get(i);
            int compareResult = comparator.compare(object, object2);
            
            if (compareResult < 0) {
                position = i;
                break;
            }
        }

        if (position != -1) {
            list.add(position, object);
        } else {
            //append at the end
            list.add(object);
        }
    }
    
    /**
     * Set all values in a list in an optimised way, so that if elements in the source list are the same instance in
     * the target list, they do not get set.
     * <p>
     * This has a limitation where the target list must implement remove(int).
     * 
     * @param <E> The element type.
     * @param target The target list to update. Required.
     * @param source The source list. Can be null.
     */
    public static <E> void setAll(@NonNull List<E> target, List<E> source) {
        if (source == null) {
            source = Collections.emptyList();
        }
        
        int tgtSize = target.size();
        int srcSize = source.size();
        
        for (int i = 0; i < srcSize; i++) {
            final E srcValue = source.get(i);
            
            if (i >= tgtSize) {
                target.add(srcValue);
            } else if (srcValue != target.get(i)) {
                target.set(i, srcValue);
            }
        }
        
        if (srcSize < tgtSize) {
            for (int i = srcSize; i < tgtSize; i++) {
                target.remove(srcSize);
            }
        }
    }
}
