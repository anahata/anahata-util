/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.collections;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
public final class MapUtils {
    
    
    public static<K,V> List<K> getKeysForValue(Map<K, V> map, V value) {
        List<K> list = new ArrayList<K>();
        for (Map.Entry<K,V> entry: map.entrySet()) {
            if (entry.getValue().equals(value)) {
                list.add(entry.getKey());
            }
        }
        return list;
    }
    
    public static<K,V> K getKeyForValue(Map<K, V> map, V value) {
        
        
        for (Map.Entry<K,V> entry: map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

}
