/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.concurrent;

import com.anahata.util.validation.ValidationUtils;
import java.lang.Thread.UncaughtExceptionHandler;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * An uncaught exception handler that just logs the error.
 * 
 * @author pablo
 */
@AllArgsConstructor
public class BasicUncaughtExceptionHandler implements UncaughtExceptionHandler{
    private Class loggerClass;

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        String details = ValidationUtils.getConstraintValidationDetails(e);
        String mssg = "Uncaught Exception on thread " + t.getName();
        if (details != null) {
            mssg += ". ConstraintValidations: " + details;
        }
        Logger logger = LoggerFactory.getLogger(loggerClass);
        logger.error(mssg, e);
    }
    
}
