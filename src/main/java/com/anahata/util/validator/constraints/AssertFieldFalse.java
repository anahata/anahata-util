package com.anahata.util.validator.constraints;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

/**
 * Assert a method returning false. This is similar to the standard version, except that a target field name can be
 * passed in, and it only applies to methods. The generated node name in the validation path will have a # prefixed to
 * it, to identify it as part of a method.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Documented
@Constraint(validatedBy = AssertFieldFalseValidator.class)
@Target(value = {METHOD})
@Retention(RUNTIME)
public @interface AssertFieldFalse {
    String message() default "{javax.validation.constraints.AssertFalse.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return The field the assert applies to. If not provided, no field is assigned.
     */
    String field() default "";
}
