package com.anahata.util.validator.constraints;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.lang3.StringUtils;

/**
 * Validates that a method returns false or a field is false.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class AssertFieldFalseValidator implements ConstraintValidator<AssertFieldFalse, Boolean> {
    private AssertFieldFalse constraint;

    @Override
    public void initialize(final AssertFieldFalse constraintAnnotation) {
        this.constraint = constraintAnnotation;
    }

    @Override
    public boolean isValid(final Boolean value, ConstraintValidatorContext context) {
        if (value == null || !value) {
            return true;
        }

        if (!StringUtils.isBlank(constraint.field())) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(constraint.message())
                    .addPropertyNode("#" + constraint.field())
                    .addConstraintViolation();
        }

        return false;
    }
}
