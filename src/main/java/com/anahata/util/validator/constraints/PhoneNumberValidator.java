package com.anahata.util.validator.constraints;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.google.i18n.phonenumbers.PhoneNumberMatch;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import java.util.Iterator;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class PhoneNumberValidator implements ConstraintValidator<PhoneNumber, String> {
    @Override
    public void initialize(PhoneNumber constraintAnnotation) {
    }

    public static boolean isValid(String value) {
        return validate(value);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return validate(value);
    }

    private static boolean validate(String value) {
        if (StringUtils.isBlank(value)) {
            return true;
        }

        final PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        Iterable<PhoneNumberMatch> numbers = phoneNumberUtil.findNumbers(value, null);
        Iterator<PhoneNumberMatch> iter = numbers.iterator();

        if (iter.hasNext()) {
            return true;
        }

        numbers = phoneNumberUtil.findNumbers(value, "AU");
        iter = numbers.iterator();

        if (!iter.hasNext()) {
            return false;
        }

        iter.next();

        if (iter.hasNext()) {
            return false;
        }

        return true;
    }
}
