package com.anahata.util.jsf;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.formatting.FormattingUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparableComparator;
import org.apache.commons.collections.comparators.NullComparator;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An enhanced list that provides automatic sorting and single or multiple row selection.
 * 
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
public final class JSFTableModel<T> {
    private static final Logger log = LoggerFactory.getLogger(JSFTableModel.class);

    private String sortColumn = null;

    private Comparator<T> comparator = null;

    private boolean sortAscending = true;

    private boolean needsResort = false;

    private boolean nullsAreHigh = false;

    private List<T> list = new ArrayList<T>();

    private Set<T> selection = new HashSet<T>();

    private boolean singleSelection = true;
    
    private boolean atLeastOne = true;

    private List<SelectionListener<T>> listeners = new ArrayList<SelectionListener<T>>();

    private SelectionFilter selectionFilter = null;

    private SelectionWrapperMap selectionWrapper = new SelectionWrapperMap();

    /**
     * Construct with a default single selection mode.
     */
    public JSFTableModel() {
    }

    /**
     * Construct with a given selection mode.
     * 
     * @param singleSelection If true, only one row can be selected at a time. If false, many can be.
     */
    public JSFTableModel(boolean singleSelection) {
        this.singleSelection = singleSelection;
        this.atLeastOne = singleSelection;
    }

    //---------------------
    //Sorting methods
    //---------------------
    /**
     * @return true if sorting ascending, false if sorting descending.
     */
    public boolean isSortAscending() {
        return sortAscending;
    }

    /**
     * Set the sorting mode.
     * 
     * @param sortAscending true for ascending, false for descending.
     */
    public void setSortAscending(boolean sortAscending) {
        if (!sortAscending == this.sortAscending) {
            needsResort = true;
        }

        this.sortAscending = sortAscending;
    }

    /**
     * @return The column being sorted on, or null if none.
     */
    public String getSortColumn() {
        return sortColumn;
    }

    /**
     * Set the sorting column.
     * 
     * @param sortColumn The sorting column.
     */
    public void setSortColumn(String sortColumn) {
        Validate.notNull(sortColumn);

        if (!sortColumn.equals(this.sortColumn)) {
            needsResort = true;
        }

        this.sortColumn = sortColumn;
    }

    /**
     * @return The comparator being used for sorting.
     */
    public Comparator<T> getComparator() {
        return comparator;
    }

    /**
     * Set the comparator for sorting.
     * 
     * @param comparator The comparator.
     */
    public void setComparator(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    /**
     * @return true if nulls sort first, false if they sort last.
     */
    public boolean isNullsAreHigh() {
        return nullsAreHigh;
    }

    /**
     * Set the null sorting policy.
     * 
     * @param nullsAreHigh true if nulls are first, false if they are last.
     */
    public void setNullsAreHigh(boolean nullsAreHigh) {
        this.nullsAreHigh = nullsAreHigh;
    }

    //---------------------
    //List methods
    //---------------------
    /**
     * Get the list of items. The will optionally be sorted.
     * 
     * @return The list of items.
     */
    public List<T> getList() {
        if (needsResort) {
            sort();
        }

        return list;
    }

    /**
     * Sort the list of items.
     */
    public void sort() {
        // Do not sort if there's no sorting defined.
        
        if (comparator == null && sortColumn == null) {
            needsResort = false;
            return;
        }
        
        Comparator c = null;

        if (comparator != null) {
            c = comparator;
        } else if (!StringUtils.isEmpty(sortColumn)) {
            c = ComparableComparator.getInstance();
            c = new BeanComparator(sortColumn, new NullComparator(c, nullsAreHigh));
        } 
        
        // Else there isn't a specified sortcolumn so no sorting.
        
        if (!sortAscending) {
            c = new ReverseComparator(c);
        }

        Collections.sort(list, c);

        needsResort = false;
    }

    /**
     * Updates the list attribute retaining the previous selection.
     * 
     * @param list The list.
     */
    public void setList(List<T> list) {
        setList(list, true);
    }

    /**
     * Updates the list attribute optionally retaining the current selection.
     * 
     * @param list The list.
     */
    public void setList(List<T> list, boolean keepSelection) {
        this.list = new ArrayList(list);

        if (keepSelection) {
            //TODO: Pablo: Rob, shall we fire event here if selection changes as an outcome of changin the list here?
            selection.retainAll(list);
        } else {
            selection.clear();
        }

        needsResort = true;
    }

    /**
     * Clears the list and selection but keeps sort attributes.
     */
    public void clear() {
        list.clear();
        clearSelection();
    }

    //----------------------
    //Selection methods
    //----------------------

    /**
     * Checks whether at least one object should be selected.
     * 
     * @return true if {@link #atLeastOne} is enabled.
     */
    public boolean isAtLeastOne() {
        return atLeastOne;
    }

    /**
     * Specifies whether at least one object should be selected.
     * 
     * @param atLeastOne 
     */
    public void setAtLeastOne(boolean atLeastOne) {
        this.atLeastOne = atLeastOne;
    }
    
    
    /**
     * Specifies a selection filter, if a selection filter is not set, all objects can be selected.
     */
    public void setSelectionFilter(SelectionFilter selectionFilter) {
        this.selectionFilter = selectionFilter;
    }

    /**
     * Selects the first object in the list if there are currently no selected objects.
     */
    public void selectFirstIfNoSelection() {
        getList();//in case it is pending a resort
        if (selection.isEmpty() && !list.isEmpty()) {
            addSelectedObject(list.get(0));
        }
    }

    /**
     * Clear all selections.
     */
    public void clearSelection() {
        for (T t : selection) {
            notifyListeners(t, false);
        }

        selection.clear();
    }

    /**
     * This is for JSF row selectors.
     * 
     * @return A map which indicates which rows are selected.
     */
    public Map<T, Boolean> getSelection() {
        return selectionWrapper;
    }

    /**
     * Set the currently selected object.
     * 
     * @param t The object. If null, all selections are cleared.
     */
    public void setSelectedObject(T t) {
        if (t == null) {
            selection.clear();
            return;
        }

        addSelectedObject(t);
    }

    /**
     * Determine if the given element is selected.
     * 
     * @param t The element.
     * @return true if selected, false if not.
     */
    public boolean isSelected(T t) {
        validateSelectable(t);
        return selection.contains(t);
    }

    /**
     * Set a list of selected objects.
     * 
     * @param l The list of objects.
     */
    public void setSelectedObjects(List<T> l) {
        for (T t1 : l) {
            validateSelectable(t1);
        }

        selection = new HashSet<T>(l);

        for (T t : selection) {
            notifyListeners(t, true);
        }
    }

    /**
     * Set a selected object.
     * TODO Why this and setSelectedObject?
     * 
     * @param t The object to select.
     */
    public void addSelectedObject(T t) {
        validateSelectable(t);

        if (!isSelectable(t)) {
            return;
        }

        if (singleSelection) {
            selection.clear();
        }

        selection.add(t);
        log.debug("Add selected Object={} totalSelected={}", t, selection.size());

        notifyListeners(t, true);
    }

    /**
     * Deselect an object.
     * TODO Rename to deselect?
     * 
     * @param t The object to deselect.
     */
    public void removeSelectedObject(T t) {
        //validateSelectable(t);
        if (atLeastOne && selection.contains(t) && selection.size() == 1) {
            log.debug("Will not remove Object={} as atLeastOne is enabled", t);
            return;
        }
        selection.remove(t);
        log.debug("Remove selected Object={} totalSelected={}", t, selection.size());
        notifyListeners(t, false);
    }

    /**
     * Get all selected objects.
     * 
     * @return The selected objects.
     */
    public List<T> getSelectedObjects() {
        List<T> ret = new ArrayList();

        for (T t : selection) {
            int idx = list.indexOf(t);

            if (idx == -1) {
                throw new IllegalStateException(
                        "Unexisting object " + t + " mark as selected but not present in the list");
            }

            ret.add(list.get(idx));
        }

        return ret;
    }

    /**
     * @return The currently selected object.
     */
    public T getSelectedObject() {
        List<T> ret = getSelectedObjects();
        int size = ret.size();

        if (size == 0) {
            return null;
        } else if (size == 1) {
            return ret.get(0);
        } else {
            // TODO Throw exception if more than 1?
            return ret.get(0);
        }
    }

    /**
     * Add a selection listener.
     * 
     * @param listener The selection listener.
     */
    public void addSelectionListener(SelectionListener<T> listener) {
        listeners.add(listener);
    }

    /**
     * Checks if an object is selectable or not. If a given {@link #selectionFilter} has been specified, the evaluation
     * is delegated to the filter, otherwise it returns true.
     * 
     * @param t
     * @return true 
     * @trows IllegalArgumentException - If t is not in {@link #list} 
     */
    public boolean isSelectable(T t) {
        validateSelectable(t);
        return selectionFilter == null || selectionFilter.isSelectable(t);
    }

    /**
     * Selects all objects in the list.
     */
    public void selectAll() {
        if (selectionFilter == null) {
            selection = new HashSet<T>(list);
        } else {
            selection.clear();
            for (T t : list) {
                if (isSelectable(t)) {
                    selection.add(t);
                }
            }
        }

    }

    /**
     * Clears all selection.
     */
    public void unselectAll() {
        selection.clear();
    }

    /**
     * Selects / Unselects all.
     */
    public void toggleSelectAll() {
        if (selection.isEmpty()) {
            selectAll();
        } else {
            unselectAll();
        }
    }

    /**
     * Checks that the object is in {@link #list} throwing an exception otherwise.
     */
    private void validateSelectable(T t) {
        Validate.isTrue(list.contains(t),
                "the object that you are trying to select or inquiring about it's selected state is not  in the list"
                + "\n\tObject: %1$s"
                + "\n\tList:\n %2$s", t, FormattingUtils.collectionToTableString(list));
    }

    private void notifyListeners(T t, boolean selected) {
        for (SelectionListener<T> listener : listeners) {
            listener.itemSelected(t, selected);


        }
    }

    /**
     * This is used when a client wants to act on a selection / deselection action.
     */
    public interface SelectionListener<T> {
        /**
         * Invoked with the selected / deselected item.
         * 
         * @param t The selected item.
         * @param selected true if selected, false if deselected.
         */
        public void itemSelected(T t, boolean selected);

    }

    /**
     * Used to determine which rows can be selected and which can't.
     */
    public interface SelectionFilter<T> {
        /**
         * Checks if an object is selectable or not.
         * @param t the object that we are inquiring about
         * @return true if the object can be selected, false otherwise.
         */
        public boolean isSelectable(T t);

    }

    public class SelectionWrapperMap implements Map<T, Boolean> {
        @Override
        public Boolean get(Object key) {

            return isSelected((T) key);
        }

        @Override
        public Boolean put(T key, Boolean value) {

            log.debug("put {}={}", key, value);
            Validate.notNull(value, "Value can not be null");

            boolean ret = isSelected(key);
            if (value) {
                addSelectedObject(key);
            } else {
                removeSelectedObject(key);
            }
            return ret;
        }

        @Override
        public int size() {
            return selection.size();
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean containsKey(Object key) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean containsValue(Object value) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Set<Entry<T, Boolean>> entrySet() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean isEmpty() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Set<T> keySet() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void putAll(Map<? extends T, ? extends Boolean> m) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Boolean remove(Object key) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Collection<Boolean> values() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

    }
}
