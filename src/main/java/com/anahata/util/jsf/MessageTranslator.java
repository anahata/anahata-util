package com.anahata.util.jsf;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * Utility Class to translate message with international message bundle.
 * //TODO Move To AnahataUtil?
 * @author Pierre Martin
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MessageTranslator {
    // FIXME work out how to get a standard path
    private static final String MSG_BUNDLE_PATH = "app.messages";

    

    /**
     * Translates a key into a internationalized String
     *
     * It will locate the resource bundle according to
     * <code>ResourceBundle.getBundle()</code> strategy.
     *
     * !!!ATTENTION!!! Make sure you are providing the locale of the user and not the server's locale.
     *
     * @param key -the key of the string to translate, not null
     * @param locale -the locale to use for the translation, not null.
     * @return the translated string, ot the key if cannot translate.
     * @see ResourceBundle
     */
    public static String translate(String key, Locale locale) {
        Validate.notNull(key);
        Validate.notNull(locale);
        try {
            ResourceBundle bundle = ResourceBundle.getBundle(MSG_BUNDLE_PATH, locale);
            if (bundle != null) {
                return bundle.getString(key);
            }

        } catch (Exception ex) {
            log.error("Cannot translate string {} with locale {}. Cause = {}", new Object[]{key, locale, ex});
        }
        return key;
    }

    /**
     * Translates a key into a internationalized String with the user Locale
     *
     * It will locate the resource bundle according to
     * <code>ResourceBundle.getBundle()</code> strategy.
     *
     * @param key The key of the string to translate
     * @param args Option arguments for parameter substitution.
     * @return the translated string
     * @see ResourceBundle
     */
    public static String translate(String key, Object... args) {
        Locale userLocale = getUserLocale();
        return MessageFormat.format(translate(key, userLocale), args);
    }
    
    /**
     * Retreive the locale of the current user.
     *
     * The locale will be identified according to the current HTTP request. If the current conext can't be found, the
     * default local will be defenied as ENGLISH
     *
     * @return the user locale
     */
    public static Locale getUserLocale() {
        FacesContext context = FacesContext.getCurrentInstance();
        
        if (context == null) {
            log.debug("getUserLocale: No faces context, returning default of en");
            return Locale.ENGLISH;
        }
        
        ExternalContext externalContext = context.getExternalContext();
        
        if (externalContext == null) {
            log.debug("getUserLocale: No external context, returning default of en");
            return Locale.ENGLISH;
        }
        
        log.debug("getUserLocale: Returning external context locale of {}", externalContext.getRequestLocale());
        return externalContext.getRequestLocale();
    }
}
