package com.anahata.util.jsf;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Faces utilities.
 * //TODO Move To AnahataUtil? I think there is a similar one we used in IBI.
 * @author Robert Nagajek
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FacesUtils {
    
    /**
     * Add a localised INFO level message. Only a summary is added.
     * 
     * @param messageKey The message summary.
     * @param args Optional parameter substitution args.
     */
    public static FacesMessage addMessageInfo(String messageKey, Object... args) {
        return addMessage(FacesMessage.SEVERITY_INFO, messageKey, null, args);
    }
    
    /**
     * Add a localised INFO level message. Only a summary is added.
     * 
     * @param messageKey The message summary.
     * @param detailKey The detail text key. 
     */
    public static FacesMessage addMessageInfo(String messageKey, String detailKey) {
        return addMessage(FacesMessage.SEVERITY_INFO, messageKey, detailKey);
    }
    
    /**
     * Add a localised WARN level message. Only a summary is added.
     * 
     * @param messageKey The message summary.
     * @param args Optional parameter substitution args.
     */
    public static FacesMessage addMessageWarn(String messageKey, Object... args) {
        return addMessage(FacesMessage.SEVERITY_WARN, messageKey, null, args);
    }
    
    /**
     * Add a localised WARN level message. Only a summary is added.
     * 
     * @param messageKey The message summary.
     * @param detailKey The detail text key. 
     */
    public static FacesMessage addMessageWarn(String messageKey, String detailKey) {
        return addMessage(FacesMessage.SEVERITY_WARN, messageKey, detailKey);
    }
    
    /**
     * Add a localised ERROR level message. Only a summary is added.
     * 
     * @param messageKey The message summary.
     * @param args Optional parameter substitution args.
     */
    public static FacesMessage addMessageError(String messageKey, Object... args) {
        return addMessage(FacesMessage.SEVERITY_ERROR, messageKey, null, args);
    }
    
    /**
     * Add a localised ERROR level message. Only a summary is added.
     * 
     * @param messageKey The message summary.
     * @param detailKey  The detail text key.
     */
    public static FacesMessage addMessageError(String messageKey, String detailKey) {
        return addMessage(FacesMessage.SEVERITY_ERROR, messageKey, detailKey);
    }
    
    /**
     * Add a localised FATAL level message. Only a summary is added.
     * 
     * @param messageKey The message summary.
     * @param args Optional parameter substitution args.
     */
    public static FacesMessage addMessageFatal(String messageKey, Object... args) {
        return addMessage(FacesMessage.SEVERITY_FATAL, messageKey, null, args);
    }
    
    /**
     * Add a localised FATAL level message. Only a summary is added.
     * 
     * @param messageKey The message summary.
     * @param detailKey  The detail text key.
     */
    public static FacesMessage addMessageFatal(String messageKey, String detailKey) {
        return addMessage(FacesMessage.SEVERITY_FATAL, messageKey, detailKey);
    }
    
    /**
     * Add a localised level message. Only a summary is added.
     * 
     * @param severity The message severity.
     * @param messageKey The message summary.
     * @param detailKey The detail text key.
     * @param args Optional parameter substitution args.
     * @return A Faces message.
     */
    private static FacesMessage addMessage(final FacesMessage.Severity severity, final String messageKey,
            final String detailKey, Object... args) {
        final String detail = detailKey == null ? null : MessageTranslator.translate(detailKey);
        
        FacesMessage msg = new FacesMessage(severity, MessageTranslator.translate(messageKey, args), detail);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return msg;
    }
}
