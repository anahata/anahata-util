package com.anahata.util.jsf;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

/**
 * Utility methods for JSF.
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
public final class JSFUtils {
    /**
     * Helper class, no constructor.
     */
    private JSFUtils() {
    }

    public static String beanReference(Class c) {
        return el(defaultBeanName(c));
    }

    /**
     * Retrieves the JSF default bean name for a given class by simply converting the class name 
     * first character to lower case.
     * 
     * @param c the class
     * @return the JSF default bean name.
     */
    public static String defaultBeanName(Class c) {
        return StringUtils.uncapitalize(c.getSimpleName());
    }

    /**
     * Converts a JSF variable to the EL format by wrapping it with: <code>#{ + var +}</code>.
     * 
     * @param var the name of the variable 
     * @return <code>#{var}</code>
     */
    public static String el(String var) {
        return "#{" + var + "}";
    }
    

}
