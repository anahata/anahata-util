package com.anahata.util.jsf.ice;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.collections.MapUtils;
import java.util.*;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
@Slf4j
public final class RowSelMap<T> implements Map<T, Boolean> {
    private Map<T, Boolean> delegate = new HashMap<T, Boolean>();

    boolean singleSelection = true;

    public RowSelMap(boolean singleSelection) {
        this.singleSelection = singleSelection;
    }

    public T getSingleSelection() {
        return MapUtils.getKeyForValue(delegate, true);
    }

    public void setSingleSelection(T t) {
        delegate.clear();
        delegate.put(t, true);
    }

    public List<T> getSelected() {
        return MapUtils.getKeysForValue(delegate, true);
    }

    //------------------------
    //Delegate methods
    //------------------------
    @Override
    public Boolean put(T key, Boolean value) {
        log.debug("RowSelMap: key={} value={}", key, value);
        return delegate.put(key, value);
    }

    @Override
    public Collection<Boolean> values() {
        return delegate.values();
    }

    @Override
    public int size() {
        return delegate.size();
    }

    @Override
    public Boolean remove(Object key) {
        return delegate.remove(key);
    }

    @Override
    public void putAll(Map<? extends T, ? extends Boolean> m) {
        delegate.putAll(m);
    }

    @Override
    public Set<T> keySet() {
        return delegate.keySet();
    }

    @Override
    public boolean isEmpty() {
        return delegate.isEmpty();
    }

    @Override
    public int hashCode() {
        return delegate.hashCode();
    }

    @Override
    public Boolean get(Object key) {
        return delegate.get(key);
    }

    @Override
    public boolean equals(Object o) {
        return delegate.equals(o);
    }

    @Override
    public Set<Entry<T, Boolean>> entrySet() {
        return delegate.entrySet();
    }

    @Override
    public boolean containsValue(Object value) {
        return delegate.containsValue(value);
    }

    @Override
    public boolean containsKey(Object key) {
        return delegate.containsKey(key);
    }

    @Override
    public void clear() {
        delegate.clear();
    }
}
