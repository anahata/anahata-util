/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.compress;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.*;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

/**
 * Zip file helpers.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ZipUtils {
    /**
     * Get a zip file entry matching a string suffix.
     *
     * @param zipFile The zip file.
     * @param suffix  The suffix.
     * @return The string value of the entry, or null if not found.
     * @throws RuntimeException If there's an error reading the zip file.
     */
    public static String getString(@NonNull byte[] zipFile, @NonNull String suffix) {
        try (ZipInputStream stream = new ZipInputStream(new ByteArrayInputStream(zipFile))) {
            ZipEntry entry;

            while ((entry = stream.getNextEntry()) != null) {
                if (entry.getName().endsWith(suffix)) {
                    return new String(IOUtils.toByteArray(stream));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    public static byte[] replaceString(@NonNull byte[] zipFile, @NonNull String suffix, @NonNull String value) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try (ZipInputStream istream = new ZipInputStream(new ByteArrayInputStream(zipFile));
                ZipOutputStream ostream = new ZipOutputStream(baos)) {
            ZipEntry entry;
            byte[] buf = new byte[1024];

            while ((entry = istream.getNextEntry()) != null) {
                if (entry.getName().endsWith(suffix)) {
                    ZipEntry newEntry = new ZipEntry(entry.getName());
                    ostream.putNextEntry(newEntry);
                    ostream.write(value.getBytes());
                } else {
                    ostream.putNextEntry(entry);
                    int len;

                    while ((len = istream.read(buf)) > 0) {
                        ostream.write(buf, 0, len);
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return baos.toByteArray();
    }

    /**
     * Compresses the given input stream to the zip file.
     *
     * @param in             InputStream that need to be compressed.
     * @param directory       Location to which the zip file need to be saved.
     * @param fileName       Name of the zip file. This will also be the name of the file inside the zip file.
     * @param actualFileExtn The file extension for the file inside the zip file.
     * @return Returns the file path if successfully compressed else returns null.
     */
    public static File compressFile(InputStream in, File directory, String fileName, String actualFileExtn) throws IOException {
        if (!directory.exists()) {
            directory.mkdirs();
        }
        final String path = fileName + ".zip";
        byte[] buffer = new byte[1024];
        final File filedst = new File(directory, path);
        try (FileOutputStream dest = new FileOutputStream(filedst);
                ZipOutputStream zip = new ZipOutputStream(new BufferedOutputStream(dest))) {
            zip.setMethod(ZipOutputStream.DEFLATED);
            zip.setLevel(Deflater.BEST_COMPRESSION);
            zip.putNextEntry(new ZipEntry(fileName + actualFileExtn));
            int length;
            while ((length = in.read(buffer)) > 0) {
                zip.write(buffer, 0, length);
            }
        } 
        return filedst;
    }

}
