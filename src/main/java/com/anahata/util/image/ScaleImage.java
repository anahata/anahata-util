package com.anahata.util.image;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;
import javax.imageio.ImageIO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;

/**
 * Image scaling utility methods.
 *
 * @author Vijay
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ScaleImage {

    /**
     * Scales with method = speed and mode = AUTOMATIc.
     *
     * @param inFile
     * @param maxWidth
     * @param maxHeight
     * @param format
     * @return
     * @throws IOException
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public static byte[] scale(byte[] inFile, int maxWidth, int maxHeight, String format) throws IOException,
            InterruptedException,
            ExecutionException {
        return scale(inFile, Method.SPEED, Mode.AUTOMATIC, maxWidth, maxHeight, format);
    }

    /**
     * Reads an image in a file and creates a thumbnail in another file. largestDimension is the largest dimension of
     * the thumbnail, the other dimension is scaled accordingly. Utilises weighted stepping method to gradually reduce
     * the image size for better results, i.e. larger steps to start with then smaller steps to finish with. Note:
     * always writes a JPEG because GIF is protected or something - so always make your outFilename end in 'jpg'. PNG's
     * with transparency are given white backgrounds
     *
     * @param inFile           The image bytes.
     * @param largestDimension The largest width or height to scale to.
     * @param format           The image format such as "jpeg".
     * @return The resized image in the given format.
     * @throws IOException If an error occurs reading or writing bytes.
     */
    public static byte[] scale(byte[] inFile, Method method, Mode mode, int maxWidth, int maxHeight, String format)
            throws IOException,
            InterruptedException,
            ExecutionException {
        BufferedImage inImage = ImageIO.read(new ByteArrayInputStream(inFile));

        double scale = calculateScale(inImage, maxWidth, maxHeight);
        // Find biggest dimension.
        if (scale < 1.0d) {
            return resize(inImage, method, mode, maxWidth, maxHeight, format);
        }

        return inFile;
    }

    /**
     * Reads an image in a file and creates a thumbnail in another file. largestDimension is the largest dimension of
     * the thumbnail, the other dimension is scaled accordingly. Utilises weighted stepping method to gradually reduce
     * the image size for better results, i.e. larger steps to start with then smaller steps to finish with. Note:
     * always writes a JPEG because GIF is protected or something - so always make your outFilename end in 'jpg'. PNG's
     * with transparency are given white backgrounds
     *
     * @param is        The input stream
     * @param maxWidth
     * @param maxHeight
     * @param format    The image format such as "jpeg".
     * @return The resized image in the given format.
     * @throws IOException If an error occurs reading or writing bytes.
     */
    public static byte[] scale(InputStream is, int maxWidth, int maxHeight, String format) throws IOException,
            InterruptedException,
            ExecutionException {

        byte[] barr = IOUtils.toByteArray(is);

        return scale(barr, maxWidth, maxHeight, format);
    }

    private static byte[] resize(BufferedImage inImage, Method method, Mode mode, int maxWidth, int maxHeight,
            String format) throws IOException,
            InterruptedException,
            ExecutionException {
        BufferedImage outImage;

        outImage = Scalr.resize(inImage, method, mode, maxWidth, maxHeight);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(outImage, format, baos);
        return baos.toByteArray();
    }

    private static double calculateScale(BufferedImage inImage, int maxWidth, int maxHeight) {

        int largestDimension = Math.max(maxWidth, maxHeight);
        if (inImage.getWidth(null) == -1 || inImage.getHeight(null) == -1) {
            throw new IllegalArgumentException("In file can not be read");
        }

        double scale;
        if (inImage.getWidth(null) > inImage.getHeight(null)) {
            scale = (double)largestDimension / (double)inImage.getWidth(null);
        } else {
            scale = (double)largestDimension / (double)inImage.getHeight(null);
        }
        return scale;
    }
}
