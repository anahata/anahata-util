package com.anahata.util.lang;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.*;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import static org.apache.commons.lang3.StringUtils.*;

/**
 * String utility classes that provide additional functionality to the Apache Commons lang3 StringUtils class.
 *
 * @author Robert Nagajek
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnahataStringUtils {
    /**
     * Removes all non numerical characters from a string.
     *
     * @param string the strings
     * @return true if they are all null or empty as per commons-lang StringUtils.isEmpty(String)
     */
    public static String digitsOnly(String string) {
        Validate.notNull(string, "The stringarray is empty");
        StringBuilder ret = new StringBuilder();

        for (char c : string.toCharArray()) {
            if (Character.isDigit(c)) {
                ret.append(c);
            }
        }

        return ret.toString();
    }
    
    /**
     * Removes all non numerical characters from a string.
     *
     * @param string the strings
     * @return true if they are all null or empty as per commons-lang StringUtils.isEmpty(String)
     */
    public static String lettersOnly(String string) {
        Validate.notNull(string, "The stringarray is empty");
        StringBuilder ret = new StringBuilder();

        for (char c : string.toCharArray()) {
            if (Character.isAlphabetic(c)) {
                ret.append(c);
            }
        }

        return ret.toString();
    }

    /**
     * Checks if all tokens in a given array of strings are all empty.
     *
     * @param strings the strings
     * @return true if they are all null or empty as per commons-lang StringUtils.isEmpty(String)
     */
    public static boolean isAllEmpty(String... strings) {
        Validate.isTrue(strings.length > 0, "The string array is empty");

        for (String string : strings) {
            if (!StringUtils.isEmpty(string)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Find a string within a string a remove it, case insensitive. Handles null values.
     *
     * @param str    The string to search.
     * @param remove The string to remove.
     * @return The updated string.
     */
    public static String removeIgnoreCase(String str, String remove) {
        int index = StringUtils.indexOfIgnoreCase(str, remove);

        if (index < 0) {
            return str;
        }

        return str.substring(0, index) + str.substring(index + remove.length());
    }

    /**
     * Given a list of strings, return common prefixes amongst them.
     *
     * @param strs     The strings. Can be null.
     * @param minMatch The minimum match length. If null, not used.
     * @return Common prefixes. Never null, this will be an empty list if there is no result.
     */
    public static List<String> getCommonPrefixes(List<String> strs, Integer minMatch) {
        if (strs == null) {
            return Collections.emptyList();
        }

        int size = strs.size();
        WeightedWords weightings = getWeightings(strs, minMatch);
        int totalWeight = weightings.getTotalWeight();

        while (totalWeight > size) {
            size = weightings.size();
            weightings = getWeightings(weightings.getWords(), minMatch);
            totalWeight = weightings.getTotalWeight();
        }

        return weightings.getWords();
    }

    /**
     * Replaces all occurrences of characters in a given array for a replacement token.
     *
     * @param source            the source string. Required,
     * @param charsToBeReplaced the chars to be replaced. Required.
     * @param replacement       the replacement for each illegal character. Required.
     * @return source with all specified characters replaced with replacement
     */
    public static String replaceChars(String source, char[] charsToBeReplaced, String replacement) {
        Validate.notNull(source, "source is required");
        Validate.notNull(charsToBeReplaced, "charsToBeReplaced is required");
        Validate.notNull(replacement, "replacement is required");

        String ret = source;
        for (char c : charsToBeReplaced) {
            ret = ret.replace(String.valueOf(c), replacement);
        }
        return ret;
    }

    /**
     * Replace the start of a string by using a case-insensitive match of a prefix, with a replacement string. Handles
     * nulls.
     *
     * @param str         The string to check, can be null.
     * @param prefix      The prefix to check for the start of the string, can be null.
     * @param replacement The replacement string, can be null.
     * @return
     */
    public static String replaceFirstIgnoreCase(String str, String prefix, String replacement) {
        if (prefix != null && StringUtils.startsWithIgnoreCase(str, prefix)) {
            return replacement + str.substring(prefix.length());
        }

        return str;
    }

    /**
     * Given a string, search for a string within it, and capitalize the character immediately following it. For
     * example, 'Mcdonald' becomes 'McDonald'.
     *
     * @param str    The string to search. Can be null.
     * @param search The search string. Can be null.
     * @return The result.
     */
    public static String capitilizeNext(String str, String search) {
        if (str == null || search == null) {
            return str;
        }

        StringBuilder sb = new StringBuilder();
        int start = 0;
        int mcPos = str.indexOf(search);

        while (mcPos >= 0) {
            int end = mcPos + search.length();
            sb.append(str.substring(start, end));
            sb.append(str.substring(end, end + 1).toUpperCase());
            start = end + 1;
            mcPos = str.indexOf(search, start);
        }

        sb.append(str.substring(start));
        return sb.toString();
    }

    /**
     * Replaces all occurrences of Strings within another String, with a case-insensitive match.
     * <p>
     * TODO Review the performance of this.
     *
     * @param text            The text to search, no-op if null.
     * @param searchList      The Strings to search for, no-op if null.
     * @param replacementList The Strings to replace them with, no-op if null.
     * @return The text with any replacements processed, null if null String input.
     * @throws IllegalArgumentException If searchList and replacementList are not the same length (null is ok, and/or
     *                                  size 0).
     */
    public static String replaceEachIgnoreCase(String text, String[] searchList, String[] replacementList) {
        if (text == null || searchList == null || replacementList == null || searchList.length == 0
                || replacementList.length == 0) {
            return text;
        }

        Validate.isTrue(searchList.length == replacementList.length,
                "The searchList and replacementList params must be equal in length, they were %d and %d",
                searchList.length, replacementList.length);

        for (int i = 0; i < searchList.length; i++) {
            final String search = searchList[i];
            final int searchLength = search.length();
            final String replace = replacementList[i];
            int pos = StringUtils.indexOfIgnoreCase(text, search);

            while (pos >= 0) {
                text = text.substring(0, pos) + replace + text.substring(pos + searchLength);
                pos = StringUtils.indexOfIgnoreCase(text, search, pos + searchLength);
            }
        }

        return text;
    }

    /**
     * Same as String.split() but returns delimiter. Taken from
     * http://stackoverflow.com/questions/275768/is-there-a-way-to-split-strings-with-string-split-and-include-the-delimiters
     *
     * @param s       the string
     * @param pattern the pattern
     * @return the tokens
     */
    public static List<String> split(String s, String pattern) {
        assert s != null;
        assert pattern != null;
        return split(s, Pattern.compile(pattern));
    }

    /**
     * Same as String.split() but returns delimiter. Taken from
     * http://stackoverflow.com/questions/275768/is-there-a-way-to-split-strings-with-string-split-and-include-the-delimiters
     *
     * @param s       the string
     * @param pattern the pattern
     * @return the tokens
     */
    public static List<String> split(String s, Pattern pattern) {
        assert s != null;
        assert pattern != null;
        Matcher m = pattern.matcher(s);
        List<String> ret = new ArrayList<>();
        int start = 0;
        while (m.find()) {
            ret.add(s.substring(start, m.start()));
            ret.add(m.group());
            start = m.end();
        }
        ret.add(start >= s.length() ? "" : s.substring(start));
        return ret;
    }

    /**
     * Compare two strings, ignoring case. Null safe. null is assumed to be less than a non-null value.
     *
     * @param str1 The first string.
     * @param str2 The second string.
     * @return A negative value if str1 &lt; str2, zero if str1 = str2 and a positive value if str1 &gt; str2.
     */
    public static int compareToIgnoreCase(String str1, String str2) {
        return compareToIgnoreCase(str1, str2, false);
    }

    /**
     * Compare two strings, ignoring case. Null safe.
     *
     * @param str1        The first string.
     * @param str2        The second string.
     * @param nullGreater If true, null is greater than non-null, or if false null is less than non-null.
     * @return A negative value if str1 &lt; str2, zero if str1 = str2 and a positive value if str1 &gt; str2.
     */
    public static int compareToIgnoreCase(String str1, String str2, boolean nullGreater) {
        if (str1 == null && str2 == null) {
            return 0;
        }

        if (str1 == null) {
            return nullGreater ? 1 : -1;
        }

        if (str2 == null) {
            return nullGreater ? -1 : 1;
        }

        return str1.compareToIgnoreCase(str2);
    }

    //== private methods ==============================================================================================
    private static WeightedWords getWeightings(List<String> strs, Integer minMatch) {
        Set<String> prefixes = new HashSet<>();
        int size = strs.size();

        for (int i = 0; i < size - 1; i++) {
            for (int j = i + 1; j < size; j++) {
                String prefix = StringUtils.getCommonPrefix(strs.get(i).toUpperCase(), strs.get(j).toUpperCase());

                if (!StringUtils.isBlank(prefix) && (minMatch == null || prefix.length() >= minMatch)) {
                    prefixes.add(prefix);
                }
            }
        }

        // Add unmatched entries.

        List<String> unmatched = new ArrayList<>(strs);

        for (Iterator<String> i = unmatched.iterator(); i.hasNext();) {
            String str = i.next();

            for (String prefix : prefixes) {
                if (str.toUpperCase().startsWith(prefix.toUpperCase())) {
                    i.remove();
                    break;
                }
            }
        }

        prefixes.addAll(unmatched);
        WeightedWords result = new WeightedWords();

        for (String prefix : prefixes) {
            result.add(countMatches(strs, prefix));
        }

        return result;
    }

    private static WeightedWord countMatches(List<String> strs, String prefix) {
        int count = 0;

        for (String str : strs) {
            if (str.toUpperCase().startsWith(prefix.toUpperCase())) {
                count++;
            }
        }

        return new WeightedWord(prefix, count);
    }

    //=================================================================================================================
    private static class WeightedWords {
        private SortedSet<WeightedWord> wws = new TreeSet<>();

        public void add(WeightedWord ww) {
            wws.add(ww);
        }

        public List<String> getWords() {
            List<String> result = new ArrayList<>(wws.size());

            for (WeightedWord ww : wws) {
                result.add(ww.getWord());
            }

            return result;
        }

        public int getTotalWeight() {
            int total = 0;

            for (WeightedWord ww : wws) {
                total += ww.getWeight();
            }

            return total;
        }

        public int size() {
            return wws.size();
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            boolean first = true;

            for (WeightedWord ww : wws) {
                if (first) {
                    first = false;
                } else {
                    sb.append('\n');
                }

                sb.append(ww.getWord());
                sb.append('\t');
                sb.append(ww.getWeight());
            }

            return sb.toString();
        }
    }

    @AllArgsConstructor
    @EqualsAndHashCode
    @Getter
    private static class WeightedWord implements Comparable<WeightedWord> {
        private String word;

        private Integer weight;

        @Override
        public int compareTo(WeightedWord o) {
            int result = o.weight.compareTo(weight);

            if (result == 0) {
                result = word.compareTo(o.word);
            }

            return result;
        }

        @Override
        public String toString() {
            return word + "\t" + weight;
        }
    }

    /**
     * Converts a string to camel case.
     *
     * @param source     - the string we are converting
     * @param splitToken - the token to split the source string by (e.g. " " or "_")
     * @return the camel cased string.
     */
    public static String toJavaPropertyName(final String source, final String splitToken) {
        if (source == null) {
            return null;
        }

        final StringBuilder ret = new StringBuilder(source.length());

        for (final String word : source.split(splitToken)) {
            if (!word.isEmpty()) {
                String firstChar = word.substring(0, 1);
                if (ret.length() > 0) {
                    firstChar = firstChar.toUpperCase();
                }
                ret.append(firstChar);
                ret.append(word.substring(1).toLowerCase());
            }
        }

        return ret.toString();
    }
    
    /**
     * Gets the Levenstain distance of two strings expressed as percentage (0-100).
     * 
     * @param s one string
     * @param t the other string
     * @return the distance
     */
    public static int getLevenshteinDistancePctg(String s, String t) {
        s = Nvl.nvl(s);
        t = Nvl.nvl(t);
        
        int n = s.length();
        int m = t.length();

        if (n == 0) {
            return m;
        } else if (m == 0) {
            return n;
        }

        int p[] = new int[n + 1];
        int d[] = new int[n + 1];
        int _d[];
        int i;
        int j;

        char t_j;

        int cost;

        for (i = 0; i <= n; i++) {
            p[i] = i;
        }

        for (j = 1; j <= m; j++) {
            t_j = t.charAt(j - 1);
            d[0] = j;

            for (i = 1; i <= n; i++) {
                cost = s.charAt(i - 1) == t_j ? 0 : 1;

                d[i] = Math.min(Math.min(d[i - 1] + 1, p[i] + 1), p[i - 1] + cost);
            }
            _d = p;
            p = d;
            d = _d;
        }

        //Determine percentage difference
        double levNum = (double)p[n];
        double percent = (levNum / Math.max(s.length(), t.length())) * 100;
        int percentDiff = (int)percent;

        return percentDiff;
    }
    
    /**
     * <p>Check if a CharSequence ends with any of an array of specified strings, ignoring case.</p>
     *
     * <pre>
     * StringUtils.endsWithAnyIgnoreCase(null, null)      = false
     * StringUtils.endsWithAnyIgnoreCase(null, new String[] {"abc"})  = false
     * StringUtils.endsWithAnyIgnoreCase("abcxyz", null)     = false
     * StringUtils.endsWithAnyIgnoreCase("abcxyz", new String[] {""}) = true
     * StringUtils.endsWithAnyIgnoreCase("abcxyz", new String[] {"xyz"}) = true
     * StringUtils.endsWithAnyIgnoreCase("abcxyz", new String[] {null, "xyz", "ABC"}) = true
     * </pre>
     *
     * @param string        the CharSequence to check, may be null
     * @param searchStrings the CharSequences to find, may be null or empty
     * @return {@code true} if the CharSequence ends with any of the the prefixes, case insensitive, or
     *         both {@code null}
     */
    public static boolean endsWithAnyIgnoreCase(CharSequence string, CharSequence... searchStrings) {
        if (isEmpty(string) || ArrayUtils.isEmpty(searchStrings)) {
            return false;
        }
        
        for (CharSequence searchString : searchStrings) {
            if (StringUtils.endsWithIgnoreCase(string, searchString)) {
                return true;
            }
        }
        
        return false;
    }
}
