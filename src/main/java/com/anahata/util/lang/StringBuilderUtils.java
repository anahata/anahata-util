package com.anahata.util.lang;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

/**
 * Utils for adding text to a StringBuilder.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class StringBuilderUtils {
    /**
     * Append a list of strings using the given separator. Only non-null values are appended. The separator will also
     * prepend the first string value if the existing StringBuilder is not empty.
     * 
     * @param sb        The StringBuilder. Required.
     * @param prefix    The value to prepend if the StringBuilder is not empty.
     * @param separator The separator. Required.
     * @param values    The values to add.
     * @throws NullPointerException If sb or separator are null.
     */
    public static void append(@NonNull StringBuilder sb, @NonNull String prefix, @NonNull String separator,
            String... values) {
        if (values != null) {
            boolean first = true;
            
            for (String value : values) {
                if (value != null) {
                    if (sb.length() > 0) {
                        if (first) {
                            sb.append(prefix);
                            first = false;
                        } else {
                            sb.append(separator);
                        }
                    }
                    
                    sb.append(value);
                }
            }
        }
    }
    
    /**
     * Append a list of strings using the given separator. Only non-blank values are appended (not null, an empty
     * string or a blank string). The separator will also prepend the first string value if the existing StringBuilder
     * is not empty.
     * 
     * @param sb        The StringBuilder. Required.
     * @param prefix    The value to prepend if the StringBuilder is not empty.
     * @param separator The separator. Required.
     * @param values    The values to add.
     * @throws NullPointerException If sb or separator are null.
     */
    public static void appendNotBlank(@NonNull StringBuilder sb, @NonNull String prefix, @NonNull String separator,
            String... values) {
        if (values != null) {
            boolean first = true;
            
            for (String value : values) {
                if (StringUtils.isNotBlank(value)) {
                    if (sb.length() > 0) {
                        if (first) {
                            sb.append(prefix);
                            first = false;
                        } else {
                            sb.append(separator);
                        }
                    }
                    
                    sb.append(value);
                }
            }
        }
    }
}
