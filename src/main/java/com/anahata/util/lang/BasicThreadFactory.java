/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.lang;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.concurrent.ThreadFactory;
import lombok.Getter;

/**
 * Thread factory implementation that allows naming of threads by a given name plus a counter, optionally specifying
 * whether threads should be daemons or not.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public final class BasicThreadFactory implements ThreadFactory {
    /**
     * The name prefix of the threads.
     */
    @Getter
    private final String name;

    /**
     * Whether created threads are daemons.
     */
    @Getter
    private final boolean daemon;

    /**
     * Whether created threads are daemons.
     */
    @Getter
    private final int priority;

    /**
     * The number of Threads created.
     */
    @Getter
    private volatile int count = 0;

    /**
     *
     * @param name the threads name.
     */
    /**
     * Named threads and daemon / not daemon threads
     *
     * @param name
     * @param daemon
     */
    public BasicThreadFactory(String name, boolean daemon, int priority) {
        this.name = name;
        this.daemon = daemon;
        this.priority = priority;
    }

    /**
     * Named daemon threads.
     *
     * @param name the threads name.
     */
    public BasicThreadFactory(String name) {
        this(name, true, Thread.NORM_PRIORITY);
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setName(name + "-" + count++);
        t.setDaemon(daemon);
        return t;
    }

}
