package com.anahata.util.lang;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.Validate;

/**
 * Number utilities.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnahataNumberUtils {
    /**
     * Determine if a value can be assigned to a given Number instance.
     *
     * @param numClass The Number class.
     * @param value    The value.
     * @return true if the value could be assigned to the Number, false if not. A null value returns true.
     */
    public static boolean isAssignableToNumber(Class<? extends Number> numClass, String value) {
        Validate.notNull(numClass);

        if (value == null) {
            return true;
        }

        try {
            if (Integer.class.isAssignableFrom(numClass)) {
                Integer.valueOf(value);
            } else if (Long.class.isAssignableFrom(numClass)) {
                Long.valueOf(value);
            } else if (Float.class.isAssignableFrom(numClass)) {
                Float.valueOf(value);
            } else if (Double.class.isAssignableFrom(numClass)) {
                Double.valueOf(value);
            } else if (BigDecimal.class.isAssignableFrom(numClass)) {
                BigDecimal val = new BigDecimal(value);
            } else {
                throw new IllegalArgumentException("Unknown class type: " + numClass.getName());
            }

            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Multiply a BigDecimal by an integer value and return the new value with a scale set. This accepts null
     * parameters.
     *
     * @param value        The BigDecimal value, can be null.
     * @param multiplicand The multiplicand, can be null.
     * @param scale        The scale.
     * @return The multiplied value at the set scale, or null if value or multiplicand is null.
     */
    public static BigDecimal multiply(BigDecimal value, Integer multiplicand, int scale) {
        if (value == null || multiplicand == null) {
            return null;
        }

        return value.multiply(new BigDecimal(multiplicand)).setScale(scale);
    }

    /**
     * Multiply a BigDecimal by an integer value and return the new value with a scale set. This accepts null
     * parameters.
     *
     * @param value   The BigDecimal value, can be null.
     * @param divisor The multiplicand, can be null.
     * @param scale   The scale.
     * @return The multiplied value at the set scale, or null if value or multiplicand is null.
     */
    public static BigDecimal divide(BigDecimal value, Integer divisor, int scale) {
        if (value == null || divisor == null) {
            return null;
        }

        return value.divide(new BigDecimal(divisor)).setScale(scale, BigDecimal.ROUND_HALF_UP);
    }
    
    /**
     * Counts the number of decimal places in a double.
     * 
     * @param d
     * @return 
     */
    public static int countDecimals(@NonNull Double d) {
        String s = d.toString();
        return countDecimals(s);
    }
    
    /**
     * Counts the number of decimals in a string that represents a number.
     * 
     * @param s
     * @return 
     */
    public static int countDecimals(@NonNull String s) {
        DecimalFormat decFormat = new DecimalFormat();
        DecimalFormatSymbols decSymbols = decFormat.getDecimalFormatSymbols();           
        char decimalSeparator = decSymbols.getDecimalSeparator();
        int idx = s.indexOf(decimalSeparator);
        if (idx == -1) {
            return 0;
        } else {
            return s.length() - (idx + 1);
        }
    }
}
