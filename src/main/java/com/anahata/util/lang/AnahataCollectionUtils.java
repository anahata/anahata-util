/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.lang;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Extension of commons collections Collectionutils class.
 * 
 * @author pablo7
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnahataCollectionUtils {
    /**
     * Splits a collation into chunks of a given size.
     * 
     * //TODO unit test.
     * 
     * @param <T> the col type
     * @param col the collection
     * @param maxSize the chunk max size
     * @return the list of collections.
     */
    public static <T> List<List<T>> split(Collection<T> col, int maxSize) {
        List<T> list = new ArrayList<>(col);
        if (col.size() <= maxSize) {
            return Collections.singletonList(list);
        } else {
            List<List<T>> ret = new ArrayList<>();
            int startIdx = 0;
            int endIdx = maxSize;
            while (startIdx < endIdx) {
                List<T> chunk = list.subList(startIdx, endIdx);
                ret.add(chunk);
                startIdx = endIdx;
                endIdx = Math.min(list.size(), (startIdx + maxSize));
            }
            return ret;
        }
    }
}
