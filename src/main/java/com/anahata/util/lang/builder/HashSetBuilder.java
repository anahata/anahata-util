package com.anahata.util.lang.builder;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.HashSet;
/**
 * Build a HashSet. Useful for auto-initializing sets in class attributes.
 *
 * @param <E> The contained value type.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class HashSetBuilder<E> {
    private HashSet<E> set = new HashSet<>();
    
    /**
     * Add a HashSet value.
     * 
     * @param value The value.
     * @return This.
     */
    public HashSetBuilder<E> add(E value) {
        set.add(value);
        return this;
    }
    
    public HashSet<E> build() {
        return set;
    }
}
