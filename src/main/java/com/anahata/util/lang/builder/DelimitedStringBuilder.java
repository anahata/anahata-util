/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.lang.builder;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

/**
 * Build a string with a delimiter.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public final class DelimitedStringBuilder {
    private final StringBuilder sb;
    
    private final String delimiter;
    
    private boolean first;
    
    /**
     * If this flag is set to true, blank strings or null objects would not cause the delimiter to be appended. Default value is false.
     */    
    private final boolean ignoreBlanks;
    
    /*
     * Constructor specifying the delimiter and {@link #ignoreBlaks}
     */
    public DelimitedStringBuilder(@NonNull String delimiter) {
        this(delimiter, false);
    }
    
    /**
     * 
     * @param delimiter the delimiter
     * @param ignoreBlanks see {@link #ignoreBlanks}
     */
    public DelimitedStringBuilder(@NonNull String delimiter, boolean ignoreBlanks) {        
        sb = new StringBuilder();
        this.delimiter = delimiter;
        first = true;
        this.ignoreBlanks = ignoreBlanks;
    }

    public DelimitedStringBuilder append(Object obj) {
        if (!ignoreBlanks || obj != null) {
            appendDelimiter();
            sb.append(obj);
        }
        return this;
    }

    public DelimitedStringBuilder append(String str) {
        if (!ignoreBlanks || !StringUtils.isBlank(str)) {
            appendDelimiter();
            sb.append(str);
        }
        return this;
    }

    public DelimitedStringBuilder append(StringBuffer sb) {
        if (!ignoreBlanks || sb.length() > 0) {
            appendDelimiter();
            this.sb.append(sb);
        }
    
        return this;
    }

    public DelimitedStringBuilder append(CharSequence s) {
        if (!ignoreBlanks || s.length() > 0) {
            appendDelimiter();
            sb.append(s);
        }
        return this;
    }

    public DelimitedStringBuilder append(CharSequence s, int start, int end) {
        if (!ignoreBlanks || s.length() > 0) {
            sb.append(s, start, end);
            appendDelimiter();
        }
        
        return this;
    }

    public DelimitedStringBuilder append(char[] str) {
        if (!ignoreBlanks || str.length > 0) {
            appendDelimiter();
            sb.append(str);
        }
        return this;
    }

    public DelimitedStringBuilder append(char[] str, int offset, int len) {
        if (!ignoreBlanks || str.length > 0) {//could check offset and len are not the same
            appendDelimiter();
            sb.append(str, offset, len);
        }
        return this;
    }

    public DelimitedStringBuilder append(boolean b) {
        appendDelimiter();
        sb.append(b);
        return this;
    }

    public DelimitedStringBuilder append(char c) {
        appendDelimiter();
        sb.append(c);
        return this;
    }

    public DelimitedStringBuilder append(int i) {
        appendDelimiter();
        sb.append(i);
        return this;
    }

    public DelimitedStringBuilder append(long lng) {
        appendDelimiter();
        sb.append(lng);
        return this;
    }

    public DelimitedStringBuilder append(float f) {
        appendDelimiter();
        sb.append(f);
        return this;
    }

    public DelimitedStringBuilder append(double d) {
        appendDelimiter();
        sb.append(d);
        return this;
    }

    @Override
    public String toString() {
        return sb.toString();
    }
    
    private void appendDelimiter() {
        if (first) {
            first = false;
        } else {
            sb.append(delimiter);
        }
    }
}
