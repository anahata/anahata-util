package com.anahata.util.lang.builder;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.HashMap;

/**
 * Build a HashMap. Useful for auto-initializing maps in class attributes.
 *
 * @param <K> The key type.
 * @param <V> The value type.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class HashMapBuilder<K, V> {
    private HashMap<K, V> map = new HashMap<>();
    
    /**
     * Add a key / value pair to the HashMap.
     * 
     * @param key   The key value.
     * @param value The value.
     * @return This.
     */
    public HashMapBuilder<K, V> add(K key, V value) {
        map.put(key, value);
        return this;
    }
    
    /**
     * Build the HashMap.
     * 
     * @return The HashMap.
     */
    public HashMap<K, V> build() {
        return map;
    }
}
