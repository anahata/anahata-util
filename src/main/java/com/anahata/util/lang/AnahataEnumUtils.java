package com.anahata.util.lang;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.Validate;

/**
 * Extension to common lang EnumUtils.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnahataEnumUtils {
    /**
     * Checks if a given value exists amongst a list of options.
     *
     * @param value   the value
     * @param options the options
     * @return true if value is one of options.
     */
    public static <T extends Enum> boolean in(T value, T... options) {
        for (T object : options) {
            if (value == object) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets a list with all the enum values of an enum except the excluded values.
     *
     * @param <T>       the enum type
     * @param enumClass the enum class
     * @param excludes  the values to be excluded
     * @return the collection without the excludes.
     */
    public static <T extends Enum> List<T> exclude(Class<T> enumClass, T... excludes) {

        List<T> ret = EnumUtils.getEnumList(enumClass);
        for (T t : excludes) {
            Validate.isTrue(ret.contains(t), "Enum Class %s does not contain value %s", enumClass.getSimpleName(), t);
            ret.remove(t);
        }

        return ret;

    }
}
