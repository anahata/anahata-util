package com.anahata.util.lang;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import java.math.BigDecimal;
import java.time.Duration;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Utility methods to convert nulls to default values (Zeros, empty strings, etc).
 *
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Nvl {

    /**
     * Returns
     * <code>BigDecimal.ZERO</code> if
     * <code>null</code>..
     *
     * @param bd the <code>BigDecimal</code>
     * @return <code>BigDecimal.ZERO</code> if  <code>null</code>..
     */
    public static BigDecimal nvl(BigDecimal bd) {
        return bd == null ? BigDecimal.ZERO : bd;
    }

    /**
     * Returns
     * <code>new char[0]</code> if
     * <code>null</code>..
     *
     * @param carr the <code>char[]</code>
     * @return <code>new char[0]</code> if <code>null</code>..
     */
    public static char[] nvl(char[] carr) {
        return carr == null ? new char[0] : carr;
    }

    /**
     * Returns
     * <code>0f</code> if
     * <code>null</code>..
     *
     * @param f the Float
     * @return <code>0f</code> if  <code>null</code>..
     */
    public static Float nvl(Float f) {
        return f == null ? 0f : f;
    }

    /**
     * Returns
     * <code>0d</code> if
     * <code>null</code>
     *
     * @param d the Double
     * @return <code>0d</code> if  <code>null</code>..
     */
    public static Double nvl(Double d) {
        return d == null ? 0d : d;
    }

    /**
     * Returns
     * <code>0L</code> if
     * <code>null</code>..
     *
     * @param l the Long
     * @return <code>0L</code> if  <code>null</code>..
     */
    public static Long nvl(Long l) {
        return l == null ? 0L : l;
    }

    /**
     * Returns
     * <code>0</code> if
     * <code>null</code>.
     *
     * @param i the Integer
     * @return <code>0L</code> if null.
     */
    public static Integer nvl(Integer i) {
        return i == null ? 0 : i;
    }

    /**
     * Returns
     * <code>""</code> if
     * <code>null</code>.
     *
     * @param s the String
     * @return <code>"</code> if null.
     */
    public static String nvl(String s) {
        return s == null ? "" : s;
    }

    /**
     * Returns
     * <code>0</code> if
     * <code>null</code>.
     *
     * @param d the Duration
     * @return <code>0</code> if null.
     */
    public static Duration nvl(Duration d) {
        return d == null ? Duration.ZERO : d;
    }

}
