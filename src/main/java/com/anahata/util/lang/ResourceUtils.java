package com.anahata.util.lang;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.net.URL;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

/**
 * Resource utilities.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ResourceUtils {
    /**
     * Get a resource from the classpath.
     * 
     * @param name The resource name. This is the absolute path, not starting with a slash, e.g. com/foo/bar/Fred.png.
     *             Required.
     * @return The URL for the resource, or null if not found.
     * @throws NullPointerException If any arg is null.
     */
    public static URL getResource(String name) {
        Validate.notNull(name);
        URL url = Thread.currentThread().getContextClassLoader().getResource(name);
        
        if (url == null) {
            url = ResourceUtils.class.getResource(name);
        }
        
        if (url == null) {
            url = ResourceUtils.class.getClassLoader().getResource(name);
        }
        
        if (url == null) {
            url = ClassLoader.getSystemResource(name);
        }
        
        return url;
    }
    
    /**
     * Get a resource from the classpath using the given classloader as first preference.
     * 
     * @param name        The resource name. This is the absolute path, not starting with a slash, e.g.
     *                    com/foo/bar/Fred.png. Required.
     * @param classLoader The classloader to load objects from. Required.
     * @return The URL for the resource, or null if not found.
     * @throws NullPointerException If any arg is null.
     */
    public static URL getResource(String name, ClassLoader classLoader) {
        Validate.notNull(name);
        Validate.notNull(classLoader);
        URL url = classLoader.getResource(name);
        
        if (url == null) {
            url = Thread.currentThread().getContextClassLoader().getResource(name);
        }
        
        if (url == null) {
            url = ResourceUtils.class.getClassLoader().getResource(name);
        }
        
        if (url == null) {
            url = ClassLoader.getSystemResource(name);
        }
        
        return url;
    }
}
