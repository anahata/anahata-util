package com.anahata.util.lang;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 * Utility methods to transform words: camelcasing, etc.
 *
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnahataWordUtils {

    /**
     * Converts 'fieldNumberOne to 'Field Number One'.
     *
     * @param s the string in camel case form
     * @return the humanised String
     */
    public static String humaniseCamelCase(String s) {
        String[] arr = StringUtils.splitByCharacterTypeCamelCase(s);
        arr[0] = StringUtils.capitalize(arr[0]);
        String ret = StringUtils.join(arr, " ");
        return ret;
    }

    /**
     * Capitalize names in a string. This will convert, for example, "JOE BLOGGS" to "Joe Bloggs". Special cases are
     * handled, e.g. "RONALD MCDONALD" becomes "Ronald McDonald", "JOSEPH O'BRIEN' becomes "Joseph O'Brien".
     *
     * @param str The string. Can be null.
     * @return The capitalized string.
     */
    public static String capitalizeNames(String str) {
        if (str == null) {
            return str;
        }

        str = org.apache.commons.lang3.text.WordUtils.capitalizeFully(str);
        str = AnahataStringUtils.capitilizeNext(str, " Mc");
        str = AnahataStringUtils.capitilizeNext(str, " O'");
        return str;
    }
}
