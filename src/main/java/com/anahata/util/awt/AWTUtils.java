/*
 *  Copyright © - 2013 Anahata Technologies.
 */

package com.anahata.util.awt;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;
/**
 *
 * @author pablo
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class AWTUtils {
    public static List<byte[]> snapshotAllMonitors() {
        return snapshotAllMonitors(null);
    }
    
    public static List<byte[]> snapshotAllMonitors(Integer maxSize) {
        long ts = System.currentTimeMillis();
        List<byte[]> ret = new ArrayList<>();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        for (GraphicsDevice gd : ge.getScreenDevices()) {
            try {
                log.debug("Taking snapshot of: {}, conf =",gd, gd.getDefaultConfiguration().getBounds());
                Robot r = new Robot(gd);                
                BufferedImage bi = r.createScreenCapture(gd.getDefaultConfiguration().getBounds());
                if (maxSize != null) {
                    bi = Scalr.resize(bi, Method.SPEED, Mode.AUTOMATIC, maxSize);
                }
                ret.add(toPNG(bi));
            } catch (Exception e) {
                log.warn("Could not take screenshot", e);
            }
        }
        ts = System.currentTimeMillis() - ts;
        log.info("snapshotAllMonitors maxSize={} took{}", maxSize, ts);
        return ret;
    }
    
    public static byte[] toPNG(BufferedImage bi) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        try {
            ImageIO.write(bi, "png", os);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        byte[] result = os.toByteArray();
        return result;
    }
    
    public static void main(String[] args) {
        snapshotAllMonitors();
    }
}
