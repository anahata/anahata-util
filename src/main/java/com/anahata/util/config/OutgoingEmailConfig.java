package com.anahata.util.config;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.props.StructuredProperties;
import java.util.Properties;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

/**
 * Configuration for sending emails. Requires the following properties to be passed to the constructor:
 *
 * <ul>
 * <li>host</li>
 * <li>port</li>
 * <li>ssl</li>
 * <li>user</li>
 * <li>password</li>
 * <li>from</li>
 * <li>to</li>
 * <li>cc</li>
 * <li>subject</li>
 * </ul>
 *
 * @author Pablo Rodriguez <pablo@anahata-it.com.au>
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OutgoingEmailConfig {
    private String host;

    private int port;

    private boolean ssl;

    private boolean starttlsEnabled;

    private boolean starttlsRequired;

    private String user;

    private String password;

    private String subject;

    private String from;

    private String replyTo;

    private String to;

    private String cc;

    private String bcc;

    private boolean imagesEnabled;

    /**
     * If set to true the sender (from) will always get bcc of the email, defaults to true.
     */
    private boolean bccSender;

    /**
     * Construct with properties, which are required.
     *
     * @param props    The properties. Required.
     * @param prefixes the prefixes the property names start with.
     * @throws NullPointerException If props is null.
     */
    public OutgoingEmailConfig(Properties props, String... prefixes) {
        Validate.notNull(props);
        StructuredProperties sprops = new StructuredProperties(props, prefixes);
        host = sprops.getString("host");
        port = sprops.getInteger("port", 25);
        ssl = sprops.getBoolean("ssl", false);
        starttlsRequired = sprops.getBoolean("starttls.required", false);
        starttlsEnabled = sprops.getBoolean("starttls.enabled", false);
        user = sprops.getString("user", null);
        password = sprops.getString("password", null);
        from = sprops.getString("from", null);
        to = sprops.getString("to", null);
        replyTo = sprops.getString("replyTo", null);
        cc = sprops.getString("cc", null);
        bcc = sprops.getString("bcc", null);
        subject = sprops.getString("subject");
        imagesEnabled = sprops.getBoolean("images.enabled", true);
        bccSender = sprops.getBoolean("bcc.sender", true);
    }
}
