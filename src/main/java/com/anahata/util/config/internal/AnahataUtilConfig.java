package com.anahata.util.config.internal;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.io.CompressionType;
import com.anahata.util.io.SerializationType;
import com.anahata.util.props.StructuredProperties;
import com.anahata.util.transport.RemoteServiceProxy;
import com.anahata.util.transport.TransportLayer;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Configuration for anahata-util. Currently has no items.
 *
 * @author Robert Nagajek
 */
@ApplicationScoped
@Slf4j
public class AnahataUtilConfig implements Serializable {
    @Inject
    private ApplicationPropertiesFactory appProps;

    @Getter
    private String nonJwsApplicationUrl;

    @Getter
    private boolean autoLogin;

    @Getter
    private String autoLoginUser;

    @Getter
    private String autoLoginPassword;

    @Getter
    private TransportLayer transportLayer;

    @Getter
    private CompressionType rpcCompression;

    @Getter
    private SerializationType rpcSerialization;
    
    @Getter
    private int rpcSocketConnectTimout;
    
    @Getter
    private int rpcSocketReadTimout;

    @Getter
    private String ejbTransportModuleName;

    @Getter
    private boolean transportLogging;

    @Getter
    private int jmsPort;

    @Getter
    private Class<? extends RemoteServiceProxy> transportProxy;

    @PostConstruct
    @SuppressWarnings("unchecked")
    void postConstruct() {
        final StructuredProperties props = new StructuredProperties(appProps.getAppProperties(),
                AnahataUtilConstants.ANAHATA_UTIL_PROPS_PREFIX);
        nonJwsApplicationUrl = props.getString("nonjws.application.url", null);
        autoLogin = props.getBoolean("security.autologin", false);
        autoLoginUser = props.getString("security.autologin.user", null);
        autoLoginPassword = props.getString("security.autologin.password", null);
        transportLayer = props.getEnum(TransportLayer.class, "transport.layer", TransportLayer.RPC);

        rpcCompression = props.getEnum(CompressionType.class, "transport.layer.rpc.compression", CompressionType.DEFLATE);
        rpcSerialization = props.getEnum(SerializationType.class, "transport.layer.rpc.serialization", SerializationType.JAVA);
        jmsPort = props.getInteger("transport.jms.port", 7676);
        ejbTransportModuleName = props.getString("transport.layer.ejb.module.name", null);

        transportLogging = props.getBoolean("transport.logging", false);
        
        rpcSocketConnectTimout = props.getInteger("transport.socket.connect.timout", 10000);
        rpcSocketReadTimout = props.getInteger("transport.socket.read.timout", 90000);
        
        try {
            transportProxy = props.getClass("transport.proxy", null);
        } catch (ClassNotFoundException ex) {
            log.warn("Classs {} Not found ", props.getString("transport.proxy"), ex);
        }
    }
}
