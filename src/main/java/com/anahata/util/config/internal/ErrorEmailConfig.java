package com.anahata.util.config.internal;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.util.props.StructuredProperties;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.Getter;
import lombok.ToString;

/**
 * Error emails configuration for anahata-util. Requires the following properties to be presented by
 * ApplicationPropertiesFactory:
 * 
 * <ul>
 *   <li>anahatautil.erroremail.host</li>
 *   <li>anahatautil.erroremail.smtpport</li>
 *   <li>anahatautil.erroremail.user</li>
 *   <li>anahatautil.erroremail.password</li>
 *   <li>anahatautil.erroremail.from</li>
 *   <li>anahatautil.erroremail.to</li>
 *   <li>anahatautil.erroremail.cc</li>
 * </ul>
 *
 * @author Robert Nagajek
 */
@ApplicationScoped
@Getter
public class ErrorEmailConfig {
    private static final String PROPS_PREFIX = "erroremail";
    
    @Inject
    private ApplicationPropertiesFactory appProps;

    private String errorEmailHost;

    private Integer errorEmailSmtpPort;
    
    private boolean errorEmailSSL;

    private String errorEmailUser;

    private String errorEmailPassword;

    private String errorEmailFrom;

    private String errorEmailTo;
    
    private String errorEmailCc;

    @PostConstruct
    void postConstruct() {
        final StructuredProperties props = new StructuredProperties(appProps.getAppProperties(),
                AnahataUtilConstants.ANAHATA_UTIL_PROPS_PREFIX, PROPS_PREFIX);
        errorEmailHost = props.getString("host");
        errorEmailSmtpPort = props.getInteger("smtpport", null);
        errorEmailSSL = props.getBoolean("ssl", false);
        errorEmailUser = props.getString("user", null);
        errorEmailPassword = props.getString("password", null);
        errorEmailFrom = props.getString("from");
        errorEmailTo = props.getString("to");
        errorEmailCc = props.getString("cc", null);
    }

    @Override
    public String toString() {
        return "ErrorEmailConfig{" + "errorEmailHost=" + errorEmailHost + ", errorEmailSmtpPort=" + errorEmailSmtpPort + ", errorEmailSSL=" + errorEmailSSL + ", errorEmailUser=" + errorEmailUser + ", errorEmailPassword=" + errorEmailPassword + ", errorEmailFrom=" + errorEmailFrom + ", errorEmailTo=" + errorEmailTo + '}';
    }
    
}
