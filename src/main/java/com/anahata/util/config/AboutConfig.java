package com.anahata.util.config;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.env.ApplicationEnvironment;
import com.anahata.util.props.StructuredProperties;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;
import java.util.Properties;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * Standardised application details - application name, application version number, build number, build timestamp,
 * runtime environment. Requires the following properties to be passed to the constructor:
 *
 * <ul>
 * <li>application.name</li>
 * <li>application.owner</li>
 * <li>application.version</li>
 * <li>application.build.number</li>
 * <li>application.build.timestamp</li>
 * <li>application.build.timestamp.format</li>
 * <li>application.environment</li>
 * </ul>
 *
 * This class is immutable.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@Slf4j
public class AboutConfig implements Serializable {
    private String appName;

    private String appOwner;

    private String appVersion;

    private String buildNumber;

    private Date buildTimestamp;

    private String formattedBuildTimestamp;

    private String buildUID;

    /**
     * Contains name, version, 
     */
    private String appTitle;

    private ApplicationEnvironment environment;

    private String baseUrl;

    /**
     * Construct with properties, which are required.
     *
     * @param props    The properties. Required.
     * @param prefixes property key prefixes. Optional.
     * @throws NullPointerException If props is null.
     */
    public AboutConfig(Properties props, String... prefixes) {
        Validate.notNull(props);
        StructuredProperties sprops = new StructuredProperties(props, prefixes);
        appName = sprops.getString("application.name");
        appOwner = sprops.getString("application.owner", "");
        appVersion = sprops.getString("application.version");
        buildNumber = sprops.getString("application.build.number");
        final String format = sprops.getString("application.build.timestamp.format");

        try {
            buildTimestamp = sprops.getDate("application.build.timestamp", format);
        } catch (Exception e) {
            throw new RuntimeException(
                    "Exception parsion buildTimestamp with format, " + format + "."
                    + " Does the pom of the project containing the properties being parsed"
                    + " have a build/resources or build/testResources section?", e);
        }

        formattedBuildTimestamp = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(
                buildTimestamp);
        final String displayVersion = displayVersion(appVersion);
        buildUID = displayVersion + " b" + buildNumber + " " + formattedBuildTimestamp;
        environment = sprops.getEnum(ApplicationEnvironment.class, "application.environment");
        appTitle = makeApplicationTitle();
        baseUrl = sprops.getString("application.home.page", null); //returning null as it is not required on the client.
    }
    
    /**
     * Returns the App name preffixed with the {@link ApplicationEnvironment} if the environment is not prod.
     * 
     * @return 
     */
    public String getAppNameWithEnvironmentIfNotProd() {
        String ret = appName;

        if (environment != ApplicationEnvironment.PROD) {
            ret = environment + " " + ret;
        }

        return ret;
    }
    
    /**
     * Makes a human friendly application title or display name given the base application name, version info and
     * environment.
     *
     * @param version The version to format, in Maven format, such as 1.0.0-SNAPSHOT.
     * @return The application version in a format like: 'DEV Application Name v1.0-Beta b108'
     */
    public static String displayVersion(String version) {
        String dver = "v" + version;
        dver = dver.replace("-SNAPSHOT", "-Beta");

        //Remove last .0 if version ending in .0 So 1.0.0 becomes 1.0, 1.1.0 becomes 1.1 but 1.0.1 stays as 1.0.1
        if (dver.endsWith(".0")) {
            dver = dver.substring(0, dver.length() - 2);
        }

        if (dver.contains(".0-")) {
            dver = dver.replace(".0-", "-");
        }

        return dver;
    }

    /**
     * Makes a human friendly application title or display name given the base application name, version info and
     * environment.
     *
     * @return the application title in a format like: 'DEV Application Name v1.0-Beta b108'
     */
    private String makeApplicationTitle() {
        String applicationTitle = appName;
        String displayVersion = displayVersion(appVersion);

        if (environment != ApplicationEnvironment.PROD) {
            applicationTitle = environment + " " + applicationTitle;
            displayVersion += " b" + buildNumber;
        }

        applicationTitle += " " + displayVersion;
        return applicationTitle;
    }
}
