package com.anahata.util.financial;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

/**
 * Currency utilities.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class CurrencyUtils {
    /**
     * Given a Currency, return the number format to use for it.
     * 
     * @param currency The currency. Required.
     * @return The number format. Null if not found for the currency.
     * @throws NullPointerException If currency is null.
     */
    public static NumberFormat getCurrencyFormat(Currency currency) {
        Validate.notNull(currency);
        
        for (Locale locale : NumberFormat.getAvailableLocales()) {
            NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
            
            if (numberFormat.getCurrency().equals(currency)) {
                return numberFormat;
            }
        }
        
        return null;
    }
    
    /**
     * Given a currency code, return the number format to use for it.
     * 
     * @param currencyCode The currency code. Required.
     * @return The number format. Null if not found for the currency code.
     * @throws NullPointerException If currencyCode is null.
     */
    public static NumberFormat getCurrencyFormat(String currencyCode) {
        Validate.notNull(currencyCode);
        return getCurrencyFormat(Currency.getInstance(currencyCode));
    }
}
