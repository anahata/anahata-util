package com.anahata.util.html;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

/**
 * Create a HTML page.
 *
 * @author Robert Nagajek
 */
@NoArgsConstructor
public final class HtmlBuilder {
    private String style;

    private StringBuilder body = new StringBuilder();

    public HtmlBuilder style(String style) {
        Validate.notNull(style);
        this.style = style;
        return this;
    }

    public HtmlBuilder body(String value) {
        if (value == null) {
            return this;
        }

        body.append(value);
        return this;
    }

    public HtmlBuilder span(String value) {
        body.append(new SpanBuilder()
                .value(value)
                .build());
        return this;
    }

    public HtmlBuilder span(String style, String value) {
        body.append(new SpanBuilder()
                .style(style)
                .value(value)
                .build());
        return this;
    }

    public HtmlBuilder pre(String value) {
        body.append("<pre>");
        body.append(value);
        body.append("</pre>");
        return this;
    }

    public HtmlBuilder hr() {
        body.append("<hr />");
        return this;
    }
    
    public HtmlBuilder h1(String text) {
        body.append("<h1>");
        body.append(text);
        body.append("</h1>");
        return this;
    }
    
    public HtmlBuilder h2(String text) {
        body.append("<h2>");
        body.append(text);
        body.append("</h2>");
        return this;
    }
    
    public HtmlBuilder h3(String text) {
        body.append("<h3>");
        body.append(text);
        body.append("</h3>");
        return this;
    }
    
    public HtmlBuilder br() {
        body.append("<br />");
        return this;
    }

    public String build() {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head></head><body");
        appendStyle(sb, style);
        sb.append(">");
        sb.append(body);
        sb.append("</body></html>");
        return sb.toString();
    }

    private void appendStyle(StringBuilder sb, String styleValue) {
        if (styleValue != null) {
            sb.append(" style='");
            sb.append(styleValue);
            sb.append("'");
        }
    }
}
