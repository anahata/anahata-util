package com.anahata.util.html;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import javax.xml.bind.DatatypeConverter;
import org.apache.commons.lang3.Validate;
/**
 * Build an image.
 * 
 * @author Robert Nagajek
 */
public final class ImageBuilder {
    private StringBuilder sb = new StringBuilder();
    
    public ImageBuilder inline(String formatName, byte[] data) {
        return inline(formatName, data, null);
    }
    
    public ImageBuilder inline(String formatName, byte[] data, String alt) {
        Validate.notNull(formatName);
        Validate.notNull(data);
        
        sb.append("<img");
        
        if (alt != null) {
            sb.append(" alt='");
            sb.append(alt);
            sb.append("'");
        }
        
        sb.append(" src='data:image/");
        sb.append(formatName);
        sb.append(";base64,");
        sb.append(DatatypeConverter.printBase64Binary(data));
        sb.append("' />");
        return this;
    }
    
    public String build() {
        return sb.toString();
    }
}
