package com.anahata.util.html;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static com.anahata.util.lang.Nvl.*;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

/**
 * Build a HTML table.
 * 
 * @author Robert Nagajek
 */
@NoArgsConstructor
public final class TableBuilder {
    private String style;
    
    private StringBuilder th = new StringBuilder();
    
    private StringBuilder tr = new StringBuilder();
    
    boolean processingRow = false;
    
    public TableBuilder style(String style) {
        Validate.notNull(style);
        this.style = style;
        return this;
    }
    
    public TableBuilder th(String value) {
        return th(null, value);
    }
    
    public TableBuilder th(String style, String value) {
        th.append("<th");
        appendStyle(th, style);
        th.append(">");
        
        if (value != null) {
            th.append(value);
        }
        
        th.append("</th>");
        return this;
    }
    
    public TableBuilder tr() {
        if (!processingRow) {
            tr.append("<tr>");
        } else {
            tr.append("</tr><tr>");
        }
        
        processingRow = true;
        return this;
    }
    
    public TableBuilder td(String value) {
        return td(null, value, null);
    }
    
    public TableBuilder td(String style, String value) {
        return td(style, value, null);
    }
    
    public TableBuilder td(String style, String value, Integer colspan) {
        if (!processingRow) {
            tr();
        }
        
        tr.append("<td");
        
        if (colspan != null) {
            tr.append(" colspan='");
            tr.append(colspan);
            tr.append("'");
        }
        
        appendStyle(tr, style);
        tr.append(">");
        
        if (value != null) {
            tr.append(nvl(value).replace("\n", "<BR/>"));
        }
        
        tr.append("</td>");
        return this;
    }
    
    public TableBuilder trTdLabelValue(String labelStyle, String label, String valueStyle, Object value) {
        tr();
        tdLabelValue(labelStyle, label, valueStyle, value);
        return this;
    }
    
    public TableBuilder tdLabelValue(String labelStyle, String label, String valueStyle, Object value) {
        if (value == null) {
            return this;
        }
        
        td(labelStyle, label);
        td(valueStyle, String.valueOf(value));
        return this;
    }
    
    public String build() {
        StringBuilder sb = new StringBuilder();
        sb.append("<table");
        appendStyle(sb, style);
        sb.append(">");
        
        if (th.length() > 0) {
            sb.append("<thead><tr>");
            sb.append(th);
            sb.append("</tr></thead>");
        }
        
        if (tr.length() > 0) {
            sb.append("<tbody>");
            sb.append(tr);
            sb.append("</tr></tbody>");
        }
        
        sb.append("</table>");
        return sb.toString();
    }
    
    private void appendStyle(StringBuilder sb, String styleValue) {
        if (styleValue != null) {
            sb.append(" style='");
            sb.append(styleValue);
            sb.append("'");
        }
    }
}
