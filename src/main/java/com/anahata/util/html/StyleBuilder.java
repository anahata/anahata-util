package com.anahata.util.html;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.apache.commons.lang3.Validate;

/**
 * Build CSS styles.
 * 
 * @author Robert Nagajek
 */
public final class StyleBuilder {
    private StringBuilder sb = new StringBuilder();
    
    public StyleBuilder() {
        sb = new StringBuilder();
    }
    
    public StyleBuilder(String currentStyle) {
        Validate.notNull(currentStyle);
        sb = new StringBuilder(currentStyle);
    }
    
    public StyleBuilder style(String style, String value) {
        Validate.notNull(style);
        
        if (value != null) {
            sb.append(style);
            sb.append(": ");
            sb.append(value);
            sb.append(";");
        }
        
        return this;
    }
    
    public StyleBuilder stylePx(String style, Integer value) {
        Validate.notNull(style);
        
        if (value != null) {
            sb.append(style);
            sb.append(": ");
            sb.append(value);
            sb.append("px;");
        }
        
        return this;
    }
    
    public String build() {
        return sb.toString();
    }
}
