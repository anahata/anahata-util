/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.swing.checksum;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.zip.CRC32;
import java.util.zip.Checksum;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

/**
 * Utility class to quickly calculate checksums.
 * 
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ChecksumUtils {
    /**
     * Calculates the crc32 of a byte array using CRC32.
     * @param bytes the byte array
     * @return the checksum
     */
    public static long crc32(byte[] bytes) {
        Validate.notNull(bytes, "bytes is required");
        Checksum checksum = new CRC32();
        checksum.update(bytes, 0, bytes.length);
        long lngChecksum = checksum.getValue();        
        return lngChecksum;
    }
}

