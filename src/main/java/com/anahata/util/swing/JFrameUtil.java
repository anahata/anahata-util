package com.anahata.util.swing;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.awt.EventQueue;
import javax.swing.JFrame;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Helpers for frames.
 * 
 * @author Robert Nagajek
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JFrameUtil {
    
    
    /**
     * Show a JFrame centered. It is invoked on the event queue.
     * 
     * @param jframe The JFrame to show.
     */
    public static void showCentered(JFrame jframe) {
        EventQueue.invokeLater(new JFrameRunner(jframe));
    }
    
    /**
     * Shows the centered JFrame.
     */
    private static final class JFrameRunner implements Runnable {
        /**
         * The JFrame.
         */
        private JFrame jframe;
        
        /**
         * Default constructor.
         * @param jframe  the jframe
         */
        private JFrameRunner(JFrame jframe) {
            this.jframe = jframe;
        }
        
        @Override
        public void run() {
            jframe.setVisible(true);
            jframe.setLocationRelativeTo(null);
        }
    }
}
