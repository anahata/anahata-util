package com.anahata.util.swing;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.awt.event.KeyEvent;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import javax.swing.UIDefaults;
import javax.swing.UIDefaults.LazyInputMap;
import javax.swing.UIManager;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Manages the configuration of the look and feel for Mac OS.
 * 
 * @author Robert Nagajek
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MacOSUIManager {
    private static final Logger LOG = LoggerFactory.getLogger(MacOSUIManager.class);
    
    private static final int MAC_META = KeyEvent.META_DOWN_MASK + KeyEvent.META_MASK;
    
    private static final KeyStroke WINDOWS_COPY = KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK);
    
    private static final KeyStroke MAC_COPY = KeyStroke.getKeyStroke(KeyEvent.VK_C, MAC_META);
    
    private static final KeyStroke WINDOWS_PASTE = KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_DOWN_MASK);
    
    private static final KeyStroke MAC_PASTE = KeyStroke.getKeyStroke(KeyEvent.VK_V, MAC_META);
    
    private static final KeyStroke WINDOWS_CUT = KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_DOWN_MASK);
    
    private static final KeyStroke MAC_CUT = KeyStroke.getKeyStroke(KeyEvent.VK_X, MAC_META);

    
    /**
     * Adds Mac style cut, copy and paste key strokes to the input map for all text components.
     */
    public static void addCopyPasteShortcutsWithMetaForMac() {
        UIDefaults defaults = UIManager.getLookAndFeel().getDefaults();
        
        for (Object entryKey : defaults.keySet()) {
            String keyToString = entryKey.toString();
            
            if (keyToString.contains("Text") && keyToString.contains("focusInputMap")) {
                LOG.info("addCopyPasteShortcutsWithMetaForMac: Will attempt to modify shortcuts for {}", keyToString);
                InputMap inputMap = null;
                Object maybeInputMap = defaults.get(entryKey);
                
                if (maybeInputMap instanceof InputMap) {
                    inputMap = (InputMap) maybeInputMap;
                } else if (maybeInputMap instanceof LazyInputMap) {
                    LazyInputMap lazyInputMap = (LazyInputMap) maybeInputMap;
                    inputMap = (InputMap) lazyInputMap.createValue(UIManager.getDefaults());
                } else {
                    LOG.warn("Unknown input map: {}", maybeInputMap);
                    continue;
                }

                try {
                    updateInputMapToMacWay(inputMap);
                    // Update LnF defaults
                    defaults.put(entryKey, inputMap);
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                }
            }
        }
    }

    /**
     * Adds Mac style cut, copy and paste key strokes to the input map.
     *
     * @param im The InputMap.
     */
    public static void updateInputMapToMacWay(InputMap im) {
        KeyStroke[] ks = im.keys();
        
        for (KeyStroke keyStroke : ks) {
            if (WINDOWS_COPY.equals(keyStroke)) {
                im.put(MAC_COPY, (String) im.get(keyStroke));
            } else if (WINDOWS_CUT.equals(keyStroke)) {
                im.put(MAC_CUT, (String) im.get(keyStroke));
            } else if (WINDOWS_PASTE.equals(keyStroke)) {
                im.put(MAC_PASTE, (String) im.get(keyStroke));
            }
        }
    }
}
