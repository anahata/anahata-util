/*
 *  Copyright © - 2014 Anahata Technologies.
 */
package com.anahata.util.redundant;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.progress.ProgressListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
public abstract class RedundantTask<T> implements ProgressListener {
    private static final ThreadLocal<RedundantTask> threadLocal = new ThreadLocal();

    private final Set<Thread> threads = new HashSet();

    @Getter
    protected boolean started = false;

    @Getter
    protected boolean suceeded = false;

    @Getter
    protected boolean failed = false;

    @Getter
    protected boolean aborted = false;

    @Getter
    protected double progress = -2;

    protected T value;

    private ProgressListener progressListener;

    @Getter
    protected List<Callable<T>> callables = new ArrayList();

    @Getter
    protected final List<Throwable> errors = new ArrayList();

    private ExecutorService executorService;

    /**
     * Determines whether after a callable succeeds, other callables should be allowed to continue or otherwise be aborted.
     */
    protected boolean runAllCallables = false;

    public RedundantTask(ExecutorService executorService) {
        this.executorService = executorService;
    }

    public RedundantTask(ExecutorService executorService, ProgressListener pl) {
        this(executorService);
        this.progressListener = pl;
    }

    public boolean isFinished() {
        return suceeded || failed || aborted;
    }

    @Override
    public void progress(double progress) {
        if (progress > this.progress) {
            this.progress = progress;
            if (progressListener != null) {
                progressListener.progress(this.progress);
            }
        }
    }

    public void addCallable(final Callable<T> callable) {
        callables.add(callable);
    }

    public synchronized T getOrFail(String message) throws RedundantTaskException {
        log.trace("getOrFail() entry");
        T ret = get(true);
        log.trace("getOrFail() got {}, failed={}, aborted-{}", String.valueOf(ret), failed, aborted);
        if (failed) {
            throw new RedundantTaskException("Task failed: " + message, getErrors());
        } else if (aborted) {
            throw new RedundantTaskException("Task aborted: " + message, getErrors());
        }
        log.trace("getOrFail() returning {}", String.valueOf(ret));
        return ret;
    }

    public synchronized T get(boolean wait) {
        if (isFinished()) {
            return value;
        }
        log.trace("get (wait = {}) entry", wait);
        if (!started) {
            log.trace("get() called and race not started, starting");
            start();
            log.trace("get() called race started");
        }

        if (!isFinished() && !isAborted() && wait) {
            try {
                log.trace("get() will now wait");
                wait();
                log.trace("get() woke up from waiting");
            } catch (InterruptedException e) {
                throw new RuntimeException("get(wait = true) interrupted", e);
            }
        }

        log.trace("get() returning {}", String.valueOf(value));

        return value;
    }

    public synchronized boolean isRunning() {
        return started && (!suceeded && !failed && !aborted);
    }

    public synchronized void start() {
        Validate.isTrue(!started, "task already started");
        started = true;
        execute();
    }

    public synchronized void abort() {
        aborted = true;
        notify();
        interruptThreads();
    }

    protected synchronized void suceeded(T t) {
        log.trace("{} entering succeeded", Thread.currentThread());
        if (!suceeded) {
            suceeded = true;
            value = t;
            if (!runAllCallables) {
                interruptThreads();
            }
            progress(1);
            notify();
        }
        log.trace("{} exiting succeeded", Thread.currentThread());
        unregisterThread();
    }

    protected synchronized void registerError(Throwable e) {
        errors.add(e);
        if (errors.size() == getCallables().size()) {
            fail();
        }
    }

    protected synchronized void fail() {
        log.debug("Redundant task fail()");
        failed = true;
        log.debug("Redundant task fail() calling interruptingThreads()");
        interruptThreads();
        log.debug("Redundant task fail() calling notify()");
        notify();
        log.debug("Redundant task fail() exiting");
    }

    protected void submit(final Runnable r) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                if (!isRunning()) {
                    log.debug("Not running: {}", r);
                    return;
                }
                registerThread();
                try {
                    r.run();
                } catch (Throwable e) {
                    //catch RuntimeExceptions
                    log.error("UncaughtException in submitted runnable", e);
                }
                unregisterThread();
            }
        });
    }

    protected abstract void execute();

    // ============================================================================================================
    private void interruptThreads() {
        for (Thread activeThread : threads) {
            log.debug("Interrupting {}", activeThread);
            activeThread.interrupt();
        }
    }

    private synchronized void registerThread() {
        log.debug("Registering {}", Thread.currentThread());
        threadLocal.set(this);
        threads.add(Thread.currentThread());
        log.trace("{} Registered", Thread.currentThread());
    }

    private synchronized void unregisterThread() {
        log.debug("Unregistering {}", Thread.currentThread());
        threadLocal.set(null);
        threads.remove(Thread.currentThread());
        log.trace("{} Unregistered", Thread.currentThread());
    }

}
