/*
 *  Copyright © - 2014 Anahata Technologies.
 */
package com.anahata.util.redundant;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.progress.ProgressListener;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Getter
@Slf4j
public class ParallellTask<T> extends RedundantTask<T> {

    public ParallellTask(ExecutorService executorService) {
        this(executorService, null);
    }

    public ParallellTask(ExecutorService executorService, ProgressListener pl) {
        super(executorService, pl);
        super.runAllCallables = true;
    }

    @Override
    public synchronized void execute() {
        for (final Callable<T> callable : getCallables()) {
            log.debug("submitting : {}", callable);
            submit(new CallableMonitor(callable));
        }
    }

    protected class CallableMonitor implements Runnable {

        private final Callable<T> callable;

        public CallableMonitor(Callable<T> callable) {
            this.callable = callable;
        }

        @Override
        public void run() {
            if (isAborted() || (isSuceeded() && !runAllCallables)) {
                log.debug("Wrapper.run, aborting callable {} on {} as the task has been either aborted or has succeeded and runAllCallables=false", callable, Thread.currentThread());
                return;
            }
            log.debug("Wrapper.run, thread {} registered ", Thread.currentThread());
            try {
                T ret = callable.call();
                suceeded(ret);
            } catch (Throwable e) {
                registerError(e);
                //all right, so as many errors as attempts, task failed
            }
        }
    }

}
