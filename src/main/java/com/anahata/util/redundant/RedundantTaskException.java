/*
 *  Copyright © - 2014 Anahata Technologies.
 */
package com.anahata.util.redundant;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Collection;
import lombok.Getter;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public class RedundantTaskException extends RuntimeException {
    @Getter
    private Collection<Throwable> exceptions;

    public RedundantTaskException(String message, Collection<Throwable> errors) {
        super(message + exceptionsToString(errors));
        this.exceptions = errors;
    }

    private static String exceptionsToString(Collection<Throwable> errors) {
        StringBuilder sb = new StringBuilder();
        int counter = 1;
        for (Throwable object : errors) {
            sb.append("\nException ").append(counter++).append(" of ").append(errors.size()).append("\n");
            sb.append(ExceptionUtils.getStackTrace(object));
        }
        return sb.toString();
    }

}
