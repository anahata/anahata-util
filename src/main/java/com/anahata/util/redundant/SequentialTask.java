/*
 *  Copyright © - 2014 Anahata Technologies.
 */
package com.anahata.util.redundant;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.progress.ProgressListener;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
public class SequentialTask<T> extends RedundantTask<T> implements ProgressListener {

    public SequentialTask(ExecutorService executorService) {
        super(executorService);
    }

    public SequentialTask(ExecutorService executorService, ProgressListener pl) {
        super(executorService, pl);
    }

    @Override
    protected void execute() {
        log.debug("Sequential task. execute():");
        Runnable r = new Runnable() {
            @Override
            public void run() {
                for (final Callable<T> callable : getCallables()) {
                    try {
                        log.debug("Sequential Task Executing: {}", callable);
                        T result = callable.call();
                        suceeded(result);
                        return;
                    } catch (Exception e) {
                        log.debug("Sequential Task registering error: {}", e.toString());
                        //register the error
                        registerError(e);
                        log.debug("Sequential Task finished registering error: {}", e.toString());
                    }
                }
                //Pablo 18/1/2016 moved fail to registerErrors when errors.size == callables.size
//                log.debug("Sequential Task calling fail() after attempting : {} callables", getCallables().size());
//                //tried all, nothing suceeded call fail
//                fail();
//                log.debug("Sequential Task finished calling fail() after attempting : {} callables", getCallables().size());
            }
        };
        log.debug("Sequential task execute() submitting single runnable {}", r);
        submit(r);
        log.debug("Finished Sequential task execute() ");
    }

}
