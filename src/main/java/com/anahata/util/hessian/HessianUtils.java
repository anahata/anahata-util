/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.hessian;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;
import java.io.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Utility classes related to hessian.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class HessianUtils {
    public static byte[] serialize(Object data) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        serialize(data, bos);
        return bos.toByteArray();
    }

    public static void serialize(Object data, OutputStream os) throws IOException {

        Hessian2Output out = new Hessian2Output(os);
        out.startMessage();
        out.writeObject(data);
        out.completeMessage();
        out.close();

    }

    public static Object deSerialize(byte[] data) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        return deSerialize(bis);
    }

    public static Object deSerialize(InputStream bis) throws IOException {
        Hessian2Input in = new Hessian2Input(bis);
        in.startMessage();
        Object ret = in.readObject();
        in.completeMessage();
        in.close();
        return ret;
    }

    public static long getDataSize(Object... data) {
        if (data == null) {
            return 0;
        }
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Hessian2Output out = new Hessian2Output(bos);
            out.startMessage();
            for (Object item : data) {
                out.writeObject(item);
            }
            out.completeMessage();
            out.close();
            return bos.toByteArray().length;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
