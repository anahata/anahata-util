package com.anahata.util.logging;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Utility methods to configure <code>java.util.logging</code>.
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JavaSELogConfigUtils {
    /**
     * Inits JavaSE Logging configuration by reading properties file from a given input stream.
     * 
     * @param is the input stream with the properties file
     */
    public static void readConfiguration(InputStream is) {

        try {
            System.out.println("Reading JUL properties file...");
            LogManager.getLogManager().readConfiguration(is);
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
            System.err.println("WARNING: Could not open configuration file");
            System.err.println("WARNING: Logging not configured (console output only)");
            throw new RuntimeException(ex);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
