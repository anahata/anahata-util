package com.anahata.util.logging;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.config.internal.ApplicationPropertiesFactory;
import com.anahata.util.logging.RemoteServiceWarning.Reason;
import com.anahata.util.transport.rpc.RpcStreamer;
import com.anahata.util.transport.rpc.client.RpcInvocationHandler;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.Dependent;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Log calls through remote services to get the size of data sent and received.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
@Dependent
public class RemoteServiceLogger implements InvocationHandler {

    private static long DEFAULT_MAX_RET_SIZE = 256 * 1024;

    private static long DEFAULT_MAX_ARGS_SIZE = 128 * 1024;

    private static long DEFAULT_MAX_TIME = 5 * 1000;
    
    @Inject
    private Event<RemoteServiceWarning> event;

    @Setter
    private Object remoteProxy;

    @Inject
    private RpcStreamer streamer;
    
    @Inject
    private ApplicationPropertiesFactory appPropsFactory;
    
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        long maxArgsSize = getMaxArgsSize(method);
        long maxRetSize = getMaxRetSize(method);
        long maxTime = getMaxTime(method);
        long compressTs = System.currentTimeMillis();
        long argsSize = streamer.getCompressedSize(args);
        Set<RemoteServiceWarning.Reason> reasons = new HashSet();
        String className = getProxyClassName(remoteProxy);

        compressTs = System.currentTimeMillis() - compressTs;

        long time = System.currentTimeMillis();

        if (log.isDebugEnabled()) {
            log.debug("Invoking @RemoteService {} {}.{} with {} bytes of args data", method.getReturnType(),
                    className,
                    method.getName(),
                    argsSize);
        }

        Object ret = null;

        try {
            ret = method.invoke(remoteProxy, args);
        } catch (Throwable e) {
            while (e instanceof InvocationTargetException && e.getCause() != null) {
                e = e.getCause();
            }
            log.debug("unwrapped exception: {}", e);
            throw e;
        }

        time = System.currentTimeMillis() - time;
        long retSize = streamer.getCompressedSize(ret);
        
        log.debug("@RemoteService {}.{} received data = {} bytes. took {} ms ",
                className,
                method.getName(), retSize, time);

        if (argsSize > maxArgsSize) {
            reasons.add(Reason.ARGS_LARGE);
        }
        
        if (retSize > maxRetSize) {
            reasons.add(Reason.RET_LARGE);            
        }
        
        if (time > maxTime) {
            reasons.add(Reason.SLOW);
        }
        
        if (!reasons.isEmpty()) {
            RemoteServiceWarning rsw = new RemoteServiceWarning();
            rsw.setReasons(reasons);
            rsw.setClassName(className);
            rsw.setMethod(method);
            rsw.setArgs(args);
            rsw.setArgsString(args != null ? Arrays.toString(args) : "");
            rsw.setArgsSize(argsSize);
            rsw.setRet(ret);
            rsw.setRetString(ret != null? ret.toString() : "");
            rsw.setRetSize(retSize);
            rsw.setTime(time);
            event.fire(rsw);
        }
        
        return ret;
    }

    private static String getProxyClassName(Object proxyObject) {

        if (Proxy.isProxyClass(proxyObject.getClass())
                && (Proxy.getInvocationHandler(proxyObject) instanceof RpcInvocationHandler)) {
            RpcInvocationHandler rpcHandler = (RpcInvocationHandler) Proxy.getInvocationHandler(proxyObject);
            return rpcHandler.getClazz().getSimpleName();
        }

        return proxyObject.getClass().getSimpleName();
    }

    private long getMaxArgsSize(Method method) {
        return DEFAULT_MAX_ARGS_SIZE;
    }

    private long getMaxRetSize(Method method) {
        return DEFAULT_MAX_RET_SIZE;
    }

    private long getMaxTime(Method method) {
        String value = appPropsFactory.getAppProperties().getProperty("yam.error.performance.unresponsive.max.millis");
        return value != null ? Long.valueOf(value) : DEFAULT_MAX_TIME;
//        return DEFAULT_MAX_TIME;
    }
}
