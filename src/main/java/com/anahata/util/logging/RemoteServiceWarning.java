/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.logging;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Getter
@Setter
@Slf4j
public final class RemoteServiceWarning extends PerformanceWarning implements Serializable {

    private String className;

    private Method method;

    private Object[] args;
    
    private long argsSize;
    
    private String argsString;

    private Object ret;
    
    private long retSize;
    
    private String retString;

    private long time;

    private Set<Reason> reasons = new HashSet();
    
    public static enum Reason {
        SLOW,
        ARGS_LARGE,
        RET_LARGE;
    }

    public String getMethodName() {
        return method.getName();
    }
       
    @Override
    public String toString() {
        return "RemoteServiceWarning{"
                + "className= " + className
                + ", method=" + getMethodName()
                + ", args=" + argsString
                + ", argsSize=" + argsSize
                + ", ret=" + retString
                + ", retSize=" + retSize
                + ", time=" + time      
                + ", reasons=" + reasons + '}';
    }
   
}
