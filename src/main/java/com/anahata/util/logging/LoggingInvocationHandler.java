package com.anahata.util.logging;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.reflect.DoNothingInvocationHandler;
import com.anahata.util.transport.rpc.RpcStreamer;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import javax.enterprise.context.Dependent;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ClassUtils;

/**
 * Log calls through Hessian to get the size of data sent and received.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
@AllArgsConstructor
public class LoggingInvocationHandler implements InvocationHandler {

    Object delegate;
    
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        String sargs = "";
        if (args != null) {            
            sargs = " arguments = " + Arrays.asList(args);
        }
        log.info("Calling {} {}", method.getName(), sargs);
        Object ret = method.invoke(delegate, args);
        log.info("Call to {} {} returned {}", method.getName(), sargs, ret);
        return ret;
    }
    
    public static <T> T newProxy(T delegate) {
        return (T)Proxy.newProxyInstance(LoggingInvocationHandler.class.getClassLoader(), ClassUtils.getAllInterfaces(delegate.getClass()).toArray(new Class[0]),
                new LoggingInvocationHandler(delegate));
    }
}
