package com.anahata.util.logging.logback;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.AbstractMatcherFilter;
import ch.qos.logback.core.spi.FilterReply;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * A Logback filter for markers. An example of usage:
 *
 * <pre>
 *   &lt;filter class="com.anahata.util.logging.logback.MarkerFilter"&gt;
 *     &lt;marker&gt;BackgroundProcess&lt;/marker&gt;
 *     &lt;onMatch&gt;DENY&lt;/onMatch&gt;
 *     &lt;onMismatch&gt;NEUTRAL&lt;/onMismatch&gt;
 *   &lt;/filter&gt;
 * </pre>
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public final class MarkerFilter extends AbstractMatcherFilter<ILoggingEvent> {
    private Marker markerToMatch = null;

    @Override
    public void start() {
        if (markerToMatch != null) {
            super.start();
        } else {
            addError("No marker set yet");
        }
    }

    @Override
    public FilterReply decide(ILoggingEvent event) {
        Marker marker = event.getMarker();

        if (!isStarted()) {
            return FilterReply.NEUTRAL;
        }

        if (marker == null) {
            return onMismatch;
        }

        if (markerToMatch.contains(marker)) {
            return onMatch;
        }

        return onMismatch;
    }

    public void setMarker(String markerStr) {
        if (markerStr != null) {
            markerToMatch = MarkerFactory.getMarker(markerStr);
        } else {
            markerToMatch = null;
        }
    }
}
