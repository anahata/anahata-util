package com.anahata.util.logging.logback;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.StackTraceElementProxy;
import ch.qos.logback.core.AppenderBase;
import com.anahata.util.cdi.Cdi;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * This is an appender that keeps a cyclic buffer of past logging events, and if an error event is encountered, it
 * notifies a CDI observer of the error and sends the past cached logging events. The appender tracks for additional
 * error logs via a timeout so a set of error logs can be notified.
 *
 * @author Robert Nagajek
 */
//@ApplicationScoped
@Slf4j
public class ErrorEventAppender extends AppenderBase<ILoggingEvent> {
    private static ErrorEventAppender instance;
    
    /** The buffer size. */
    @Getter
    @Setter
    private int bufferSize = 300;

    /** How long to wait for error logs to finish. */
    @Getter
    @Setter
    private long errorTimeout = 300L;

    @Getter
    private String ignoreErrors;
    
    @Getter
    private String ignoreThreads;
    
    @Getter
    private String ignoreLoggers;

    private List<String> ignoreErrorsList = new ArrayList();
    
    private List<String> ignoreThreadsList = new ArrayList();
    
    private List<String> ignoreLoggersList = new ArrayList();

    /** Log buffer protected for concurrent access. */
    @Getter(AccessLevel.PACKAGE)
    private Deque<ILoggingEvent> logBuffer = new ConcurrentLinkedDeque<>();

    /** The last timestamp of a logging action. */
    @Getter(AccessLevel.PACKAGE)
    private long lastLogTs;

    /** The current state of log parsing. */
    @Getter(AccessLevel.PACKAGE)
    @Setter(AccessLevel.PACKAGE)
    private State state = State.SEARCHING;

    /** The event to send to observers listening for error events. */
    @Inject
    private Event<ErrorEventAppender> eventMonitor;
    
    public ErrorEventAppender() {        
        instance = this;
    }
    
    public static ErrorEventAppender getInstance() {
        return instance;
    }

    /**
     * Append a logging event. Note that the parent method calling this is synchronized, so it's thread safe.
     *
     * @param e The logging event.
     */
    @Override
    protected void append(ILoggingEvent e) {
        //System.out.println("");
        
        lastLogTs = e.getTimeStamp();
        boolean errorOrHigher = e.getLevel().isGreaterOrEqual(Level.ERROR);
        logBuffer.addLast(e);
        
        if (logBuffer.size() > bufferSize) {
            logBuffer.removeFirst();
        }

        if (state == State.SEARCHING && errorOrHigher) {
            if (!shouldIgnore(e)) {
                state = State.FOUND;
//                System.out.println(
//                        "Firing event as not ignoring " + e.getFormattedMessage() + " " + e.getThrowableProxy());
                // Notify listeners of the logging event.
                try {
                    Cdi.fireEvent(this);
                } catch (Exception ex) {
                    log.warn("Could not find error event", ex);
                }
                
            }
        }

        if (state == State.FOUND && !errorOrHigher) {
            state = State.SEARCHING;
        }
    }

    private boolean shouldIgnore(ILoggingEvent e) {
        
        if (!ignoreErrorsList.isEmpty()) {               
            if (matches(ignoreErrorsList, e.getMessage())) {
                log.debug("message : '" + e.getMessage() + "' matches ignoreErrorsList");
                return true;
            } else if (matches(ignoreErrorsList, e.getFormattedMessage())) {
                log.debug("Formatted message : '" + e.getFormattedMessage() + "' matches ignoreErrorsList");
                return true;
            } 
            
            //see if any stack trace matches
            IThrowableProxy itp = e.getThrowableProxy();
            whileItp:
            while (itp != null) {
                String nameWithMessage = itp.getClassName() + ": " + itp.getMessage();
                if(matches(ignoreErrorsList, nameWithMessage)) {
                    log.debug("IThrowableProxy nameWithMessage : '" + nameWithMessage + "' matches ignoreErrorsList ");
                    return true;
                }
                if(matches(ignoreErrorsList, itp.getClassName())) {
                    log.debug("IThrowableProxy class name: '" + itp.getClassName() + "' matches ignoreErrorsList ");
                    return true;
                }
                if (matches(ignoreErrorsList, itp.getMessage())) {
                    log.debug("IThrowableProxy message: '" + itp.getMessage() + "' matches ignoreErrorsList");
                    return true;
                }
                for (StackTraceElementProxy ste : itp.getStackTraceElementProxyArray()) {                    
                    if (matches(ignoreErrorsList, ste.toString())) {
                        log.debug("StackTraceElementProxy ste.toString {}, matches ignoreErrorsList", ste.toString());
                        return true;
                    } 
                }
                itp = itp.getCause();
            }
        }
        
        if (matches(ignoreThreadsList, e.getThreadName())) {
            log.debug("Ignoreing thread: " + e.getThreadName());
            return true;
        }
        
        if (matches(ignoreLoggersList, e.getLoggerName())) {
            log.debug("Ignoring Logger: " + e.getLoggerName());
            return true;
        }
        
        return false;
        
    }

    /**
     * Get the current set of buffered logging events.
     *
     * @return The logging events. Never null.
     */
    public synchronized List<ILoggingEvent> getLoggingEvents() {        
        return new ArrayList<>(logBuffer);
    }

    enum State {
        SEARCHING,
        FOUND;
    }

    public void setIgnoreErrors(String ignoreErrors) {
        this.ignoreErrors = ignoreErrors;
        this.ignoreErrorsList = toList(ignoreErrors);
    }
    
    public void setIgnoreThreads(String ignoreThreads) {
        this.ignoreThreads = ignoreThreads;
        this.ignoreThreadsList = toList(ignoreThreads);
    }
    
    public void setIgnoreLoggers(String ignoreLoggers) {
        this.ignoreLoggers = ignoreLoggers;
        this.ignoreLoggersList = toList(ignoreLoggers);
    }
    
    
    private static boolean matches(List<String> patterns, String error) {
        if (error == null) {
            return false;
        }
        for (String pattern : patterns) {
            log.trace("Comparing error {} with pattern {}, matches={}", error, pattern);
            boolean match = error.toLowerCase().contains(pattern.toLowerCase());
            if (log.isTraceEnabled()) {
                log.trace("error {} with pattern {}, matches={}", error.toLowerCase(), pattern.toLowerCase(), match);
            }
            if (match) {
                log.debug("{} matches {}", error, pattern);
                return true;
            }
        }
        return false;
    }
    
    private static List<String> toList(String ignoreErrors) {
        List<String> ret = new ArrayList<>();
        if (ignoreErrors != null) {
            String[] lines = ignoreErrors.split("\n");
            for (String string : lines) {
                string = string.trim();
                if (!string.isEmpty()) {
                    ret.add(string);
                }
            }
        }
        return ret;
    }
}
