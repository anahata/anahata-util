package com.anahata.util.logging.logback;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import ch.qos.logback.classic.spi.ILoggingEvent;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * An error event, invoked through CDI. This contains a list of logback logging events.
 * 
 * @author Robert Nagajek
 */
@AllArgsConstructor
public class ErrorEvent {
    @Getter
    private List<ILoggingEvent> loggingEvents;
}
