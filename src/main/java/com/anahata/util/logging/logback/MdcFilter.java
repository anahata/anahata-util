package com.anahata.util.logging.logback;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.AbstractMatcherFilter;
import ch.qos.logback.core.spi.FilterReply;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.MDC;

/**
 * Filter by MDC.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Getter
@Setter
public final class MdcFilter extends AbstractMatcherFilter<ILoggingEvent> {
    private String key;

    private String value;

    @Override
    public FilterReply decide(ILoggingEvent event) {
        if (key == null || value == null) {
            return onMatch;
        }
        
        String val = MDC.get(key);
        
        if (value.equals(val)) {
            return onMatch;
        }
        
        return onMismatch;
    }
}
