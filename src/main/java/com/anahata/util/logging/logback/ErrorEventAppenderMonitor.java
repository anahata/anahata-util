package com.anahata.util.logging.logback;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.util.cdi.Cdi;
import com.anahata.util.logging.logback.ErrorEventAppender.State;
import java.util.ArrayList;
import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Any;
import javax.inject.Inject;

/**
 * Monitor error logging events. In a Java EE environment, this will run as a singleton and asynchronous execution so
 * that the logback appender producing events does not block. In a Java SE environment, it spawns its own thread.
 * 
 * @author Robert Nagajek
 */
@Singleton
public class ErrorEventAppenderMonitor {
    @Inject
    @Any
    private Event<ErrorEvent> errorEvent;
    
    private ErrorEventAppender appender;
    
    /**
     * Event observer for the error event appender. Initiates a monitoring process that checks for additional error
     * logs up to a defined timeout value configured in the appender.
     * 
     * @param appender The appender.
     */
    @Asynchronous
    public void processError(@Observes ErrorEventAppender appender) {
        this.appender = appender;
        
        if (Cdi.isSe()) {
            new JavaSeMonitor().start();
        } else {
            process();
        }
    }
    
    private void process() {
        long currTs = System.currentTimeMillis();
        boolean found = appender.getState() == State.FOUND;

        while (appender.getState() == State.FOUND && (currTs - appender.getLastLogTs()) < appender.getErrorTimeout()) {
            found = true;
            
            try {
                Thread.sleep(appender.getErrorTimeout());
            } catch (InterruptedException e1) {
            }
            
            currTs = System.currentTimeMillis();
        }

        appender.setState(State.SEARCHING);
        
        if (found) {
            errorEvent.fire(new ErrorEvent(new ArrayList<>(appender.getLogBuffer())));
        }
    }
    
    private class JavaSeMonitor extends Thread {
        @Override
        public void run() {
            process();
        }
    }
}
