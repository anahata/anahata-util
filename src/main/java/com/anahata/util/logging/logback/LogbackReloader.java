/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.logging.logback;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;
import com.anahata.util.io.AnahataFileUtils;
import com.anahata.util.io.FileModificationListener;
import com.anahata.util.io.FileMonitor;
import com.anahata.util.lang.ResourceUtils;
import com.anahata.util.plaf.OSUtils;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import org.slf4j.LoggerFactory;

/**
 *
 * @author Zeshan
 */
@Slf4j
public final class LogbackReloader {
    static File tempFile;

    public synchronized static void showEditor() throws Exception {
        if (tempFile == null) {
            tempFile = File.createTempFile("logback", ".xml", AnahataFileUtils.getAnahataUserDirectory("logback"));
            tempFile.deleteOnExit();
            log.debug("tempFile at {}", tempFile);
            InputStream is = ResourceUtils.getResource("/logback.xml").openStream();
            String logBackXmlContent = IOUtils.toString(is);            
            if (OSUtils.isWindows()) {
                logBackXmlContent = logBackXmlContent.replace("\n", "\r");
            }
            IOUtils.write(logBackXmlContent, new FileOutputStream(tempFile));
            new FileMonitor(tempFile, new FileModificationListener() {
                @Override
                public void fileModified(FileMonitor monitor, Date prevLastModified, Date newLastModified) {
                    LoggerContext loggerContext = (LoggerContext)LoggerFactory.getILoggerFactory();

                    try {
                        JoranConfigurator configurator = new JoranConfigurator();
                        configurator.setContext(loggerContext);
                        loggerContext.reset();
                        configurator.doConfigure(tempFile);
                    } catch (JoranException je) {

                    }
                    StatusPrinter.printInCaseOfErrorsOrWarnings(loggerContext);
                }
            });

        }

        Desktop.getDesktop().edit(tempFile);
    }
}
