package com.anahata.util.logging.slf4j;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;

/**
 * Convenience methods for SLF4J.
 * 
 * @author Pablo Rodriguez Pina (pablo at anahata-it.com)
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Slf4jUtils {
    
    /**
     * Log an entry of a method with optional arguments. Logged at trace level.
     * 
     * @param log The logger.
     * @param sourceMethod The method executing.
     * @param args Optional args.
     */
    public static void entering(Logger log, String sourceMethod, Object... args) {
        if (!log.isTraceEnabled()) {
            return;
        }
        
        StringBuilder msg = new StringBuilder(sourceMethod);
        msg.append(": ENTRY");
        boolean first = true;
        
        for (Object obj : args) {
            if (first) {
                first = false;
                msg.append(" ");
            } else {
                msg.append(", ");
            }
            
            msg.append(obj);
        }
        
        log.trace(msg.toString());
    }
    
    /**
     * Log the completion of a method.
     * 
     * @param log The logger.
     * @param sourceMethod The method executing.
     */
    public static void exiting(Logger log, String sourceMethod) {
        log.trace("{}: RETURN", sourceMethod);
    }
    
    /**
     * Log the completion of a method.
     * 
     * @param log The logger.
     * @param sourceMethod The method executing.
     * @param result The returned value of the method.
     */
    public static void exiting(Logger log, String sourceMethod, Object result) {
        log.trace("{}: RETURN {}", sourceMethod, result);
    }
}
