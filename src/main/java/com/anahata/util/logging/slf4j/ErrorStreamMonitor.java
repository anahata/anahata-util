package com.anahata.util.logging.slf4j;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.util.error.StackTraceParser;
import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
final class ErrorStreamMonitor extends Thread {
    private static final Logger log = LoggerFactory.getLogger("System.err");

    private static final boolean LOG_ENABLED = false;

    private static final long ERROR_TIMEOUT = 200L;

    private ErrorMonitoringPrintStream ps;

    public ErrorStreamMonitor(ErrorMonitoringPrintStream ps) {
        this.ps = ps;
    }

    @Override
    public void run() {
        try {
            long currTs = System.currentTimeMillis();
            log("Waiting");

            while (ps.isCollecting() && (currTs - ps.getLastLogTs()) < ERROR_TIMEOUT) {
                try {
                    synchronized (this) {
                        wait(ERROR_TIMEOUT);
                    }
                } catch (InterruptedException e) {
                }

                currTs = System.currentTimeMillis();
            }

            final List<String> buffer = ps.getBufferAndReset();
            final Throwable error = StackTraceParser.parse(buffer);
            log.error("Unhandled exception caught from System.err", error);
        } catch (Throwable t) {
            log.error("An error was encountered attempting to parse System.err errors", t);
        }

        log("Error logged, setting collecting to false");
        ps.setCollecting(false);
        log("Exiting");
    }

    private void log(String message) {
        if (LOG_ENABLED) {
            System.out.println("ErrorStreamMonitor: " + message);
        }
    }
}
