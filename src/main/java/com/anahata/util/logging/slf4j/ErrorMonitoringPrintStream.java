package com.anahata.util.logging.slf4j;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Print stream monitoring output to System.err.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
final class ErrorMonitoringPrintStream extends PrintStream {
    private static final Logger log = LoggerFactory.getLogger("System.err");
    
    @Getter(AccessLevel.PACKAGE)
    @Setter(AccessLevel.PACKAGE)
    private boolean collecting = false;
    
    private Deque<String> buffer = new ConcurrentLinkedDeque<>();
    
    @Getter(AccessLevel.PACKAGE)
    private long lastLogTs;
    
    private ErrorStreamMonitor monitor;
    
    ErrorMonitoringPrintStream() {
        super(new NullOutputStream());
    }

    @Override
    public void println() {
    }

    @Override
    public void println(boolean x) {
        process(String.valueOf(x));
    }
    
    @Override
    public void println(char x) {
        process(String.valueOf(x));
    }

    @Override
    public void println(int x) {
        process(String.valueOf(x));
    }

    @Override
    public void println(long x) {
        process(String.valueOf(x));
    }

    @Override
    public void println(float x) {
        process(String.valueOf(x));
    }

    @Override
    public void println(double x) {
        process(String.valueOf(x));
    }

    @Override
    public void println(char[] x) {
        process(String.valueOf(x));
    }

    @Override
    public void println(String x) {
        process(String.valueOf(x));
    }

    @Override
    public void println(Object x) {
        process(String.valueOf(x));
    }
    
    /**
     * Get a copy of the buffer, and reset the source buffer to get a new set of logs.
     * 
     * @return A copy of the buffer.
     */
    public List<String> getBufferAndReset() {
        List<String> bufferCopy = new ArrayList<>(buffer);
        buffer.clear();
        return bufferCopy;
    }
    
    private void process(String value) {
        if (isPrintStackTrace()) {
            buffer.add(value);
        } else if (isWarning(value)) {
            log.warn(value);
        } else {
            log.error(value);
        }
    }
    
    private boolean isWarning(String value) {
        return StringUtils.startsWith(value, "WARNING:");
    }
    
    private boolean isPrintStackTrace() {
        lastLogTs = System.currentTimeMillis();
        StackTraceElement[] stes = Thread.currentThread().getStackTrace();
        boolean found = false;
        
        for (StackTraceElement ste : stes) {
            if (Throwable.class.getName().equals(ste.getClassName())
                    && "printStackTrace".equals(ste.getMethodName())) {
                found = true;
                break;
            }
        }

        if (!collecting && found) {
            collecting = true;
            monitor = new ErrorStreamMonitor(this);
            monitor.start();
        }

        if (collecting && !found) {
            collecting = false;
            
            synchronized(monitor) {
                monitor.notify();
            }
        }

        return found;
    }
    
    private static class NullOutputStream extends OutputStream {
        @Override
        public void write(int b) throws IOException {
        }
    }
}
