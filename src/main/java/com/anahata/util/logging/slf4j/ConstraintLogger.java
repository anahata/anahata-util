package com.anahata.util.logging.slf4j;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import javax.validation.ConstraintViolationException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;

/**
 * Log constraint violations.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ConstraintLogger {
    /**
     * Log an exception, and any constraint violations that may exist. Logging is done at ERROR level.
     * 
     * @param log The SLF4J logger to log to.
     * @param t The throwable.
     */
    public static void logConstraints(Logger log, Throwable t) {
        log.error("Exception thrown", t);
        Throwable next = t;
        logConstraintViolationException(log, t);

        while (next.getCause() != null) {
            next = next.getCause();
            logConstraintViolationException(log, next);
        }
    }
    
    private static void logConstraintViolationException(Logger log, Throwable t) {
        if (t instanceof ConstraintViolationException) {
            ConstraintViolationException cve = (ConstraintViolationException)t;

            for (Object o : cve.getConstraintViolations()) {
                log.error("Constraint violation: {}", o);
            }
        }
    }
}
