package com.anahata.util.logging.slf4j;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.persistence.logging.AbstractSessionLog;
import org.eclipse.persistence.logging.SessionLogEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements an EclipseLink SessionLog to redirect logging through SLF4J. To enable it in EclipseLink,
 * use property eclipselink.logging.logger in persistence.xml, setting this class to the value.
 * 
 * @author Robert Nagajek
 */
public final class EclipseLinkSlf4jLogger extends AbstractSessionLog {
    @Override
    public void log(SessionLogEntry sessionLogEntry) {
        final String message = formatMessage(sessionLogEntry);
        final String nameSpace = sessionLogEntry.getNameSpace();
        final Throwable exception = sessionLogEntry.getException();
        final Logger log = LoggerFactory.getLogger(nameSpace == null ? "eclipselink" : "eclipselink." + nameSpace);
        
        switch (sessionLogEntry.getLevel()) {
            case SEVERE:
                if (exception != null) {
                    log.error(message, exception);
                } else {
                    log.error(message);
                }
                
                break;
                
            case WARNING:
                if (exception != null) {
                    log.warn(message, exception);
                } else {
                    log.warn(message);
                }
                
                break;
                
            case INFO:
                if (exception != null) {
                    log.info(message, exception);
                } else {
                    log.info(message);
                }
                
                break;
                
            case CONFIG:
                if (exception != null) {
                    log.debug(message, exception);
                } else {
                    log.debug(message);
                }
                
                break;
                
            case FINE:
                if (exception != null) {
                    log.debug(message, exception);
                } else {
                    log.debug(message);
                }
                
                break;
                
            case FINER:
                if (exception != null) {
                    log.debug(message, exception);
                } else {
                    log.debug(message);
                }
                
                break;
                
            case FINEST:
                if (exception != null) {
                    log.trace(message, exception);
                } else {
                    log.trace(message);
                }
                
                break;
                
            default:
                if (exception != null) {
                    log.debug(message, exception);
                } else {
                    log.debug(message);
                }
                
                break;
        }
    }
}
