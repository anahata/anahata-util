/*
 * Copyright 2016 Anahata.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.util.logging;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author goran
 */
@Slf4j
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FxThreadWarning extends PerformanceWarning {

    private final List<FxThreadSnapshot> fxThreadSnapshots = new ArrayList();

    private List<byte[]> before;

    private List<byte[]> after;
    
    private Long unresponsiveTime;

    public void addSnapshot(long unresponsiveTime, StackTraceElement[] trace) {
        fxThreadSnapshots.add(new FxThreadSnapshot(unresponsiveTime, trace));
    }

    @AllArgsConstructor
    @Getter
    public static class FxThreadSnapshot {
        private long unresponsiveTime;

        protected StackTraceElement[] trace;

        public String stackTraceHtml() {
            StringBuilder sb = new StringBuilder();
            for (StackTraceElement stackTraceElement : trace) {
                sb.append("at ");
                sb.append(stackTraceElement.toString());
                sb.append("<br/>");
            }
            return sb.toString();
        }
    }

    @Override
    public String toString() {
        return "FxThreadWarning{" + super.getTimestamp() + " fxThreadSnapshots=" + fxThreadSnapshots.size() + ", unresponsiveTime=" + unresponsiveTime + '}';
    }
    
    

}
