package com.anahata.util.logging;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Convenience methods for JUL (Java Util Logging).
 * 
 * @author pablo
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JULUtils {

    /**
     * When retrieving the calling class or method, the number of method calls between the caller of this class
     * and {@link #getCallingStackTraceElement()}.
     */
    private static final int STACK_ELEMENTS_TO_CALLER = 3;
    
    /**
     * @return a {@link java.util.logging.Logger} whose name is the calling classes name.
     */
    public static Logger getLogger() {
        return Logger.getLogger(getCallingClassName());
    }
    
    /**
     * Logs an exception with SEVERE level.
     * 
     * @param msg the message
     * @param t the exception
     */
    public static void exception(String msg, Throwable t) {
        Logger.getLogger(getCallingClassName()).log(Level.SEVERE, msg, t);
    }


    /**
     * Convenience method for {@link java.util.logging.Logger#exiting(java.lang.String, java.lang.String)}
     * that saves passing class name and method name by calculating it from the given stack.
     * 
     */
    public static void exiting() {
        Logger.getLogger(getCallingClassName()).exiting(getCallingClassName(), getCallingMethod());
    }

    /**
     * Convenience method for 
     * {@link java.util.logging.Logger#exiting(java.lang.String, java.lang.String, java.lang.Object)}
     * that saves passing class name and method name by calculating it from the given stack.
     * 
     * @param result - as in 
     * {@link java.util.logging.Logger#exiting(java.lang.String, java.lang.String, java.lang.Object)}
     */
    public static void exiting(Object result) {
        Logger.getLogger(getCallingClassName()).exiting(getCallingClassName(), getCallingMethod(), result);
    }

    /**
     * Convenience method for {@link java.util.logging.Logger#entering(java.lang.String, java.lang.String) 
     * that saves passing class name and method name by calculating it from the given stack.
     * 
     */
    public static void entering() {

        Logger.getLogger(getCallingClassName()).entering(getCallingClassName(), getCallingMethod());
    }

    /**
     * Convenience method for
     * {@link java.util.logging.Logger#entering(java.lang.String, java.lang.String, java.lang.Object[])
     * that saves passing class name and method name by calculating it from the given stack.
     * 
     * @param params - as in
     * {@link java.util.logging.Logger#entering(java.lang.String, java.lang.String, java.lang.Object[])
     */
    public static void entering(Object... params) {
        Logger.getLogger(getCallingClassName()).entering(getCallingClassName(), getCallingMethod(), params);
    }

    //-----------------------------
    //Private methods
    //-----------------------------

    /**
     * @return the calling class name (from the current stack).
     */
    private static String getCallingClassName() {
        return getCallingStackTraceElement().getClassName();
    }

    /**
     * @return the calling method name (from the current stack).
     */
    private static String getCallingMethod() {
        return getCallingStackTraceElement().getMethodName();
    }

    /**
     * @return the calling stack trace element, knowing that the first three lines are within this class.
     */
    private static StackTraceElement getCallingStackTraceElement() {
        Exception e = new Exception();
        StackTraceElement[] ste = e.getStackTrace();
        StackTraceElement caller = ste[STACK_ELEMENTS_TO_CALLER];
        return caller;
    }


}
