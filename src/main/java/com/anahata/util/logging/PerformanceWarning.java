/*
 * Copyright 2016 Anahata.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.util.logging;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author goran
 */
@Slf4j
public abstract class PerformanceWarning {
    
    @Getter
    @Setter
    private Date timestamp = new Date();
    
    public void logWarn() {
        log.warn("SLOW SLOW SLOW!!! " + toString());
    }
    
}
