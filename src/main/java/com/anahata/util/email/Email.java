/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.email;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.plaf.ByteUnit;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author pablo
 */
@Getter
@ToString(exclude = "zippedContent")
public class Email {
    @Setter
    private String from;

    @Setter
    private String subject;

    private List<String> to = new ArrayList<>();

    private List<String> cc = new ArrayList<>();

    private List<String> bcc = new ArrayList<>();

    private List<ByteArrayAttachment> attachments = new ArrayList<>();

    @Setter
    private byte[] zippedContent;

    public long getTotalAttachmentsSize() {
        long ret = 0;
        for (ByteArrayAttachment attachment : attachments) {
            ret += attachment.getData().length;
        }
        return ret;
    }

    public void compressAttachments(String name) {
        if (!name.toLowerCase().endsWith(".zip")) {
            name = name + ".zip";
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream((int)getTotalAttachmentsSize());
        ZipOutputStream zos = new ZipOutputStream(baos);
        try {
            for (ByteArrayAttachment attachment : attachments) {
                zos.putNextEntry(new ZipEntry(attachment.getName()));
                zos.write(attachment.getData());
                zos.closeEntry();
            }
            zos.close();
            attachments.clear();
            attachments.add(new ByteArrayAttachment(name, baos.toByteArray()));
        } catch (Exception e) {
            throw new RuntimeException("Exception compressing attachments", e);
        }
    }

    public boolean compressAttachmentsIfLargerThanMb(int mb, String name) {
        double byteSize = ByteUnit.MEGABYTE.to(mb, ByteUnit.BYTE);
        if (getTotalAttachmentsSize() > byteSize) {
            compressAttachments(name);
            return true;
        }
        return false;
    }

}
