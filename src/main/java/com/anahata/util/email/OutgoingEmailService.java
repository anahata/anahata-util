/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.email;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.config.OutgoingEmailConfig;
import com.anahata.util.mime.MimeUtils;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.activation.DataSource;
import javax.enterprise.context.Dependent;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.DataSourceResolver;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.ImageHtmlEmail;


/**
 * Email delivery methods.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@Dependent
public class OutgoingEmailService {
    @Getter
    @Setter(AccessLevel.PROTECTED)
    private OutgoingEmailConfig config;

//    /**
//     * Sends an image replacing embedding images referenced in the email body.
//     *
//     * TODO: This zippedContent input would be better of replaced bu the private inner class HtmlImageEmailContent
//     *
//     * @param from          the from email address. optional. if not provided will be taken from the config.
//     * @param subject       the email subject
//     * @param zippedContent the email body
//     * @param to            the to address
//     * @param attachments   the attachments. optional.
//     * @throws EmailException
//     */
//    @Deprecated
//    public void sendEmail(String from, String subject, byte[] zippedContent, String to,
//            ByteArrayAttachment... attachments) throws EmailException {
//        Email email = new Email();
//        email.setFrom(from);
//        email.setSubject(subject);
//        email.setZippedContent(zippedContent);
//        email.getAttachments().addAll(Arrays.asList(attachments));
//        sendEmail(email);
//    }
//
//    /**
//     * Sends an image replacing embedding images referenced in the email body.
//     *
//     * TODO: This zippedContent input would be better of replaced bu the private inner class HtmlImageEmailContent
//     *
//     * @param from          the from email address. optional. if not provided will be taken from the config.
//     * @param subject       the email subject
//     * @param zippedContent the email body
//     * @param to            the to addresses
//     * @param attachments   the attachments. optional.
//     * @throws EmailException
//     */
//
//    @Deprecated
//    public void sendEmail(String from, String subject, byte[] zippedContent, String[] to,
//            List<ByteArrayAttachment> attachments) throws  EmailException {
//         sendEmail(from, subject, zippedContent, from, attachments.toArray(new ByteArrayAttachment[attachments.size()]));
//    }
//
//    @Deprecated
//    public void sendEmail(String from, String subject, byte[] zippedContent, String[] to,
//            ByteArrayAttachment... attachments) throws EmailException {
//
//    }
    public void sendEmail(Email email) throws EmailException {
        ImageHtmlEmailContents contents = new ImageHtmlEmailContents(email.getZippedContent());
        HtmlEmail htmlEmail;
        //From libreoffice 4.2 onwards images are embedded in the html so no need to use datasource resolver
        if (!contents.images.isEmpty()) {
            htmlEmail = new ImageHtmlEmail() {
                protected MimeMessage createMimeMessage(Session aSession) {
                    return new MimeMessage(aSession) {
                        protected void updateHeaders() throws MessagingException {
                            System.out.println("removing message id");
                            super.updateHeaders();
                            super.removeHeader("Message-ID");
                            System.out.println("remvoed message id");

                        }
                    };
                }
            };
            ((ImageHtmlEmail)htmlEmail).setDataSourceResolver(contents);
        } else {
            htmlEmail = new HtmlEmail();
        }
        htmlEmail.setHtmlMsg(contents.getBody());
        htmlEmail.setCharset("UTF-8");
        htmlEmail.setHostName(getConfig().getHost());
        htmlEmail.setSmtpPort(getConfig().getPort());
        if (!StringUtils.isBlank(getConfig().getUser())) {
            htmlEmail.setAuthentication(getConfig().getUser(), getConfig().getPassword());
        }
        htmlEmail.setSSLOnConnect(getConfig().isSsl());
        htmlEmail.setStartTLSEnabled(getConfig().isStarttlsEnabled());
        htmlEmail.setStartTLSRequired(getConfig().isStarttlsRequired());

        String from = email.getFrom();

        if (from == null) {
            from = getConfig().getFrom();
        }

        htmlEmail.setFrom(from);
        if (!StringUtils.isBlank(getConfig().getReplyTo())) {
            htmlEmail.addReplyTo(getConfig().getReplyTo());
        }

        String subject = email.getSubject();

        if (!StringUtils.isEmpty(getConfig().getSubject())) {
            subject = String.format(getConfig().getSubject(), subject);
        }

        htmlEmail.setSubject(subject);

        boolean overrideTo = !StringUtils.isBlank(getConfig().getTo());
        //to specified in properties means override TO
        if (overrideTo) {
            htmlEmail.addTo(getConfig().getTo());
        } else if (!email.getTo().isEmpty()) {
            htmlEmail.addTo(email.getTo().toArray(new String[0]));
        }

        //but cc specified in properties means cc in addition
        if (!StringUtils.isEmpty((getConfig().getCc()))) {
            htmlEmail.addCc(getConfig().getCc());
        }

        if (!overrideTo && !email.getCc().isEmpty()) {
            htmlEmail.addCc(email.getCc().toArray(new String[0]));
        }

        //but cc specified in properties means bcc in adition
        if (!StringUtils.isEmpty((getConfig().getBcc()))) {
            htmlEmail.addBcc(getConfig().getBcc());
        }

        if (!overrideTo && !email.getBcc().isEmpty()) {
            htmlEmail.addBcc(email.getBcc().toArray(new String[0]));
        }

        if (getConfig()
                .isBccSender()) {
            log.debug("adding sender as bcc= {}", from);
            htmlEmail.addBcc(from);
        } else {
            log.debug("NOT adding sender as bcc= {}", from);
        }

        log.debug(
                "Sending email: from={}, to={}, cc={}, bcc={}, subject={}, attachments={}",
                htmlEmail.getFromAddress(),
                htmlEmail.getToAddresses(), htmlEmail.getCcAddresses(), htmlEmail.getBccAddresses(),
                htmlEmail.getSubject(), email.getAttachments().size());

        for (final ByteArrayAttachment attachment
                : email.getAttachments()) {
            log.debug("Attaching {} size = {} ", attachment.getName(), FileUtils.byteCountToDisplaySize(
                    attachment.getData().length));
            htmlEmail.attach(attachment, attachment.getName(), "");
        }

        htmlEmail.send();

        log.debug(
                "Email sent: from={}, to={}, cc={}, bcc={}, subject={}, attachments={}", htmlEmail.getFromAddress(),
                htmlEmail.getToAddresses(), htmlEmail.getCcAddresses(), htmlEmail.getBccAddresses(),
                htmlEmail.getSubject(), email.getAttachments().size());
    }

    @Getter
    private static class ImageHtmlEmailContents implements DataSourceResolver {
        final String emeddedImagePreffix = "data:image/";

        final String emeddedImageSuffix = ";base64,";

        public String body;

        public Map<String, byte[]> images = new HashMap<>();

        public ImageHtmlEmailContents(byte[] zipFile) {
            try {
                try (ZipInputStream stream = new ZipInputStream(new ByteArrayInputStream(zipFile))) {
                    ZipEntry entry;

                    while ((entry = stream.getNextEntry()) != null) {
                        byte[] content = IOUtils.toByteArray(stream);
                        if (entry.getName().endsWith("html")) {
                            body = new String(content);
                            if (body.contains(emeddedImagePreffix)) {
                                org.jsoup.nodes.Document d = org.jsoup.Jsoup.parse(body);
                                org.jsoup.select.Elements elements = d.select("*");
                                int counter = 0;
                                for (org.jsoup.nodes.Element e : elements) {
                                    for (org.jsoup.nodes.Attribute a : e.attributes()) {
                                        String val = a.getValue();
                                        if (val.startsWith(emeddedImagePreffix)) {
                                            String type = val.substring(emeddedImagePreffix.length(), val.indexOf(
                                                    emeddedImageSuffix));
                                            val = val.substring(
                                                    val.indexOf(emeddedImageSuffix) + emeddedImageSuffix.length());
                                            byte[] barr = java.util.Base64.getDecoder().decode(val);
                                            String name = "image-" + (counter++) + "." + type;
                                            a.setValue(name);
                                            images.put(name, barr);
                                        }
                                    }
                                }
                                body = d.toString();
                            }

                        } else {
                            images.put(entry.getName(), content);
                        }
                    }
                }

            } catch (Exception e) {
                log.error("exception inflating zip byte array", e);
                throw new RuntimeException("exception inflating zip byte array", e);
            }
        }

        @Override
        public DataSource resolve(String resourceLocation) throws IOException {
            return resolve(resourceLocation, false);
        }

        @Override
        public DataSource resolve(String resourceLocation, boolean isLenient) throws IOException {
            //docmosis 3.0.6 references the images as if they were in the parent directory.
            //final String name = new File(resourceLocation).getName();
            final String name = new File(resourceLocation).getName();
            final byte[] data = images.get(name);
            final String type = MimeUtils.getMimeType(data, name);

            return new DataSource() {
                @Override
                public InputStream getInputStream() throws IOException {
                    return new ByteArrayInputStream(data);
                }

                @Override
                public OutputStream getOutputStream() throws IOException {
                    throw new UnsupportedOperationException("Not supported.");
                }

                @Override
                public String getContentType() {
                    return type;
                }

                @Override
                public String getName() {
                    return name;
                }
            };
        }
    }

    public static void main(String[] args) throws Exception {
        org.jsoup.nodes.Document d = org.jsoup.Jsoup.parse(new File("/home/pablo/Desktop/test.hml"), "UTF-16");
        org.jsoup.select.Elements images = d.select("img[src^=data:image/png;base64,]");
        for (org.jsoup.nodes.Element e : images) {
            String val = e.attr("src");
            String prefix = "data:image/png;base64,";
            val = val.substring(prefix.length());
        }
    }
}
