package com.anahata.util.email;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import static org.apache.commons.lang3.StringUtils.*;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class EmailUtils {
    /**
     * Characters other than letters and digits that are valid mail address characters:
     * <code>! # $ % & ' * + - / = ? ^ _ ` { | } ~</code>. Source:
     * http://stackoverflow.com/questions/2049502/what-characters-are-allowed-in-email-address
     */
    private static final List<Character> EMAIL_ADDRESS_ADDITIONAL_VALID_CHARS = (List<Character>)Collections.unmodifiableList(
            Arrays.asList(
            new Character[]{'@', '!', '#', '$', '%', '&', '\'', '*', '+', '-', '/', '=', '?', '^', '_', '`', '{', '|', '}', '~', '.'}));

    /**
     * Checks if a given character is a valid email address character.
     *
     * @param c - the character to evaluate
     * @return - true if it is a letter, a digit or any of {@link #EMAIL_ADDRESS_ADDITIONAL_VALID_CHARS}
     */
    public static boolean isValidEmailAddressChar(char c) {
        if (Character.isLetterOrDigit(c) || EMAIL_ADDRESS_ADDITIONAL_VALID_CHARS.contains(c)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Parse email addresses found in text.
     *
     * @param text The text to parse. Can be null.
     * @return The email addresses. Never null.
     */
    public static List<String> parseEmailAddresses(String text) {
        List<String> emails = new ArrayList<>();

        if (text != null) {
            Pattern pattern = Pattern.compile(
                    "([_A-Za-z0-9-]+)(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})");
            Matcher matcher = pattern.matcher(text);

            while (matcher.find()) {
                emails.add(matcher.group());
            }
        }

        return emails;
    }

    /**
     * Checks if a given string is a valid email address.
     *
     * @param email the email address.
     * @return true if valid.
     */
    public static boolean isValidEmailAddress(String email) {
        try {
            new InternetAddress(email).validate();
        } catch (AddressException ex) {
            return false;
        }
        return true;
    }
    
    /**
     * Checks if a given string is a valid email address.
     *
     * @param email the email address.
     * @return true if valid.
     */
    public static boolean isValidEmailAddresses(String email) {
        try {
            InternetAddress[] ia = InternetAddress.parse(email, false);
            for (InternetAddress ia1 : ia) {
                ia1.validate();
            }
        } catch (AddressException ex) {
            return false;
        }
        return true;
    }
    
    /**
     * Parses the addresses in Unicode format.
     * 
     * @param s
     * @return
     * @throws AddressException 
     */
    public static String[] parseAddresses(String s) throws AddressException {
        if (!isBlank(s)) {
            InternetAddress[] ia = InternetAddress.parse(s);
            String[] ret = new String[ia.length];
            for (int i = 0; i < ia.length; i++) {
                ret[i] = ia[i].toUnicodeString();
            }
            return ret;
        }
        return new String[0];
    }
    
    /**
     * Parses the enmail address component of the email address (excluding the persons's name if present).
     * 
     * @param s
     * @return
     * @throws AddressException 
     */
    public static String[] parseAddressesOnly(String s) throws AddressException {
        if (!isBlank(s)) {
            InternetAddress[] ia = InternetAddress.parse(s);
            String[] ret = new String[ia.length];
            for (int i = 0; i < ia.length; i++) {
                ret[i] = ia[i].getAddress();
            }
            return ret;
        }
        return new String[0];
    }
}
