package com.anahata.util.text.highlight;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.lang.StringLengthComparator;
import com.anahata.util.search.AnahataSearchUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Utility class to aid in highlighting tokens (e.g. search tokens) in texts.
 *
 * @author pablo
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class HighlightUtils {
    /**
     * Splits a string into tokens that either match or do not match any of the strings inside a criteria array. When
     * matching, white spaces are ignored. Criteria tokens are prioritized by longest first.
     *
     * @param text           the source string
     * @param criteriaTokens the source criteria.
     * @return
     */
    public static List<TextMatchToken> textMatchTokens(String text, String... criteriaTokens) {
        List<TextMatchToken> ret = new ArrayList<>();

        // As one token can contain other tokens, we will search for longest tokens first, then smaller ones.
        Arrays.sort(criteriaTokens, StringLengthComparator.INSTANCE);

        // Start with one long token which contains the entire 'source' string.
        ret.add(new TextMatchToken(text, false));

        for (int i = 0; i < criteriaTokens.length; i++) {
            String criteriaToken = criteriaTokens[i];

            if (criteriaToken.isEmpty()) {
                continue;
            }

            for (int j = 0; j < ret.size();) {
                TextMatchToken currentToken = ret.get(j);

                if (!currentToken.isMatching()) {
                    //this token has not had any matches yet, let's process it

                    List<TextMatchToken> l = split(currentToken.getString(), criteriaToken);

                    //all right, so the token we where processing has either been divided into subtokens that match or
                    //don't match or it didn't match the criteriaToken being processed.

                    if (l.size() > 1 || l.iterator().next().isMatching()) {
                        //it matches criteriaToken, so let's convert the token into subtokens
                        //and update the master list (sourceTokens)
                        ret.remove(j);
                        ret.addAll(j, l);
                        j = j + l.size();
                    } else {
                        //didn't match, let's continue with the next token
                        j++;
                    }

                } else {
                    //this taken already matches one of the criteria tokens
                    j++;
                }
            }
        }

        return ret;

    }

    /**
     * Similar to String.split() but with two differences: 1) returns delimiter (Taken from
     * http://stackoverflow.com/questions/275768/is-there-a-way-to-split-strings-with-string-split-and-include-the-delimiters}
     * and 2)ignores white spaces in source.
     *
     * @param source  the string
     * @param pattern the pattern
     * @return the tokens
     */
    private static List<TextMatchToken> split(String source, String criteria) {
        assert source != null;
        assert criteria != null;
        List<TextMatchToken> ret = new ArrayList<>();

        if (source.isEmpty()) {
            ret.add(new TextMatchToken(source, false));
            return ret;
        }

        Pattern pattern = AnahataSearchUtils.makeTokenMatchPattern(criteria);
        Matcher m = pattern.matcher(source);

        int start = 0;

        while (m.find()) {
            String preffix = source.substring(start, m.start());
            String token = m.group();
            start = m.end();

            if (!preffix.isEmpty()) {
                ret.add(new TextMatchToken(preffix, false));
            }

            if (!token.isEmpty()) {
                ret.add(new TextMatchToken(token, true));
            }
        }

        if (start < source.length()) {
            String suffix = source.substring(start);

            if (!suffix.isEmpty()) {
                ret.add(new TextMatchToken(suffix, false));
            }
        }

        return ret;
    }
}
