/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.io;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.jms.JMSException;
import javax.jms.Message;
import javax.servlet.http.HttpServletRequest;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnahataSerializationUtils {
    public static final String HEADER = "AnahataSerialization";

    public static SerializationType readHeader(HttpServletRequest request) {
        return parse(request.getHeader(HEADER));
    }

    public static SerializationType readHeader(Message message) throws JMSException {
        return parse(message.getStringProperty(HEADER));
    }

    public static SerializationType parse(String serialization) {
        SerializationType ret = null;
        if (serialization != null) {
            ret = SerializationType.valueOf(serialization);
        }
        return ret;
    }
}
