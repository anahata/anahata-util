/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.io;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.progress.ProgressListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Extension methods to commons-io AnahataIOUtils.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnahataIOUtils {
    /**
     * The default buffer size to use.
     */
    public static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

    /**
     * Copies an input stream to an output stream notifiying a progress listener of the copying progress
     *
     * @param input
     * @param output
     * @param length
     * @param progressListener
     * @return
     * @throws IOException
     */
    public static long copy(InputStream input, OutputStream output, long length, ProgressListener progressListener)
            throws IOException {
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        long count = 0;
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
            if (count > 0 && length > 0) {
                progressListener.progress(length / count);
            }
        }
        progressListener.progress(1);
        return count;
    }
}
