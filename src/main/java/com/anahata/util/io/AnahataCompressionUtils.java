/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.io;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.servlet.http.HttpServletRequest;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnahataCompressionUtils {
    public static final String HEADER = "AnahataCompression";

    private static final CompressorStreamFactory compressorStreamFactory = new CompressorStreamFactory();

    public static CompressionType readHeader(HttpServletRequest request) {
        return parse(request.getHeader(HEADER));
    }

    public static CompressionType readHeader(Message message) throws JMSException {
        return parse(message.getStringProperty(HEADER));
    }

    public static CompressionType parse(String serialization) {
        CompressionType ret = null;
        if (serialization != null) {
            ret = CompressionType.valueOf(serialization);
        }
        return ret;
    }

    public static String getRatio(long uncompressed, long compressed) {
        double ratio = 100 * (compressed + 1) / (uncompressed + 1);
        return ((int)(100 - ratio)) + "%";
    }

    public static File compress(CompressionType compressionType, File source) throws IOException, CompressorException {
        long ts = System.currentTimeMillis();
        log.debug("Compressing {}", source);

        File tempFile = File.createTempFile("anahata-compress-", "." + compressionType.toString().toLowerCase());
        tempFile.deleteOnExit();
        FileOutputStream fos = new FileOutputStream(tempFile);
        try (OutputStream compressorOutputStream = newCompressingOutputStream(compressionType, fos);
                FileInputStream fis = new FileInputStream(source)) {
            IOUtils.copy(fis, compressorOutputStream);
        }
        ts = System.currentTimeMillis() - ts;
        log.debug("Compressing {} took {} ms, original {}, compressed {}, ratio {}", source, ts, source.length(),
                tempFile.length(), getRatio(source.length(),
                        tempFile.length()));
        return tempFile;
    }

    public static byte[] compress(CompressionType compressionType, byte[] source) throws IOException,
            CompressorException {
        if (compressionType == com.anahata.util.io.CompressionType.NONE) {
            return source;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        OutputStream os = newCompressingOutputStream(compressionType, baos);
        IOUtils.write(source, os);
        os.flush();
        os.close();
        byte[] ret = baos.toByteArray();
        if (log.isDebugEnabled()) {
            BigDecimal ratio = new BigDecimal(100f * ((float)ret.length / (float)source.length));
            ratio = ratio.setScale(2, RoundingMode.HALF_UP);
            log.trace("{} compression: source={}, compressed={}, ratio={}%", compressionType, source.length, ret.length,
                    ratio);
            if (ratio.intValue() > 100) {
                log.trace("{} compression made data bigger: source={}, compressed={}, ratio={}%", compressionType,
                        source.length, ret.length,
                        ratio);
            }
        }

        return ret;
    }

    public static byte[] uncompress(CompressionType compressionType, byte[] source) throws IOException,
            CompressorException {
        if (compressionType == com.anahata.util.io.CompressionType.NONE) {
            return source;
        }
        ByteArrayInputStream bais = new ByteArrayInputStream(source);
        InputStream is = newUncompressingInputStream(compressionType, bais);
        byte[] ret = IOUtils.toByteArray(is);
        log.trace("{} decompressed size : {}", compressionType, ret.length);
        return ret;
    }

    public static InputStream newUncompressingInputStream(CompressionType compressionType, InputStream is) throws
            CompressorException {

        if (com.anahata.util.io.CompressionType.NONE == compressionType) {
        } else if (com.anahata.util.io.CompressionType.DEFLATE == compressionType) {
            is = new InflaterInputStream(is);
        } else {
            is = compressorStreamFactory.createCompressorInputStream(compressionType.name(), is);
        }

        log.trace("InputStream: {}", is.getClass().getSimpleName());
        return is;
    }

    public static OutputStream newCompressingOutputStream(CompressionType compressionType, OutputStream os) throws
            CompressorException {
        if (com.anahata.util.io.CompressionType.NONE == compressionType) {
            //do nothing
        } else if (com.anahata.util.io.CompressionType.DEFLATE == compressionType) {
            os = new DeflaterOutputStream(os, new Deflater(Deflater.BEST_COMPRESSION));
        } else {
            os = compressorStreamFactory.createCompressorOutputStream(compressionType.name(), os);
        }
        return os;
    }
}
