/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.io;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import java.io.File;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * Monitors a file for changes.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FileMonitor implements Runnable {
    /**
     * Check the file every half a second.
     */
    public static final long POLL_INTERVAL = 500;

    /**
     * Thread pool for file modification checking.
     */
    public static ExecutorService fileWatchThreadPool = Executors.newCachedThreadPool(new ThreadFactory() {
        volatile int threadCount = 1;

        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r);
            t.setName("File modif thread " + (threadCount++));
            t.setDaemon(true);
            t.setPriority(Thread.MIN_PRIORITY);
            return t;
        }
    });

    /**
     * The file we are monitoring.
     */
    @Getter
    private java.io.File file;

    /**
     *
     */
    @Getter
    private FileModificationListener listener;

    /**
     * Boolean flag that indicates if the monitor is running.
     */
    @Getter
    private boolean running = true;

    /**
     * Creates the file modfication monitor and starts monitoring for changes.
     *
     * @param file     the file being monitored. Required.
     * @param listener the file modification listener.
     *
     */
    public FileMonitor(File file, FileModificationListener listener) {
        Validate.notNull(file, "file is required");
        this.file = file;
        this.listener = listener;
        fileWatchThreadPool.submit(this);
    }

    /**
     * Stops monitoring the file. The thread ends.
     */
    public void close() {
        running = false;
    }

    /**
     * Monitors the file while {@link #running} is <tt>true</tt> and shows a confirmation dialog which invokes
     * <code>item.getActionSupport().update(file)</code> if the user is happy to upload the new version.
     */
    @Override
    public void run() {
        log.debug("File monitor launched for {}", file);

        //store lastPrompTime so if the user decides not to update a file or the dialog is open while
        //the file gets modified again, we only display the dialog once.
        long lastPromptTime = file.lastModified();

        while (running && !Thread.currentThread().isInterrupted()) {

            //regardless of the information contained in the WatchKey event,
            //we'll simply check if the file that was modified in the file system
            //has a later timestamp thatn the one in google drive
            if (file.lastModified() > lastPromptTime) {

                listener.fileModified(this, new Date(lastPromptTime), new Date(file.lastModified()));
                lastPromptTime = file.lastModified();
                log.trace("File got modified file={}, prev modified date ={}, current modified date={}", file,
                        new Date(lastPromptTime), file.lastModified());

            } else {
                log.trace("File {} hasn't changed last modified = {}", file.getName(), new Date(file.lastModified()));
            }

            try {
                //this causes the thread to wait
                Thread.sleep(POLL_INTERVAL);
            } catch (InterruptedException e) {
                log.warn("Watch interrupted while monitoring file " + file, e);
            }

            log.trace("running = {}, interrupted={}", running, Thread.currentThread().isInterrupted());

        }
        running = false;
        log.debug("File monitor finished for {}", file);

    }

}
