/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.io;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import static com.anahata.util.io.AnahataFileUtils.*;
import com.anahata.util.plaf.AnahataFilenameUtils;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.*;
import static java.util.Arrays.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.commons.lang3.ArrayUtils;
import static com.anahata.util.plaf.OSUtils.*;

/**
 * Apache commons FileUtils extension.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnahataFileUtils {

    /**
     * Creates a temp file with a specified name. The file will be created under a temp directory within the temp dir
     * for the only purpose of having the exact file name given in <tt>name</tt>.
     *
     * Both file and containing directory are marked for deletion on JVM exit.
     *
     * @param name the file name
     * @return the temp file
     * @throws IOException
     */
    public static File createTempFile(String name, boolean deleteOnExit) throws IOException {
        return createTempFile(name, null, deleteOnExit);
    }

    /**
     * Creates a temp file with a specified name and data. The file will be created under a temp directory within the
     * temp dir for the only purpose of having the exact file name given in <tt>name</tt>.
     *
     * Both file and containing directory are marked for deletion on JVM exit.
     *
     * @param name the file name
     * @param data the data
     * @return the temp file
     * @throws IOException
     */
    public static File createTempFile(String name, byte[] data, boolean deleteOnExit) throws IOException {
        name = AnahataFilenameUtils.toValidFileName(name);
        File tempDir = FileUtils.getTempDirectory();
        tempDir = new File(tempDir, UUID.randomUUID().toString());
        tempDir.mkdirs();
        if (deleteOnExit) {
            tempDir.deleteOnExit();
        }
        File tempFile = new File(tempDir, name);
        if (deleteOnExit) {
            tempFile.deleteOnExit();
        }
        if (data != null) {
            FileUtils.writeByteArrayToFile(tempFile, data);
        }
        return tempFile;
    }

    public static File createTempDir(String name, boolean unique, boolean deleteOnExit) {
        name = AnahataFilenameUtils.toValidFileName(name);
        File tempDir = FileUtils.getTempDirectory();
        if (unique) {
            tempDir = new File(tempDir, UUID.randomUUID().toString());
            if (deleteOnExit) {
                tempDir.deleteOnExit();
            }
        }
        tempDir = new File(tempDir, name);
        if (deleteOnExit) {
            tempDir.deleteOnExit();
        }
        tempDir.mkdirs();
        return tempDir;
    }

    /**
     * Gets the Application Data directory for Windows, the user-dir/Library/Application Suport directory on Mac or the
     * user home on linux.
     *
     * @return the application data directory.
     */
    public static File getApplicationDataDirectory(String appName) {
        File ret;
        if (isWindows()) {
            ret = new File(System.getenv("APPDATA") + File.separator + appName);
        } else if (isMac()) {
            ret = new File(
                    getUserHome() + File.separator + "Library" + File.separator + "Application Support" + File.separator + appName);
        } else {
            ret = new File(getUserHome(), "." + appName);
        }
        ret.mkdirs();
        return ret;
    }

    /**
     * Gets, creating if necessary a directory called .anahata under the user home directory.
     *
     * @return
     */
    public static File getAnahataUserDirectory() {
        File f = new File(getUserHome(), ".anahata");
        f.mkdirs();
        return f;
    }

    /**
     * Returns a directory under the anahata user directory creating a new one if it doesn't exist.
     *
     * @param name the directory name
     * @return the directory
     */
    public static File getAnahataUserDirectory(String name) {
        File f = new File(getAnahataUserDirectory(), name);
        if (!f.exists()) {
            f.mkdir();
        }
        return f;
    }

    /**
     * Gets a directory called '.anahata' under {@link #getApplicationDataDirectory() }. Creates the directory if
     * required.
     *
     * @return a directory called '.anahata' under {@link #getApplicationDataDirectory() }.
     */
    public static File getAnahataAppDataDirectory() {
        File f = getApplicationDataDirectory("anahata");
        f.mkdirs();
        return f;
    }

    /**
     * Returns a directory under the 'anahata' {@link #getApplicationDataDirectory() } . Create the directory if doesn't
     * exist
     *
     * @param name the directory name
     * @return
     */
    public static File getAnahataAppDataDirectory(String name) {
        File f = new File(AnahataFileUtils.getAnahataAppDataDirectory(), name);
        if (!f.exists()) {
            f.mkdir();
        }
        return f;
    }

    /**
     * Convert the given file to byte array.
     *
     * @param filePath Path of the file.
     * @return Byte array.
     */
    public static byte[] convertToByteArray(String filePath) {
        File file = new File(filePath);
        byte[] b = new byte[(int)file.length()];
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            fileInputStream.read(b);
        } catch (FileNotFoundException e) {
            log.error("File Not Found {}", filePath);
        } catch (IOException e1) {
            log.error("Error in reading the file {}", filePath);
        }
        return b;
    }

    /**
     * Deletes files in a directory older than a given date.
     *
     * @param file
     * @param thresholdDate
     */
    public static void deleteFilesOlderThan(File file, Date thresholdDate) {
        Iterator<File> filesToDelete = FileUtils.iterateFiles(
                file,
                new AgeFileFilter(thresholdDate), null);

        while (filesToDelete.hasNext()) {
            File fileToDelete = filesToDelete.next();

            try {
                log.debug("deleting file filename= {}", fileToDelete.getName());
                fileToDelete.delete();
            } catch (Throwable t) {
                // cathing here so it continues deleting files in case some file is locked etc...
                log.error("could not delte file= {}", fileToDelete.getAbsolutePath(), t);
            }
        }
    }

    /**
     * Reads string from a file.
     *
     * @param file   where to read file from
     * @param offset initial offset into buffer
     * @param length length to read, must be &gt;= 0
     * @return string
     * @throws IOException if a read error occurs
     */
    public static String readString(File file, int offset, int length) throws IOException {
        int totalLength = offset + length;
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            byte[] arr1 = new byte[totalLength];
            read(fileInputStream, arr1, 0, totalLength);
            byte[] arr2 = ArrayUtils.subarray(arr1, offset, totalLength);
            return new String(arr2, "UTF-8");
        }
    }

    /**
     * Reads bytes from an input stream. This implementation guarantees that it will read as many bytes as possible
     * before giving up; this may not always be the case for subclasses of {@link InputStream}.
     *
     * @param input  where to read input from
     * @param buffer destination
     * @param offset initial offset into buffer
     * @param length length to read, must be &gt;= 0
     * @return actual length read; may be less than requested if EOF was reached
     * @throws IOException if a read error occurs
     */
    public static int read(final InputStream input, final byte[] buffer, final int offset, final int length) throws
            IOException {
        int EOF = -1;
        if (length < 0) {
            throw new IllegalArgumentException("Length must not be negative: " + length);
        }
        int remaining = length;
        while (remaining > 0) {
            final int location = length - remaining;
            final int count = input.read(buffer, offset + location, remaining);
            if (EOF == count) { // EOF
                break;
            }
            remaining -= count;
        }
        return length - remaining;
    }

    public static boolean isFileOpenedInAnotherProcess(File file) {
        boolean flag = false;
        try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
            FileChannel channel = raf.getChannel();
            FileLock lock = null;
            try {
                lock = channel.tryLock();
            } catch (OverlappingFileLockException e) {
                flag = true;
            } finally {
                if (lock != null) {
                    lock.release();
                }
            }
        } catch (IOException e) {
            flag = true;
        }
        return flag;
    }

    /**
     * Gets the total size of a directory or the size of a file if the argument is a normal file.
     * 
     * @param fileOrDir
     * @return 
     */
    public static long getTotalSize(File fileOrDir) {

        if (fileOrDir.isFile()) {
            return fileOrDir.length();
        } else {            
            return getTotalSize(asList(fileOrDir.listFiles()));
        }
    }

    /**
     * Gets the total size of a collection of files and / or directories (recursive).
     * 
     * @param files the files and / or directories
     * @return the total size
     */
    public static long getTotalSize(Collection<File> files) {
        return files.parallelStream().mapToLong( (f) -> getTotalSize(f)).sum();
    }
    
    /**
     * Gets the total size of a collection of files and / or directories (recursive).
     * 
     * @param files the files and / or directories
     * @return the total size
     */
    public static long getTotalFiles(Collection<File> files) {
        return files.parallelStream().mapToLong((f) -> f.isFile() ? 1 : f.listFiles() != null ? getTotalFiles(asList(f.listFiles())) : 1 ).sum();
    }
    
    public static void main(String[] args) {
        System.out.println("Total files " + getTotalFiles(asList(new File("/home/pablo/NetBeansProjects"))));
    }
}
