package com.anahata.util.metamodel;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

/**
 * General utilities for metamodel processing.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MetaModelUtils {
    /**
     * Convert an array of metamodel property names into a nested path separated by periods.
     *
     * @param props The properties. Required.
     * @return The property path.
     * @throws NullPointerException If props is null.
     */
    public static String getNestedPropertyName(MetaModelProperty[] props) {
        Validate.notNull(props);
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < props.length; i++) {
            if (i > 0) {
                sb.append('.');
            }

            sb.append(props[i].getName());
        }

        return sb.toString();
    }
}
