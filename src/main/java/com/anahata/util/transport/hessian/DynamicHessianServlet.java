package com.anahata.util.transport.hessian;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.util.transport.ServiceLookupUtils;
import com.caucho.hessian.io.SerializerFactory;
import com.caucho.hessian.server.HessianSkeleton;
import com.caucho.services.server.ServiceContext;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * Execute an EJB invoked via Hessian.
 *
 * @author Robert Nagajek
 */
@Slf4j
@WebServlet(name = "DynamicHessianServlet", urlPatterns = {"/hessian/*"})
public class DynamicHessianServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private HessianSkeleton homeSkeleton;

    private SerializerFactory serializerFactory;

    public DynamicHessianServlet() {
    }

    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    public String getServletInfo() {
        return "Dynamic Hessian Servlet";
    }

    public SerializerFactory getSerializerFactory() {
        if (serializerFactory == null) {
            serializerFactory = new SerializerFactory();
        }

        return serializerFactory;
    }

    public void setSendCollectionType(boolean sendType) {
        getSerializerFactory().setSendCollectionType(sendType);
    }

    @Override
    public void service(ServletRequest request, ServletResponse response) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;

        if (!req.getMethod().equals("POST")) {
            res.sendError(500, "Hessian Requires POST");
            PrintWriter out = res.getWriter();
            res.setContentType("text/html");
            out.println("<h1>Hessian Requires POST</h1>");
            return;
        }

        String serviceId = req.getPathInfo().substring(1);
        String objectId = req.getParameter("id");

        if (objectId == null) {
            objectId = req.getParameter("ejbid");
        }

        Object user = ((HttpServletRequest)request).getUserPrincipal();

        log.trace("serviceId={}, objectId={}, user={}", serviceId, objectId, user);

        ServiceContext.begin(req, res, serviceId, objectId);

        try {
            InputStream is = request.getInputStream();
            OutputStream os = response.getOutputStream();

            response.setContentType("application/x-hessian");
            SerializerFactory serFactory = getSerializerFactory();

            Class<?> serviceInterface = loadClass(serviceId);

            Object service = ServiceLookupUtils.serviceLookup(serviceInterface);

            homeSkeleton = new HessianSkeleton(service,
                    serviceInterface);
            homeSkeleton.invoke(is, os, serFactory);
        } catch (RuntimeException | ServletException e) {
            throw e;
        } catch (Throwable e) {
            throw new ServletException(e);
        } finally {
            ServiceContext.end();
        }
    }

    private Class<?> loadClass(String className) throws ClassNotFoundException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        if (loader != null) {
            return Class.forName(className, false, loader);
        }

        return Class.forName(className);
    }
}
