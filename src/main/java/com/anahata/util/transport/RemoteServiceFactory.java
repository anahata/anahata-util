package com.anahata.util.transport;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.net.MalformedURLException;
import javax.naming.NamingException;

/**
 * CDI producer for hessian service implementations.
 *
 * @author Robert Nagajek
 */
public interface RemoteServiceFactory {
    /**
     * Creates a hessian client stub instance for a given interface
     *
     * @param <T>   Class type to instantiate.
     * @param clazz The class to instantiate.
     * @return The instantiated object.
     * @throws Exception If there is an error.
     */
    <T> T create(Class<T> clazz) throws Exception;

    <T> T createHessianProxy(Class<T> type) throws MalformedURLException;

    <T> T createRpcProxy(Class<T> type) throws MalformedURLException;

    <T> T createRemoteEJBProxy(Class<T> type) throws MalformedURLException, NamingException;
}
