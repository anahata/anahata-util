/*
 *  Copyright © - 2014 Anahata Technologies.
 */
package com.anahata.util.transport.rpc.client.impl;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.application.JWSUtils;
import com.anahata.util.config.internal.AnahataUtilConfig;
import com.anahata.util.env.ApplicationInstanceUUID;
import com.anahata.util.io.AnahataCompressionUtils;
import com.anahata.util.io.AnahataSerializationUtils;
import com.anahata.util.transport.rpc.Rpc;
import com.anahata.util.transport.rpc.RpcStreamer;
import com.anahata.util.transport.rpc.client.RpcClientTransport;
import java.io.IOException;
import java.net.URL;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpHost;
import org.apache.http.StatusLine;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.*;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@ApplicationScoped
@Slf4j
public class CommonsRpcClientTransport implements RpcClientTransport {

    @Inject
    private JWSUtils jwsUtils;
    
    @Inject
    private AnahataUtilConfig anahataUtilConfig;

    @Getter
    private CloseableHttpClient httpClient;

    @Getter
    private HttpHost targetHost;

    @Inject
    @RpcCredentialsProvider
    private Instance<CredentialsProvider> credentialsProviderFactory;

    @PostConstruct
    public void init() {
        targetHost = new HttpHost(jwsUtils.getApplicationHost(), jwsUtils.getApplicationPort(),
                jwsUtils.getApplicationProtocol());        
        httpClient = HttpClients.custom()
                .disableAutomaticRetries().setMaxConnPerRoute(20).build();
                
    }

    public CloseableHttpResponse execute(HttpUriRequest method) throws IOException {
        log.trace("executing {}", method);

        CredentialsProvider credentialsProvider = credentialsProviderFactory.isUnsatisfied() ? null : credentialsProviderFactory.get();
        if (credentialsProvider == null) {
            log.info("No credentials provider found via CDI @RpcCredentialsProvider");
        }
        //preemtpive auth
        AuthCache authCache = new BasicAuthCache();
        BasicScheme basicAuth = new BasicScheme();
        authCache.put(targetHost, basicAuth);
        HttpClientContext context = HttpClientContext.create();
        context.setCredentialsProvider(credentialsProvider);
        context.setAuthCache(authCache);
        context.getAuthCache().put(targetHost, basicAuth);        
        //could be configured via annotation on remote interface if some server side methods were known to take more than 60 seconds
        RequestConfig rc = RequestConfig.copy(RequestConfig.DEFAULT)                
                .setConnectTimeout(anahataUtilConfig.getRpcSocketConnectTimout())
                .setSocketTimeout(anahataUtilConfig.getRpcSocketReadTimout()).build();                
        context.setRequestConfig(rc);

        //log.debug("About to execute() {}", method);
        //long ts = System.currentTimeMillis();
        CloseableHttpResponse resp = getHttpClient().execute(method, context);
        //ts = System.currentTimeMillis() - ts;
        //log.debug("method execute() finished in {} ms. for {}", ts, method);
        StatusLine sl = resp.getStatusLine();
        if (sl.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
            resp.close();
            if (credentialsProvider != null) {
                credentialsProvider.clear();
                return execute(method);
            } else {
                throw new IOException("HTTP Unauthorized and no credentials provider has been set on" + this);
            }
        }
        return resp;
    }

    @Override
    public Object sendReceive(@NonNull RpcStreamer streamer, @NonNull URL url, @NonNull Rpc req) throws Exception {
        //InputStream is = streamer.streamToInputStream(req);
        byte[] barr = streamer.stream(req);
        HttpPost post = new HttpPost(url.toURI());        
        post.setEntity(new ByteArrayEntity(barr));
        post.addHeader(AnahataCompressionUtils.HEADER, streamer.getCompressionType().name());
        post.addHeader(AnahataSerializationUtils.HEADER, streamer.getSerializationType().name());        
        post.addHeader(ApplicationInstanceUUID.HEADER, ApplicationInstanceUUID.VALUE);
        try (CloseableHttpResponse res = execute(post)) {
            StatusLine sl = res.getStatusLine();
            if (sl.getStatusCode() == HttpStatus.SC_OK) {
                return streamer.receiveStreaming(res.getEntity().getContent());
            } else {
                throw new Exception(
                        "Server retured " + sl.getStatusCode() + " " + sl.getReasonPhrase() + " for URL " + url);
            }
        }
    }

}
