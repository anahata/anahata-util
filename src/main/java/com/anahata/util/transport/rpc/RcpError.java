/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.transport.rpc;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * A server side error on a RPC call.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor
@Slf4j
public class RcpError implements Serializable {
    /**
     * The serialized exception.
     */
    private byte[] exception;

    /**
     * The exception stack trace in string format.
     */
    private String exceptionString;

    /**
     * Constructor giving the server side exception.
     *
     * @param t
     */
    public RcpError(Throwable t) {
        try {
            exception = SerializationUtils.serialize(t);
        } catch (Exception e) {
            log.debug("Non serializable exception", e);
        }
        
        exceptionString = ExceptionUtils.getStackTrace(t);
    }

    /**
     * Gets the server side exception if it can be deserialized or a {@link NonDeserializableException} with a string
     * version of the server side exception stack trace if it can't.
     *
     * @return
     */
    public Throwable getException() {
        try {
            return (Throwable)SerializationUtils.deserialize(exception);
        } catch (Exception e) {
            return new NonDeserializableException(exceptionString);
        }
    }

    /**
     * the server side exception if it can be deserialized or a {@link NonDeserializableException} with a string version
     * of the server side exception stack trace if it can't.
     *
     * @throws Throwable
     */
    public void doThrow() throws Throwable {
        Throwable t = getException();
        log.debug("RpcError throwing {}", t.toString());
        throw t;
    }

    @Override
    public String toString() {
        return "RcpError{" + "exception=" + exception + ", exceptionString=" + exceptionString + '}';
    }
}
