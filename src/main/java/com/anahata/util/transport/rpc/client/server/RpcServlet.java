package com.anahata.util.transport.rpc.client.server;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.cdi.Cdi;
import com.anahata.util.transport.ServiceLookupUtils;
import com.anahata.util.transport.rpc.RcpError;
import com.anahata.util.transport.rpc.Rpc;
import com.anahata.util.transport.rpc.RpcStreamer;
import com.anahata.util.validation.ValidationUtils;
import com.anahata.util.web.ServletUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.ejb.EJBException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * Servlet for RPC invocations.
 * 
 * @author pablo
 */
@Slf4j
@WebServlet(name = "RpcServlet", urlPatterns = {"/rpc/*"})
public class RpcServlet extends HttpServlet {

    public RpcServlet() {
    }

    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    public String getServletInfo() {
        return "Rpc Servlet";
    }

    @Override
    public void service(ServletRequest request, ServletResponse response) throws IOException, ServletException {
        long tsEntry = System.currentTimeMillis();
        if (log.isTraceEnabled()) {
            log.trace(ServletUtils.headersToString(request));
        }

        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;

        if (!req.getMethod().equals("POST")) {
            res.sendError(500, "Rpc Servlet Requires POST");
            PrintWriter out = res.getWriter();
            res.setContentType("text/html");
            out.println("<h1>Rpc Servlet Requires POST</h1>");
            return;
        }

        RpcStreamer streamer = Cdi.get(RpcStreamer.class);
        streamer.init(req);

        Rpc rpc = null;
        Method method = null;
        Serializable ret = null;
        Throwable exception = null;
        try {

            long tsReceive = System.currentTimeMillis();
            rpc = streamer.receiveStreaming(req.getInputStream());
            tsReceive = System.currentTimeMillis() - tsReceive;
            log.trace("Received request: {} in {} ms.", rpc, tsReceive);

            Class<?> serviceInterface = rpc.getClazz();
            Object service = ServiceLookupUtils.serviceLookup(serviceInterface);
            Validate.notNull(service, "Could not look up service object for %s", serviceInterface);

//            method = rpc.getMethod();
//            Validate.notNull(method, "Method was null on Rpc %s", rpc);
//            log.trace("Invoking {} with args {}: ", method, rpc.getArgs());
//            if (log.isTraceEnabled()) {
//                if (rpc.getArgs() != null) {
//                    for (Object o : rpc.getArgs()) {
//                        log.trace("args type for {} = {} ", o, o != null ? o.getClass() : " <none>");
//                    }
//                }
//            }

            long tsInvoke = System.currentTimeMillis();
            ret = (Serializable)rpc.invoke(service);
            tsInvoke = System.currentTimeMillis() - tsInvoke;
            log.trace("Invocation of {} took {} ms.", method, tsInvoke);

        } catch (Throwable e) {
            Throwable cause = e;
            String s = ValidationUtils.getConstraintValidationDetails(e);
            if (s == null) {
                s = " ";
            }
            log.error("Exception caught from service invocation " + s, e);
            while ((cause instanceof InvocationTargetException || cause instanceof EJBException) && (cause.getCause() != null)) {
                log.debug("Unwrapping {}", e.toString());
                cause = cause.getCause();
            }
            log.debug("Creating throwable for {}", cause.toString());

            ret = new RcpError(cause);
        }

        try {
//            if (log.isDebugEnabled()) {
//                RpcStreamerBenchmark bm = new RpcStreamerBenchmark(new RpcStreamer(), ret);
//                bm.run(); // discard one
//                log.debug("{}:{}", rpc, bm);
//            }
            //byte[] serializedAndCompressed = streamer.stream(ret);
            HttpServletResponse httpResp = (HttpServletResponse)response;
            //httpResp.setHeader("Content-Length", String.valueOf(serializedAndCompressed.length));
            httpResp.setHeader("Content-Type", "application/octet-stream");
            streamer.stream(ret, response.getOutputStream());
            //IOUtils.write(serializedAndCompressed, response.getOutputStream());
        } catch (Exception e) {
            log.error("Exception serializing response from " + rpc, e);
            throw new ServletException(e);
        }
        tsEntry = System.currentTimeMillis() - tsEntry;
        log.trace("RpcServlet took {} for {}.", tsEntry, method, method);
    }
}
