/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.transport.rpc;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.cdi.Cdi;
import com.anahata.util.io.AnahataCompressionUtils;
import com.anahata.util.io.AnahataSerializationUtils;
import com.anahata.util.io.CompressionType;
import com.anahata.util.io.SerializationType;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.lang3.Validate;

/**
 * RPC invocation.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Getter
@Slf4j
public final class Rpc implements Serializable {

    private static final Map<String, Class<?>> primitiveClasses = new HashMap<>();

    static {
        primitiveClasses.put("byte", byte.class);
        primitiveClasses.put("byte[]", byte[].class);
        primitiveClasses.put("short", short.class);
        primitiveClasses.put("short[]", short[].class);
        primitiveClasses.put("char", char.class);
        primitiveClasses.put("char[]", char[].class);
        primitiveClasses.put("int", int.class);
        primitiveClasses.put("int[]", int[].class);
        primitiveClasses.put("long", long.class);
        primitiveClasses.put("long[]", long[].class);
        primitiveClasses.put("float", float.class);
        primitiveClasses.put("float[]", float[].class);
        primitiveClasses.put("double", double.class);
        primitiveClasses.put("double[]", double[].class);
        primitiveClasses.put("boolean", boolean.class);
        primitiveClasses.put("boolean[]", boolean[].class);
    }

    /**
     * The name of the class of the object to be invoked.
     */
    private final String clazzName;

    /**
     * The name of the method in the class of the object to be invoked.
     */
    private final String methodName;

    /**
     * The name of the types of the parameters of the method to be invoked.
     */
    private final String[] methodParamTypeNames;

    /**
     * The arguments of the method to be invoked.
     */
    private final Object[] args;

    /**
     * Cache of the resolved class to be invoked (non serializable)
     */
    private transient Class clazz = null;

    /**
     * Cache of the method to be invoked (non serializable)
     */
    private transient Method method = null;

    /**
     * Cache of the parameters of the method to be invoked.
     */
    private transient Class[] methodParamTypes = null;

    /**
     * Cache of the arguments for the method to be invoked after the types have been checked for serialization type mismatches (like hessian when serializing a char[] as a String).
     */
    private transient Object[] checkedArgs = null;

    public Rpc(Method method, Object[] args) {
        this(method.getDeclaringClass(), method, args);
    }

    public Rpc(Class clazz, Method method, Object[] args) {
        this.clazz = clazz;
        this.clazzName = clazz.getName();
        this.method = method;
        this.methodName = method.getName();
        this.methodParamTypes = method.getParameterTypes();
        this.methodParamTypeNames = new String[method.getParameterTypes().length];
        for (int i = 0; i < methodParamTypeNames.length; i++) {
            methodParamTypeNames[i] = className(method.getParameterTypes()[i]);
        }
        this.args = args;
        this.checkedArgs = args;
    }

    /**
     * Gets the target class resolving it the first time this method is invoked after deserialization.
     *
     * @return
     * @throws ClassNotFoundException
     */
    public Class getClazz() {
        if (clazz != null) {
            return clazz;
        }
        try {
            clazz = Class.forName(clazzName);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load class " + clazzName, e);
        }
        
        return clazz;
    }

    /**
     * Gets the target method resolving it the first time this method is invoked after deserialization.
     *
     * @return
     * @throws NoSuchMethodException
     * @throws ClassNotFoundException
     */
    public Method getMethod() throws NoSuchMethodException, ClassNotFoundException {
        if (method != null) {
            return method;
        }
        Class loadedClazz = getClazz();
        log.trace("Class {}, ", loadedClazz);
        log.trace("Parameter Types {}, ", (Object[])getMethodParamTypes());
        log.trace("Method name {}, ", methodName);
        method = MethodUtils.getAccessibleMethod(loadedClazz, methodName, getMethodParamTypes());
        Validate.notNull(method, "Could not find accessible method for %s.%s %s", getClazz(), methodName,
                getMethodParamTypes());
        log.trace("Method {}, ", method);
        return method;
    }

    /**
     * Gets the method parameter types.
     *
     * @return
     * @throws ClassNotFoundException
     */
    private Class[] getMethodParamTypes() {
        if (methodParamTypes != null) {
            return this.methodParamTypes;
        }
        Class[] ret = new Class[methodParamTypeNames.length];
        for (int i = 0; i < methodParamTypeNames.length; i++) {
            try {
                ret[i] = loadClass(methodParamTypeNames[i]);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }

        }
        this.methodParamTypes = ret;
        return ret;
    }

    /**
     * Gets the method arguments, checking performing type conversion if required.
     *
     * @return the method arguments.
     */
    public Object[] getArgs() {
        if (args == null) {
            return args;
        }
        if (checkedArgs != null) {
            return checkedArgs;
        }
        Object[] ret = new Object[args.length];

        Class[] paramTypes = getMethodParamTypes();
        for (int i = 0; i < args.length; i++) {
            Object arg = args[i];
            if (arg != null) {
                Class type = paramTypes[i];
                if (arg.getClass() != type) {
                    //hessian quirck
                    if (arg.getClass() == String.class && type == char[].class) {
                        arg = ((String)arg).toCharArray();
                    } else {
                        arg = ConvertUtils.convert(arg, type);
                    }
                }
            }
            ret[i] = arg;
        }

        checkedArgs = ret;//avoid repetitive conversions
        return checkedArgs;
    }
    
    /**
     * Invokes the rpc on the given object.
     *
     * @param target the target object
     * @return whatever the object returns
     *
     * @throws NoSuchMethodException
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     */
    public Object invoke(Object target) throws NoSuchMethodException, ClassNotFoundException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException {
        return getMethod().invoke(target, getArgs());
    }

    @Override
    public String toString() {
        return "RpcRequest{" + "clazzName=" + clazzName + ", methodName=" + methodName + ", methodParamTypeNames=" + Arrays.toString(
                methodParamTypeNames) + ", args=" + Arrays.toString(args) + '}';
    }

    private static Class<?> loadClass(String name) throws ClassNotFoundException {
        if (primitiveClasses.containsKey(name)) {
            return primitiveClasses.get(name);
        } else {
            return Class.forName(name);
        }
    }

    private static String className(Class clazz) {
        Set<Map.Entry<String, Class<?>>> set = primitiveClasses.entrySet();
        for (Entry<String, Class<?>> entry : set) {
            if (entry.getValue() == clazz) {
                return entry.getKey();
            }
        }
        return clazz.getName();
    }

    public static Rpc parse(Message message) throws JMSException, IOException, CompressorException {
        RpcStreamer streamer = Cdi.get(RpcStreamer.class);
        CompressionType compression = AnahataCompressionUtils.readHeader(message);
        SerializationType serialization = AnahataSerializationUtils.readHeader(message);
        streamer.setCompressionType(compression);
        streamer.setSerializationType(serialization);
        BytesMessage bytesMessage = (BytesMessage)message;
        log.trace("BodyLength: {}", bytesMessage.getBodyLength());
        byte[] barr = new byte[(int)bytesMessage.getBodyLength()];
        bytesMessage.readBytes(barr);
        log.trace("received bytes {} ", barr);
        final Rpc rpc = streamer.uncompressDeserialize(barr);
        return rpc;
    }

}
