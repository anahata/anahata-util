/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.transport.rpc;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.formatting.FormattingUtils;
import com.anahata.util.io.CompressionType;
import com.anahata.util.io.SerializationType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@AllArgsConstructor
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RpcStreamerBenchmark {
    private RpcStreamer streamer;

    long fastest = Long.MAX_VALUE;

    private String fastestDesc = "";

    private long smallest = Long.MAX_VALUE;

    private String smallestDesc = "";

    private Object test;

    private long smallestTime;

    private long fastestSize;

    public RpcStreamerBenchmark(RpcStreamer streamer, Object test) {
        this.streamer = streamer;
        this.test = test;

    }

    public void run() {
        List<Result> results = new ArrayList<>();
        fastest = Long.MAX_VALUE;
        fastestDesc = "";
        smallest = Long.MAX_VALUE;
        smallestDesc = "";

        for (SerializationType st : SerializationType.values()) {
            for (CompressionType ct : CompressionType.values()) {

                streamer.setSerializationType(st);
                streamer.setCompressionType(ct);
                try {
                    long singleTs = System.currentTimeMillis();
                    byte[] b = streamer.stream(test);
                    streamer.uncompressDeserialize(b);
                    singleTs = System.currentTimeMillis() - singleTs;
                    results.add(new Result(streamer.toString(), singleTs, b.length));
                    //log.debug(streamer + " took " + singleTs);
                    if (b.length < smallest) {
                        smallest = b.length;
                        smallestDesc = streamer.toString();
                        smallestTime = singleTs;
                    }
                    if (singleTs < fastest) {
                        fastest = singleTs;
                        fastestDesc = streamer.toString();
                        fastestSize = b.length;
                    }

                } catch (Exception e) {
                    System.out.println("Could not ser/unser: " + streamer);
                    e.printStackTrace();
                }
            }
        }

        Collections.sort(results);
        String s = FormattingUtils.collectionToTableString(results);
        log.debug("\n" + s);
        
    }

    @Override
    public String toString() {
        return "RpcStreamerBenchmark{" + "fastest=" + fastest + ", fastestDesc=" + fastestDesc + ", smallest=" + smallest + ", smallestDesc=" + smallestDesc + ", smallestTime=" + smallestTime + ", fastestSize=" + fastestSize + '}';
    }

    @AllArgsConstructor
    private static class Result implements Comparable<Result> {
        String name;

        long time;

        int size;

        @Override
        public int compareTo(Result o) {
            return o.size - size;
        }

        @Override
        public String toString() {
            return "Result{" + "name=" + name + ", time=" + time + ", size=" + size + '}';
        }
    }
}
