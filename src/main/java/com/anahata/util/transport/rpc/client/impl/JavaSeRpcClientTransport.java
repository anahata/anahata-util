/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.transport.rpc.client.impl;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.env.ApplicationInstanceUUID;
import com.anahata.util.io.AnahataCompressionUtils;
import com.anahata.util.io.AnahataSerializationUtils;
import com.anahata.util.transport.rpc.Rpc;
import com.anahata.util.transport.rpc.client.RpcClientTransport;
import com.anahata.util.transport.rpc.RpcException;
import com.anahata.util.transport.rpc.RpcStreamer;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.enterprise.context.ApplicationScoped;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@ApplicationScoped
@Slf4j
public class JavaSeRpcClientTransport implements RpcClientTransport {

    @Override
    public Object sendReceive(RpcStreamer streamer, URL url, Rpc rpc) throws Exception {
        log.trace("Preparing post of {}", rpc);

        Object ret;
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection)url.openConnection();
        } catch (ConnectException e) {
            throw new RpcException("Exception connecting to " + url, e);
        }
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        //log.trace("Setting Content-Length: {}", rpc.length);
        //connection.addRequestProperty("Content-Length", String.valueOf(rpc.length));
        connection.addRequestProperty("Content-Type", "application/octet-stream");
        connection.addRequestProperty(AnahataCompressionUtils.HEADER, streamer.getCompressionType().name());
        connection.addRequestProperty(AnahataSerializationUtils.HEADER, streamer.getSerializationType().name());
        connection.addRequestProperty(ApplicationInstanceUUID.HEADER, ApplicationInstanceUUID.VALUE);
        //connection.addRequestProperty("Content-Encoding", "...");
        connection.setAllowUserInteraction(true);
        try {
            connection.connect();
        } catch (IOException e) {
            throw new RpcException("Exception connecting to " + url, e);
        }

        try (DataOutputStream dstream = new DataOutputStream(connection.getOutputStream())) {
            streamer.stream(rpc, dstream);
        }
        connection.getOutputStream().flush();

        log.trace("Connected to {} POSTed {} bytes", url, rpc);
        ret = streamer.receive(connection.getInputStream());
        connection.disconnect();

        return ret;
    }

}
