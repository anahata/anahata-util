/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.transport.rpc.client;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.transport.rpc.Rpc;
import com.anahata.util.transport.rpc.RpcStreamer;
import java.io.InputStream;
import java.net.URL;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public interface RpcClientTransport {
    public Object sendReceive(RpcStreamer streamer, URL url, Rpc req) throws Exception;
}
