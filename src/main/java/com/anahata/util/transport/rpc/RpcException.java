/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.transport.rpc;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public final class RpcException extends Exception {
    /**
     * Creates a new instance of
     * <code>RpcException</code> without detail message.
     */
    public RpcException() {
    }

    /**
     * Constructs an instance of
     * <code>RpcException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public RpcException(String msg) {
        super(msg);
    }

    public RpcException(String message, Throwable cause) {
        super(message, cause);
    }
}
