/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.transport.rpc;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.config.internal.AnahataUtilConfig;
import com.anahata.util.hessian.HessianUtils;
import com.anahata.util.io.AnahataCompressionUtils;
import com.anahata.util.io.AnahataSerializationUtils;
import com.anahata.util.io.CompressionType;
import com.anahata.util.io.SerializationType;
import com.anahata.util.lang.BasicThreadFactory;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.DefaultSerializers.CollectionsEmptyListSerializer;
import com.esotericsoftware.kryo.serializers.DefaultSerializers.CollectionsEmptyMapSerializer;
import com.esotericsoftware.kryo.serializers.DefaultSerializers.CollectionsEmptySetSerializer;
import com.esotericsoftware.kryo.serializers.DefaultSerializers.CollectionsSingletonListSerializer;
import com.esotericsoftware.kryo.serializers.DefaultSerializers.CollectionsSingletonMapSerializer;
import com.esotericsoftware.kryo.serializers.DefaultSerializers.CollectionsSingletonSetSerializer;
import java.io.*;
import java.lang.reflect.InvocationHandler;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.eclipse.persistence.jpa.jpql.parser.DateTime;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
@Dependent
public class RpcStreamer {

    @Inject
    private AnahataUtilConfig config;

    @Getter
    private SerializationType serializationType;

    @Getter
    private CompressionType compressionType;

    private Kryo kryo;

    private CompressorStreamFactory compressorStreamFactory = new CompressorStreamFactory();

    private static ExecutorService streamerPool = Executors.newCachedThreadPool(new BasicThreadFactory("RpcStreamer",
            true,
            Thread.NORM_PRIORITY));

    public RpcStreamer() {
    }

    @PostConstruct()
    private void init() {
        serializationType = config.getRpcSerialization();
        compressionType = config.getRpcCompression();
    }

    public void setSerializationType(SerializationType serializationType) {
        this.serializationType = serializationType;
        if (SerializationType.KRYO == serializationType && kryo == null) {
            kryo = new Kryo();
            //kryo.setAsmEnabled(true);
            kryo.setRegistrationRequired(false);
            //kryo.setInstantiatorStrategy(new StdInstantiatorStrategy());
            //kryo.register(Arrays.asList("").getClass(), new ArraysAsListSerializer());
            kryo.register(Collections.EMPTY_LIST.getClass(), new CollectionsEmptyListSerializer());
            kryo.register(Collections.EMPTY_MAP.getClass(), new CollectionsEmptyMapSerializer());
            kryo.register(Collections.EMPTY_SET.getClass(), new CollectionsEmptySetSerializer());
            kryo.register(Collections.singletonList("").getClass(), new CollectionsSingletonListSerializer());
            kryo.register(Collections.singleton("").getClass(), new CollectionsSingletonSetSerializer());
            kryo.register(Collections.singletonMap("", "").getClass(), new CollectionsSingletonMapSerializer());
            //kryo.register(GregorianCalendar.class, new GregorianCalendarSerializer());
            //kryo.register(InvocationHandler.class, new JdkProxySerializer());
            //UnmodifiableCollectionsSerializer.registerSerializers(kryo);
            //SynchronizedCollectionsSerializer.registerSerializers(kryo);
            //kryo.register(CGLibProxySerializer.CGLibProxyMarker.class, new CGLibProxySerializer());
            //kryo.register(DateTime.class, new JodaDateTimeSerializer());
        }
    }

    public void setCompressionType(CompressionType compressionType) {
        this.compressionType = compressionType;
    }

    public void init(HttpServletRequest request) {
        CompressionType reqCompressionType = AnahataCompressionUtils.readHeader(request);
        if (reqCompressionType != null) {
            setCompressionType(reqCompressionType);            ;
        }

        SerializationType reqSerializationType = AnahataSerializationUtils.readHeader(request);
        if (reqSerializationType != null) {
            setSerializationType(reqSerializationType);
        }

    }

    public long getCompressedSize(Object... objects) throws IOException, CompressorException {
        return stream(objects).length;
    }

    public long getSerializedSize(Object... objects) throws IOException, CompressorException {
        return serialize(objects).length;
    }

    public byte[] stream(Object o) throws IOException, CompressorException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (OutputStream compressingOS = newCompressingOutputStream(baos)) {
            serialize(o, compressingOS);
        }
        return baos.toByteArray();
    }

    public InputStream streamToInputStream(final Object o) throws IOException, CompressorException {
        //doesn't work in jws
        PipedInputStream in = new PipedInputStream();
        final PipedOutputStream out = new PipedOutputStream(in);
        streamerPool.submit(
                new Runnable() {
            @Override
            public void run() {
                try {
                    stream(o, out);
                } catch (IOException | CompressorException e) {
                    log.error("Exception streaming {}", o, e);
                }
            }
        });
        return in;

    }

    public byte[] serialize(Object o) throws IOException, CompressorException {
        long ts = System.currentTimeMillis();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        serialize(o, baos);
        byte[] ret = baos.toByteArray();
        ts = System.currentTimeMillis() - ts;
        log.trace("{} serialized size : {}, took {}", toString(), ret.length, ts);
        return ret;
    }

    public void serialize(Object o, OutputStream baos) throws IOException, CompressorException {
        if (serializationType == SerializationType.JAVA) {
            SerializationUtils.serialize((Serializable) o, baos);
        } else if (serializationType == SerializationType.KRYO) {
            try (Output out = new Output(baos)) {
                kryo.writeClassAndObject(out, o);
            }
        } else if (serializationType == SerializationType.HESSIAN) {
            HessianUtils.serialize(o, baos);
        } else {
            throw new IllegalArgumentException("serializationType " + serializationType + " not supported");
        }

    }

    public byte[] compress(byte[] source) throws IOException, CompressorException {
        long ts = System.currentTimeMillis();
        if (compressionType == CompressionType.NONE) {
            return source;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (OutputStream os = newCompressingOutputStream(baos)) {
            IOUtils.write(source, os);
            os.flush();
        }
        byte[] ret = baos.toByteArray();
        if (log.isDebugEnabled()) {
            BigDecimal ratio = new BigDecimal(100f * ((float) ret.length / (float) source.length));
            ratio = ratio.setScale(2, RoundingMode.HALF_UP);
            log.trace("{} compression: source={}, compressed={}, ratio={}%", toString(), source.length, ret.length,
                    ratio);
            if (ratio.intValue() > 100) {
                log.trace("{} compression made data bigger: source={}, compressed={}, ratio={}%", toString(),
                        source.length, ret.length,
                        ratio);
            }
        }
        ts = System.currentTimeMillis() - ts;
        log.trace("Compression took {}", ts);

        return ret;
    }

    public byte[] uncompress(byte[] source) throws IOException, CompressorException {
        long ts = System.currentTimeMillis();
        if (compressionType == CompressionType.NONE) {
            return source;
        }
        ByteArrayInputStream bais = new ByteArrayInputStream(source);
        InputStream is = newUncompressingInputStream(bais);
        byte[] ret = IOUtils.toByteArray(is);
        ts = System.currentTimeMillis() - ts;
        log.trace("{} decompressed size : {}, took {} ms.", toString(), ret.length, ts);
        return ret;
    }

    public void stream(Object o, OutputStream os) throws IOException, CompressorException {
        os = newCompressingOutputStream(os);
        serialize(o, os);
        //Should this be flush? otherwise, would be closing servlet's output stream
        os.close();
    }

    public void send(Object o, OutputStream os) throws IOException, CompressorException {
        if (compressionType == CompressionType.NONE) {
            serialize(o, os);
        } else {
            long ts = System.currentTimeMillis();
            byte[] data = serialize(o);
            data = compress(data);
            IOUtils.write(data, os);
            ts = System.currentTimeMillis() - ts;
            log.debug("serialization, compression and writing {} :took {}", toString(), ts);
        }

    }

    public <T> T uncompressDeserialize(byte[] data) throws IOException, CompressorException {
        data = uncompress(data);
        return deserialize(data);
    }

    public <T> T receive(InputStream is) throws IOException, CompressorException {
        return uncompressDeserialize(IOUtils.toByteArray(is));
    }

    public void stream(Object o, BytesMessage message) throws IOException, CompressorException, JMSException {
        message.setStringProperty(AnahataCompressionUtils.HEADER, getCompressionType().name());
        message.setStringProperty(AnahataSerializationUtils.HEADER, getSerializationType().name());
        byte[] data = stream(o);
        log.debug("streamed object {}: writing {} bytes to message", o, data.length);
        message.writeBytes(data);
    }

    public <T> T receive(BytesMessage message) throws JMSException, IOException, CompressorException {
        CompressionType compression = AnahataCompressionUtils.readHeader(message);
        SerializationType serialization = AnahataSerializationUtils.readHeader(message);
        setCompressionType(compression);
        setSerializationType(serialization);
        BytesMessage bytesMessage = message;
        log.trace("BodyLength: {}", bytesMessage.getBodyLength());
        byte[] barr = new byte[(int) bytesMessage.getBodyLength()];
        bytesMessage.readBytes(barr);
        log.trace("received bytes {} ", barr);
        return uncompressDeserialize(barr);
    }

    public <T> T receiveStreaming(InputStream is) throws IOException, CompressorException {
        is = newUncompressingInputStream(is);
        return deserialize(is);
    }

    public <T> T deserialize(InputStream is) throws IOException, CompressorException {
        long ts = System.currentTimeMillis();
        T ret;
        if (serializationType == SerializationType.JAVA) {
            ret = (T) SerializationUtils.deserialize(is);
        } else if (serializationType == SerializationType.KRYO) {
            try (Input input = new Input(is)) {
                ret = (T) kryo.readClassAndObject(input);
            }
        } else if (serializationType == SerializationType.HESSIAN) {
            ret = (T) HessianUtils.deSerialize(is);
        } else {
            throw new IllegalArgumentException("serializationType " + serializationType + " not supported");
        }
        ts = System.currentTimeMillis() - ts;
        log.trace("deserialization {} took {}", toString(), ts);
        return ret;
    }

    public <T> T deserialize(byte[] data) throws IOException, CompressorException {
        return deserialize(new ByteArrayInputStream(data));
    }

    public Object roundTrip(Object o) throws IOException, CompressorException {
        long ts = System.currentTimeMillis();
        byte[] b = stream(o);
        Object ret = uncompressDeserialize(b);
        ts = System.currentTimeMillis() - ts;
        log.debug("{} roundtrip took {}, size: {}", toString(), ts,
                b.length);
        return ret;
    }

    private InputStream newUncompressingInputStream(InputStream is) throws CompressorException {

        if (CompressionType.NONE == compressionType) {
        } else if (CompressionType.DEFLATE == compressionType) {
            is = new InflaterInputStream(is);
        } else {
            is = compressorStreamFactory.createCompressorInputStream(compressionType.name(), is);
        }

        log.trace("InputStream: {}", is.getClass().getSimpleName());
        return is;
    }

    private OutputStream newCompressingOutputStream(OutputStream os) throws CompressorException {
        if (CompressionType.NONE == compressionType) {
        } else if (CompressionType.DEFLATE == compressionType) {
            os = new DeflaterOutputStream(os);
        } else {
            os = compressorStreamFactory.createCompressorOutputStream(compressionType.name(), os);
        }
        return os;

    }

    public String toString() {
        return getClass().getSimpleName() + serializationType + "/" + compressionType;
    }
}
