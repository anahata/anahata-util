/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.transport.rpc.client;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.util.cdi.Cdi;
import com.anahata.util.transport.rpc.RcpError;
import com.anahata.util.transport.rpc.Rpc;
import com.anahata.util.transport.rpc.RpcStreamer;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.net.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * JDK proxy InvocationHandler for RPC calls.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@AllArgsConstructor
@Slf4j
public class RpcInvocationHandler implements InvocationHandler {

    private URL url;

    @Getter
    private Class clazz;

    private RpcClientTransport transport;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        log.debug("RpcInvocationHandler entry {} {}.{} ", method.getReturnType(),
                clazz.getSimpleName(),
                method.getName());

        if (method.getDeclaringClass().equals(Object.class)) {
            return method.invoke(this, args);
        }

        Rpc req = new Rpc(clazz, method, args);

        RpcStreamer streamer = Cdi.get(RpcStreamer.class);

        long ts = System.currentTimeMillis();
        Object resp = transport.sendReceive(streamer, url, req);
        ts = System.currentTimeMillis() - ts;

        log.debug("{} ms full round trip for {} ", ts, method);

        if (resp instanceof RcpError) {
            ((RcpError) resp).doThrow();
        }

        return resp;
    }

}
