/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.transport;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.naming.InitialContext;
import javax.naming.NamingException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Utility methods to lookup Services via jndi.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j

public final class ServiceLookupUtils {

    /**
     * Looks up a service using "java:global/appName/ notation;
     *
     * @param <T>
     * @param appName          the app name e.g. jobtracking
     * @param serviceInterface the service interface
     * @return the service instance
     * @throws ClassNotFoundException
     * @throws NamingException
     * @throws SecurityException
     */
    public static <T> T serviceLookup(String appName, Class<T> serviceInterface) throws ClassNotFoundException,
            NamingException, SecurityException {
        return serviceLookupInternal("java:global/" + appName + "/", serviceInterface);
    }

    /**
     * Looks up a service using "java:module/" + appName notation.
     *
     * @param <T>
     * @param serviceInterface the service interface
     * @return the service instance
     * @throws ClassNotFoundException
     * @throws NamingException
     * @throws SecurityException
     */
    public static <T> T serviceLookup(Class<T> serviceInterface) throws ClassNotFoundException, NamingException,
            SecurityException {
        return serviceLookupInternal("java:module/", serviceInterface);
    }

    private static <T> T serviceLookupInternal(String prefix, Class<T> serviceInterface) throws ClassNotFoundException,
            NamingException,
            SecurityException {

        String serviceImplName;
        String serviceInterfaceName = serviceInterface.getName();
        if (serviceInterfaceName.endsWith("Local")) {
            serviceImplName = serviceInterfaceName.substring(0, serviceInterfaceName.lastIndexOf("Local")) + "Impl";
        } else {
            serviceImplName = serviceInterface.getName() + "Impl";
        }
        Class<?> serviceImpl = loadClass(serviceImplName);

        String lookupName2 = prefix + serviceImpl.getSimpleName();
        String lookupName1 = lookupName2 + "!" + serviceInterface.getName();

        log.trace("Looking ejb by jndi name {}", lookupName1);
        InitialContext ctx = new InitialContext();
        long ts = System.currentTimeMillis();
        T ret;
        try {
            log.trace("Looking ejb by jndi name {}", lookupName1);
            ret = (T)ctx.lookup(lookupName1);
        } catch (Exception e) {

            log.trace("Looking ejb by jndi name {}", lookupName2);
            ret = (T)ctx.lookup(lookupName2);
        }
        ts = System.currentTimeMillis() - ts;
        log.trace("Looking ejb by jndi {} took {} ", ts);
        return ret;

    }

    private static Class<?> loadClass(String className) throws ClassNotFoundException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        if (loader != null) {
            return Class.forName(className, false, loader);
        }

        return Class.forName(className);
    }
}
