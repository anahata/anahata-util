package com.anahata.util.transport;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.util.application.JWSUtils;
import com.anahata.util.config.internal.AnahataUtilConfig;
import com.anahata.util.logging.RemoteServiceLogger;
import com.anahata.util.transport.rpc.client.RpcInvocationHandler;
import com.anahata.util.transport.rpc.client.impl.CommonsRpcClientTransport;
import com.caucho.hessian.client.HessianProxyFactory;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.SystemDefaultCredentialsProvider;

/**
 * CDI producer for hessian service implementations.
 *
 * @author Robert Nagajek
 */
@Slf4j
@ApplicationScoped
public class RemoteServiceFactoryImpl implements RemoteServiceFactory {
    /**
     * Path to hessian servlet.
     */
    private static final String HESSIAN_SERVLET_PATH = "/hessian/";

    private static final String RPC_SERVLET_PATH = "/rpc/";

    @Inject
    private AnahataUtilConfig config;

    @Inject
    private JWSUtils jwsUtils;

    @Inject
    private CommonsRpcClientTransport transport;

    @Inject
    private Instance<RemoteServiceLogger> removeServiceLoggerFactory;

    /**
     * Keeps the base hessian URL which will be used in all hessian calls.
     */
    private String baseHessianURL;

    /**
     * Keeps the base hessian URL which will be used in all hessian calls.
     */
    private URL rpcURL;

    /**
     * The hessian factory.
     */
    private final HessianProxyFactory factory = new HessianProxyFactory();

    /**
     * The remote EJB initial context.
     */
    private InitialContext remoteEJBInitialContext = null;

    /**
     * Cache so services only have to be looked up once.
     */
    private final HashMap<Class, Object> cache = new HashMap<>();

    private CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(
            new SystemDefaultCredentialsProvider()).build();

    /**
     * Default Constructor, calculates the base hessian url.
     */
    @PostConstruct
    public void postConstruct() {
        //this is needed to allow 2 service methods to have same method name but different method signature
        factory.setOverloadEnabled(true);

//        String user = config.getAutoLoginUser();
//        String password = config.getAutoLoginPassword();
//
//        if (user != null && password != null) {
//            log.info("Using auto-login with user {}", user);
//            factory.setUser(user);
//            factory.setPassword(password);
//        }
        this.baseHessianURL = jwsUtils.getApplicationURL() + HESSIAN_SERVLET_PATH;
        log.info("base hessian URL = " + baseHessianURL);
        try {
            rpcURL = new URL(jwsUtils.getApplicationURL() + RPC_SERVLET_PATH);
            log.info("base rpc URL = " + rpcURL);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T create(Class<T> clazz) throws Exception {
        Validate.isTrue(clazz.isInterface());

        T service = (T)cache.get(clazz);
        if (service != null) {
            return service;
        }
        if (config.getTransportLayer() == TransportLayer.HESSIAN) {
            service = createHessianProxy(clazz);
        } else if (config.getTransportLayer() == TransportLayer.RPC) {
            service = createRpcProxy(clazz);
        } else if (config.getTransportLayer() == TransportLayer.EJB) {
            service = createRemoteEJBProxy(clazz);
        } else {
            throw new IllegalStateException("Transport Layer: " + config.getTransportLayer() + " Not implemeneted");
        }

        log.debug("config.isTransportLogging()  {} ", config.isTransportLogging());
        if (config.isTransportLogging()) {
            RemoteServiceLogger logger = removeServiceLoggerFactory.get();
            logger.setRemoteProxy(service);
            log.debug("Wrapping remote service client {} created in logger {} : ", service, logger);

            service = (T)Proxy.newProxyInstance(service.getClass().getClassLoader(), new Class<?>[]{clazz}, logger);

            if (log.isTraceEnabled()) {
                for (Method m : service.getClass().getDeclaredMethods()) {
                    log.trace("RemoteServiceLogger proxy method: {}", m);
                }
            }

        }

        if (config.getTransportProxy() != null) {
            RemoteServiceProxy proxy = config.getTransportProxy().newInstance();
            proxy.setService(service);
            service = (T)Proxy.newProxyInstance(service.getClass().getClassLoader(), new Class<?>[]{clazz}, proxy);
        }

        cache.put(clazz, service);

        //Add logger
        return service;

    }

    @Override
    public <T> T createHessianProxy(Class<T> type) throws MalformedURLException {
        String url = baseHessianURL + type.getName();
        log.debug("Creating service hessian client for class {}. URL: {}", type.getName(), url);
        @SuppressWarnings("unchecked")
        T service = (T)factory.create(type, url);
        log.debug("Service hessian client created: " + service);
        return service;
    }

    @Override
    public <T> T createRpcProxy(Class<T> type) throws MalformedURLException {
        RpcInvocationHandler handler = new RpcInvocationHandler(rpcURL, type, transport);
        @SuppressWarnings("unchecked")
        T proxy = (T)Proxy.newProxyInstance(getClass().getClassLoader(), new Class<?>[]{type}, handler);
        log.debug("Rpc proxy created for {} handler {} proxy {} ", type, handler, proxy.getClass());
        if (log.isTraceEnabled()) {
            for (Method m : proxy.getClass().getDeclaredMethods()) {
                log.trace("Rpc proxy method: {}", m);
            }
        }

        return proxy;
    }

    @Override
    public <T> T createRemoteEJBProxy(Class<T> type) throws MalformedURLException, NamingException {
        String ejbModuleName = config.getEjbTransportModuleName();

        if (ejbModuleName == null) {
            log.debug("ejb module name not specified, will attempt application context path as module name");
            ejbModuleName = jwsUtils.getApplicationContextPath();
        }

        String s = "java:global/" + ejbModuleName + "/" + type.getSimpleName() + "Impl!" + type.getName();
        log.debug("Looking up {}", s);
        long ts = System.currentTimeMillis();
        @SuppressWarnings("unchecked")
        T ret = (T)getRemoteEJBInitialContext().lookup(s);
        ts = System.currentTimeMillis() - ts;
        log.debug("Looking up {} took {}", s, ts);
        return ret;
    }

    private synchronized InitialContext getRemoteEJBInitialContext() throws MalformedURLException, NamingException {
        if (remoteEJBInitialContext == null) {
            Hashtable<String, String> props = new Hashtable<>();
            props.put("org.omg.CORBA.ORBInitialHost", jwsUtils.getApplicationHost());
            props.put("org.omg.CORBA.ORBInitialPort", "3700");
            remoteEJBInitialContext = new InitialContext(props);
        }

        return remoteEJBInitialContext;
    }
}
