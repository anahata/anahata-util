/*
 *  Copyright Â© - 2013 Anahata Technologies.
 */
package com.anahata.util.transport;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import javafx.scene.Node;
import lombok.Setter;

/**
 *
 * @author Vijay
 */
public abstract class RemoteServiceProxy implements InvocationHandler {

    @Setter
    protected Node nodeToDisable;

    @Setter
    protected Object service;

    public static void disableWhileRunning(Node node, Object... remoteServices) {
        for (Object remoteService : remoteServices) {
            ((RemoteServiceProxy)Proxy.getInvocationHandler(remoteService)).setNodeToDisable(node);
        }
    }

}
