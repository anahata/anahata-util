package com.anahata.util.test;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.util.config.internal.ApplicationPropertiesFactory;
import java.util.Properties;
import javax.enterprise.context.ApplicationScoped;
import lombok.Getter;

/**
 * Unit test properties.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@ApplicationScoped
public class TestProperties implements ApplicationPropertiesFactory {
    @Getter
    private static final Properties props = new Properties();

    @Override
    public Properties getAppProperties() {
        return props;
    }
}
