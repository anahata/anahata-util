package com.anahata.util.validation;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.util.validator.constraints.AssertFieldFalse;
import com.anahata.util.validator.constraints.AssertFieldTrue;
import java.math.BigDecimal;
import java.util.*;
import javax.validation.*;
import javax.validation.constraints.NotNull;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for ValidationUtils.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class ValidationUtilsTest {
    @Test
    public void testGetValidationPropertyName() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        TestParent parent = new TestParent();
        TestChild testChild = new TestChild();
        testChild.favouriteFoods.add(new TestFood());
        parent.children.add(testChild);
        Set<ConstraintViolation<TestParent>> cvs = validator.validate(parent);
        assertNotNull(cvs);

        for (ConstraintViolation<TestParent> cv : cvs) {
            final String path = cv.getPropertyPath().toString();
            final String propName = ValidationUtils.getValidationPropertyName(cv);
            final String propName2 = ValidationUtils.getValidationPropertyName(cv, true);
            System.out.printf("path=%s propName=%s root=%s leaf=%s message=%s%n", path, propName,
                    cv.getRootBeanClass().getSimpleName(), cv.getLeafBean().getClass().getSimpleName(), cv.getMessage());

            switch (path) {
                case "name":
                    assertEquals("name", propName);
                    break;

                case "valid.#name":
                    assertEquals("name", propName);
                    break;

                case "validToo.#name":
                    assertEquals("name", propName);
                    break;

                case "child.name":
                    assertEquals("child.name", propName);
                    break;

                case "children[0].name":
                    assertEquals("children.name", propName);
                    break;

                case "children[0].favouriteFoods[0].food":
                    assertEquals("children.favouriteFoods.food", propName);
                    break;

                case "child.valid.#name":
                    assertEquals("child.name", propName);
                    break;

                case "children[0].valid.#name":
                    assertEquals("children.name", propName);
                    assertEquals("children[0].name", propName2);
                    break;

                case "costValid.#cost.amount":
                    assertEquals("cost.amount", propName);
                    break;

                default:
                    fail("Unknown path: " + path);
                    break;
            }
        }
    }

    public static class TestParent {
        @NotNull
        private String name;

        @Valid
        private TestChild child = new TestChild();

        @Valid
        private List<TestChild> children = new ArrayList<>();

        @Valid
        private Money cost = new Money();

        @AssertFieldTrue(field = "name", message = "Test custom assert")
        public boolean isValid() {
            return false;
        }

        @AssertFieldFalse(field = "name", message = "Test custom assert")
        public boolean isValidToo() {
            return true;
        }

        @AssertFieldTrue(field = "cost.amount", message = "The cost must be greater than $10")
        public boolean isCostValid() {
            return cost.amount.compareTo(BigDecimal.TEN) > 0;
        }
    }

    public static class TestChild {
        @NotNull
        private String name;

        @Valid
        private List<TestFood> favouriteFoods = new ArrayList<>();

        @AssertFieldTrue(field = "name", message = "Test custom child assert")
        public boolean isValid() {
            return false;
        }
    }

    public static class TestFood {
        @NotNull
        private String food;
    }

    public static class Money {
        private BigDecimal amount = BigDecimal.ZERO;

        private Currency currency = Currency.getInstance("AUD");
    }
}
