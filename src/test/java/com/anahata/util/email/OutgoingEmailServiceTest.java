/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.email;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import lombok.extern.slf4j.Slf4j;
import org.junit.*;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
public class OutgoingEmailServiceTest {
    public OutgoingEmailServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of sendEmail method, of class OutgoingEmailService.
     */
    @Test
    public void testImageReplacingRegExp() throws Exception {

//        Properties props = new Properties();
//        props.load(getClass().getResource("outgoingemail.properties").openStream());
//
//        byte[] content = IOUtils.toByteArray(getClass().getResource("zippedemailcontent.zip"));
//        OutgoingEmailService os = new OutgoingEmailService(new OutgoingEmailConfig(props, "outgoing.email"));
//        Email email = new Email();
//        email.setFrom("pablo@anahata-it.com.au");
//        email.getTo().add("AAAA3gcJEgQA@appmaildev.com");
//        email.setSubject("test");
//        email.setZippedContent(content);
//        try {
//            os.sendEmail(email);
//        } catch (EmailException e) {
//            e.printStackTrace();
//        }
        //os.sendEmail("pablo@anahata-it.com.au", "unit-test", content, "pablo@anahata-it.com.au");
    }

    @Test
    public void testSendEmail_4args() throws Exception {

//        org.jsoup.nodes.Document d = org.jsoup.Jsoup.parse(new File("/home/pablo/Desktop/test.hml"), "UTF-16");
//        //org.jsoup.select.Elements images = d.select("img[src^=data:image/png;base64,]");
//        org.jsoup.select.Elements images = d.select("img");
//        for (org.jsoup.nodes.Element e : images) {
//            System.out.println("------------");
//            String val = e.attr("src");
//            String prefix = "data:image/png;base64,";
//            val = val.substring(prefix.length());
//            System.out.println("Got val = " + val);
//
//        }
//        
//        System.out.println("finished");
    }

    @Test
    public void testSendEmail_6args() throws Exception {
    }

    @Test
    public void testGetConfig() {
    }
}
