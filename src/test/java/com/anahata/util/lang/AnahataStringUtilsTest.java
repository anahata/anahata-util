package com.anahata.util.lang;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for AnahataStringUtils.
 *
 * @author Robert Nagajek
 */
public class AnahataStringUtilsTest {
    private static final List<String> CUSTOMER_NAMES = Arrays.asList(
            "AUSLEC - GEELONG (DLT)",
            "AUSLEC - LAVERTON",
            "AUSLEC - WELSHPOOL",
            "AUSLEC - WINGFIELD (DRD)",
            "AUSLEC WOLLONGONG",
            "AUSLEC-BHP CONISTON",
            "AUSLEC-BLACKTOWN",
            "AUSLEC-BLAIR ATHOL",
            "AUSLEC-BUNBURY",
            "AUSLEC-DARWIN",
            "AUSLEC-DOVETON",
            "AUSLEC-FORTITUDE VALLEY",
            "AUSLEC-GERALDTON",
            "AUSLEC-KARRATHA",
            "AUSLEC-MOUNT GAMBIER",
            "AUSLEC-PORT HEDLAND",
            "AUSLEC-SALISBURY",
            "AUSLEC-TOWNSVILLE)",
            "AUSLEC-WETHERILL PARK",
            "AUSLEC-WHYALLA",
            "HAGEMEYER - CANNING VALE",
            "HAGEMEYER - ROXBY DOWNS (DSB)",
            "LAWRENCE & HANSON",
            "LAWRENCE & HANSON - PORTLAND",
            "LAWRENCE & HANSON-HOPPERS CROSSING",
            "LAWRENCE & HANSON-NEWCASTLE",
            "LAWRENCE & HANSON-SHEPPARTON",
            "LAWRENCE & HANSON-TRARALGON",
            "LAWRENCE & HANSON-WOLLONGONG",
            "ROBERT TEST",
            "Robert Test",
            "BLAH 1",
            "BLDF");

    /**
     * Test of removeIgnoreCase method, of class AnahataStringUtils.
     */
    @Test
    public void testRemoveIgnoreCase() {
        assertNull(AnahataStringUtils.removeIgnoreCase(null, null));
        assertNull(AnahataStringUtils.removeIgnoreCase(null, "Test"));
        assertEquals("Search", AnahataStringUtils.removeIgnoreCase("Search", null));
        assertEquals("The rain in  falls mainly on the plain", AnahataStringUtils.removeIgnoreCase(
                "The rain in Spain falls mainly on the plain", "Spain"));
        assertEquals("The rain in  falls mainly on the plain", AnahataStringUtils.removeIgnoreCase(
                "The rain in Spain falls mainly on the plain", "spain"));
        assertEquals("The rain in Spain falls mainly on the plain", AnahataStringUtils.removeIgnoreCase(
                "The rain in Spain falls mainly on the plain", "Spain1"));
    }

    @Test
    public void testGetCommonPrefixes() {
        List<String> prefixes = AnahataStringUtils.getCommonPrefixes(CUSTOMER_NAMES, null);
        assertNotNull(prefixes);
        assertEquals(5, prefixes.size());
//        AnahataAssert.assertCollectionEquals(Arrays.asList("AUSLEC", "BL", "HAGEMEYER - ", "LAWRENCE & HANSON",
//                "ROBERT TEST"), prefixes);

        prefixes = AnahataStringUtils.getCommonPrefixes(CUSTOMER_NAMES, 3);
        assertNotNull(prefixes);
        assertEquals(6, prefixes.size());
//        AnahataAssert.assertCollectionEquals(Arrays.asList("AUSLEC", "BLAH 1", "BLDF", "HAGEMEYER - ",
//                "LAWRENCE & HANSON", "ROBERT TEST"), prefixes);
    }

    @Test
    public void testReplaceFirstIgnoreCase() {
        assertEquals("PO Box 123 Somewhere", AnahataStringUtils.replaceFirstIgnoreCase("P.O. BOX 123 Somewhere",
                "P.O. BOX", "PO Box"));
    }

    @Test
    public void testCapitilizeNext() {
        assertEquals("McDonald", AnahataStringUtils.capitilizeNext("Mcdonald", "Mc"));
        assertEquals("McDonald McFatty", AnahataStringUtils.capitilizeNext("Mcdonald Mcfatty", "Mc"));
    }

    @Test
    public void testReplaceEachIgnoreCase() {
        assertEquals(" ABB Engineering Construction ", AnahataStringUtils.replaceEachIgnoreCase(
                " Abb Engineering Construction ", new String[]{" ABB "}, new String[]{" ABB "}));
        assertEquals(" Adsteel Brokers PTY LTD ", AnahataStringUtils.replaceEachIgnoreCase(
                " Adsteel Brokers PTY LTD ", new String[]{" PTY ", " LTD "}, new String[]{" PTY ", " LTD "}));
    }

    @Test
    public void testCompareToIgnoreCase() {
        assertEquals(0, AnahataStringUtils.compareToIgnoreCase("Joe", "joe"));
        assertEquals(0, AnahataStringUtils.compareToIgnoreCase(null, null));
        assertEquals(-1, AnahataStringUtils.compareToIgnoreCase(null, "Test"));
        assertEquals(1, AnahataStringUtils.compareToIgnoreCase("Test", null));
        assertTrue(AnahataStringUtils.compareToIgnoreCase("Joe", "Moe") < 0);
    }

    @Test
    public void testCamelCase() {
        assertEquals("omNamahaShivaya", AnahataStringUtils.toJavaPropertyName("om_namaha_shivaya", "_"));
    }
}
