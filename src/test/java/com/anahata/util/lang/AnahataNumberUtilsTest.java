package com.anahata.util.lang;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.math.BigDecimal;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for AnahataNumberUtils.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class AnahataNumberUtilsTest {
    @Test
    public void testIsAssignableToNumber() {
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Integer.class, "5"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Integer.class, "-255"));
        assertFalse(AnahataNumberUtils.isAssignableToNumber(Integer.class, "53.2"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Integer.class, null));
        assertFalse(AnahataNumberUtils.isAssignableToNumber(Integer.class, "wer"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Long.class, "5"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Long.class, "5234234234"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Long.class, null));
        assertFalse(AnahataNumberUtils.isAssignableToNumber(Long.class, "wer"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Float.class, "5"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Float.class, "-255"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Float.class, "53.2"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Float.class, null));
        assertFalse(AnahataNumberUtils.isAssignableToNumber(Float.class, "wer"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Double.class, "5"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Double.class, "-255"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Double.class, "53.2"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(Double.class, null));
        assertFalse(AnahataNumberUtils.isAssignableToNumber(Double.class, "wer"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(BigDecimal.class, "5"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(BigDecimal.class, "-255"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(BigDecimal.class, "53.2"));
        assertTrue(AnahataNumberUtils.isAssignableToNumber(BigDecimal.class, null));
        assertFalse(AnahataNumberUtils.isAssignableToNumber(BigDecimal.class, "wer"));
    }
}
