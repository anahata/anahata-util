package com.anahata.util.html;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Robert Nagajek
 */
public class HtmlBuilderTest {
    /**
     * Test of build method, of class HtmlBuilder.
     */
    @Test
    public void testBuild() {
        String html = new HtmlBuilder()
                .style("font-family: Arial;")
                .body("BOO")
                .build();
        assertEquals("<html><head></head><body style='font-family: Arial;'>BOO</body></html>", html);
    }
}
