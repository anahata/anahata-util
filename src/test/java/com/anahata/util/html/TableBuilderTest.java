package com.anahata.util.html;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Robert Nagajek
 */
public class TableBuilderTest {
    /**
     * Test of build method, of class TableBuilder.
     */
    @Test
    public void testBuild() {
        String table = new TableBuilder()
                .style("font-size: 12px;")
                .th("First Column")
                .th("color: red;", "Second Column")
                .tr()
                    .td("width: 120px;", "Row 1 Col 1")
                    .td("Row 1 Col 2")
                .tr()
                    .td("Row 2 Col 1")
                    .td("Row 2 Col 2")
                .tr()
                    .td(null, "Row 3 Col1-2", 2)
                .build();
        final String expected = "<table style='font-size: 12px;'><thead><tr><th>First Column</th><th style='color: red;'>Second Column</th></tr></thead><tbody><tr><td style='width: 120px;'>Row 1 Col 1</td><td>Row 1 Col 2</td></tr><tr><td>Row 2 Col 1</td><td>Row 2 Col 2</td></tr><tr><td colspan='2'>Row 3 Col1-2</td></tr></tbody></table>";
        System.out.println("Expected: " + expected);
        System.out.println("Actual  : " + table);
        assertEquals(expected, table);
    }
}
