package com.anahata.util.rpc;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.anahata.util.cdi.Cdi;
import com.anahata.util.io.CompressionType;
import com.anahata.util.populate.PopulateUtils;
import com.anahata.util.transport.rpc.RpcStreamer;
import com.anahata.util.transport.rpc.RpcStreamerBenchmark;
import java.io.Serializable;
import java.util.*;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public class RpcStreamTest {
    public RpcStreamTest() {
    }

    @Test
    public void testCompressUncompress() throws Exception {

        RpcStreamer streamer = Cdi.get(RpcStreamer.class);

        Random r = new Random();
        byte[] source = new byte[1024];
        for (byte b : source) {
            b = (byte)r.nextInt(255);
        }

        for (CompressionType ct : CompressionType.values()) {
            streamer.setCompressionType(ct);
            try {
                byte[] compressed = streamer.compress(source);
                byte[] decompressed = streamer.uncompress(compressed);
                boolean equalsArrays = Arrays.equals(source, decompressed);
                assertTrue(equalsArrays);
            } catch (Exception e) {
                System.out.println("Could not ser/unser: " + streamer);
                e.printStackTrace();
            }
        }

    }

    @Test
    public void testPerformance() throws Exception {

        List l = new ArrayList();
        for (int i = 0; i < 1000; i++) {
            Object data = PopulateUtils.newInstanceMockFields(TestClass.class, UUID.randomUUID().toString());
            l.add(data);
        }

        RpcStreamer streamer = new RpcStreamer();
        RpcStreamerBenchmark bm = new RpcStreamerBenchmark(streamer, l);
        bm.run();
        System.out.println(bm);
        bm.run();
        System.out.println(bm);
    }

    public static class TestClass implements Serializable {
        public String s1;

        public String s2;

        public Long l1;
    }
}
