package com.anahata.util.reflect;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.beans.PropertyDescriptor;
import lombok.Getter;
import lombok.Setter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for AnahataPropertyUtils.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class AnahataPropertyUtilsTest {
    @Test
    public void testGetPropertyDescriptor_Class_String() {
        PropertyDescriptor pd = AnahataPropertyUtils.getPropertyDescriptor(TestClass.class, "name");
        assertNotNull(pd);
        assertEquals(pd.getPropertyType(), String.class);
        pd = AnahataPropertyUtils.getPropertyDescriptor(ChildClass.class, "name");
        assertNotNull(pd);
        assertEquals(pd.getPropertyType(), String.class);
    }
    
    @Getter
    @Setter
    private static class TestClass {
        private String name;
    }
    
    @Getter
    @Setter
    private static class ChildClass extends TestClass {
        private String boo;
    }
}
