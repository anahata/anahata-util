package com.anahata.util.error;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public class StackTraceParserTest {
    /**
     * Test of parse method, of class StackTraceParser.
     * 
     * @throws Exception If an error occurs.
     */
    @Test
    public void testParse() throws Exception {
        List<String> lines = readFile("/com/anahata/util/error/StackTrace1.txt");
        Throwable t = StackTraceParser.parse(lines);
        System.err.printf("%n%n============================%nStack Trace 1:%n%n");
        t.printStackTrace();
        
        lines = readFile("/com/anahata/util/error/StackTrace2.txt");
        t = StackTraceParser.parse(lines);
        System.err.printf("%n%n============================%nStack Trace 2:%n%n");
        t.printStackTrace();
        
        lines = readFile("/com/anahata/util/error/StackTrace3.txt");
        t = StackTraceParser.parse(lines);
        System.err.printf("%n%n============================%nStack Trace 3:%n%n");
        t.printStackTrace();
        
        lines = readFile("/com/anahata/util/error/StackTrace4.txt");
        t = StackTraceParser.parse(lines);
        System.err.printf("%n%n============================%nStack Trace 4:%n%n");
        t.printStackTrace();
    }
    
    private List<String> readFile(String fileName) throws IOException {
        List<String> lines = new ArrayList<>(80);
        
        try (
            InputStream is = this.getClass().getResourceAsStream(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
        ) {
            String line = br.readLine();
            
            while (line != null) {
                lines.add(line);
                line = br.readLine();
            }
        }
        
        return lines;
    }
}
