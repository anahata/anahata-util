package com.anahata.util.financial;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for CurrencyUtil.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class CurrencyUtilsTest {
    @Test
    public void testGetCurrencyFormat() {
        assertEquals("135,26 €", CurrencyUtils.getCurrencyFormat("EUR").format(135.26));
        assertEquals("$135.26", CurrencyUtils.getCurrencyFormat("AUD").format(135.26));
        assertEquals("$135.26", CurrencyUtils.getCurrencyFormat("USD").format(135.26));
    }
}
