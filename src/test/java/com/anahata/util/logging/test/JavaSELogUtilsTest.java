package com.anahata.util.logging.test;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.util.logging.JULUtils;
import com.anahata.util.logging.JavaSELogConfigUtils;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 *
 * @author pablo
 */
public class JavaSELogUtilsTest {

    /**
     * Loads logging properties file.
     * @throws Exception
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
        
        JavaSELogConfigUtils.readConfiguration(JavaSELogUtilsTest.class.getResourceAsStream("logging.properties"));
        
    }

    /**
     * Test of entering method, of class Log.
     */
    @Test
    public void testEntering() {

        JULUtils.entering();
        
        //Console should show:
        //Aug 19, 2011 9:42:35 PM com.anahata.commons.logging.JavaSELogUtilsTest testEntering
        //FINER: ENTRY

    }

    /**
     * Test of entering method, of class Log.
     */
    @Test
    public void testEnteringObjectArr() {


        Object[] params = {"param1", "param2"};

        JULUtils.getLogger().fine("HI THERE");
        JULUtils.getLogger().exiting("aaa", "bbb");

        JULUtils.entering(params);
        
        //Console should show:
        //Aug 19, 2011 9:42:35 PM com.anahata.commons.logging.JavaSELogUtilsTest testEntering
        //FINER: ENTRY param1 param2

    }
}
