package com.anahata.util.logging.slf4j;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public class ErrorMonitoringPrintStreamTest {
    @Test
    @Ignore
    public void test() throws Exception {
        log.debug("Starting the test");
        ConsoleSlf4jRedirector.init();
        System.err.println("A non-error");
        System.err.println("WARNING: A warning");
        new Exception("Test exception").printStackTrace();
        System.err.println("Another non-error");
    }
}
