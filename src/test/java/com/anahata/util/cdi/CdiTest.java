/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.cdi;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.junit.Test;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public class CdiTest {
    @Test
    public void testCdiBootstrap() throws Exception {
        
        boolean se = Cdi.isSe();
        foo();
    }

    public void foo() {
        Cdi.get(PreDestroyTest.class);
        Cdi.get(PreDestroyTest.class);
        Cdi.get(PreDestroyTest.class);
    }

    private static class PreDestroyTest {
        @PostConstruct
        public void pc() {
            System.out.println("pc");
        }

        @PreDestroy
        public void pd() {
            System.out.println("pd");
        }
    }
    
    
    public static void main(String[] args) {
        Cdi.isSe();
    }

}
