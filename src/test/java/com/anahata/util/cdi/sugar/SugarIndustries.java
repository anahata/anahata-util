/*
 * Copyright 2015 Anahata.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.util.cdi.sugar;

import com.anahata.util.lang.AnahataStringUtils;
import java.awt.Desktop;
import java.io.File;
import org.apache.commons.io.FileUtils;
/**
 *
 * @author pablo
 */
public class SugarIndustries {
    
    public static void main(String[] args) throws Exception{
        System.out.println("starting");
        String industries = FileUtils.readFileToString(new File("/home/pablo/Documents/industries-sorted2.txt"));
        File out = new File("/home/pablo/Documents/industries-sorted2.php");
        out.delete();
        StringBuilder sb = new StringBuilder();
        for (String line: industries.split("\n")) {
            //System.out.println("line=" + line);
            String key = AnahataStringUtils.lettersOnly(line);
            String val = line.substring(0, line.length()-1).replace("'", "\\'");            
            //String php = "'" + key + "'=> '" + val + "' ,";
            sb.append("'");
            sb.append(key);
            sb.append("' => '");
            sb.append(val);
            sb.append("' ,\n");
        }
        System.out.println(sb.toString());
        FileUtils.writeStringToFile(out, sb.toString());
        Desktop.getDesktop().edit(out);
    }
}
