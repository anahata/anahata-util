/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.util.plaf;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import junit.framework.Assert;
import org.apache.commons.io.FilenameUtils;
import org.junit.Test;

/**
 *
 * @author pablo
 */
public class FilenameUtilsTest {
    public FilenameUtilsTest() {
    }

    /**
     * Test of replaceIllegalCharacters method, of class FileNameUtils.
     */
    @Test
    public void testReplaceIllegalCharacters() {

        String hariom = "hari om.txt";

        Assert.assertTrue(hariom + " is not valid when it is ment to be", AnahataFilenameUtils.isValidFileName(hariom));

        for (char c : AnahataFilenameUtils.ILLEGAL_FILE_NAME_CHARACTERS) {

            String fileName = c + hariom;
            Assert.assertFalse(fileName + " was valid when it is not ment to be", AnahataFilenameUtils.isValidFileName(
                    fileName));

            fileName = fileName + c;
            Assert.assertFalse(fileName + " was valid when it is not ment to be", AnahataFilenameUtils.isValidFileName(
                    fileName));
            String valid = AnahataFilenameUtils.replaceIllegalCharacters(fileName, "");

            Assert.assertEquals(valid, hariom);
            String valid2 = AnahataFilenameUtils.replaceIllegalCharacters(fileName, "_");
            Assert.assertEquals(valid2, "_" + hariom + "_");

        }

    }

    @Test
    public void testFileLength() {
        
        String s = makeAaaaString(256);
        Assert.assertEquals(s.length(), 256);

        //256 long ok
        Assert.assertTrue(AnahataFilenameUtils.isValidFileName(s));

        //257 too long
        s += "X";
        Assert.assertEquals(s.length(), 257);
        Assert.assertFalse(AnahataFilenameUtils.isValidFileName(s));
    }

    @Test
    public void testTrimToMaxLength() {
        String ext = ".txt";
        
        String tooLong = makeAaaaString(300) + "abc0123456789" + ext;
        
        String fixed = AnahataFilenameUtils.trimToMaxLength(tooLong);
        
        //name is now valid
        Assert.assertTrue(AnahataFilenameUtils.isValidFileName(fixed));
        
        //preserved the extension
        Assert.assertEquals(FilenameUtils.getExtension(fixed), FilenameUtils.getExtension(tooLong));
        
        //still as long as possible
        Assert.assertEquals(fixed.length(), AnahataFilenameUtils.MAX_FILE_NAME_LENGTH);

        
        
    }
    
    private static String makeAaaaString(int length) {
        String s = "";
        for (int i = 0; i < length; i++) {
            s += "a";
        }
        return s;
    }
}
