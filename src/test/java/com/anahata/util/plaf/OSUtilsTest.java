package com.anahata.util.plaf;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public class OSUtilsTest {
    @Test
    public void testFindExecutableOnPath() {
        File file1 = OSUtils.findExecutableOnPath("java.exe", "java", "java");
        System.out.println("java at " + file1);
        assertNotNull(file1);
        File file2 = OSUtils.findExecutableOnPath("soffice.exe", "soffice", "soffice");
        System.out.println("java at " + file2);
    }
}
