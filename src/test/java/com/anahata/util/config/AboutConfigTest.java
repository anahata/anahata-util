package com.anahata.util.config;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.util.env.ApplicationEnvironment;
import java.util.Properties;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for AboutConfig.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class AboutConfigTest {
    @Test
    public void testGetAppTitle() {
        final Properties props = makeProps("Yarayara", "1.2.3", "108", "20130409-1105", "yyyyMMdd-HHmm",
                ApplicationEnvironment.BUILD);
        AboutConfig config = new AboutConfig(props);
        assertEquals("BUILD Yarayara v1.2.3 b108", config.getAppTitle());
        
        // Test Beta for SNAPSHOT.
        
        props.put("application.version", "1.1.1-SNAPSHOT");
        config = new AboutConfig(props);
        assertEquals("BUILD Yarayara v1.1.1-Beta b108", config.getAppTitle());
        
        // Test drop last zero.
        
        props.put("application.version", "1.1.0");
        config = new AboutConfig(props);
        assertEquals("BUILD Yarayara v1.1 b108", config.getAppTitle());
        
        // Test no env name or build number in prod.
        
        props.put("application.environment", ApplicationEnvironment.PROD.name());
        config = new AboutConfig(props);
        assertEquals("Yarayara v1.1", config.getAppTitle());
    }
    
    private Properties makeProps(String appName, String version, String buildNumber, String buildTimestamp,
            String buildTimestampFormat, ApplicationEnvironment env) {
        Properties props = new Properties();
        props.put("application.name", appName);
        props.put("application.version", version);
        props.put("application.build.number", buildNumber);
        props.put("application.build.timestamp", buildTimestamp);
        props.put("application.build.timestamp.format", buildTimestampFormat);
        props.put("application.environment", env.name());
        return props;
    }
}
