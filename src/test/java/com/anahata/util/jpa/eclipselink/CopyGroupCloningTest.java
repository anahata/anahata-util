/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.jpa.eclipselink;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.persistence.sessions.CopyGroup;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public class CopyGroupCloningTest {
    @Test
    public void testCopyGroupCloningPreserversObjectIdentity() {
        CopyGroup foo = new CopyGroup();
        CopyGroup bar = new CopyGroup();

        foo.addAttribute("id");
        foo.addAttribute("bar", bar);
        foo.addAttribute("foofoo", foo);

        bar.addAttribute("id");
        bar.addAttribute("foo", foo);
        bar.addAttribute("barbar", bar);

        CopyGroup fooClone = foo.clone();
        CopyGroup barClone = foo.getGroup("bar");
        assertTrue(fooClone.getGroup("foofoo") == fooClone);
        assertTrue(barClone.getGroup("foo") == foo);
        assertTrue(barClone.getGroup("barbar") == barClone);

        System.out.println("identity preserved after cloning");
    }
}
