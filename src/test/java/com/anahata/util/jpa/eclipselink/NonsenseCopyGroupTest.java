/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.jpa.eclipselink;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.persistence.sessions.CopyGroup;
import org.junit.Test;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public class NonsenseCopyGroupTest {
    @Test
    public void test() {
        CopyGroup cg1 = new CopyGroup();
        cg1.addAttribute("att1");
        cg1.addAttribute("att2");

        CopyGroup cg2 = new CopyGroup();
        cg2.addAttribute("cg2att1");
        cg2.addAttribute("cg2att2");

        cg2.addAttribute("x", cg1);
        cg2.addAttribute("x");

        cg2.removeAttribute("x");
        System.out.println(cg2.getGroup("x"));

    }
}
