/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.jpa;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.util.test.model.LeafFromAbstract;
import com.anahata.util.test.model.receipt.Receipt;
import java.util.Map;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for JPAPropertyUtils.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class JPAPropertyUtilsTest {
    @Test
    public void testDescribe() {
        Map<String, JpaPropertyDescriptor> props = JPAPropertyUtils.describe(LeafFromAbstract.class);
        assertNotNull(props);
        Set<String> fields = props.keySet();
        //assertCollectionContains("id", fields);
        //assertCollectionContains("leafField", fields);
        
        props = JPAPropertyUtils.describe(Receipt.class);
        assertNotNull(props);
        fields = props.keySet();
        //assertCollectionContains("id", fields);
    }
}
