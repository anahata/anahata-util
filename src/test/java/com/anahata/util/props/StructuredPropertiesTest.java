/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.util.props;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2014 Anahata Technologies Pty Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.List;
import java.util.Properties;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class StructuredPropertiesTest {
    @Test
    public void testGetStringList() {
        Properties props = new Properties();
        props.put("yam.themes.Dark", "dark");
        props.put("yam.themes.Light", "light");
        StructuredProperties sprops = new StructuredProperties(props, "yam");
        List<StructuredPropertyEntry<String>> themes = sprops.getStringList("themes");
        assertNotNull(themes);
        assertEquals(2, themes.size());
        //assertCollectionContains(new StructuredPropertyEntry<>("Dark", "dark"), themes);
    }
}
